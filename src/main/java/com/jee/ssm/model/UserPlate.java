package com.jee.ssm.model;

import com.jee.ssm.model.base.BaseModel;
import java.util.Date;

/**
 * 
 * 表名 b_user_plate
 * @author GaoXiang
 * @version 1.0
 * Create time 2018/04/22
 */
public class UserPlate extends BaseModel {
	
	private static final long serialVersionUID = 4475359369216468522L;

	/**
     * 
     * 表字段 : b_user_plate.id
     * Create time 2018/04/22
     */
    private String id;

    /**
     * 用户id
     * 表字段 : b_user_plate.user_id
     * Create time 2018/04/22
     */
    private String userId;

    /**
     * 车牌号码
     * 表字段 : b_user_plate.number
     * Create time 2018/04/22
     */
    private String number;

    /**
     * 车牌颜色（字典表键值）
     * 表字段 : b_user_plate.plate_colour
     * Create time 2018/04/22
     */
    private String plateColour;

    /**
     * 车辆类型（0：小，1：大）
     * 表字段 : b_user_plate.type
     * Create time 2018/04/22
     */
    private Integer type;

    /**
     * 状态（0：非默认，1：默认）
     * 表字段 : b_user_plate.status
     * Create time 2018/04/22
     */
    private Integer status;
    
    /**
     * 是否自动扣款（0：否，1：是）
     * 表字段 : b_user_plate.auto_pay
     * Create time 2018/09/10
     */
    private Integer autoPay;

    /**
     * 添加时间
     * 表字段 : b_user_plate.create_time
     * Create time 2018/04/22
     */
    private Date createTime;

    /**
     * 上次修改时间
     * 表字段 : b_user_plate.update_time
     * Create time 2018/04/22
     */
    private Date updateTime;

    /**
     * 用户昵称（与user表userName保持一致）
     * 表字段 : b_user_plate.user_name
     * Create time 2018/04/22
     */
    private String userName;

    /**
     * 用户手机号码
     * 表字段 : b_user_plate.user_phone
     * Create time 2018/04/22
     */
    private String userPhone;

    /**
     * 用户真实姓名
     * 表字段 : b_user_plate.real_name
     * Create time 2018/04/22
     */
    private String realName;

    /**
     * 用户身份证号码
     * 表字段 : b_user_plate.id_number
     * Create time 2018/04/22
     */
    private String idNumber;

    /**
     * 构造方法
     */
    public UserPlate(String id, String userId, String number, String plateColour, Integer type, Integer status, Integer autoPay, Date createTime, Date updateTime, String userName, String userPhone, String realName, String idNumber) {
        this.id = id;
        this.userId = userId;
        this.number = number;
        this.plateColour = plateColour;
        this.type = type;
        this.status = status;
        this.autoPay = autoPay;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.userName = userName;
        this.userPhone = userPhone;
        this.realName = realName;
        this.idNumber = idNumber;
    }

    /**
     * 构造方法
     */
    public UserPlate() {
        super();
    }
    
    public UserPlate(String id, String number, String plateColour, Integer type, Integer autoPay, Date updateTime, String userId) {
    	this.id = id;
    	this.number = number;
    	this.plateColour = plateColour;
    	this.type = type;
    	this.autoPay = autoPay;
    	this.updateTime = updateTime;
    	this.userId = userId;
    }

    public UserPlate(String number, String plateColour) {
        super();
        this.number = number;
        this.plateColour = plateColour;
    }

    /**
     * 
     * @return id 
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 用户id
     * @return user_id 用户id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 用户id
     * @param userId 用户id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 车牌号码
     * @return number 车牌号码
     */
    public String getNumber() {
        return number;
    }

    /**
     * 车牌号码
     * @param number 车牌号码
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * 车牌颜色（字典表键值）
     * @return plate_colour 车牌颜色（字典表键值）
     */
    public String getPlateColour() {
        return plateColour;
    }

    /**
     * 车牌颜色（字典表键值）
     * @param plateColour 车牌颜色（字典表键值）
     */
    public void setPlateColour(String plateColour) {
        this.plateColour = plateColour;
    }

    /**
     * 车辆类型（0：小，1：大）
     * @return type 车辆类型（0：小，1：大）
     */
    public Integer getType() {
        return type;
    }

    /**
     * 车辆类型（0：小，1：大）
     * @param type 车辆类型（0：小，1：大）
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 状态（0：非默认，1：默认）
     * @return status 状态（0：非默认，1：默认）
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 状态（0：非默认，1：默认）
     * @param status 状态（0：非默认，1：默认）
     */
    public void setStatus(Integer status) {
        this.status = status;
    }
    
    /**
     * 是否自动扣款（0：否，1：是）
     * @return auto_pay 是否自动扣款（0：否，1：是）
     */
    public Integer getAutoPay() {
        return autoPay;
    }

    /**
     * 是否自动扣款（0：否，1：是）
     * @param autoPay 是否自动扣款（0：否，1：是）
     */
    public void setAutoPay(Integer autoPay) {
        this.autoPay = autoPay;
    }

    /**
     * 添加时间
     * @return create_time 添加时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 添加时间
     * @param createTime 添加时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 上次修改时间
     * @return update_time 上次修改时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 上次修改时间
     * @param updateTime 上次修改时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 用户昵称（与user表userName保持一致）
     * @return user_name 用户昵称（与user表userName保持一致）
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 用户昵称（与user表userName保持一致）
     * @param userName 用户昵称（与user表userName保持一致）
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 用户手机号码
     * @return user_phone 用户手机号码
     */
    public String getUserPhone() {
        return userPhone;
    }

    /**
     * 用户手机号码
     * @param userPhone 用户手机号码
     */
    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    /**
     * 用户真实姓名
     * @return real_name 用户真实姓名
     */
    public String getRealName() {
        return realName;
    }

    /**
     * 用户真实姓名
     * @param realName 用户真实姓名
     */
    public void setRealName(String realName) {
        this.realName = realName;
    }

    /**
     * 用户身份证号码
     * @return id_number 用户身份证号码
     */
    public String getIdNumber() {
        return idNumber;
    }

    /**
     * 用户身份证号码
     * @param idNumber 用户身份证号码
     */
    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }
}