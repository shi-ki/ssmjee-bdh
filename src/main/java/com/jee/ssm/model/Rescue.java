package com.jee.ssm.model;

import com.jee.ssm.model.base.BaseModel;
import java.util.Date;

/**
 * 
 * 表名 b_rescue
 * @author GaoXiang
 * @version 1.0
 * Create time 2018/06/07
 */
public class Rescue extends BaseModel {
    /**
     * 标识
     * 表字段 : b_rescue.id
     * Create time 2018/06/07
     */
    private String id;

    /**
     * 创建时间
     * 表字段 : b_rescue.create_time
     * Create time 2018/06/07
     */
    private Date createTime;

    /**
     * 处理时间
     * 表字段 : b_rescue.handle_time
     * Create time 2018/06/07
     */
    private Date handleTime;

    /**
     * 联系方式
     * 表字段 : b_rescue.phone
     * Create time 2018/06/07
     */
    private String phone;

    /**
     * 维度
     * 表字段 : b_rescue.lat
     * Create time 2018/06/07
     */
    private Double lat;

    /**
     * 经度
     * 表字段 : b_rescue.lng
     * Create time 2018/06/07
     */
    private Double lng;

    /**
     * 分类（0：紧急求助）
     * 表字段 : b_rescue.type
     * Create time 2018/06/07
     */
    private Integer type;

    /**
     * 状态（0：未读，1：已读）
     * 表字段 : b_rescue.status
     * Create time 2018/06/07
     */
    private Integer status;

    /**
     * 备注内容
     * 表字段 : b_rescue.intro
     * Create time 2018/06/07
     */
    private String intro;

    /**
     * 构造方法
     */
    public Rescue(String id, Date createTime, Date handleTime, String phone, Double lat, Double lng, Integer type, Integer status, String intro) {
        this.id = id;
        this.createTime = createTime;
        this.handleTime = handleTime;
        this.phone = phone;
        this.lat = lat;
        this.lng = lng;
        this.type = type;
        this.status = status;
        this.intro = intro;
    }

    /**
     * 构造方法
     */
    public Rescue() {
        super();
    }

    public Rescue(String id, Date createTime, String phone, Double lat, Double lng, Integer type, Integer status) {
		super();
		this.id = id;
		this.createTime = createTime;
		this.phone = phone;
		this.lat = lat;
		this.lng = lng;
		this.type = type;
		this.status = status;
	}

	/**
     * 标识
     * @return id 标识
     */
    public String getId() {
        return id;
    }

    /**
     * 标识
     * @param id 标识
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 创建时间
     * @return create_time 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 创建时间
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 处理时间
     * @return handle_time 处理时间
     */
    public Date getHandleTime() {
        return handleTime;
    }

    /**
     * 处理时间
     * @param handleTime 处理时间
     */
    public void setHandleTime(Date handleTime) {
        this.handleTime = handleTime;
    }

    /**
     * 联系方式
     * @return phone 联系方式
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 联系方式
     * @param phone 联系方式
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 维度
     * @return lat 维度
     */
    public Double getLat() {
        return lat;
    }

    /**
     * 维度
     * @param lat 维度
     */
    public void setLat(Double lat) {
        this.lat = lat;
    }

    /**
     * 经度
     * @return lng 经度
     */
    public Double getLng() {
        return lng;
    }

    /**
     * 经度
     * @param lng 经度
     */
    public void setLng(Double lng) {
        this.lng = lng;
    }

    /**
     * 分类（0：紧急求助）
     * @return type 分类（0：紧急求助）
     */
    public Integer getType() {
        return type;
    }

    /**
     * 分类（0：紧急求助）
     * @param type 分类（0：紧急求助）
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 状态（0：未读，1：已读）
     * @return status 状态（0：未读，1：已读）
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 状态（0：未读，1：已读）
     * @param status 状态（0：未读，1：已读）
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 备注内容
     * @return intro 备注内容
     */
    public String getIntro() {
        return intro;
    }

    /**
     * 备注内容
     * @param intro 备注内容
     */
    public void setIntro(String intro) {
        this.intro = intro;
    }

}