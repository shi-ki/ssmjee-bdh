package com.jee.ssm.model;

import com.jee.ssm.model.base.BaseModel;

/**
 * 
 * 表名 b_timer
 * @author GaoXiang
 * @version 1.0
 * Create time 2018/04/24
 */
public class Timer extends BaseModel {
    /**
	 * 
	 */
	private static final long serialVersionUID = -3246320777907576118L;

	/**
     * 标识
     * 表字段 : b_timer.id
     * Create time 2018/04/24
     */
    private String id;

    /**
     * 编号
     * 表字段 : b_timer.code_num
     * Create time 2018/04/24
     */
    private String codeNum;

    /**
     * 执行时间
     * 表字段 : b_timer.execute_time
     * Create time 2018/04/24
     */
    private String executeTime;

    /**
     * 定时类型（0：包夜开始，1：包夜结束）
     * 表字段 : b_timer.type
     * Create time 2018/04/24
     */
    private Integer type;

    /**
     * 构造方法
     */
    public Timer(String id, String codeNum, String executeTime, Integer type) {
        this.id = id;
        this.codeNum = codeNum;
        this.executeTime = executeTime;
        this.type = type;
    }

    /**
     * 构造方法
     */
    public Timer() {
        super();
    }

    /**
     * 标识
     * @return id 标识
     */
    public String getId() {
        return id;
    }

    /**
     * 标识
     * @param id 标识
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 编号
     * @return code_num 编号
     */
    public String getCodeNum() {
        return codeNum;
    }

    /**
     * 编号
     * @param codeNum 编号
     */
    public void setCodeNum(String codeNum) {
        this.codeNum = codeNum;
    }

    /**
     * 执行时间
     * @return execute_time 执行时间
     */
    public String getExecuteTime() {
        return executeTime;
    }

    /**
     * 执行时间
     * @param executeTime 执行时间
     */
    public void setExecuteTime(String executeTime) {
        this.executeTime = executeTime;
    }

    /**
     * 定时类型（0：包夜开始，1：包夜结束）
     * @return type 定时类型（0：包夜开始，1：包夜结束）
     */
    public Integer getType() {
        return type;
    }

    /**
     * 定时类型（0：包夜开始，1：包夜结束）
     * @param type 定时类型（0：包夜开始，1：包夜结束）
     */
    public void setType(Integer type) {
        this.type = type;
    }
}