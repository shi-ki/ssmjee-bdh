package com.jee.ssm.model;

import com.jee.ssm.model.base.BaseModel;

/**
 * 
 * 表名 b_clear_city
 * @author GaoXiang
 * @version 1.0
 * Create time 2018/10/16
 */
public class ClearCity extends BaseModel {
    /**
     * 标识
     * 表字段 : b_clear_city.id
     * Create time 2018/10/16
     */
    private String id;

    /**
     * 
     * 表字段 : b_clear_city.city_id
     * Create time 2018/10/16
     */
    private String cityId;

    /**
     * 
     * 表字段 : b_clear_city.city_name
     * Create time 2018/10/16
     */
    private String cityName;

    /**
     * 
     * 表字段 : b_clear_city.lat
     * Create time 2018/10/16
     */
    private String lat;

    /**
     * 
     * 表字段 : b_clear_city.lng
     * Create time 2018/10/16
     */
    private String lng;

    /**
     * 构造方法
     */
    public ClearCity(String id, String cityId, String cityName, String lat, String lng) {
        this.id = id;
        this.cityId = cityId;
        this.cityName = cityName;
        this.lat = lat;
        this.lng = lng;
    }

    /**
     * 构造方法
     */
    public ClearCity() {
        super();
    }

    /**
     * 标识
     * @return id 标识
     */
    public String getId() {
        return id;
    }

    /**
     * 标识
     * @param id 标识
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return city_id 
     */
    public String getCityId() {
        return cityId;
    }

    /**
     * 
     * @param cityId 
     */
    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    /**
     * 
     * @return city_name 
     */
    public String getCityName() {
        return cityName;
    }

    /**
     * 
     * @param cityName 
     */
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    /**
     * 
     * @return lat 
     */
    public String getLat() {
        return lat;
    }

    /**
     * 
     * @param lat 
     */
    public void setLat(String lat) {
        this.lat = lat;
    }

    /**
     * 
     * @return lng 
     */
    public String getLng() {
        return lng;
    }

    /**
     * 
     * @param lng 
     */
    public void setLng(String lng) {
        this.lng = lng;
    }
}