package com.jee.ssm.model;

import com.jee.ssm.model.base.BaseModel;
import java.util.Date;

/**
 * 
 * 表名 b_monitor
 * @author GaoXiang
 * @version 1.0
 * Create time 2018/05/07
 */
public class Monitor extends BaseModel {
	
	private static final long serialVersionUID = 8753605908585897626L;

	/**
     * 标识
     * 表字段 : b_monitor.id
     * Create time 2018/05/07
     */
    private String id;

    /**
     * 编号
     * 表字段 : b_monitor.number
     * Create time 2018/05/07
     */
    private String number;

    /**
     * 名称
     * 表字段 : b_monitor.name
     * Create time 2018/05/07
     */
    private String name;

    /**
     * 地址
     * 表字段 : b_monitor.address
     * Create time 2018/05/07
     */
    private String address;

    /**
     * 纬度
     * 表字段 : b_monitor.lat
     * Create time 2018/05/07
     */
    private Double lat;

    /**
     * 经度
     * 表字段 : b_monitor.lng
     * Create time 2018/05/07
     */
    private Double lng;

    /**
     * 类型（0：未区分）
     * 表字段 : b_monitor.type
     * Create time 2018/05/07
     */
    private Integer type;

    /**
     * 状态（0：异常，1：正常）
     * 表字段 : b_monitor.status
     * Create time 2018/05/07
     */
    private Integer status;

    /**
     * 添加时间
     * 表字段 : b_monitor.create_time
     * Create time 2018/05/07
     */
    private Date createTime;
    
    private String playUrl;

    /**
     * 关联设备id
     * 表字段 : b_monitor.relate_id
     * Create time 2018/05/07
     */
    private String relateId;

    /**
     * 构造方法
     */
    public Monitor(String id, String number, String name, String address, Double lat, Double lng, Integer type, Integer status, Date createTime, String playUrl, String relateId) {
        this.id = id;
        this.number = number;
        this.name = name;
        this.address = address;
        this.lat = lat;
        this.lng = lng;
        this.type = type;
        this.status = status;
        this.createTime = createTime;
        this.playUrl = playUrl;
        this.relateId = relateId;
    }

    /**
     * 构造方法
     */
    public Monitor() {
        super();
    }

    /**
     * 标识
     * @return id 标识
     */
    public String getId() {
        return id;
    }

    /**
     * 标识
     * @param id 标识
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 编号
     * @return number 编号
     */
    public String getNumber() {
        return number;
    }

    /**
     * 编号
     * @param number 编号
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * 名称
     * @return name 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 名称
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 地址
     * @return address 地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 地址
     * @param address 地址
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 纬度
     * @return lat 纬度
     */
    public Double getLat() {
        return lat;
    }

    /**
     * 纬度
     * @param lat 纬度
     */
    public void setLat(Double lat) {
        this.lat = lat;
    }

    /**
     * 经度
     * @return lng 经度
     */
    public Double getLng() {
        return lng;
    }

    /**
     * 经度
     * @param lng 经度
     */
    public void setLng(Double lng) {
        this.lng = lng;
    }

    /**
     * 类型（0：未区分）
     * @return type 类型（0：未区分）
     */
    public Integer getType() {
        return type;
    }

    /**
     * 类型（0：未区分）
     * @param type 类型（0：未区分）
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 状态（0：异常，1：正常）
     * @return status 状态（0：异常，1：正常）
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 状态（0：异常，1：正常）
     * @param status 状态（0：异常，1：正常）
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 添加时间
     * @return create_time 添加时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 添加时间
     * @param createTime 添加时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getPlayUrl() {
		return playUrl;
	}

	public void setPlayUrl(String playUrl) {
		this.playUrl = playUrl;
	}

	/**
     * 关联设备id
     * @return relate_id 关联设备id
     */
    public String getRelateId() {
        return relateId;
    }

    /**
     * 关联设备id
     * @param relateId 关联设备id
     */
    public void setRelateId(String relateId) {
        this.relateId = relateId;
    }
}