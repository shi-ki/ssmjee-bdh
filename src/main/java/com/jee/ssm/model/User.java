package com.jee.ssm.model;

import com.jee.ssm.model.base.BaseModel;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * 表名 b_user
 * @author GaoXiang
 * @version 1.0
 * Create time 2018/06/14
 */
public class User extends BaseModel {

	private static final long serialVersionUID = -6785478223341943832L;

	/**
     * 
     * 表字段 : b_user.id
     * Create time 2018/06/14
     */
    private String id;

    /**
     * 用户名
     * 表字段 : b_user.user_name
     * Create time 2018/06/14
     */
    private String userName;

    /**
     * 默认车牌
     * 表字段 : b_user.plate_number
     * Create time 2018/06/14
     */
    private String plateNumber;
    
    /**
     * 默认车牌颜色
     */
    private String commonPlateColour;

    /**
     * 默认地址
     * 表字段 : b_user.address
     * Create time 2018/06/14
     */
    private String address;

    /**
     * 姓名
     * 表字段 : b_user.name
     * Create time 2018/06/14
     */
    private String name;

    /**
     * 性别（0：女，1：男，2：未知）
     * 表字段 : b_user.sex
     * Create time 2018/06/14
     */
    private Integer sex;

    /**
     * 出生日期
     * 表字段 : b_user.birth_time
     * Create time 2018/06/14
     */
    private Date birthTime;

    /**
     * 身份证号码
     * 表字段 : b_user.id_number
     * Create time 2018/06/14
     */
    private String idNumber;

    /**
     * 注册手机号码
     * 表字段 : b_user.phone
     * Create time 2018/06/14
     */
    private String phone;

    /**
     * 类型（用户：1）
     * 表字段 : b_user.type
     * Create time 2018/06/14
     */
    private Integer type;

    /**
     * 状态（正常：1）
     * 表字段 : b_user.status
     * Create time 2018/06/14
     */
    private Integer status;

    /**
     * 备注
     * 表字段 : b_user.intro
     * Create time 2018/06/14
     */
    private String intro;

    /**
     * 添加时间
     * 表字段 : b_user.create_time
     * Create time 2018/06/14
     */
    private Date createTime;

    /**
     * 上次修改时间
     * 表字段 : b_user.update_time
     * Create time 2018/06/14
     */
    private Date updateTime;

    /**
     * 
     * 表字段 : b_user.head_image
     * Create time 2018/06/14
     */
    private String headImage;

    /**
     * 可用余额
     * 表字段 : b_user.wallet_balance
     * Create time 2018/06/14
     */
    private BigDecimal walletBalance;

    /**
     * 冻结余额
     * 表字段 : b_user.frozen_balance
     * Create time 2018/06/14
     */
    private BigDecimal frozenBalance;
    
    /**
     * 支付密码（加密后）
     * 表字段 : b_user.pay_password
     * Create time 2018/09/20
     */
    private String payPassword;
    
    /**
     * 查询时间yyyy-MM-dd 用于查询
     */
    private String checkTime;
    
    /**
     * 未读信息数量
     */
    private Integer unread;

    /**
     * 构造方法
     */
    public User(String id, String userName, String plateNumber, String address, String name, Integer sex, Date birthTime, String idNumber, String phone, Integer type, Integer status, String intro, Date createTime, Date updateTime, String headImage, BigDecimal walletBalance, BigDecimal frozenBalance, String payPassword) {
        this.id = id;
        this.userName = userName;
        this.plateNumber = plateNumber;
        this.address = address;
        this.name = name;
        this.sex = sex;
        this.birthTime = birthTime;
        this.idNumber = idNumber;
        this.phone = phone;
        this.type = type;
        this.status = status;
        this.intro = intro;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.headImage = headImage;
        this.walletBalance = walletBalance;
        this.frozenBalance = frozenBalance;
        this.payPassword = payPassword;
    }

    /**
     * 构造方法
     */
    public User() {
        super();
    }
    
    public User(String checkTime) {
    	this.checkTime = checkTime;
    }
    
    public User(String id, String phone) {
    	this.id = id;
    	this.phone = phone;
    }

    /**
     * 
     * @return id 
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 用户名
     * @return user_name 用户名
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 用户名
     * @param userName 用户名
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 默认车牌
     * @return plate_number 默认车牌
     */
    public String getPlateNumber() {
        return plateNumber;
    }

    /**
     * 默认车牌
     * @param plateNumber 默认车牌
     */
    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getCommonPlateColour() {
		return commonPlateColour;
	}

	public void setCommonPlateColour(String commonPlateColour) {
		this.commonPlateColour = commonPlateColour;
	}

	/**
     * 默认地址
     * @return address 默认地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 默认地址
     * @param address 默认地址
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 姓名
     * @return name 姓名
     */
    public String getName() {
        return name;
    }

    /**
     * 姓名
     * @param name 姓名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 性别（0：女，1：男，2：未知）
     * @return sex 性别（0：女，1：男，2：未知）
     */
    public Integer getSex() {
        return sex;
    }

    /**
     * 性别（0：女，1：男，2：未知）
     * @param sex 性别（0：女，1：男，2：未知）
     */
    public void setSex(Integer sex) {
        this.sex = sex;
    }

    /**
     * 出生日期
     * @return birth_time 出生日期
     */
    public Date getBirthTime() {
        return birthTime;
    }

    /**
     * 出生日期
     * @param birthTime 出生日期
     */
    public void setBirthTime(Date birthTime) {
        this.birthTime = birthTime;
    }

    /**
     * 身份证号码
     * @return id_number 身份证号码
     */
    public String getIdNumber() {
        return idNumber;
    }

    /**
     * 身份证号码
     * @param idNumber 身份证号码
     */
    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    /**
     * 注册手机号码
     * @return phone 注册手机号码
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 注册手机号码
     * @param phone 注册手机号码
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 类型（用户：1）
     * @return type 类型（用户：1）
     */
    public Integer getType() {
        return type;
    }

    /**
     * 类型（用户：1）
     * @param type 类型（用户：1）
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 状态（正常：1）
     * @return status 状态（正常：1）
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 状态（正常：1）
     * @param status 状态（正常：1）
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 备注
     * @return intro 备注
     */
    public String getIntro() {
        return intro;
    }

    /**
     * 备注
     * @param intro 备注
     */
    public void setIntro(String intro) {
        this.intro = intro;
    }

    /**
     * 添加时间
     * @return create_time 添加时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 添加时间
     * @param createTime 添加时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 上次修改时间
     * @return update_time 上次修改时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 上次修改时间
     * @param updateTime 上次修改时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 
     * @return head_image 
     */
    public String getHeadImage() {
        return headImage;
    }

    /**
     * 
     * @param headImage 
     */
    public void setHeadImage(String headImage) {
        this.headImage = headImage;
    }

    /**
     * 可用余额
     * @return wallet_balance 可用余额
     */
    public BigDecimal getWalletBalance() {
        return walletBalance;
    }

    /**
     * 可用余额
     * @param walletBalance 可用余额
     */
    public void setWalletBalance(BigDecimal walletBalance) {
        this.walletBalance = walletBalance;
    }

    /**
     * 冻结余额
     * @return frozen_balance 冻结余额
     */
    public BigDecimal getFrozenBalance() {
        return frozenBalance;
    }

    /**
     * 冻结余额
     * @param frozenBalance 冻结余额
     */
    public void setFrozenBalance(BigDecimal frozenBalance) {
        this.frozenBalance = frozenBalance;
    }
    
    /**
     * 支付密码（加密后）
     * @return pay_password 支付密码（加密后）
     */
    public String getPayPassword() {
        return payPassword;
    }

    /**
     * 支付密码（加密后）
     * @param payPassword 支付密码（加密后）
     */
    public void setPayPassword(String payPassword) {
        this.payPassword = payPassword;
    }

	public String getCheckTime() {
		return checkTime;
	}

	public void setCheckTime(String checkTime) {
		this.checkTime = checkTime;
	}

	public Integer getUnread() {
		return unread;
	}

	public void setUnread(Integer unread) {
		this.unread = unread;
	}
	
}