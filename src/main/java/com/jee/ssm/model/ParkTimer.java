package com.jee.ssm.model;

import com.jee.ssm.model.base.BaseModel;
import java.util.Date;

/**
 * 
 * 表名 b_park_timer
 * @author GaoXiang
 * @version 1.0
 * Create time 2018/04/24
 */
public class ParkTimer extends BaseModel {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1312111284163191925L;

	/**
     * 标识
     * 表字段 : b_park_timer.id
     * Create time 2018/04/24
     */
    private String id;

    /**
     * 定时类型（0：包夜开始，1：包夜结束）
     * 表字段 : b_park_timer.type
     * Create time 2018/04/24
     */
    private Integer type;

    /**
     * 停车场id
     * 表字段 : b_park_timer.park_id
     * Create time 2018/04/24
     */
    private String parkId;

    /**
     * 定时器id
     * 表字段 : b_park_timer.timer_id
     * Create time 2018/04/24
     */
    private String timerId;

    /**
     * 
     * 表字段 : b_park_timer.create_time
     * Create time 2018/04/24
     */
    private Date createTime;

    /**
     * 构造方法
     */
    public ParkTimer(String id, Integer type, String parkId, String timerId, Date createTime) {
        this.id = id;
        this.type = type;
        this.parkId = parkId;
        this.timerId = timerId;
        this.createTime = createTime;
    }

    /**
     * 构造方法
     */
    public ParkTimer() {
        super();
    }

    /**
     * 标识
     * @return id 标识
     */
    public String getId() {
        return id;
    }

    /**
     * 标识
     * @param id 标识
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 定时类型（0：包夜开始，1：包夜结束）
     * @return type 定时类型（0：包夜开始，1：包夜结束）
     */
    public Integer getType() {
        return type;
    }

    /**
     * 定时类型（0：包夜开始，1：包夜结束）
     * @param type 定时类型（0：包夜开始，1：包夜结束）
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 停车场id
     * @return park_id 停车场id
     */
    public String getParkId() {
        return parkId;
    }

    /**
     * 停车场id
     * @param parkId 停车场id
     */
    public void setParkId(String parkId) {
        this.parkId = parkId;
    }

    /**
     * 定时器id
     * @return timer_id 定时器id
     */
    public String getTimerId() {
        return timerId;
    }

    /**
     * 定时器id
     * @param timerId 定时器id
     */
    public void setTimerId(String timerId) {
        this.timerId = timerId;
    }

    /**
     * 
     * @return create_time 
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 
     * @param createTime 
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}