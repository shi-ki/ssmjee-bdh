package com.jee.ssm.model;

import com.jee.ssm.model.base.BaseModel;

/**
 * 
 * 表名 b_suggestion_image
 * @author GaoXiang
 * @version 1.0
 * Create time 2018/06/23
 */
public class SuggestionImage extends BaseModel {

	private static final long serialVersionUID = -3953683808010174609L;

	/**
     * 标识
     * 表字段 : b_suggestion_image.id
     * Create time 2018/06/23
     */
    private String id;

    /**
     * 状态（1：正常）
     * 表字段 : b_suggestion_image.status
     * Create time 2018/06/23
     */
    private Integer status;

    /**
     * 类型（0：意见）
     * 表字段 : b_suggestion_image.type
     * Create time 2018/06/23
     */
    private Integer type;

    /**
     * 图片名称
     * 表字段 : b_suggestion_image.name
     * Create time 2018/06/23
     */
    private String name;

    /**
     * 图片地址
     * 表字段 : b_suggestion_image.url
     * Create time 2018/06/23
     */
    private String url;

    /**
     * 关联意见id
     * 表字段 : b_suggestion_image.suggestion_id
     * Create time 2018/06/23
     */
    private String suggestionId;

    /**
     * 构造方法
     */
    public SuggestionImage(String id, Integer status, Integer type, String name, String url, String suggestionId) {
        this.id = id;
        this.status = status;
        this.type = type;
        this.name = name;
        this.url = url;
        this.suggestionId = suggestionId;
    }

    /**
     * 构造方法
     */
    public SuggestionImage() {
        super();
    }

    /**
     * 标识
     * @return id 标识
     */
    public String getId() {
        return id;
    }

    /**
     * 标识
     * @param id 标识
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 状态（1：正常）
     * @return status 状态（1：正常）
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 状态（1：正常）
     * @param status 状态（1：正常）
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 类型（0：意见）
     * @return type 类型（0：意见）
     */
    public Integer getType() {
        return type;
    }

    /**
     * 类型（0：意见）
     * @param type 类型（0：意见）
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 图片名称
     * @return name 图片名称
     */
    public String getName() {
        return name;
    }

    /**
     * 图片名称
     * @param name 图片名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 图片地址
     * @return url 图片地址
     */
    public String getUrl() {
        return url;
    }

    /**
     * 图片地址
     * @param url 图片地址
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 关联意见id
     * @return suggestion_id 关联意见id
     */
    public String getSuggestionId() {
        return suggestionId;
    }

    /**
     * 关联意见id
     * @param suggestionId 关联意见id
     */
    public void setSuggestionId(String suggestionId) {
        this.suggestionId = suggestionId;
    }
}