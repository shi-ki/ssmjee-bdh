package com.jee.ssm.model;

import com.jee.ssm.model.base.BaseModel;
import java.util.Date;

/**
 * 
 * 表名 b_guide_screen
 * @author GaoXiang
 * @version 1.0
 * Create time 2018/07/07
 */
public class GuideScreen extends BaseModel {

	private static final long serialVersionUID = 9047493143850934834L;

	/**
     * 
     * 表字段 : b_guide_screen.id
     * Create time 2018/07/07
     */
    private String id;

    /**
     * 名称
     * 表字段 : b_guide_screen.name
     * Create time 2018/07/07
     */
    private String name;

    /**
     * 编号
     * 表字段 : b_guide_screen.number
     * Create time 2018/07/07
     */
    private String number;

    /**
     * 纬度
     * 表字段 : b_guide_screen.lat
     * Create time 2018/07/07
     */
    private Double lat;

    /**
     * 经度
     * 表字段 : b_guide_screen.lng
     * Create time 2018/07/07
     */
    private Double lng;

    /**
     * 百度编号
     * 表字段 : b_guide_screen.baidu_number
     * Create time 2018/07/07
     */
    private String baiduNumber;

    /**
     * 上行停车场编号
     * 表字段 : b_guide_screen.park_top_number
     * Create time 2018/07/07
     */
    private String parkTopNumber;

    /**
     * 上行停车场编号
     * 表字段 : b_guide_screen.park_bottom_number
     * Create time 2018/07/07
     */
    private String parkBottomNumber;

    /**
     * 图片名称
     * 表字段 : b_guide_screen.image_name
     * Create time 2018/07/07
     */
    private String imageName;

    /**
     * 最近更新结果
     * 表字段 : b_guide_screen.latest_result
     * Create time 2018/07/07
     */
    private String latestResult;

    /**
     * 文件编号
     * 表字段 : b_guide_screen.play_number
     * Create time 2018/07/07
     */
    private Integer playNumber;

    /**
     * 文件名称
     * 表字段 : b_guide_screen.play_file_name
     * Create time 2018/07/07
     */
    private String playFileName;

    /**
     * 文件路径
     * 表字段 : b_guide_screen.play_file_url
     * Create time 2018/07/07
     */
    private String playFileUrl;

    /**
     * 上行显示文字
     * 表字段 : b_guide_screen.top_word
     * Create time 2018/07/07
     */
    private String topWord;

    /**
     * 下行显示文字
     * 表字段 : b_guide_screen.bottom_word
     * Create time 2018/07/07
     */
    private String bottomWord;

    /**
     * 纯文字内容
     * 表字段 : b_guide_screen.pure_word
     * Create time 2018/07/07
     */
    private String pureWord;

    /**
     * 状态（0：未关联；1：已关联）
     * 表字段 : b_guide_screen.status
     * Create time 2018/07/07
     */
    private Integer status;

    /**
     * 类型（0：文字；1：图片类型）
     * 表字段 : b_guide_screen.type
     * Create time 2018/07/07
     */
    private Integer type;

    /**
     * 添加时间
     * 表字段 : b_guide_screen.create_time
     * Create time 2018/07/07
     */
    private Date createTime;

    /**
     * 修改时间
     * 表字段 : b_guide_screen.update_time
     * Create time 2018/07/07
     */
    private Date updateTime;
    
    /**
     * 最后心跳时间
     */
    private Date lastTime;

    /**
     * 构造方法
     */
    public GuideScreen(String id, String name, String number, Double lat, Double lng, String baiduNumber, String parkTopNumber, String parkBottomNumber, String imageName, String latestResult, Integer playNumber, String playFileName, String playFileUrl, String topWord, String bottomWord, String pureWord, Integer status, Integer type, Date createTime, Date updateTime, Date lastTime) {
        this.id = id;
        this.name = name;
        this.number = number;
        this.lat = lat;
        this.lng = lng;
        this.baiduNumber = baiduNumber;
        this.parkTopNumber = parkTopNumber;
        this.parkBottomNumber = parkBottomNumber;
        this.imageName = imageName;
        this.latestResult = latestResult;
        this.playNumber = playNumber;
        this.playFileName = playFileName;
        this.playFileUrl = playFileUrl;
        this.topWord = topWord;
        this.bottomWord = bottomWord;
        this.pureWord = pureWord;
        this.status = status;
        this.type = type;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.lastTime = lastTime;
    }
    
    public GuideScreen(String id, String number, Integer status, Integer type, Date createTime, Date updateTime, Date lastTime) {
    	this.id = id;
    	this.number = number;
    	this.status = status;
        this.type = type;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.lastTime = lastTime;
    }
    
    public GuideScreen(String id) {
    	this.id = id;
    }

    /**
     * 构造方法
     */
    public GuideScreen() {
        super();
    }

    /**
     * 
     * @return id 
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 名称
     * @return name 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 名称
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 编号
     * @return number 编号
     */
    public String getNumber() {
        return number;
    }

    /**
     * 编号
     * @param number 编号
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * 纬度
     * @return lat 纬度
     */
    public Double getLat() {
        return lat;
    }

    /**
     * 纬度
     * @param lat 纬度
     */
    public void setLat(Double lat) {
        this.lat = lat;
    }

    /**
     * 经度
     * @return lng 经度
     */
    public Double getLng() {
        return lng;
    }

    /**
     * 经度
     * @param lng 经度
     */
    public void setLng(Double lng) {
        this.lng = lng;
    }

    /**
     * 百度编号
     * @return baidu_number 百度编号
     */
    public String getBaiduNumber() {
        return baiduNumber;
    }

    /**
     * 百度编号
     * @param baiduNumber 百度编号
     */
    public void setBaiduNumber(String baiduNumber) {
        this.baiduNumber = baiduNumber;
    }

    /**
     * 上行停车场编号
     * @return park_top_number 上行停车场编号
     */
    public String getParkTopNumber() {
        return parkTopNumber;
    }

    /**
     * 上行停车场编号
     * @param parkTopNumber 上行停车场编号
     */
    public void setParkTopNumber(String parkTopNumber) {
        this.parkTopNumber = parkTopNumber;
    }

    /**
     * 上行停车场编号
     * @return park_bottom_number 上行停车场编号
     */
    public String getParkBottomNumber() {
        return parkBottomNumber;
    }

    /**
     * 上行停车场编号
     * @param parkBottomNumber 上行停车场编号
     */
    public void setParkBottomNumber(String parkBottomNumber) {
        this.parkBottomNumber = parkBottomNumber;
    }

    /**
     * 图片名称
     * @return image_name 图片名称
     */
    public String getImageName() {
        return imageName;
    }

    /**
     * 图片名称
     * @param imageName 图片名称
     */
    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    /**
     * 最近更新结果
     * @return latest_result 图片地址
     */
    public String getLatestResult() {
        return latestResult;
    }

    /**
     * 最近更新结果
     * @param latest_result 图片地址
     */
    public void setLatestResult(String latestResult) {
        this.latestResult = latestResult;
    }

    /**
     * 文件编号
     * @return play_number 文件编号
     */
    public Integer getPlayNumber() {
        return playNumber;
    }

    /**
     * 文件编号
     * @param playNumber 文件编号
     */
    public void setPlayNumber(Integer playNumber) {
        this.playNumber = playNumber;
    }

    /**
     * 文件名称
     * @return play_file_name 文件名称
     */
    public String getPlayFileName() {
        return playFileName;
    }

    /**
     * 文件名称
     * @param playFileName 文件名称
     */
    public void setPlayFileName(String playFileName) {
        this.playFileName = playFileName;
    }

    /**
     * 文件路径
     * @return play_file_url 文件路径
     */
    public String getPlayFileUrl() {
        return playFileUrl;
    }

    /**
     * 文件路径
     * @param playFileUrl 文件路径
     */
    public void setPlayFileUrl(String playFileUrl) {
        this.playFileUrl = playFileUrl;
    }

    /**
     * 上行显示文字
     * @return top_word 上行显示文字
     */
    public String getTopWord() {
        return topWord;
    }

    /**
     * 上行显示文字
     * @param topWord 上行显示文字
     */
    public void setTopWord(String topWord) {
        this.topWord = topWord;
    }

    /**
     * 下行显示文字
     * @return bottom_word 下行显示文字
     */
    public String getBottomWord() {
        return bottomWord;
    }

    /**
     * 下行显示文字
     * @param bottomWord 下行显示文字
     */
    public void setBottomWord(String bottomWord) {
        this.bottomWord = bottomWord;
    }

    /**
     * 纯文字内容
     * @return pure_word 纯文字内容
     */
    public String getPureWord() {
        return pureWord;
    }

    /**
     * 纯文字内容
     * @param pureWord 纯文字内容
     */
    public void setPureWord(String pureWord) {
        this.pureWord = pureWord;
    }

    /**
     * 状态（0：未关联；1：已关联）
     * @return status 状态（0：未关联；1：已关联）
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 状态（0：未关联；1：已关联）
     * @param status 状态（0：未关联；1：已关联）
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 类型（0：文字；1：图片类型）
     * @return type 类型（0：文字；1：图片类型）
     */
    public Integer getType() {
        return type;
    }

    /**
     * 类型（0：文字；1：图片类型）
     * @param type 类型（0：文字；1：图片类型）
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 添加时间
     * @return create_time 添加时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 添加时间
     * @param createTime 添加时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 修改时间
     * @return update_time 修改时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 修改时间
     * @param updateTime 修改时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

	public Date getLastTime() {
		return lastTime;
	}

	public void setLastTime(Date lastTime) {
		this.lastTime = lastTime;
	}
    
}