package com.jee.ssm.model;

import com.jee.ssm.model.base.BaseModel;
import java.util.Date;

/**
 * 
 * 表名 b_sm_record
 * @author GaoXiang
 * @version 1.0
 * Create time 2018/04/20
 */
public class SmRecord extends BaseModel {

	private static final long serialVersionUID = -5028097067582964934L;

	/**
     * 
     * 表字段 : b_sm_record.id
     * Create time 2018/04/20
     */
    private String id;

    /**
     * 手机号码
     * 表字段 : b_sm_record.number
     * Create time 2018/04/20
     */
    private String number;

    /**
     * 添加时间
     * 表字段 : b_sm_record.create_time
     * Create time 2018/04/20
     */
    private Date createTime;

    /**
     * 验证码
     * 表字段 : b_sm_record.verify_code
     * Create time 2018/04/20
     */
    private String verifyCode;

    /**
     * 短信内容
     * 表字段 : b_sm_record.sm_content
     * Create time 2018/04/20
     */
    private String smContent;

    /**
     * 短信类型
     * 表字段 : b_sm_record.type
     * Create time 2018/04/20
     */
    private String type;

    /**
     * 短信发送状态
     * 表字段 : b_sm_record.status
     * Create time 2018/04/20
     */
    private String status;

    /**
     * 构造方法
     */
    public SmRecord(String id, String number, Date createTime, String verifyCode, String smContent, String type, String status) {
        this.id = id;
        this.number = number;
        this.createTime = createTime;
        this.verifyCode = verifyCode;
        this.smContent = smContent;
        this.type = type;
        this.status = status;
    }

    /**
     * 构造方法
     */
    public SmRecord() {
        super();
    }
    
    public SmRecord(String number, String type) {
    	this.number = number;
    	this.type = type;
    }

    /**
     * 
     * @return id 
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 手机号码
     * @return number 手机号码
     */
    public String getNumber() {
        return number;
    }

    /**
     * 手机号码
     * @param number 手机号码
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * 添加时间
     * @return create_time 添加时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 添加时间
     * @param createTime 添加时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 验证码
     * @return verify_code 验证码
     */
    public String getVerifyCode() {
        return verifyCode;
    }

    /**
     * 验证码
     * @param verifyCode 验证码
     */
    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }

    /**
     * 短信内容
     * @return sm_content 短信内容
     */
    public String getSmContent() {
        return smContent;
    }

    /**
     * 短信内容
     * @param smContent 短信内容
     */
    public void setSmContent(String smContent) {
        this.smContent = smContent;
    }

    /**
     * 短信类型
     * @return type 短信类型
     */
    public String getType() {
        return type;
    }

    /**
     * 短信类型
     * @param type 短信类型
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 短信发送状态
     * @return status 短信发送状态
     */
    public String getStatus() {
        return status;
    }

    /**
     * 短信发送状态
     * @param status 短信发送状态
     */
    public void setStatus(String status) {
        this.status = status;
    }
}