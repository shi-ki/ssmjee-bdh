package com.jee.ssm.model.hik8200.bo;

/**
 * @author zhangtuo
 * @Date 2017/5/16
 */
public class ControlUnitBo {
    private static final long serialVersionUID = 1L;

    private String cameraId;

    private String indexCode;

    private String name;

    private String parentIndexCode;

    private Integer cameraType;

    private String decodetag;

    private String description;

    private Integer insOnline;

    private String latitude;

    private String longitude;

    private Integer pixel;

    private String createTime;

    private String updateTime;

    public String getCameraId() {
        return cameraId;
    }

    public void setCameraId(String cameraId) {
        this.cameraId = cameraId;
    }

    public String getIndexCode() {
        return indexCode;
    }

    public void setIndexCode(String indexCode) {
        this.indexCode = indexCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentIndexCode() {
        return parentIndexCode;
    }

    public void setParentIndexCode(String parentIndexCode) {
        this.parentIndexCode = parentIndexCode;
    }

    public Integer getCameraType() {
        return cameraType;
    }

    public void setCameraType(Integer cameraType) {
        this.cameraType = cameraType;
    }

    public String getDecodetag() {
        return decodetag;
    }

    public void setDecodetag(String decodetag) {
        this.decodetag = decodetag;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getInsOnline() {
        return insOnline;
    }

    public void setInsOnline(Integer insOnline) {
        this.insOnline = insOnline;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Integer getPixel() {
        return pixel;
    }

    public void setPixel(Integer pixel) {
        this.pixel = pixel;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}
