package com.jee.ssm.model.hik8200.dto;

/**
 * @author zhangtuo
 * @Date 2017/5/16
 */
public class AgreementDto {
    private String artemisIp;
    private String artemisPort;
    private String appKey;
    private String appSecret;
    private String time;
    private String timeSecret;
    private String Schema;

    public AgreementDto(String artemisIp, String artemisPort, String appKey, String appSecret, String time, String timeSecret, String Schema) {
        this.artemisIp = artemisIp;
        this.artemisPort = artemisPort;
        this.appKey = appKey;
        this.appSecret = appSecret;
        this.time = time;
        this.timeSecret = timeSecret;
        this.Schema = Schema;
    }

    public AgreementDto() {
    }

    public String getArtemisIp() {
        return artemisIp;
    }

    public void setArtemisIp(String artemisIp) {
        this.artemisIp = artemisIp;
    }

    public String getArtemisPort() {
        return artemisPort;
    }

    public void setArtemisPort(String artemisPort) {
        this.artemisPort = artemisPort;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTimeSecret() {
        return timeSecret;
    }

    public void setTimeSecret(String timeSecret) {
        this.timeSecret = timeSecret;
    }

    public String getSchema() {
        return Schema;
    }

    public void setSchema(String schema) {
        Schema = schema;
    }

    @Override
    public String toString() {
        return "AgreementDto{" + "artemisIp='" + artemisIp + '\'' + ", artemisPort='" + artemisPort
            + '\'' + ", appKey='" + appKey + '\'' + ", appSecret='" + appSecret + '\'' + ", time='"
            + time + '\'' + ", timeSecret='" + timeSecret + '\'' + ", Schema='" + Schema + '\''
            + '}';
    }
}
