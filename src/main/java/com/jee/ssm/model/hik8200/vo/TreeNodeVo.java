package com.jee.ssm.model.hik8200.vo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class TreeNodeVo {

    private String indexCode; //节点编号

    private String parentIndexCode;//父节点编号

    private String text;   //节点文本内容

    private String nodeType; //节点类型

    private boolean open;   //是否展开

    private boolean isParent;  //是否父节点

    private List<TreeNodeVo> children = new ArrayList<TreeNodeVo>(); //子节点

    public TreeNodeVo() {
    }

    public TreeNodeVo(String indexCode, String parentIndexCode, String text, String nodeType) {
        this.indexCode = indexCode;
        this.parentIndexCode = parentIndexCode;
        this.text = text;
        this.nodeType = nodeType;
    }

    public TreeNodeVo(String indexCode, String parentIndexCode, String text, String nodeType, boolean open, boolean isParent, List<TreeNodeVo> children) {
        this.indexCode = indexCode;
        this.parentIndexCode = parentIndexCode;
        this.text = text;
        this.nodeType = nodeType;
        this.open = open;
        this.isParent = isParent;
        this.children = children;
    }

    public TreeNodeVo(String indexCode, String parentIndexCode, String text, String nodeType, boolean open, boolean isParent) {
        this.indexCode = indexCode;
        this.parentIndexCode = parentIndexCode;
        this.text = text;
        this.nodeType = nodeType;
        this.open = open;
        this.isParent = isParent;
    }

    public void addChild(TreeNodeVo treeNode) {
        children.add(treeNode);
    }

    public void addChildren(List<TreeNodeVo> treeNodeList) {
        children.addAll(treeNodeList);
    }

    public String getIndexCode() {
        return indexCode;
    }

    public void setIndexCode(String indexCode) {
        this.indexCode = indexCode;
    }

    public String getParentIndexCode() {
        return parentIndexCode;
    }

    public void setParentIndexCode(String parentIndexCode) {
        this.parentIndexCode = parentIndexCode;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getNodeType() {
        return nodeType;
    }

    public void setNodeType(String nodeType) {
        this.nodeType = nodeType;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public List<TreeNodeVo> getChildren() {
        return children;
    }

    public void setChildren(List<TreeNodeVo> children) {
        this.children = children;
    }

    public boolean getIsParent() {
        return isParent;
    }

    public void setIsParent(boolean parent) {
        isParent = parent;
    }

    @Override
    public String toString() {
        return "TreeNodeVo{" +
                "indexCode='" + indexCode + '\'' +
                ", parentIndexCode='" + parentIndexCode + '\'' +
                ", text='" + text + '\'' +
                ", nodeType='" + nodeType + '\'' +
                ", open=" + open +
                ", isParent=" + isParent +
                ", children=" + children +
                '}';
    }
}
