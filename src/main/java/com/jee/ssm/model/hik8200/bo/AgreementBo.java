package com.jee.ssm.model.hik8200.bo;

/**
 * @author zhangtuo
 * @Date 2017/6/28
 */
public class AgreementBo {
    private String appSecret;//加密后的appSecret
    private String time;
    private String timeSecret;

    public AgreementBo() {
    }

    public AgreementBo(String appSecret, String time, String timeSecret) {
        this.appSecret = appSecret;
        this.time = time;
        this.timeSecret = timeSecret;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTimeSecret() {
        return timeSecret;
    }

    public void setTimeSecret(String timeSecret) {
        this.timeSecret = timeSecret;
    }
}
