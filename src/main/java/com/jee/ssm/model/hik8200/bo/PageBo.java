package com.jee.ssm.model.hik8200.bo;

/**
 * @author zhangtuo
 * @Date 2017/5/16
 */
public class PageBo {
    private Integer start;

    private Integer size;

    private String order;

    private String orderby;

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getOrderby() {
        return orderby;
    }

    public void setOrderby(String orderby) {
        this.orderby = orderby;
    }

    public PageBo(Integer start, Integer size, String order, String orderby) {
        this.start = start;
        this.size = size;
        this.order = order;
        this.orderby = orderby;
    }

    public PageBo() {
    }

    @Override
    public String toString() {
        return "PageBo{" +
                "start=" + start +
                ", size=" + size +
                ", order='" + order + '\'' +
                ", orderby='" + orderby + '\'' +
                '}';
    }
}
