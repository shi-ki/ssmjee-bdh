package com.jee.ssm.model;

import com.jee.ssm.model.base.BaseModel;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * 表名 b_bill_record
 * @author GaoXiang
 * @version 1.0
 * Create time 2018/04/28
 */
public class BillRecord extends BaseModel {
    /**
     * 
     * 表字段 : b_bill_record.id
     * Create time 2018/04/28
     */
    private String id;

    /**
     * 订单编号
     * 表字段 : b_bill_record.number
     * Create time 2018/04/28
     */
    private String number;

    /**
     * 支付金额
     * 表字段 : b_bill_record.pay_amount
     * Create time 2018/04/28
     */
    private BigDecimal payAmount;

    /**
     * 创建时间
     * 表字段 : b_bill_record.create_time
     * Create time 2018/04/28
     */
    private Date createTime;

    /**
     * 订单内容
     * 表字段 : b_bill_record.content
     * Create time 2018/04/28
     */
    private String content;

    /**
     * 加或减（+、-）
     * 表字段 : b_bill_record.add_or_sub
     * Create time 2018/04/28
     */
    private String addOrSub;

    /**
     * 是否完成（0：未完成，1：已完成）
     * 表字段 : b_bill_record.is_finished
     * Create time 2018/04/28
     */
    private Integer isFinished;
    
    /**
     * 状态（默认：1）
     * 表字段 : b_bill_record.status
     * Create time 2018/09/10
     */
    private Integer status;

    /**
     * 类型（0:提现，1：充值，2：预约缴费，3：停车缴费）
     * 表字段 : b_bill_record.type
     * Create time 2018/09/10
     */
    private Integer type;

    /**
     * 关联它表id
     * 表字段 : b_bill_record.relate_id
     * Create time 2018/09/10
     */
    private String relateId;

    /**
     * 备注
     * 表字段 : b_bill_record.intro
     * Create time 2018/04/28
     */
    private String intro;

    /**
     * 用户id
     * 表字段 : b_bill_record.user_id
     * Create time 2018/04/28
     */
    private String userId;

    /**
     * 用户昵称
     * 表字段 : b_bill_record.user_name
     * Create time 2018/04/28
     */
    private String userName;

    /**
     * 用户手机号码
     * 表字段 : b_bill_record.user_phone
     * Create time 2018/04/28
     */
    private String userPhone;

    /**
     * 用户姓名
     * 表字段 : b_bill_record.real_name
     * Create time 2018/04/28
     */
    private String realName;

    /**
     * 身份证号码
     * 表字段 : b_bill_record.id_number
     * Create time 2018/04/28
     */
    private String idNumber;

    /**
     * 支付方式（0：余额，1：冻结余额，2：APP支付宝支付，3：APP微信支付，4：银联，5支付宝扫码支付，6微信扫码支付，7微信小程序支付）
     * 表字段 : b_bill_record.pay_method
     * Create time 2018/04/28
     */
    private String payMethod;
    /**
     * 入场时间
     * 表字段 : b_hik_bill.in_date_time
     * Create time 2018/09/08
     */
    private Date inDateTime;

    /**
     * 离场时间
     * 表字段 : b_hik_bill.out_date_time
     * Create time 2018/09/08
     */
    private Date outDateTime;

    /**
     * 停车时长（分钟）
     * 表字段 : b_hik_bill.park_time
     * Create time 2018/09/08
     */
    private Integer parkTime;

    /**
     * 车牌号码
     * 表字段 : b_hik_bill.plate_no
     * Create time 2018/09/08
     */
    private String plateNo;

    /**
     * 构造方法
     */
    public BillRecord(String id, String number, BigDecimal payAmount, Date createTime, String content, String addOrSub, Integer isFinished, Integer status, Integer type, String relateId, String intro, String userId, String userName, String userPhone, String realName, String idNumber, String payMethod, Date inDateTime,Date outDateTime,Integer parkTime,String  plateNo) {
        this.id = id;
        this.number = number;
        this.payAmount = payAmount;
        this.createTime = createTime;
        this.content = content;
        this.addOrSub = addOrSub;
        this.isFinished = isFinished;
        this.status = status;
        this.type = type;
        this.relateId = relateId;
        this.intro = intro;
        this.userId = userId;
        this.userName = userName;
        this.userPhone = userPhone;
        this.realName = realName;
        this.idNumber = idNumber;
        this.payMethod = payMethod;
        this.inDateTime= inDateTime;
        this.outDateTime = outDateTime;
        this.parkTime = parkTime;
        this.plateNo = plateNo;
    }

    /**
     * 构造方法
     */
    public BillRecord() {
        super();
    }

    /**
     * 
     * @return id 
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 订单编号
     * @return number 订单编号
     */
    public String getNumber() {
        return number;
    }

    /**
     * 订单编号
     * @param number 订单编号
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * 支付金额
     * @return pay_amount 支付金额
     */
    public BigDecimal getPayAmount() {
        return payAmount;
    }

    /**
     * 支付金额
     * @param payAmount 支付金额
     */
    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }

    /**
     * 创建时间
     * @return create_time 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 创建时间
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 订单内容
     * @return content 订单内容
     */
    public String getContent() {
        return content;
    }

    /**
     * 订单内容
     * @param content 订单内容
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 加或减（+、-）
     * @return add_or_sub 加或减（+、-）
     */
    public String getAddOrSub() {
        return addOrSub;
    }

    /**
     * 加或减（+、-）
     * @param addOrSub 加或减（+、-）
     */
    public void setAddOrSub(String addOrSub) {
        this.addOrSub = addOrSub;
    }

    /**
     * 是否完成（0：未完成，1：已完成）
     * @return is_finished 是否完成（0：未完成，1：已完成）
     */
    public Integer getIsFinished() {
        return isFinished;
    }

    /**
     * 是否完成（0：未完成，1：已完成）
     * @param isFinished 是否完成（0：未完成，1：已完成）
     */
    public void setIsFinished(Integer isFinished) {
        this.isFinished = isFinished;
    }
    
    /**
     * 状态（默认：1）
     * @return status 状态（默认：1）
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 状态（默认：1）
     * @param status 状态（默认：1）
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 类型（1：充值，2：预约缴费，3：停车缴费）
     * @return type 类型（1：充值，2：预约缴费，3：停车缴费）
     */
    public Integer getType() {
        return type;
    }

    /**
     * 类型（1：充值，2：预约缴费，3：停车缴费）
     * @param type 类型（1：充值，2：预约缴费，3：停车缴费）
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 关联它表id
     * @return relate_id 关联它表id
     */
    public String getRelateId() {
        return relateId;
    }

    /**
     * 关联它表id
     * @param relateId 关联它表id
     */
    public void setRelateId(String relateId) {
        this.relateId = relateId;
    }

    /**
     * 备注
     * @return intro 备注
     */
    public String getIntro() {
        return intro;
    }

    /**
     * 备注
     * @param intro 备注
     */
    public void setIntro(String intro) {
        this.intro = intro;
    }

    /**
     * 用户id
     * @return user_id 用户id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 用户id
     * @param userId 用户id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 用户昵称
     * @return user_name 用户昵称
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 用户昵称
     * @param userName 用户昵称
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 用户手机号码
     * @return user_phone 用户手机号码
     */
    public String getUserPhone() {
        return userPhone;
    }

    /**
     * 用户手机号码
     * @param userPhone 用户手机号码
     */
    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    /**
     * 用户姓名
     * @return real_name 用户姓名
     */
    public String getRealName() {
        return realName;
    }

    /**
     * 用户姓名
     * @param realName 用户姓名
     */
    public void setRealName(String realName) {
        this.realName = realName;
    }

    /**
     * 身份证号码
     * @return id_number 身份证号码
     */
    public String getIdNumber() {
        return idNumber;
    }

    /**
     * 身份证号码
     * @param idNumber 身份证号码
     */
    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    /**
     * 支付方式（0：余额，1：冻结余额，2：APP支付宝支付，3：APP微信支付，4：银联，5支付宝扫码支付，6微信扫码支付，7微信小程序支付）
     * @return pay_method 支付方式（0：余额，1：冻结余额，2：APP支付宝支付，3：APP微信支付，4：银联，5支付宝扫码支付，6微信扫码支付，7微信小程序支付）
     */
    public String getPayMethod() {
        return payMethod;
    }

    /**
     * 支付方式（0：余额，1：冻结余额，2：APP支付宝支付，3：APP微信支付，4：银联，5支付宝扫码支付，6微信扫码支付，7微信小程序支付）
     * @param payMethod 支付方式（0：余额，1：冻结余额，2：APP支付宝支付，3：APP微信支付，4：银联，5支付宝扫码支付，6微信扫码支付，7微信小程序支付）
     */
    public void setPayMethod(String payMethod) {
        this.payMethod = payMethod;
    }

    public Date getInDateTime() {
        return inDateTime;
    }

    public void setInDateTime(Date inDateTime) {
        this.inDateTime = inDateTime;
    }

    public Date getOutDateTime() {
        return outDateTime;
    }

    public void setOutDateTime(Date outDateTime) {
        this.outDateTime = outDateTime;
    }

    public Integer getParkTime() {
        return parkTime;
    }

    public void setParkTime(Integer parkTime) {
        this.parkTime = parkTime;
    }

    public String getPlateNo() {
        return plateNo;
    }

    public void setPlateNo(String plateNo) {
        this.plateNo = plateNo;
    }
}