package com.jee.ssm.model;

import com.jee.ssm.model.base.BaseModel;

import java.util.Date;

/**
 * 
 * 表名 b_image
 * @author GaoXiang
 * @version 1.0
 * Create time 2018/09/25
 */
public class Image extends BaseModel {
    /**
     * 
     * 表字段 : b_image.id
     * Create time 2018/09/25
     */
    private String id;

    /**
     * 图片地址
     * 表字段 : b_image.url
     * Create time 2018/09/25
     */
    private String url;

    /**
     * 图片名称
     * 表字段 : b_image.name
     * Create time 2018/09/25
     */
    private String name;

    /**
     * 图片类型
     * 表字段 : b_image.type
     * Create time 2018/09/25
     */
    private String type;

    private Date createTime;

    /**
     * 构造方法
     */
    public Image(String id, String url, String name, String type) {
        this.id = id;
        this.url = url;
        this.name = name;
        this.type = type;
    }

    /**
     * 构造方法
     */
    public Image() {
        super();
    }

    /**
     * 
     * @return id 
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 图片地址
     * @return url 图片地址
     */
    public String getUrl() {
        return url;
    }

    /**
     * 图片地址
     * @param url 图片地址
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 图片名称
     * @return name 图片名称
     */
    public String getName() {
        return name;
    }

    /**
     * 图片名称
     * @param name 图片名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 图片类型
     * @return type 图片类型
     */
    public String getType() {
        return type;
    }

    /**
     * 图片类型
     * @param type 图片类型
     */
    public void setType(String type) {
        this.type = type;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}