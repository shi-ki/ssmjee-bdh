package com.jee.ssm.model.hik;

import java.util.List;


public class ParkInfoRequest extends BaseRequest{

    //停车场
    private String parkCode;

    //停车场列表
    List<ParkInfo> parkInfos;

    public List<ParkInfo> getParkInfos() {
        return parkInfos;
    }

    public void setParkInfos(List<ParkInfo> parkInfos) {
        this.parkInfos = parkInfos;
    }

    public String getParkCode() {
        return parkCode;
    }

    public void setParkCode(String parkCode) {
        this.parkCode = parkCode;
    }
}