package com.jee.ssm.model.hik;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


public class ParkInfo implements Serializable {
	// serialVersionUID:序列化
	private static final long serialVersionUID = -383323669659003346L;

	// 主键
	private Long parkId;

	// 运营商ID
	private Long dealerId;

	// 停车场编号
	private String parkCode;

	// 停车场名称
	private String parkName;

	// 创建时间
	private Date createTime;

	// 修改时间
	private Date updateTime;

	// 总车位数
	private Short totalParkingSpaceNum;

	// 剩余车位数
	private Short leftParkingSpaceNum;

	// 停车场经度
	private BigDecimal longitude;

	// 停车场纬度
	private BigDecimal latitude;

	// 停车场类型：0-路边停车场，1-路边简易停车场，2-场内停车场
	private Byte parkType;

	// 描述信息
	private String description;

	// 停车场别名
	private String parkingAlias;

	// 停车场地址
	private String parkingAddress;

	// 运营状态，0-正常营业，1-暂停营业
	private Byte operationState;

	// 是否支持车位预订，0-不支持，1-支持并可预定到指定车位，2-支持但不能预定到指定车位
	private Byte bookSupport;

	// 是否可包期，0 否， 1 是
	private Byte bagcarSupport;

	// 是否支持将停车预订订金作为停车费的一部分0 否， 1 是
	private Byte depositSupport;

	// 可预订总车位数
	private Short totalBookParkingSpaceNum;

	// 可预订剩余车位数
	private Short leftBookParkingSpaceNum;

	// 是否支持优惠券：0 不支持；1 支持
	private Byte couponSupport;

	// 剩余可申请包期车的数量
	private Short leftBagCarNum;

	// 收费规则的简短描述
	private String payRuleDesc;

	// 是否已经删除：0 否； 1 是
	private String isDelete;

	// 场库系统ID
	private Long dockingId;

	// 场库名称
	private String platformName;

	// 停车场所属的区域ID
	private Long localeId;
	private String localeName;

	// 是否接入支付宝车主服务：0 否； 1 是
	private Byte alipaySupport;

	// 停车场开始营业时间，格式 HH:mm:ss 07:00:00
	private String parkingStartTime;

	// 停车场结束营业时间，格式 HH:mm:ss 03:07:50
	private String parkingEndTime;

	// 停车场类型，1为小区停车场、2为商圈停车场、3为路面停车场、4为园区停车场、5为写字楼停车场、6为私人停车场
	private Byte parkingLotType;

	// 停车场位置(1为地面，2为地下，3为路边)
	private Byte parkingType;

	// 缴费模式（1为停车卡缴费，2为物料缴费，3为中央缴费机）
	private Byte paymentMode;

	// 支付方式（1为支付宝在线缴费，2为支付宝代扣缴费，3当面付)，如支持多种方式以'',''进行间隔
	private String payType;

	// 支付宝停车场ID ，系统唯一(新增支付宝推送的时候返回)
	private String alipayParkId;

	public Long getLocaleId() {
		return localeId;
	}

	public void setLocaleId(Long localeId) {
		this.localeId = localeId;
	}

	public ParkInfo() {
		super();
	}

	public Long getParkId() {
		return parkId;
	}

	public void setParkId(Long parkId) {
		this.parkId = parkId;
	}

	public Long getDealerId() {
		return dealerId;
	}

	public void setDealerId(Long dealerId) {
		this.dealerId = dealerId;
	}

	public String getParkCode() {
		return parkCode;
	}

	public void setParkCode(String parkCode) {
		this.parkCode = parkCode == null ? null : parkCode.trim();
	}

	public String getParkName() {
		return parkName;
	}

	public void setParkName(String parkName) {
		this.parkName = parkName == null ? null : parkName.trim();
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Short getTotalParkingSpaceNum() {
		return totalParkingSpaceNum;
	}

	public void setTotalParkingSpaceNum(Short totalParkingSpaceNum) {
		this.totalParkingSpaceNum = totalParkingSpaceNum;
	}

	public Short getLeftParkingSpaceNum() {
		return leftParkingSpaceNum;
	}

	public void setLeftParkingSpaceNum(Short leftParkingSpaceNum) {
		this.leftParkingSpaceNum = leftParkingSpaceNum;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public Byte getParkType() {
		return parkType;
	}

	public void setParkType(Byte parkType) {
		this.parkType = parkType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description == null ? null : description.trim();
	}

	public String getParkingAlias() {
		return parkingAlias;
	}

	public void setParkingAlias(String parkingAlias) {
		this.parkingAlias = parkingAlias == null ? null : parkingAlias.trim();
	}

	public String getParkingAddress() {
		return parkingAddress;
	}

	public void setParkingAddress(String parkingAddress) {
		this.parkingAddress = parkingAddress == null ? null : parkingAddress
				.trim();
	}

	public Byte getOperationState() {
		return operationState;
	}

	public void setOperationState(Byte operationState) {
		this.operationState = operationState;
	}

	public Byte getBookSupport() {
		return bookSupport;
	}

	public void setBookSupport(Byte bookSupport) {
		this.bookSupport = bookSupport;
	}

	public Byte getBagcarSupport() {
		return bagcarSupport;
	}

	public void setBagcarSupport(Byte bagcarSupport) {
		this.bagcarSupport = bagcarSupport;
	}

	public Byte getDepositSupport() {
		return depositSupport;
	}

	public void setDepositSupport(Byte depositSupport) {
		this.depositSupport = depositSupport;
	}

	public Short getTotalBookParkingSpaceNum() {
		return totalBookParkingSpaceNum;
	}

	public void setTotalBookParkingSpaceNum(Short totalBookParkingSpaceNum) {
		this.totalBookParkingSpaceNum = totalBookParkingSpaceNum;
	}

	public Short getLeftBookParkingSpaceNum() {
		return leftBookParkingSpaceNum;
	}

	public void setLeftBookParkingSpaceNum(Short leftBookParkingSpaceNum) {
		this.leftBookParkingSpaceNum = leftBookParkingSpaceNum;
	}

	public Byte getCouponSupport() {
		return couponSupport;
	}

	public void setCouponSupport(Byte couponSupport) {
		this.couponSupport = couponSupport;
	}

	public Short getLeftBagCarNum() {
		return leftBagCarNum;
	}

	public void setLeftBagCarNum(Short leftBagCarNum) {
		this.leftBagCarNum = leftBagCarNum;
	}

	public String getPayRuleDesc() {
		return payRuleDesc;
	}

	public void setPayRuleDesc(String payRuleDesc) {
		this.payRuleDesc = payRuleDesc == null ? null : payRuleDesc.trim();
	}

	public String getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete == null ? null : isDelete.trim();
	}

	public Long getDockingId() {
		return dockingId;
	}

	public void setDockingId(Long dockingId) {
		this.dockingId = dockingId;
	}

	public String getPlatformName() {
		return platformName;
	}

	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}

	public Byte getAlipaySupport() {
		return alipaySupport;
	}

	public void setAlipaySupport(Byte alipaySupport) {
		this.alipaySupport = alipaySupport;
	}

	public String getParkingStartTime() {
		return parkingStartTime;
	}

	public void setParkingStartTime(String parkingStartTime) {
		this.parkingStartTime = parkingStartTime;
	}

	public String getParkingEndTime() {
		return parkingEndTime;
	}

	public void setParkingEndTime(String parkingEndTime) {
		this.parkingEndTime = parkingEndTime;
	}

	public Byte getParkingLotType() {
		return parkingLotType;
	}

	public void setParkingLotType(Byte parkingLotType) {
		this.parkingLotType = parkingLotType;
	}

	public Byte getParkingType() {
		return parkingType;
	}

	public void setParkingType(Byte parkingType) {
		this.parkingType = parkingType;
	}

	public Byte getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(Byte paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getAlipayParkId() {
		return alipayParkId;
	}

	public void setAlipayParkId(String alipayParkId) {
		this.alipayParkId = alipayParkId;
	}

	public String getLocaleName() {
		return localeName;
	}

	public void setLocaleName(String localeName) {
		this.localeName = localeName;
	}


	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("ParkInfo{");
		sb.append("parkId=").append(parkId);
		sb.append(", dealerId=").append(dealerId);
		sb.append(", parkCode='").append(parkCode).append('\'');
		sb.append(", parkName='").append(parkName).append('\'');
		sb.append(", createTime=").append(createTime);
		sb.append(", updateTime=").append(updateTime);
		sb.append(", totalParkingSpaceNum=").append(totalParkingSpaceNum);
		sb.append(", leftParkingSpaceNum=").append(leftParkingSpaceNum);
		sb.append(", longitude=").append(longitude);
		sb.append(", latitude=").append(latitude);
		sb.append(", parkType=").append(parkType);
		sb.append(", description='").append(description).append('\'');
		sb.append(", parkingAlias='").append(parkingAlias).append('\'');
		sb.append(", parkingAddress='").append(parkingAddress).append('\'');
		sb.append(", operationState=").append(operationState);
		sb.append(", bookSupport=").append(bookSupport);
		sb.append(", bagcarSupport=").append(bagcarSupport);
		sb.append(", depositSupport=").append(depositSupport);
		sb.append(", totalBookParkingSpaceNum=").append(totalBookParkingSpaceNum);
		sb.append(", leftBookParkingSpaceNum=").append(leftBookParkingSpaceNum);
		sb.append(", couponSupport=").append(couponSupport);
		sb.append(", leftBagCarNum=").append(leftBagCarNum);
		sb.append(", payRuleDesc='").append(payRuleDesc).append('\'');
		sb.append(", isDelete='").append(isDelete).append('\'');
		sb.append(", dockingId=").append(dockingId);
		sb.append(", platformName='").append(platformName).append('\'');
		sb.append(", localeId=").append(localeId);
		sb.append(", localeName='").append(localeName).append('\'');
		sb.append(", alipaySupport=").append(alipaySupport);
		sb.append(", parkingStartTime='").append(parkingStartTime).append('\'');
		sb.append(", parkingEndTime='").append(parkingEndTime).append('\'');
		sb.append(", parkingLotType=").append(parkingLotType);
		sb.append(", parkingType=").append(parkingType);
		sb.append(", paymentMode=").append(paymentMode);
		sb.append(", payType='").append(payType).append('\'');
		sb.append(", alipayParkId='").append(alipayParkId).append('\'');
		sb.append('}');
		return sb.toString();
	}
}