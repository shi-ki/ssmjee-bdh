package com.jee.ssm.model.hik;

public class HikReturn implements java.io.Serializable {
	
	private static final long serialVersionUID = -6552220983217995966L;

	private String code;
	
	private String mesg;
	
	private int status;
	
	private String busNo;

	public HikReturn(String code, String mesg, int status, String busNo) {
		super();
		this.code = code;
		this.mesg = mesg;
		this.status = status;
		this.busNo = busNo;
	}

	public HikReturn(String code, String mesg, String busNo) {
		super();
		this.code = code;
		this.mesg = mesg;
		this.busNo = busNo;
	}

	public HikReturn() {
		super();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMesg() {
		return mesg;
	}

	public void setMesg(String mesg) {
		this.mesg = mesg;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getBusNo() {
		return busNo;
	}

	public void setBusNo(String busNo) {
		this.busNo = busNo;
	}

	@Override
	public String toString() {
		return "HikReturn [code=" + code + ", mesg=" + mesg + ", status=" + status + ", busNo=" + busNo + "]";
	}
	
}
