package com.jee.ssm.model.hik;


public class GetGateInfosByParkCodesRequest extends BaseRequest {
    // 停车场编号
    private String parkCodes;

    public String getParkCodes() {
        return parkCodes;
    }

    public void setParkCodes(String parkCodes) {
        this.parkCodes = parkCodes;
    }

}
