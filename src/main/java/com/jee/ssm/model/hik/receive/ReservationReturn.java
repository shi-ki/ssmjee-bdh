package com.jee.ssm.model.hik.receive;

import java.io.Serializable;

public class ReservationReturn implements Serializable {
	
	private static final long serialVersionUID = 6777452895930591465L;

	private String billNo;
	
	private String plotNo;

	public String getBillNo() {
		return billNo;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	public String getPlotNo() {
		return plotNo;
	}

	public void setPlotNo(String plotNo) {
		this.plotNo = plotNo;
	}

	@Override
	public String toString() {
		return "ReservationReturn [billNo=" + billNo + ", plotNo=" + plotNo + "]";
	}

}
