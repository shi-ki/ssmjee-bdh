package com.jee.ssm.model.hik.receive;

import com.jee.ssm.model.base.BaseModel;

public class LaneInfo  extends BaseModel {
    private String parkCode;
    private String  laneNo;
    private String  laneName;
    private Integer  laneType;

    public String getLaneNo() {
        return laneNo;
    }

    public void setLaneNo(String laneNo) {
        this.laneNo = laneNo;
    }

    public String getLaneName() {
        return laneName;
    }

    public void setLaneName(String laneName) {
        this.laneName = laneName;
    }

    public Integer getLaneType() {
        return laneType;
    }

    public void setLaneType(Integer laneType) {
        this.laneType = laneType;
    }

    public String getParkCode() {
        return parkCode;
    }

    public void setParkCode(String parkCode) {
        this.parkCode = parkCode;
    }
}
