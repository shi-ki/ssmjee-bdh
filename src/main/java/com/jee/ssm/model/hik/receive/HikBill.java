package com.jee.ssm.model.hik.receive;

import java.io.Serializable;
import java.util.Date;

public class HikBill implements Serializable {

    private static final long serialVersionUID = -2677199217694290832L;

    /**
     * 标识
     * 表字段 : b_hik_bill.id
     * Create time 2018/09/08
     */
    private String id;

    /**
     * 停车场编号
     * 表字段 : b_hik_bill.park_code
     * Create time 2018/09/08
     */
    private String parkCode;

    /**
     * 停车场名称
     * 表字段 : b_hik_bill.park_name
     * Create time 2018/09/08
     */
    private String parkName;

    /**
     * 海康标识
     * 表字段 : b_hik_bill.in_unid
     * Create time 2018/09/08
     */
    private String inUnid;

    /**
     * 海康账单编号
     * 表字段 : b_hik_bill.bill_no
     * Create time 2018/09/08
     */
    private String billNo;
    private String billCode;
    /**
     * 入场时间（毫秒数）
     * 表字段 : b_hik_bill.in_time
     * Create time 2018/09/08
     */
    private Long inTime;

    /**
     * 离场时间（毫秒数）
     * 表字段 : b_hik_bill.out_time
     * Create time 2018/09/08
     */
    private Long outTime;

    /**
     * 入场时间
     * 表字段 : b_hik_bill.in_date_time
     * Create time 2018/09/08
     */
    private Date inDateTime;

    /**
     * 离场时间
     * 表字段 : b_hik_bill.out_date_time
     * Create time 2018/09/08
     */
    private Date outDateTime;

    /**
     * 停车时长（分钟）
     * 表字段 : b_hik_bill.park_time
     * Create time 2018/09/08
     */
    private Integer parkTime;

    /**
     * 车牌号码
     * 表字段 : b_hik_bill.plate_no
     * Create time 2018/09/08
     */
    private String plateNo;

    /**
     * 车牌颜色
     * 表字段 : b_hik_bill.plate_color
     * Create time 2018/09/08
     */
    private Integer plateColor;

    /**
     * 车辆类型
     * 表字段 : b_hik_bill.car_type
     * Create time 2018/09/08
     */
    private Integer carType;

    /**
     * 停车费用（分）
     * 表字段 : b_hik_bill.need_pay
     * Create time 2018/09/08
     */
    private Integer needPay;

    /**
     * 添加时间
     * 表字段 : b_hik_bill.create_time
     * Create time 2018/09/08
     */
    private Date createTime;

    /**
     * 修改时间
     * 表字段 : b_hik_bill.update_time
     * Create time 2018/09/08
     */
    private Date updateTime;

    /**
     * 支付状态（0：未支付，1：已支付）
     * 表字段 : b_hik_bill.pay_status
     * Create time 2018/09/08
     */
    private Integer payStatus;

    /**
     * 支付方式（0：现金，1：余额，2：支付宝，3：微信，4：建行无感支付）
     * 表字段 : b_hik_bill.pay_method
     * Create time 2018/09/08
     */
    private Integer payMethod;

    /**
     * 状态（默认：1）
     * 表字段 : b_hik_bill.status
     * Create time 2018/09/08
     */
    private Integer status;

    /**
     * 类型（默认：1）
     * 表字段 : b_hik_bill.type
     * Create time 2018/09/08
     */
    private Integer type;

    /**
     * 用户标识
     * 表字段 : b_hik_bill.user_id
     * Create time 2018/09/11
     */
    private String userId;

    /**
     *
     * 表字段 : b_hik_bill.user_name
     * Create time 2018/09/11
     */
    private String userName;

    /**
     * 用户手机号码
     * 表字段 : b_hik_bill.user_phone
     * Create time 2018/09/11
     */
    private String userPhone;

    /**
     * 真实姓名
     * 表字段 : b_hik_bill.real_name
     * Create time 2018/09/11
     */
    private String realName;

    /**
     * 身份证号码
     * 表字段 : b_hik_bill.id_number
     * Create time 2018/09/11
     */
    private String idNumber;
    /**
     *车道序号
     * 表字段 : b_hik_bill.lane_no
     * Create time 2018/10/23
     */
    private String laneNo;
    /**
     * 车道方向  0:入场 1：出场 2：出入
     * 表字段 : b_hik_bill.direction
     * Create time 2018/10/23
     */
    private String direction;
    private Integer  totalCost;

    /**
     * 构造方法
     */
    public HikBill(String id, String parkCode,String   billCode, String parkName, String inUnid, String billNo, Long inTime, Long outTime, Date inDateTime, Date outDateTime, Integer parkTime, String plateNo, Integer plateColor, Integer carType, Integer needPay, Date createTime, Date updateTime, Integer payStatus, Integer payMethod, Integer status, Integer type, String userId, String userName, String userPhone, String realName, String idNumber,String laneNo,String direction,Integer totalCost) {
        this.id = id;
        this.parkCode = parkCode;
        this.parkName = parkName;
        this.inUnid = inUnid;
        this.billNo = billNo;
        this.inTime = inTime;
        this.outTime = outTime;
        this.inDateTime = inDateTime;
        this.outDateTime = outDateTime;
        this.parkTime = parkTime;
        this.plateNo = plateNo;
        this.plateColor = plateColor;
        this.carType = carType;
        this.needPay = needPay;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.payStatus = payStatus;
        this.payMethod = payMethod;
        this.status = status;
        this.type = type;
        this.userId = userId;
        this.userName = userName;
        this.userPhone = userPhone;
        this.realName = realName;
        this.idNumber = idNumber;
        this.laneNo = laneNo;
        this.direction = direction;
        this.totalCost = totalCost;
        this.billCode =  billCode;
    }

    /**
     * 构造方法
     */
    public HikBill() {
        super();
    }

    /**
     * 标识
     * @return id 标识
     */
    public String getId() {
        return id;
    }

    /**
     * 标识
     * @param id 标识
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 停车场编号
     * @return park_code 停车场编号
     */
    public String getParkCode() {
        return parkCode;
    }

    /**
     * 停车场编号
     * @param parkCode 停车场编号
     */
    public void setParkCode(String parkCode) {
        this.parkCode = parkCode;
    }

    /**
     * 停车场名称
     * @return park_name 停车场名称
     */
    public String getParkName() {
        return parkName;
    }

    /**
     * 停车场名称
     * @param parkName 停车场名称
     */
    public void setParkName(String parkName) {
        this.parkName = parkName;
    }

    /**
     * 海康标识
     * @return in_unid 海康标识
     */
    public String getInUnid() {
        return inUnid;
    }

    /**
     * 海康标识
     * @param inUnid 海康标识
     */
    public void setInUnid(String inUnid) {
        this.inUnid = inUnid;
    }

    /**
     * 海康账单编号
     * @return bill_no 海康账单编号
     */
    public String getBillNo() {
        return billNo;
    }

    /**
     * 海康账单编号
     * @param billNo 海康账单编号
     */
    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    /**
     * 入场时间（毫秒数）
     * @return in_time 入场时间（毫秒数）
     */
    public Long getInTime() {
        return inTime;
    }

    /**
     * 入场时间（毫秒数）
     * @param inTime 入场时间（毫秒数）
     */
    public void setInTime(Long inTime) {
        this.inTime = inTime;
    }

    /**
     * 离场时间（毫秒数）
     * @return out_time 离场时间（毫秒数）
     */
    public Long getOutTime() {
        return outTime;
    }

    /**
     * 离场时间（毫秒数）
     * @param outTime 离场时间（毫秒数）
     */
    public void setOutTime(Long outTime) {
        this.outTime = outTime;
    }

    /**
     * 入场时间
     * @return in_date_time 入场时间
     */
    public Date getInDateTime() {
        return inDateTime;
    }

    /**
     * 入场时间
     * @param inDateTime 入场时间
     */
    public void setInDateTime(Date inDateTime) {
        this.inDateTime = inDateTime;
    }

    /**
     * 离场时间
     * @return out_date_time 离场时间
     */
    public Date getOutDateTime() {
        return outDateTime;
    }

    /**
     * 离场时间
     * @param outDateTime 离场时间
     */
    public void setOutDateTime(Date outDateTime) {
        this.outDateTime = outDateTime;
    }

    /**
     * 停车时长（分钟）
     * @return park_time 停车时长（分钟）
     */
    public Integer getParkTime() {
        return parkTime;
    }

    /**
     * 停车时长（分钟）
     * @param parkTime 停车时长（分钟）
     */
    public void setParkTime(Integer parkTime) {
        this.parkTime = parkTime;
    }

    /**
     * 车牌号码
     * @return plate_no 车牌号码
     */
    public String getPlateNo() {
        return plateNo;
    }

    /**
     * 车牌号码
     * @param plateNo 车牌号码
     */
    public void setPlateNo(String plateNo) {
        this.plateNo = plateNo;
    }

    /**
     * 车牌颜色
     * @return plate_color 车牌颜色
     */
    public Integer getPlateColor() {
        return plateColor;
    }

    /**
     * 车牌颜色
     * @param plateColor 车牌颜色
     */
    public void setPlateColor(Integer plateColor) {
        this.plateColor = plateColor;
    }

    /**
     * 车辆类型
     * @return car_type 车辆类型
     */
    public Integer getCarType() {
        return carType;
    }

    /**
     * 车辆类型
     * @param carType 车辆类型
     */
    public void setCarType(Integer carType) {
        this.carType = carType;
    }

    /**
     * 停车费用（分）
     * @return need_pay 停车费用（分）
     */
    public Integer getNeedPay() {
        return needPay;
    }

    /**
     * 停车费用（分）
     * @param needPay 停车费用（分）
     */
    public void setNeedPay(Integer needPay) {
        this.needPay = needPay;
    }

    /**
     * 添加时间
     * @return create_time 添加时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 添加时间
     * @param createTime 添加时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 修改时间
     * @return update_time 修改时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 修改时间
     * @param updateTime 修改时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 支付状态（0：未支付，1：已支付）
     * @return pay_status 支付状态（0：未支付，1：已支付）
     */
    public Integer getPayStatus() {
        return payStatus;
    }

    /**
     * 支付状态（0：未支付，1：已支付）
     * @param payStatus 支付状态（0：未支付，1：已支付）
     */
    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    /**
     * 支付方式
     * @return pay_method 支付方式
     */
    public Integer getPayMethod() {
        return payMethod;
    }

    /**
     * 支付方式
     * @param payMethod 支付方式
     */
    public void setPayMethod(Integer payMethod) {
        this.payMethod = payMethod;
    }

    /**
     * 状态（默认：1）
     * @return status 状态（默认：1）
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 状态（默认：1）
     * @param status 状态（默认：1）
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 类型（默认：1）
     * @return type 类型（默认：1）
     */
    public Integer getType() {
        return type;
    }

    /**
     * 类型（默认：1）
     * @param type 类型（默认：1）
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 用户标识
     * @return user_id 用户标识
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 用户标识
     * @param userId 用户标识
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     *
     * @return user_name
     */
    public String getUserName() {
        return userName;
    }

    /**
     *
     * @param userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 用户手机号码
     * @return user_phone 用户手机号码
     */
    public String getUserPhone() {
        return userPhone;
    }

    /**
     * 用户手机号码
     * @param userPhone 用户手机号码
     */
    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    /**
     * 真实姓名
     * @return real_name 真实姓名
     */
    public String getRealName() {
        return realName;
    }

    /**
     * 真实姓名
     * @param realName 真实姓名
     */
    public void setRealName(String realName) {
        this.realName = realName;
    }

    /**
     * 身份证号码
     * @return id_number 身份证号码
     */
    public String getIdNumber() {
        return idNumber;
    }

    /**
     * 身份证号码
     * @param idNumber 身份证号码
     */
    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getLaneNo() {
        return laneNo;
    }

    public void setLaneNo(String laneNo) {
        this.laneNo = laneNo;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public Integer getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Integer totalCost) {
        this.totalCost = totalCost;
    }

    public String getBillCode() {
        return billCode;
    }

    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    @Override
    public String toString() {
        return "HikBill{" +
                "id='" + id + '\'' +
                ", parkCode='" + parkCode + '\'' +
                ", parkName='" + parkName + '\'' +
                ", inUnid='" + inUnid + '\'' +
                ", billNo='" + billNo + '\'' +
                ", inTime=" + inTime +
                ", outTime=" + outTime +
                ", inDateTime=" + inDateTime +
                ", outDateTime=" + outDateTime +
                ", parkTime=" + parkTime +
                ", plateNo='" + plateNo + '\'' +
                ", plateColor=" + plateColor +
                ", carType=" + carType +
                ", needPay=" + needPay +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", payStatus=" + payStatus +
                ", payMethod=" + payMethod +
                ", status=" + status +
                ", type=" + type +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", userPhone='" + userPhone + '\'' +
                ", realName='" + realName + '\'' +
                ", idNumber='" + idNumber + '\'' +
                ", laneNo='" + laneNo + '\'' +
                ", direction='" + direction + '\'' +
                ", totalCost='" + totalCost + '\'' +
                ", billCode='" + billCode + '\'' +

                '}';
    }
}
