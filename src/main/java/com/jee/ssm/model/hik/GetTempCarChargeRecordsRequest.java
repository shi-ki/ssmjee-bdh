package com.jee.ssm.model.hik;

import java.util.Date;
import java.util.List;


public class GetTempCarChargeRecordsRequest extends BaseRequest{

    //当前页码
    private Integer pageNo;

    //每页数据记录数
    private Integer pageSize;

    // 分页始
    private Integer offset;

    //停车场编号
    private String parkCode;

    //车牌号码
    private String plateNo;

    //车牌颜色
    private Integer plateColor;

    //收费类型
    private Integer payType;

    //开始查询的缴费时间
    private Long startTime;
    private Date payStartTime;

    //结束查询的缴费时间
    private Long endTime;
    private Date payEndTime;

    //停车场列表
    List<ParkInfo> parkInfos;

    public List<ParkInfo> getParkInfos() {
        return parkInfos;
    }

    public void setParkInfos(List<ParkInfo> parkInfos) {
        this.parkInfos = parkInfos;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getParkCode() {
        return parkCode;
    }

    public void setParkCode(String parkCode) {
        this.parkCode = parkCode;
    }

    public String getPlateNo() {
        return plateNo;
    }

    public void setPlateNo(String plateNo) {
        this.plateNo = plateNo;
    }

    public Integer getPlateColor() {
        return plateColor;
    }

    public void setPlateColor(Integer plateColor) {
        this.plateColor = plateColor;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Date getPayStartTime() {
        return payStartTime;
    }

    public void setPayStartTime(Date payStartTime) {
        this.payStartTime = payStartTime;
    }

    public Date getPayEndTime() {
        return payEndTime;
    }

    public void setPayEndTime(Date payEndTime) {
        this.payEndTime = payEndTime;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }
}