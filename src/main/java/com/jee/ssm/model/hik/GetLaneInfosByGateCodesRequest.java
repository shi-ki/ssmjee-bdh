package com.jee.ssm.model.hik;


public class GetLaneInfosByGateCodesRequest extends BaseRequest{
    // 出入口编号
    private String gateCodes;

    public String getGateCodes() {
        return gateCodes;
    }

    public void setGateCodes(String gateCodes) {
        this.gateCodes = gateCodes;
    }
}
