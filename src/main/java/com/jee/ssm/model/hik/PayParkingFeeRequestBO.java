package com.jee.ssm.model.hik;


public class PayParkingFeeRequestBO extends BaseRequest{
    //账单编号
    private String billNo;
    //支付时间单位：毫秒
    private Long payTime;
    //支付金额(分)
    private Integer payAmount;
    //支付方式
    private String payType;

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public Long getPayTime() {
        return payTime;
    }

    public void setPayTime(Long payTime) {
        this.payTime = payTime;
    }

    public Integer getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(Integer payAmount) {
        this.payAmount = payAmount;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("PayParkingFeeRequestBO{");
        sb.append("billNo='").append(billNo).append('\'');
        sb.append(", payTime=").append(payTime);
        sb.append(", payAmount='").append(payAmount).append('\'');
        sb.append(", payType='").append(payType).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
