package com.jee.ssm.model.hik.receive;

public class ParkLeftPlot {

	private String parkCode;
	
	private String parkName;
	
	private String totalPlot;
	
	private String leftPlot;

	public String getParkCode() {
		return parkCode;
	}

	public void setParkCode(String parkCode) {
		this.parkCode = parkCode;
	}

	public String getParkName() {
		return parkName;
	}

	public void setParkName(String parkName) {
		this.parkName = parkName;
	}

	public String getTotalPlot() {
		return totalPlot;
	}

	public void setTotalPlot(String totalPlot) {
		this.totalPlot = totalPlot;
	}

	public String getLeftPlot() {
		return leftPlot;
	}

	public void setLeftPlot(String leftPlot) {
		this.leftPlot = leftPlot;
	}
	
}
