package com.jee.ssm.model.hik;


public class GetParkingPaymentInfoRequestBO extends BaseRequest{
    //车牌号码
    private String plateNo;
    //车牌颜色
    private Integer plateColor;

    public String getPlateNo() {
        return plateNo;
    }

    public void setPlateNo(String plateNo) {
        this.plateNo = plateNo;
    }

    public Integer getPlateColor() {
        return plateColor;
    }

    public void setPlateColor(Integer plateColor) {
        this.plateColor = plateColor;
    }

}
