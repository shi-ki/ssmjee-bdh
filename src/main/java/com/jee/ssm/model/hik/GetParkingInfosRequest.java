package com.jee.ssm.model.hik;


public class GetParkingInfosRequest extends BaseRequest {
    // 当前页码
    private Integer pageNo;
    // 每页数据记录数
    private Integer pageSize;

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

}
