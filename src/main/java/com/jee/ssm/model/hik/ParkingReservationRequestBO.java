package com.jee.ssm.model.hik;


public class ParkingReservationRequestBO extends BaseRequest{
    //停车场编号
    private String parkCode;
    //车牌号码
    private String plateNo;
    //车牌颜色
    private Integer plateColor;
    //支付金额(分)
    private Integer payAmount;
    //预订开始时间
    private String payTime;
    //预订时长
    private Integer bookDuration;
    //联系人电话
    private String Telephone;
    //车位号 如果不为空，表示预约到指定车位
    private String plotNum;

    public String getParkCode() {
        return parkCode;
    }

    public void setParkCode(String parkCode) {
        this.parkCode = parkCode;
    }

    public String getPlateNo() {
        return plateNo;
    }

    public void setPlateNo(String plateNo) {
        this.plateNo = plateNo;
    }

    public Integer getPlateColor() {
        return plateColor;
    }

    public void setPlateColor(Integer plateColor) {
        this.plateColor = plateColor;
    }

    public Integer getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(Integer payAmount) {
        this.payAmount = payAmount;
    }

    public String getPayTime() {
        return payTime;
    }

    public void setPayTime(String payTime) {
        this.payTime = payTime;
    }

    public Integer getBookDuration() {
        return bookDuration;
    }

    public void setBookDuration(Integer bookDuration) {
        this.bookDuration = bookDuration;
    }

    public String getTelephone() {
        return Telephone;
    }

    public void setTelephone(String telephone) {
        Telephone = telephone;
    }

    public String getPlotNum() {
        return plotNum;
    }

    public void setPlotNum(String plotNum) {
        this.plotNum = plotNum;
    }
}
