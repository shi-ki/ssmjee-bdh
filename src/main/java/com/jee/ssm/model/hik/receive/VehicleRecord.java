package com.jee.ssm.model.hik.receive;

public class VehicleRecord {
	/**
	 * 过车记录id
	 */
	private String unid;
	/**
	 * 车牌号
	 */
	private String plateNo;
	/**
	 * 车牌颜色
	 */
	private Integer plateColor;
	/**
	 * 通过时间
	 */
	private String passTime;
	/**
	 * 停车场编号
	 */
	private String parkCode;
	/**
	 * 停车场名称
	 */
	private String parkName;
	/**
	 * 出入口编号
	 */
	private String 	gateCode;
	/**
	 * 出入口名称
	 */
	private String gateName;
	/**
	 * 车道编号
	 */
	private String laneNo;
	/**
	 * 车道名称
	 */
	private String laneName;
	/**
	 * 车辆类型
	 */
	private Integer carType;
	/**
	 * 过车方向
	 */
	private Integer direct;
	/**
	 * 泊位编号
	 */
	private Integer berthCode;

	public String getUnid() {
		return unid;
	}

	public void setUnid(String unid) {
		this.unid = unid;
	}

	public String getPlateNo() {
		return plateNo;
	}

	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}

	public Integer getPlateColor() {
		return plateColor;
	}

	public void setPlateColor(Integer plateColor) {
		this.plateColor = plateColor;
	}

	public String getPassTime() {
		return passTime;
	}

	public void setPassTime(String passTime) {
		this.passTime = passTime;
	}

	public String getParkCode() {
		return parkCode;
	}

	public void setParkCode(String parkCode) {
		this.parkCode = parkCode;
	}

	public String getParkName() {
		return parkName;
	}

	public void setParkName(String parkName) {
		this.parkName = parkName;
	}

	public String getGateCode() {
		return gateCode;
	}

	public void setGateCode(String gateCode) {
		this.gateCode = gateCode;
	}

	public String getGateName() {
		return gateName;
	}

	public void setGateName(String gateName) {
		this.gateName = gateName;
	}

	public String getLaneNo() {
		return laneNo;
	}

	public void setLaneNo(String laneNo) {
		this.laneNo = laneNo;
	}

	public String getLaneName() {
		return laneName;
	}

	public void setLaneName(String laneName) {
		this.laneName = laneName;
	}

	public Integer getCarType() {
		return carType;
	}

	public void setCarType(Integer carType) {
		this.carType = carType;
	}

	public Integer getDirect() {
		return direct;
	}

	public void setDirect(Integer direct) {
		this.direct = direct;
	}

	public Integer getBerthCode() {
		return berthCode;
	}

	public void setBerthCode(Integer berthCode) {
		this.berthCode = berthCode;
	}

	@Override
	public String toString() {
		return "VehicleRecord{" +
				"unid='" + unid + '\'' +
				", plateNo='" + plateNo + '\'' +
				", plateColor=" + plateColor +
				", passTime='" + passTime + '\'' +
				", parkCode='" + parkCode + '\'' +
				", parkName='" + parkName + '\'' +
				", gateCode='" + gateCode + '\'' +
				", gateName='" + gateName + '\'' +
				", laneNo='" + laneNo + '\'' +
				", laneName='" + laneName + '\'' +
				", carType=" + carType +
				", direct=" + direct +
				", berthCode=" + berthCode +
				'}';
	}
}
