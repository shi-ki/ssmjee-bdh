package com.jee.ssm.model.hik.receive;

import com.jee.ssm.model.ParkDepot;

import java.math.BigDecimal;

public class ParkDepotInfo {

    private String parkCode;

    private String parkName;

    private Integer parkType;

    private BigDecimal parkLatitude;

    private BigDecimal parkLongitude;

    private Integer totalPlot;

    private Integer leftPlot;

    private Integer parkLevel;

    private String description;

    private Integer totalFixedPlot;

    private Integer leftFixedPlot;

    private Integer leftFiexdPlot;

    private ParkDepot parkDepot;

    public String getParkCode() {
        return parkCode;
    }

    public void setParkCode(String parkCode) {
        this.parkCode = parkCode;
    }

    public String getParkName() {
        return parkName;
    }

    public void setParkName(String parkName) {
        this.parkName = parkName;
    }

    public Integer getParkType() {
        return parkType;
    }

    public void setParkType(Integer parkType) {
        this.parkType = parkType;
    }

    public BigDecimal getParkLatitude() {
        return parkLatitude;
    }

    public void setParkLatitude(BigDecimal parkLatitude) {
        this.parkLatitude = parkLatitude;
    }

    public BigDecimal getParkLongitude() {
        return parkLongitude;
    }

    public void setParkLongitude(BigDecimal parkLongitude) {
        this.parkLongitude = parkLongitude;
    }

    public Integer getTotalPlot() {
        return totalPlot;
    }

    public void setTotalPlot(Integer totalPlot) {
        this.totalPlot = totalPlot;
    }

    public Integer getLeftPlot() {
        return leftPlot;
    }

    public void setLeftPlot(Integer leftPlot) {
        this.leftPlot = leftPlot;
    }

    public Integer getParkLevel() {
        return parkLevel;
    }

    public void setParkLevel(Integer parkLevel) {
        this.parkLevel = parkLevel;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getTotalFixedPlot() {
        return totalFixedPlot;
    }

    public void setTotalFixedPlot(Integer totalFixedPlot) {
        this.totalFixedPlot = totalFixedPlot;
    }

    public Integer getLeftFixedPlot() {
        return leftFixedPlot;
    }

    public void setLeftFixedPlot(Integer leftFixedPlot) {
        this.leftFixedPlot = leftFixedPlot;
    }

    public Integer getLeftFiexdPlot() {
        return leftFiexdPlot;
    }

    public void setLeftFiexdPlot(Integer leftFiexdPlot) {
        this.leftFiexdPlot = leftFiexdPlot;
    }

    public ParkDepot getParkDepot() {
        return parkDepot;
    }

    public void setParkDepot(ParkDepot parkDepot) {
        this.parkDepot = parkDepot;
    }

}
