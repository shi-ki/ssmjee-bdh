package com.jee.ssm.model.hik;

public class GetPassPicByUuidRequest extends BaseRequest{
    /**
     * 过车记录id
     */
    private String unid;
    /**
     * 停车场编号
     */
    private String parkCode;
    public String getUnid() {
        return unid;
    }

    public void setUnid(String unid) {
        this.unid = unid;
    }

    public String getParkCode() {
        return parkCode;
    }

    public void setParkCode(String parkCode) {
        this.parkCode = parkCode;
    }
}
