package com.jee.ssm.model.hik;

import java.util.Date;
import java.util.List;


public class GetVehicleRecordsRequest extends BaseRequest{

    // 页码
    private Integer pageNo;

    // 每页记录数
    private Integer pageSize;

    // 分页始
    private Integer offset;

    // 停车场编号
    private String parkCode;

    // 出入口编号
    private String gateCode;

    // 车牌号
    private String plateNo;

    // 过车方向
    private Integer direct;

    // 车辆类型
    private Integer carType;

    // 查询开始的过车时间
    private Long startTime;
    private Date passStartTime;

    // 查询结束的过车时间
    private Long endTime;
    private Date passEndTime;

    //停车场列表
    List<ParkInfo> parkInfos;

    public List<ParkInfo> getParkInfos() {
        return parkInfos;
    }

    public void setParkInfos(List<ParkInfo> parkInfos) {
        this.parkInfos = parkInfos;
    }

    public String getParkCode() {
        return parkCode;
    }

    public void setParkCode(String parkCode) {
        this.parkCode = parkCode;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public String getGateCode() {
        return gateCode;
    }

    public void setGateCode(String gateCode) {
        this.gateCode = gateCode;
    }

    public String getPlateNo() {
        return plateNo;
    }

    public void setPlateNo(String plateNo) {
        this.plateNo = plateNo;
    }

    public Integer getDirect() {
        return direct;
    }

    public void setDirect(Integer direct) {
        this.direct = direct;
    }

    public Integer getCarType() {
        return carType;
    }

    public void setCarType(Integer carType) {
        this.carType = carType;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Date getPassStartTime() {
        return passStartTime;
    }

    public void setPassStartTime(Date passStartTime) {
        this.passStartTime = passStartTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Date getPassEndTime() {
        return passEndTime;
    }

    public void setPassEndTime(Date passEndTime) {
        this.passEndTime = passEndTime;
    }
}