package com.jee.ssm.model;

import com.jee.ssm.model.base.BaseModel;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * 表名 b_reservation_record
 * @author GaoXiang
 * @version 1.0
 * Create time 2018/06/24
 */
public class ReservationRecord extends BaseModel {

	private static final long serialVersionUID = -6147434120564550603L;

	/**
     * 标识
     * 表字段 : b_reservation_record.id
     * Create time 2018/06/24
     */
    private String id;

    /**
     * 编号
     * 表字段 : b_reservation_record.number
     * Create time 2018/06/24
     */
    private String number;

    /**
     * 添加时间
     * 表字段 : b_reservation_record.create_time
     * Create time 2018/06/24
     */
    private Date createTime;

    /**
     * 用户id
     * 表字段 : b_reservation_record.user_id
     * Create time 2018/06/24
     */
    private String userId;

    /**
     * 用户手机号码
     * 表字段 : b_reservation_record.user_phone
     * Create time 2018/06/24
     */
    private String userPhone;

    /**
     * 用户名
     * 表字段 : b_reservation_record.user_name
     * Create time 2018/06/24
     */
    private String userName;

    /**
     * 姓名
     * 表字段 : b_reservation_record.name
     * Create time 2018/06/24
     */
    private String name;

    /**
     * 身份证号
     * 表字段 : b_reservation_record.id_number
     * Create time 2018/06/24
     */
    private String idNumber;
    
    /**
     * 预约单价
     * 表字段 : b_reservation_record.order_fee
     * Create time 2018/09/14
     */
    private BigDecimal orderFee;
    
    /**
     * 预订时长
     * 表字段 : b_reservation_record.book_duration
     * Create time 2018/09/14
     */
    private Integer bookDuration;

    /**
     * 预约费用
     * 表字段 : b_reservation_record.book_fee
     * Create time 2018/06/24
     */
    private String bookFee;

    /**
     * 停车场名称
     * 表字段 : b_reservation_record.park_name
     * Create time 2018/06/24
     */
    private String parkName;

    /**
     * 停车场编号
     * 表字段 : b_reservation_record.park_code
     * Create time 2018/06/24
     */
    private String parkCode;

    /**
     * 车牌号
     * 表字段 : b_reservation_record.plate_number
     * Create time 2018/06/24
     */
    private String plateNumber;

    /**
     * 车牌颜色
     * 表字段 : b_reservation_record.plate_colour
     * Create time 2018/06/24
     */
    private Integer plateColour;
    
    /**
     * 预约开始时间
     * 表字段 : b_reservation_record.payTime
     * Create time 2018/09/14
     */
    private Date payTime;

    /**
     * 停车时间
     * 表字段 : b_reservation_record.park_time
     * Create time 2018/06/24
     */
    private Date parkTime;

    /**
     * 联系电话
     * 表字段 : b_reservation_record.phone
     * Create time 2018/06/24
     */
    private String phone;

    /**
     * 车位号
     * 表字段 : b_reservation_record.plot_number
     * Create time 2018/06/24
     */
    private String plotNumber;
    
    /**
     * 预约状态
     * 表字段 : b_reservation_record.status
     * Create time 2018/09/14
     */
    private Integer status;

    /**
     * 关联他表id（预留）
     * 表字段 : b_reservation_record.relate_id
     * Create time 2018/09/14
     */
    private String relateId;

    /**
     * 构造方法
     */
    public ReservationRecord(String id, String number, Date createTime, String userId, String userPhone, String userName, String name, String idNumber, BigDecimal orderFee, Integer bookDuration, String bookFee, String parkName, String parkCode, String plateNumber, Integer plateColour, Date payTime, Date parkTime, String phone, String plotNumber, Integer status, String relateId) {
        this.id = id;
        this.number = number;
        this.createTime = createTime;
        this.userId = userId;
        this.userPhone = userPhone;
        this.userName = userName;
        this.name = name;
        this.idNumber = idNumber;
        this.orderFee = orderFee;
        this.bookDuration = bookDuration;
        this.bookFee = bookFee;
        this.parkName = parkName;
        this.parkCode = parkCode;
        this.plateNumber = plateNumber;
        this.plateColour = plateColour;
        this.payTime = payTime;
        this.parkTime = parkTime;
        this.phone = phone;
        this.plotNumber = plotNumber;
        this.status = status;
        this.relateId = relateId;
    }
    
    public ReservationRecord(BigDecimal orderFee,int bookDuration, String bookFee, Date payTime, Date parkTime) {
        super();
        this.orderFee = orderFee;
        this.bookDuration = bookDuration;
        this.bookFee = bookFee;
        this.payTime = payTime;
        this.parkTime = parkTime;
    }

    public ReservationRecord(String userId, Integer status) {
        super();
        this.userId = userId;
        this.status = status;
    }
    
    public ReservationRecord(Integer status) {
        super();
        this.status = status;
    }

    /**
     * 构造方法
     */
    public ReservationRecord() {
        super();
    }

    /**
     * 标识
     * @return id 标识
     */
    public String getId() {
        return id;
    }

    /**
     * 标识
     * @param id 标识
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 编号
     * @return number 编号
     */
    public String getNumber() {
        return number;
    }

    /**
     * 编号
     * @param number 编号
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * 添加时间
     * @return create_time 添加时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 添加时间
     * @param createTime 添加时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 用户id
     * @return user_id 用户id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 用户id
     * @param userId 用户id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 用户手机号码
     * @return user_phone 用户手机号码
     */
    public String getUserPhone() {
        return userPhone;
    }

    /**
     * 用户手机号码
     * @param userPhone 用户手机号码
     */
    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    /**
     * 用户名
     * @return user_name 用户名
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 用户名
     * @param userName 用户名
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 姓名
     * @return name 姓名
     */
    public String getName() {
        return name;
    }

    /**
     * 姓名
     * @param name 姓名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 身份证号
     * @return id_number 身份证号
     */
    public String getIdNumber() {
        return idNumber;
    }

    /**
     * 身份证号
     * @param idNumber 身份证号
     */
    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }
    
    /**
     * 预约单价
     * @return order_fee 预约单价
     */
    public BigDecimal getOrderFee() {
        return orderFee;
    }

    /**
     * 预约单价
     * @param orderFee 预约单价
     */
    public void setOrderFee(BigDecimal orderFee) {
        this.orderFee = orderFee;
    }
    
    /**
     * 预订时长
     * @return book_duration 预订时长
     */
    public Integer getBookDuration() {
        return bookDuration;
    }

    /**
     * 预订时长
     * @param bookDuration 预订时长
     */
    public void setBookDuration(Integer bookDuration) {
        this.bookDuration = bookDuration;
    }

    /**
     * 预约费用
     * @return book_fee 预约费用
     */
    public String getBookFee() {
        return bookFee;
    }

    /**
     * 预约费用
     * @param bookFee 预约费用
     */
    public void setBookFee(String bookFee) {
        this.bookFee = bookFee;
    }

    /**
     * 停车场名称
     * @return park_name 停车场名称
     */
    public String getParkName() {
        return parkName;
    }

    /**
     * 停车场名称
     * @param parkName 停车场名称
     */
    public void setParkName(String parkName) {
        this.parkName = parkName;
    }

    /**
     * 停车场编号
     * @return park_code 停车场编号
     */
    public String getParkCode() {
        return parkCode;
    }

    /**
     * 停车场编号
     * @param parkCode 停车场编号
     */
    public void setParkCode(String parkCode) {
        this.parkCode = parkCode;
    }

    /**
     * 车牌号
     * @return plate_number 车牌号
     */
    public String getPlateNumber() {
        return plateNumber;
    }

    /**
     * 车牌号
     * @param plateNumber 车牌号
     */
    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    /**
     * 车牌颜色
     * @return plate_colour 车牌颜色
     */
    public Integer getPlateColour() {
        return plateColour;
    }

    /**
     * 车牌颜色
     * @param plateColour 车牌颜色
     */
    public void setPlateColour(Integer plateColour) {
        this.plateColour = plateColour;
    }
    
    /**
     * 预约开始时间
     * @return pay_time 预约开始时间
     */
    public Date getPayTime() {
        return payTime;
    }

    /**
     * 预约开始时间
     * @param payTime 预约开始时间
     */
    public void setPaytime(Date payTime) {
        this.payTime = payTime;
    }

    /**
     * 停车时间
     * @return park_time 停车时间
     */
    public Date getParkTime() {
        return parkTime;
    }

    /**
     * 停车时间
     * @param parkTime 停车时间
     */
    public void setParkTime(Date parkTime) {
        this.parkTime = parkTime;
    }

    /**
     * 联系电话
     * @return phone 联系电话
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 联系电话
     * @param phone 联系电话
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 车位号
     * @return plot_number 车位号
     */
    public String getPlotNumber() {
        return plotNumber;
    }

    /**
     * 车位号
     * @param plotNumber 车位号
     */
    public void setPlotNumber(String plotNumber) {
        this.plotNumber = plotNumber;
    }
    
    /**
     * 预约状态
     * @return status 预约状态
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 预约状态
     * @param status 预约状态
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 关联他表id（预留）
     * @return relate_id 关联他表id（预留）
     */
    public String getRelateId() {
        return relateId;
    }

    /**
     * 关联他表id（预留）
     * @param relateId 关联他表id（预留）
     */
    public void setRelateId(String relateId) {
        this.relateId = relateId;
    }
    
}