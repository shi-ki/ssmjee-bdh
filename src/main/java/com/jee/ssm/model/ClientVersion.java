package com.jee.ssm.model;

import com.jee.ssm.model.base.BaseModel;

import javax.mail.Multipart;
import java.util.Date;

/**
 * 
 * 表名 b_client_version
 * @author GaoXiang
 * @version 1.0
 * Create time 2018/06/23
 */
public class ClientVersion extends BaseModel {
    /**
     * 
     * 表字段 : b_client_version.id
     * Create time 2018/06/23
     */
    private String id;

    /**
     *  版本号
     * 表字段 : b_client_version.version_code
     * Create time 2018/06/23
     */
    private String versionCode;

    /**
     * 0 app版本
     * 表字段 : b_client_version.type
     * Create time 2018/06/23
     */
    private Integer type;

    /**
     * 0正常
     * 表字段 : b_client_version.status
     * Create time 2018/06/23
     */
    private Integer status;

    /**
     * 
     * 表字段 : b_client_version.create_time
     * Create time 2018/06/23
     */
    private Date createTime;

    /**
     * 
     * 表字段 : b_client_version.url
     * Create time 2018/06/23
     */
    private String url;

    /**
     * 
     * 表字段 : b_client_version.info
     * Create time 2018/06/23
     */
    private String intro;

    private Multipart image;
    private String fileName;
    private String downloadPath;

    /**
     * 构造方法
     */

    public ClientVersion(String id, String versionCode, Integer type, Integer status, Date createTime, String url, String info) {
        this.id = id;
        this.versionCode = versionCode;
        this.type = type;
        this.status = status;
        this.createTime = createTime;
        this.url = url;
        this.intro = info;
    }

    /**
     * 构造方法
     */
    public ClientVersion() {
        super();
    }

    /**
     * 
     * @return id 
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *  版本号
     * @return version_code  版本号
     */
    public String getVersionCode() {
        return versionCode;
    }

    /**
     *  版本号
     * @param versionCode  版本号
     */
    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }

    /**
     * 0 app版本
     * @return type 0 app版本
     */
    public Integer getType() {
        return type;
    }

    /**
     * 0 app版本
     * @param type 0 app版本
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 0正常
     * @return status 0正常
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 0正常
     * @param status 0正常
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 
     * @return create_time 
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 
     * @param createTime 
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 
     * @return url 
     */
    public String getUrl() {
        return url;
    }

    /**
     * 
     * @param url 
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 
     * @return info 
     */
    public String getIntro() {
        return intro;
    }

    /**
     * 
     * @param intro
     */
    public void setIntro(String intro) {
        this.intro = intro;
    }

    public Multipart getImage() {
        return image;
    }

    public void setImage(Multipart image) {
        this.image = image;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDownloadPath() {
        return downloadPath;
    }

    public void setDownloadPath(String downloadPath) {
        this.downloadPath = downloadPath;
    }
}