package com.jee.ssm.model;

import com.jee.ssm.model.base.BaseModel;
import java.util.Date;

/**
 * 
 * 表名 b_screen_image
 * @author GaoXiang
 * @version 1.0
 * Create time 2018/07/08
 */
public class ScreenImage extends BaseModel {

	private static final long serialVersionUID = -3732097895822650475L;

	/**
     * 标识
     * 表字段 : b_screen_image.id
     * Create time 2018/07/08
     */
    private String id;

    /**
     * 
     * 表字段 : b_screen_image.number
     * Create time 2018/07/08
     */
    private Integer number;

    /**
     * 文件路径
     * 表字段 : b_screen_image.file_url
     * Create time 2018/07/08
     */
    private String fileUrl;

    /**
     * 文件名
     * 表字段 : b_screen_image.file_name
     * Create time 2018/07/08
     */
    private String fileName;

    /**
     * 图片名
     * 表字段 : b_screen_image.image_name
     * Create time 2018/07/08
     */
    private String imageName;

    /**
     * 纯文字
     * 表字段 : b_screen_image.pure_word
     * Create time 2018/07/08
     */
    private String pureWord;

    /**
     * 状态（0：未使用，1：当前使用）
     * 表字段 : b_screen_image.status
     * Create time 2018/07/08
     */
    private Integer status;

    /**
     * 类型（0：文字，1：图片）
     * 表字段 : b_screen_image.type
     * Create time 2018/07/08
     */
    private Integer type;

    /**
     * 添加时间
     * 表字段 : b_screen_image.create_time
     * Create time 2018/07/08
     */
    private Date createTime;

    /**
     * 诱导屏编号
     * 表字段 : b_screen_image.screen_number
     * Create time 2018/07/08
     */
    private String screenNumber;

    /**
     * 诱导屏id
     * 表字段 : b_screen_image.screen_id
     * Create time 2018/07/08
     */
    private String screenId;

    /**
     * 构造方法
     */
    public ScreenImage(String id, Integer number, String fileUrl, String fileName, String imageName, String pureWord, Integer status, Integer type, Date createTime, String screenNumber, String screenId) {
        this.id = id;
        this.number = number;
        this.fileUrl = fileUrl;
        this.fileName = fileName;
        this.imageName = imageName;
        this.pureWord = pureWord;
        this.status = status;
        this.type = type;
        this.createTime = createTime;
        this.screenNumber = screenNumber;
        this.screenId = screenId;
    }

    /**
     * 构造方法
     */
    public ScreenImage() {
        super();
    }

    /**
     * 标识
     * @return id 标识
     */
    public String getId() {
        return id;
    }

    /**
     * 标识
     * @param id 标识
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return number 
     */
    public Integer getNumber() {
        return number;
    }

    /**
     * 
     * @param number 
     */
    public void setNumber(Integer number) {
        this.number = number;
    }

    /**
     * 文件路径
     * @return file_url 文件路径
     */
    public String getFileUrl() {
        return fileUrl;
    }

    /**
     * 文件路径
     * @param fileUrl 文件路径
     */
    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    /**
     * 文件名
     * @return file_name 文件名
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * 文件名
     * @param fileName 文件名
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * 图片名
     * @return image_name 图片名
     */
    public String getImageName() {
        return imageName;
    }

    /**
     * 图片名
     * @param imageName 图片名
     */
    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    /**
     * 纯文字
     * @return pure_word 纯文字
     */
    public String getPureWord() {
        return pureWord;
    }

    /**
     * 纯文字
     * @param pureWord 纯文字
     */
    public void setPureWord(String pureWord) {
        this.pureWord = pureWord;
    }

    /**
     * 状态（0：未使用，1：当前使用）
     * @return status 状态（0：未使用，1：当前使用）
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 状态（0：未使用，1：当前使用）
     * @param status 状态（0：未使用，1：当前使用）
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 类型（0：文字，1：图片）
     * @return type 类型（0：文字，1：图片）
     */
    public Integer getType() {
        return type;
    }

    /**
     * 类型（0：文字，1：图片）
     * @param type 类型（0：文字，1：图片）
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 添加时间
     * @return create_time 添加时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 添加时间
     * @param createTime 添加时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 诱导屏编号
     * @return screen_number 诱导屏编号
     */
    public String getScreenNumber() {
        return screenNumber;
    }

    /**
     * 诱导屏编号
     * @param screenNumber 诱导屏编号
     */
    public void setScreenNumber(String screenNumber) {
        this.screenNumber = screenNumber;
    }

    /**
     * 诱导屏id
     * @return screen_id 诱导屏id
     */
    public String getScreenId() {
        return screenId;
    }

    /**
     * 诱导屏id
     * @param screenId 诱导屏id
     */
    public void setScreenId(String screenId) {
        this.screenId = screenId;
    }
}