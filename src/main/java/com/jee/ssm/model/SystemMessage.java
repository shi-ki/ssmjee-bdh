package com.jee.ssm.model;

import com.jee.ssm.model.base.BaseModel;
import java.util.Date;

/**
 * 
 * 表名 b_system_message
 * @author GaoXiang
 * @version 1.0
 * Create time 2018/06/24
 */
public class SystemMessage extends BaseModel {

	private static final long serialVersionUID = -3897331831202607606L;

	/**
     * 标识
     * 表字段 : b_system_message.id
     * Create time 2018/06/24
     */
    private String id;

    /**
     * 创建时间
     * 表字段 : b_system_message.create_time
     * Create time 2018/06/24
     */
    private Date createTime;

    /**
     * 标题
     * 表字段 : b_system_message.title
     * Create time 2018/06/24
     */
    private String title;

    /**
     * 状态（0：默认）
     * 表字段 : b_system_message.status
     * Create time 2018/06/24
     */
    private Integer status;

    /**
     * 类型（0：后台发布）
     * 表字段 : b_system_message.type
     * Create time 2018/06/24
     */
    private Integer type;

    /**
     * 发布用户id
     * 表字段 : b_system_message.user_id
     * Create time 2018/06/24
     */
    private String userId;

    /**
     * 发布用户名
     * 表字段 : b_system_message.user_name
     * Create time 2018/06/24
     */
    private String userName;

    /**
     * 内容
     * 表字段 : b_system_message.content
     * Create time 2018/06/24
     */
    private String content;

    /**
     * 构造方法
     */
    public SystemMessage(String id, Date createTime, String title, Integer status, Integer type, String userId, String userName, String content) {
        this.id = id;
        this.createTime = createTime;
        this.title = title;
        this.status = status;
        this.type = type;
        this.userId = userId;
        this.userName = userName;
        this.content = content;
    }

    /**
     * 构造方法
     */
    public SystemMessage() {
        super();
    }

    /**
     * 标识
     * @return id 标识
     */
    public String getId() {
        return id;
    }

    /**
     * 标识
     * @param id 标识
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 创建时间
     * @return create_time 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 创建时间
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 标题
     * @return title 标题
     */
    public String getTitle() {
        return title;
    }

    /**
     * 标题
     * @param title 标题
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 状态（0：默认）
     * @return status 状态（0：默认）
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 状态（0：默认）
     * @param status 状态（0：默认）
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 类型（0：后台发布）
     * @return type 类型（0：后台发布）
     */
    public Integer getType() {
        return type;
    }

    /**
     * 类型（0：后台发布）
     * @param type 类型（0：后台发布）
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 发布用户id
     * @return user_id 发布用户id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 发布用户id
     * @param userId 发布用户id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 发布用户名
     * @return user_name 发布用户名
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 发布用户名
     * @param userName 发布用户名
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 内容
     * @return content 内容
     */
    public String getContent() {
        return content;
    }

    /**
     * 内容
     * @param content 内容
     */
    public void setContent(String content) {
        this.content = content;
    }
}