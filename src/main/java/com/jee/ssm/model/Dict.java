package com.jee.ssm.model;

import com.jee.ssm.model.base.BaseModel;

/**
 * 
 * 表名 t_dict
 * @author GaoXiang
 * @version 1.0
 * Create time 2018/04/23
 */
public class Dict extends BaseModel {
    /**
     * 
     * 表字段 : t_dict.id
     * Create time 2018/04/23
     */
    private String id;

    /**
     * 名称
     * 表字段 : t_dict.name
     * Create time 2018/04/23
     */
    private String name;

    /**
     * 键值
     * 表字段 : t_dict.key_wod
     * Create time 2018/04/23
     */
    private String keyWod;

    /**
     * 类型
     * 表字段 : t_dict.type
     * Create time 2018/04/23
     */
    private String type;

    /**
     * 
     * 表字段 : t_dict.status
     * Create time 2018/04/23
     */
    private Integer status;

    /**
     * 排序
     * 表字段 : t_dict.sort
     * Create time 2018/04/23
     */
    private Integer sort;

    /**
     * 备注
     * 表字段 : t_dict.intro
     * Create time 2018/04/23
     */
    private String intro;

    /**
     * 
     * 表字段 : t_dict.parent_id
     * Create time 2018/04/23
     */
    private String parentId;

    /**
     * 构造方法
     */
    public Dict(String id, String name, String keyWod, String type, Integer status, Integer sort, String intro, String parentId) {
        this.id = id;
        this.name = name;
        this.keyWod = keyWod;
        this.type = type;
        this.status = status;
        this.sort = sort;
        this.intro = intro;
        this.parentId = parentId;
    }

    /**
     * 构造方法
     */
    public Dict() {
        super();
    }

    /**
     * 
     * @return id 
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 名称
     * @return name 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 名称
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 键值
     * @return key_wod 键值
     */
    public String getKeyWod() {
        return keyWod;
    }

    /**
     * 键值
     * @param keyWod 键值
     */
    public void setKeyWod(String keyWod) {
        this.keyWod = keyWod;
    }

    /**
     * 类型
     * @return type 类型
     */
    public String getType() {
        return type;
    }

    /**
     * 类型
     * @param type 类型
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return status 
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 
     * @param status 
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 排序
     * @return sort 排序
     */
    public Integer getSort() {
        return sort;
    }

    /**
     * 排序
     * @param sort 排序
     */
    public void setSort(Integer sort) {
        this.sort = sort;
    }

    /**
     * 备注
     * @return intro 备注
     */
    public String getIntro() {
        return intro;
    }

    /**
     * 备注
     * @param intro 备注
     */
    public void setIntro(String intro) {
        this.intro = intro;
    }

    /**
     * 
     * @return parent_id 
     */
    public String getParentId() {
        return parentId;
    }

    /**
     * 
     * @param parentId 
     */
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }
}