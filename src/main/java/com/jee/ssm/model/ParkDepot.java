package com.jee.ssm.model;

import com.jee.ssm.model.base.BaseModel;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 
 * 表名 b_park_depot
 * @author GaoXiang
 * @version 1.0
 * Create time 2018/06/28
 */
public class ParkDepot extends BaseModel {

	private static final long serialVersionUID = 7561213349966120157L;

	/**
     * 
     * 表字段 : b_park_depot.id
     * Create time 2018/06/28
     */
    private String id;

    /**
     * 名称
     * 表字段 : b_park_depot.name
     * Create time 2018/06/28
     */
    private String name;

    /**
     * 地图获取地址
     * 表字段 : b_park_depot.map_address
     * Create time 2018/06/28
     */
    private String mapAddress;

    /**
     * 地址描述
     * 表字段 : b_park_depot.address
     * Create time 2018/06/28
     */
    private String address;

    /**
     * 层数
     * 表字段 : b_park_depot.floor_num
     * Create time 2018/06/28
     */
    private Integer floorNum;

    /**
     * 纬度
     * 表字段 : b_park_depot.lat
     * Create time 2018/06/28
     */
    private Double lat;

    /**
     * 经度
     * 表字段 : b_park_depot.lng
     * Create time 2018/06/28
     */
    private Double lng;

    /**
     * 出口数
     * 表字段 : b_park_depot.exit_num
     * Create time 2018/06/28
     */
    private Integer exitNum;

    /**
     * 入口数
     * 表字段 : b_park_depot.entrance_num
     * Create time 2018/06/28
     */
    private Integer entranceNum;

    /**
     * 小车车位数
     * 表字段 : b_park_depot.car_num
     * Create time 2018/06/28
     */
    private Integer carNum;

    /**
     * 小车车位使用数量
     * 表字段 : b_park_depot.car_used_num
     * Create time 2018/06/28
     */
    private Integer carUsedNum;

    /**
     * 大车车位数
     * 表字段 : b_park_depot.trunk_num
     * Create time 2018/06/28
     */
    private Integer trunkNum;

    /**
     * 大车车位使用数量
     * 表字段 : b_park_depot.trunk_used_num
     * Create time 2018/06/28
     */
    private Integer trunkUsedNum;

    /**
     * 计费标准（字典表键值）
     * 表字段 : b_park_depot.fee_scale
     * Create time 2018/06/28
     */
    private Integer feeScale;

    /**
     * 添加时间
     * 表字段 : b_park_depot.create_time
     * Create time 2018/06/28
     */
    private Date createTime;

    /**
     * 上次修改时间
     * 表字段 : b_park_depot.update_time
     * Create time 2018/06/28
     */
    private Date updateTime;

    /**
     * 海康平台停车场编号
     * 表字段 : b_park_depot.hik_id
     * Create time 2018/06/28
     */
    private String hikId;
    
    /**
     * 互通上传状态
     */
    private Integer aiparkStatus;

    /**
     * 互通停车编号
     * 表字段 : b_park_depot.aipark_code
     * Create time 2018/06/28
     */
    private String aiparkCode;

    /**
     * 预约费用（元 /时）
     * 表字段 : b_park_depot.order_fee
     * Create time 2018/06/28
     */
    private BigDecimal orderFee;

    /**
     * 白天停车费用（元 /时）
     * 表字段 : b_park_depot.day_fee
     * Create time 2018/06/28
     */
    private BigDecimal dayFee;

    /**
     * 夜晚停车费用（元 /晚）
     * 表字段 : b_park_depot.night_fee
     * Create time 2018/06/28
     */
    private BigDecimal nightFee;
    
    /**
     * 状态（默认正常：1）
     * 表字段 : b_park_depot.status
     * Create time 2018/09/17
     */
    private Integer status;

    /**
     * 类型（默认：1）
     * 表字段 : b_park_depot.type
     * Create time 2018/09/17
     */
    private Integer type;
    
    /**
     * 停车场类型（1：封闭式场库，2：路边停车场）
     * 表字段 : b_park_depot.park_type
     * Create time 2018/09/17
     */
    private Integer parkType;

    /**
     * 停车点等级
     * 表字段 : b_park_depot.park_level
     * Create time 2018/09/17
     */
    private Integer parkLevel;

    /**
     * 收费规则
     * 表字段 : b_park_depot.charge_rules
     * Create time 2018/06/28
     */
    private String chargeRules;
    
    private Double minlat;
    
    private Double maxlat;
    
    private Double minlng;
    
    private Double maxlng;
    
    private String searchContent;
    
    private Integer totalPlot;
    
    private Integer leftPlot;
    
    private Integer totalFixedPlot;
    
    private Integer leftFixedPlot;

    /**
     * 构造方法
     */
    public ParkDepot(String id, String name, String mapAddress, String address, Integer floorNum, Double lat, Double lng, Integer exitNum, Integer entranceNum, Integer carNum, Integer carUsedNum, Integer trunkNum, Integer trunkUsedNum, Integer feeScale, Date createTime, Date updateTime, String hikId, Integer aiparkStatus, String aiparkCode, BigDecimal orderFee, BigDecimal dayFee, BigDecimal nightFee, Integer status, Integer type, Integer parkType, Integer parkLevel, String chargeRules) {
        this.id = id;
        this.name = name;
        this.mapAddress = mapAddress;
        this.address = address;
        this.floorNum = floorNum;
        this.lat = lat;
        this.lng = lng;
        this.exitNum = exitNum;
        this.entranceNum = entranceNum;
        this.carNum = carNum;
        this.carUsedNum = carUsedNum;
        this.trunkNum = trunkNum;
        this.trunkUsedNum = trunkUsedNum;
        this.feeScale = feeScale;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.hikId = hikId;
        this.aiparkStatus = aiparkStatus;
        this.aiparkCode = aiparkCode;
        this.orderFee = orderFee;
        this.dayFee = dayFee;
        this.nightFee = nightFee;
        this.status = status;
        this.type = type;
        this.parkType = parkType;
        this.parkLevel = parkLevel;
        this.chargeRules = chargeRules;
    }

    /**
     * 构造方法
     */
    public ParkDepot() {
        super();
    }
    
    public ParkDepot(String searchContent) {
        super();
        this.searchContent = searchContent;
    }

    /**
     * 
     * @return id 
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 名称
     * @return name 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 名称
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 地图获取地址
     * @return map_address 地图获取地址
     */
    public String getMapAddress() {
        return mapAddress;
    }

    /**
     * 地图获取地址
     * @param mapAddress 地图获取地址
     */
    public void setMapAddress(String mapAddress) {
        this.mapAddress = mapAddress;
    }

    /**
     * 地址描述
     * @return address 地址描述
     */
    public String getAddress() {
        return address;
    }

    /**
     * 地址描述
     * @param address 地址描述
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 层数
     * @return floor_num 层数
     */
    public Integer getFloorNum() {
        return floorNum;
    }

    /**
     * 层数
     * @param floorNum 层数
     */
    public void setFloorNum(Integer floorNum) {
        this.floorNum = floorNum;
    }

    /**
     * 纬度
     * @return lat 纬度
     */
    public Double getLat() {
        return lat;
    }

    /**
     * 纬度
     * @param lat 纬度
     */
    public void setLat(Double lat) {
        this.lat = lat;
    }

    /**
     * 经度
     * @return lng 经度
     */
    public Double getLng() {
        return lng;
    }

    /**
     * 经度
     * @param lng 经度
     */
    public void setLng(Double lng) {
        this.lng = lng;
    }

    /**
     * 出口数
     * @return exit_num 出口数
     */
    public Integer getExitNum() {
        return exitNum;
    }

    /**
     * 出口数
     * @param exitNum 出口数
     */
    public void setExitNum(Integer exitNum) {
        this.exitNum = exitNum;
    }

    /**
     * 入口数
     * @return entrance_num 入口数
     */
    public Integer getEntranceNum() {
        return entranceNum;
    }

    /**
     * 入口数
     * @param entranceNum 入口数
     */
    public void setEntranceNum(Integer entranceNum) {
        this.entranceNum = entranceNum;
    }

    /**
     * 小车车位数
     * @return car_num 小车车位数
     */
    public Integer getCarNum() {
        return carNum;
    }

    /**
     * 小车车位数
     * @param carNum 小车车位数
     */
    public void setCarNum(Integer carNum) {
        this.carNum = carNum;
    }

    /**
     * 小车车位使用数量
     * @return car_used_num 小车车位使用数量
     */
    public Integer getCarUsedNum() {
        return carUsedNum;
    }

    /**
     * 小车车位使用数量
     * @param carUsedNum 小车车位使用数量
     */
    public void setCarUsedNum(Integer carUsedNum) {
        this.carUsedNum = carUsedNum;
    }

    /**
     * 大车车位数
     * @return trunk_num 大车车位数
     */
    public Integer getTrunkNum() {
        return trunkNum;
    }

    /**
     * 大车车位数
     * @param trunkNum 大车车位数
     */
    public void setTrunkNum(Integer trunkNum) {
        this.trunkNum = trunkNum;
    }

    /**
     * 大车车位使用数量
     * @return trunk_used_num 大车车位使用数量
     */
    public Integer getTrunkUsedNum() {
        return trunkUsedNum;
    }

    /**
     * 大车车位使用数量
     * @param trunkUsedNum 大车车位使用数量
     */
    public void setTrunkUsedNum(Integer trunkUsedNum) {
        this.trunkUsedNum = trunkUsedNum;
    }

    /**
     * 计费标准（字典表键值）
     * @return fee_scale 计费标准（字典表键值）
     */
    public Integer getFeeScale() {
        return feeScale;
    }

    /**
     * 计费标准（字典表键值）
     * @param feeScale 计费标准（字典表键值）
     */
    public void setFeeScale(Integer feeScale) {
        this.feeScale = feeScale;
    }

    /**
     * 添加时间
     * @return create_time 添加时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 添加时间
     * @param createTime 添加时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 上次修改时间
     * @return update_time 上次修改时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 上次修改时间
     * @param updateTime 上次修改时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 海康平台停车场编号
     * @return hik_id 海康平台停车场编号
     */
    public String getHikId() {
        return hikId;
    }

    /**
     * 海康平台停车场编号
     * @param hikId 海康平台停车场编号
     */
    public void setHikId(String hikId) {
        this.hikId = hikId;
    }

    public Integer getAiparkStatus() {
		return aiparkStatus;
	}

	public void setAiparkStatus(Integer aiparkStatus) {
		this.aiparkStatus = aiparkStatus;
	}

	/**
     * 互通停车编号
     * @return aipark_code 互通停车编号
     */
    public String getAiparkCode() {
		return aiparkCode;
	}

    /**
     * 互通停车编号
     * @param aipark_code 互通停车编号
     */
	public void setAiparkCode(String aiparkCode) {
		this.aiparkCode = aiparkCode;
	}

    /**
     * 预约费用（元 /时）
     * @return order_fee 预约费用（元 /时）
     */
    public BigDecimal getOrderFee() {
        return orderFee;
    }

    /**
     * 预约费用（元 /时）
     * @param orderFee 预约费用（元 /时）
     */
    public void setOrderFee(BigDecimal orderFee) {
        this.orderFee = orderFee;
    }

    /**
     * 白天停车费用（元 /时）
     * @return day_fee 白天停车费用（元 /时）
     */
    public BigDecimal getDayFee() {
        return dayFee;
    }

    /**
     * 白天停车费用（元 /时）
     * @param dayFee 白天停车费用（元 /时）
     */
    public void setDayFee(BigDecimal dayFee) {
        this.dayFee = dayFee;
    }

    /**
     * 夜晚停车费用（元 /晚）
     * @return night_fee 夜晚停车费用（元 /晚）
     */
    public BigDecimal getNightFee() {
        return nightFee;
    }

    /**
     * 夜晚停车费用（元 /晚）
     * @param nightFee 夜晚停车费用（元 /晚）
     */
    public void setNightFee(BigDecimal nightFee) {
        this.nightFee = nightFee;
    }
    
    /**
     * 状态（默认正常：1）
     * @return status 状态（默认正常：1）
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 状态（默认正常：1）
     * @param status 状态（默认正常：1）
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 类型（默认：1）
     * @return type 类型（默认：1）
     */
    public Integer getType() {
        return type;
    }

    /**
     * 类型（默认：1）
     * @param type 类型（默认：1）
     */
    public void setType(Integer type) {
        this.type = type;
    }
    
    /**
     * 停车场类型（1：封闭式场库，2：路边停车场）
     * @return park_type 停车场类型（1：封闭式场库，2：路边停车场）
     */
    public Integer getParkType() {
        return parkType;
    }

    /**
     * 停车场类型（1：封闭式场库，2：路边停车场）
     * @param parkType 停车场类型（1：封闭式场库，2：路边停车场）
     */
    public void setParkType(Integer parkType) {
        this.parkType = parkType;
    }

    /**
     * 停车点等级
     * @return park_level 停车点等级
     */
    public Integer getParkLevel() {
        return parkLevel;
    }

    /**
     * 停车点等级
     * @param parkLevel 停车点等级
     */
    public void setParkLevel(Integer parkLevel) {
        this.parkLevel = parkLevel;
    }

    /**
     * 收费规则
     * @return charge_rules 收费规则
     */
    public String getChargeRules() {
        return chargeRules;
    }

    /**
     * 收费规则
     * @param chargeRules 收费规则
     */
    public void setChargeRules(String chargeRules) {
        this.chargeRules = chargeRules;
    }

	public Double getMinlat() {
		return minlat;
	}

	public void setMinlat(Double minlat) {
		this.minlat = minlat;
	}

	public Double getMaxlat() {
		return maxlat;
	}

	public void setMaxlat(Double maxlat) {
		this.maxlat = maxlat;
	}

	public Double getMinlng() {
		return minlng;
	}

	public void setMinlng(Double minlng) {
		this.minlng = minlng;
	}

	public Double getMaxlng() {
		return maxlng;
	}

	public void setMaxlng(Double maxlng) {
		this.maxlng = maxlng;
	}

	public String getSearchContent() {
		return searchContent;
	}

	public void setSearchContent(String searchContent) {
		this.searchContent = searchContent;
	}

	public Integer getTotalPlot() {
		return totalPlot;
	}

	public void setTotalPlot(Integer totalPlot) {
		this.totalPlot = totalPlot;
	}

	public Integer getLeftPlot() {
		return leftPlot;
	}

	public void setLeftPlot(Integer leftPlot) {
		this.leftPlot = leftPlot;
	}

	public Integer getTotalFixedPlot() {
		return totalFixedPlot;
	}

	public void setTotalFixedPlot(Integer totalFixedPlot) {
		this.totalFixedPlot = totalFixedPlot;
	}

	public Integer getLeftFixedPlot() {
		return leftFixedPlot;
	}

	public void setLeftFixedPlot(Integer leftFixedPlot) {
		this.leftFixedPlot = leftFixedPlot;
	}


}