package com.jee.ssm.model;

import com.jee.ssm.model.base.BaseModel;

/**
 * 
 * 表名 b_map_point
 * @author GaoXiang
 * @version 1.0
 * Create time 2020/03/06
 */
public class MapPoint extends BaseModel {
    /**
     * 
     * 表字段 : b_map_point.id
     * Create time 2020/03/06
     */
    private String id;

    /**
     * 名称
     * 表字段 : b_map_point.name
     * Create time 2020/03/06
     */
    private String name;

    /**
     * 经度
     * 表字段 : b_map_point.lng
     * Create time 2020/03/06
     */
    private String lng;

    /**
     * 纬度
     * 表字段 : b_map_point.lat
     * Create time 2020/03/06
     */
    private String lat;

    /**
     * 是否显示
     * 表字段 : b_map_point.is_marker
     * Create time 2020/03/06
     */
    private Integer isMarker;

    /**
     * 排序
     * 表字段 : b_map_point.sort
     * Create time 2020/03/06
     */
    private Integer sort;

    /**
     * 
     * 表字段 : b_map_point.img
     * Create time 2020/03/06
     */
    private String img;

    /**
     * 构造方法
     */
    public MapPoint(String id, String name, String lng, String lat, Integer isMarker, Integer sort, String img) {
        this.id = id;
        this.name = name;
        this.lng = lng;
        this.lat = lat;
        this.isMarker = isMarker;
        this.sort = sort;
        this.img = img;
    }

    /**
     * 构造方法
     */
    public MapPoint() {
        super();
    }

    /**
     * 
     * @return id 
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 名称
     * @return name 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 名称
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 经度
     * @return lng 经度
     */
    public String getLng() {
        return lng;
    }

    /**
     * 经度
     * @param lng 经度
     */
    public void setLng(String lng) {
        this.lng = lng;
    }

    /**
     * 纬度
     * @return lat 纬度
     */
    public String getLat() {
        return lat;
    }

    /**
     * 纬度
     * @param lat 纬度
     */
    public void setLat(String lat) {
        this.lat = lat;
    }

    /**
     * 是否显示
     * @return is_marker 是否显示
     */
    public Integer getIsMarker() {
        return isMarker;
    }

    /**
     * 是否显示
     * @param isMarker 是否显示
     */
    public void setIsMarker(Integer isMarker) {
        this.isMarker = isMarker;
    }

    /**
     * 排序
     * @return sort 排序
     */
    public Integer getSort() {
        return sort;
    }

    /**
     * 排序
     * @param sort 排序
     */
    public void setSort(Integer sort) {
        this.sort = sort;
    }

    /**
     * 
     * @return img 
     */
    public String getImg() {
        return img;
    }

    /**
     * 
     * @param img 
     */
    public void setImg(String img) {
        this.img = img;
    }
}