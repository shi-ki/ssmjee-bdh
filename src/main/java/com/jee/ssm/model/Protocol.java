package com.jee.ssm.model;

import com.jee.ssm.model.base.BaseModel;
import java.util.Date;

/**
 * 
 * 表名 b_protocol
 * @author GaoXiang
 * @version 1.0
 * Create time 2018/06/06
 */
public class Protocol extends BaseModel {
    
	private static final long serialVersionUID = 2656394560008291422L;

	/**
     * 标识
     * 表字段 : b_protocol.id
     * Create time 2018/06/06
     */
    private String id;

    /**
     * 协议名称
     * 表字段 : b_protocol.name
     * Create time 2018/06/06
     */
    private String name;

    /**
     * 版本号
     * 表字段 : b_protocol.version
     * Create time 2018/06/06
     */
    private String version;

    /**
     * 类型（0：app用户使用协议）
     * 表字段 : b_protocol.type
     * Create time 2018/06/06
     */
    private Integer type;

    /**
     * 是否启用（0：不启用，1：启用）
     * 表字段 : b_protocol.status
     * Create time 2018/06/06
     */
    private Integer status;

    /**
     * 创建时间
     * 表字段 : b_protocol.create_time
     * Create time 2018/06/06
     */
    private Date createTime;

    /**
     * 协议内容
     * 表字段 : b_protocol.context
     * Create time 2018/06/06
     */
    private String context;

    /**
     * 构造方法
     */
    public Protocol(String id, String name, String version, Integer type, Integer status, Date createTime, String context) {
        this.id = id;
        this.name = name;
        this.version = version;
        this.type = type;
        this.status = status;
        this.createTime = createTime;
        this.context = context;
    }

    /**
     * 构造方法
     */
    public Protocol() {
        super();
    }

    /**
     * 标识
     * @return id 标识
     */
    public String getId() {
        return id;
    }

    /**
     * 标识
     * @param id 标识
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 协议名称
     * @return name 协议名称
     */
    public String getName() {
        return name;
    }

    /**
     * 协议名称
     * @param name 协议名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 版本号
     * @return version 版本号
     */
    public String getVersion() {
        return version;
    }

    /**
     * 版本号
     * @param version 版本号
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * 类型（0：app用户使用协议）
     * @return type 类型（0：app用户使用协议）
     */
    public Integer getType() {
        return type;
    }

    /**
     * 类型（0：app用户使用协议）
     * @param type 类型（0：app用户使用协议）
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 是否启用（0：不启用，1：启用）
     * @return status 是否启用（0：不启用，1：启用）
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 是否启用（0：不启用，1：启用）
     * @param status 是否启用（0：不启用，1：启用）
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 创建时间
     * @return create_time 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 创建时间
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 协议内容
     * @return context 协议内容
     */
    public String getContext() {
        return context;
    }

    /**
     * 协议内容
     * @param context 协议内容
     */
    public void setContext(String context) {
        this.context = context;
    }
}