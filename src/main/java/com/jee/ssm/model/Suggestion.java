package com.jee.ssm.model;

import com.jee.ssm.model.base.BaseModel;
import java.util.Date;
import java.util.List;

/**
 * 
 * 表名 b_suggestion
 * @author GaoXiang
 * @version 1.0
 * Create time 2018/06/23
 */
public class Suggestion extends BaseModel {

	private static final long serialVersionUID = 3939246511171573205L;

	/**
     * 标识
     * 表字段 : b_suggestion.id
     * Create time 2018/06/23
     */
    private String id;

    /**
     * 创建时间
     * 表字段 : b_suggestion.create_time
     * Create time 2018/06/23
     */
    private Date createTime;

    /**
     * 处理时间
     * 表字段 : b_suggestion.handle_time
     * Create time 2018/06/23
     */
    private Date handleTime;

    /**
     * 状态（0：未处理，1：已处理）
     * 表字段 : b_suggestion.status
     * Create time 2018/06/23
     */
    private Integer status;

    /**
     * 类型（0：意见）
     * 表字段 : b_suggestion.type
     * Create time 2018/06/23
     */
    private Integer type;

    /**
     * 用户id
     * 表字段 : b_suggestion.user_id
     * Create time 2018/06/23
     */
    private String userId;

    /**
     * 用户名
     * 表字段 : b_suggestion.user_name
     * Create time 2018/06/23
     */
    private String userName;

    /**
     * 手机号码
     * 表字段 : b_suggestion.phone
     * Create time 2018/06/23
     */
    private String phone;

    /**
     * 姓名
     * 表字段 : b_suggestion.name
     * Create time 2018/06/23
     */
    private String name;

    /**
     * 身份证号码
     * 表字段 : b_suggestion.id_number
     * Create time 2018/06/23
     */
    private String idNumber;

    /**
     * 后台处理用户id
     * 表字段 : b_suggestion.handle_user_id
     * Create time 2018/06/23
     */
    private String handleUserId;

    /**
     * 后台处理用户名
     * 表字段 : b_suggestion.handle_user_name
     * Create time 2018/06/23
     */
    private String handleUserName;

    /**
     * 意见内容
     * 表字段 : b_suggestion.content
     * Create time 2018/06/23
     */
    private String content;

    /**
     * 反馈内容
     * 表字段 : b_suggestion.handle_content
     * Create time 2018/06/23
     */
    private String handleContent;
    
    private List<SuggestionImage> list;

    /**
     * 构造方法
     */
    public Suggestion(String id, Date createTime, Date handleTime, Integer status, Integer type, String userId, String userName, String phone, String name, String idNumber, String handleUserId, String handleUserName, String content, String handleContent) {
        this.id = id;
        this.createTime = createTime;
        this.handleTime = handleTime;
        this.status = status;
        this.type = type;
        this.userId = userId;
        this.userName = userName;
        this.phone = phone;
        this.name = name;
        this.idNumber = idNumber;
        this.handleUserId = handleUserId;
        this.handleUserName = handleUserName;
        this.content = content;
        this.handleContent = handleContent;
    }
    
    public Suggestion(String id, Date createTime, Integer status, Integer type, String userId, String userName, String phone, String name, String idNumber, String content) {
    	this.id = id;
        this.createTime = createTime;
        this.status = status;
        this.type = type;
        this.userId = userId;
        this.userName = userName;
        this.phone = phone;
        this.name = name;
        this.idNumber = idNumber;
        this.content = content;
    }

    /**
     * 构造方法
     */
    public Suggestion() {
        super();
    }

    /**
     * 标识
     * @return id 标识
     */
    public String getId() {
        return id;
    }

    /**
     * 标识
     * @param id 标识
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 创建时间
     * @return create_time 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 创建时间
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 处理时间
     * @return handle_time 处理时间
     */
    public Date getHandleTime() {
        return handleTime;
    }

    /**
     * 处理时间
     * @param handleTime 处理时间
     */
    public void setHandleTime(Date handleTime) {
        this.handleTime = handleTime;
    }

    /**
     * 状态（0：未处理，1：已处理）
     * @return status 状态（0：未处理，1：已处理）
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 状态（0：未处理，1：已处理）
     * @param status 状态（0：未处理，1：已处理）
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 类型（0：意见）
     * @return type 类型（0：意见）
     */
    public Integer getType() {
        return type;
    }

    /**
     * 类型（0：意见）
     * @param type 类型（0：意见）
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 用户id
     * @return user_id 用户id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 用户id
     * @param userId 用户id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 用户名
     * @return user_name 用户名
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 用户名
     * @param userName 用户名
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 手机号码
     * @return phone 手机号码
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 手机号码
     * @param phone 手机号码
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 姓名
     * @return name 姓名
     */
    public String getName() {
        return name;
    }

    /**
     * 姓名
     * @param name 姓名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 身份证号码
     * @return id_number 身份证号码
     */
    public String getIdNumber() {
        return idNumber;
    }

    /**
     * 身份证号码
     * @param idNumber 身份证号码
     */
    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    /**
     * 后台处理用户id
     * @return handle_user_id 后台处理用户id
     */
    public String getHandleUserId() {
        return handleUserId;
    }

    /**
     * 后台处理用户id
     * @param handleUserId 后台处理用户id
     */
    public void setHandleUserId(String handleUserId) {
        this.handleUserId = handleUserId;
    }

    /**
     * 后台处理用户名
     * @return handle_user_name 后台处理用户名
     */
    public String getHandleUserName() {
        return handleUserName;
    }

    /**
     * 后台处理用户名
     * @param handleUserName 后台处理用户名
     */
    public void setHandleUserName(String handleUserName) {
        this.handleUserName = handleUserName;
    }

    /**
     * 意见内容
     * @return content 意见内容
     */
    public String getContent() {
        return content;
    }

    /**
     * 意见内容
     * @param content 意见内容
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 反馈内容
     * @return handle_content 反馈内容
     */
    public String getHandleContent() {
        return handleContent;
    }

    /**
     * 反馈内容
     * @param handleContent 反馈内容
     */
    public void setHandleContent(String handleContent) {
        this.handleContent = handleContent;
    }

	public List<SuggestionImage> getList() {
		return list;
	}

	public void setList(List<SuggestionImage> list) {
		this.list = list;
	}
	
}