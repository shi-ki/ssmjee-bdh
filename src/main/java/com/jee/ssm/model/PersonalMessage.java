package com.jee.ssm.model;

import com.jee.ssm.model.base.BaseModel;
import java.util.Date;

/**
 * 
 * 表名 b_personal_message
 * @author GaoXiang
 * @version 1.0
 * Create time 2018/06/24
 */
public class PersonalMessage extends BaseModel {

	private static final long serialVersionUID = -6830918853434345236L;

	/**
     * 标识
     * 表字段 : b_personal_message.id
     * Create time 2018/06/24
     */
    private String id;

    /**
     * 标题
     * 表字段 : b_personal_message.title
     * Create time 2018/06/24
     */
    private String title;

    /**
     * 用户id
     * 表字段 : b_personal_message.user_id
     * Create time 2018/06/24
     */
    private String userId;

    /**
     * 手机号码
     * 表字段 : b_personal_message.phone
     * Create time 2018/06/24
     */
    private String phone;

    /**
     * 用户名
     * 表字段 : b_personal_message.user_name
     * Create time 2018/06/24
     */
    private String userName;

    /**
     * 姓名
     * 表字段 : b_personal_message.name
     * Create time 2018/06/24
     */
    private String name;

    /**
     * 身份证号码
     * 表字段 : b_personal_message.id_number
     * Create time 2018/06/24
     */
    private String idNumber;

    /**
     * 信息状态（0：未读，1：已读）
     * 表字段 : b_personal_message.status
     * Create time 2018/06/24
     */
    private Integer status;

    /**
     * 类型（0：app消息，1：特别通知）
     * 表字段 : b_personal_message.type
     * Create time 2018/06/24
     */
    private Integer type;

    /**
     * 创建时间
     * 表字段 : b_personal_message.create_time
     * Create time 2018/06/24
     */
    private Date createTime;

    /**
     * 处理时间
     * 表字段 : b_personal_message.handle_time
     * Create time 2018/06/24
     */
    private Date handleTime;

    /**
     * 内容
     * 表字段 : b_personal_message.content
     * Create time 2018/06/24
     */
    private String content;
    
    private String ids;

    /**
     * 构造方法
     */
    public PersonalMessage(String id, String title, String userId, String phone, String userName, String name, String idNumber, Integer status, Integer type, Date createTime, Date handleTime, String content) {
        this.id = id;
        this.title = title;
        this.userId = userId;
        this.phone = phone;
        this.userName = userName;
        this.name = name;
        this.idNumber = idNumber;
        this.status = status;
        this.type = type;
        this.createTime = createTime;
        this.handleTime = handleTime;
        this.content = content;
    }

    /**
     * 构造方法
     */
    public PersonalMessage() {
        super();
    }

    /**
     * 标识
     * @return id 标识
     */
    public String getId() {
        return id;
    }

    /**
     * 标识
     * @param id 标识
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 标题
     * @return title 标题
     */
    public String getTitle() {
        return title;
    }

    /**
     * 标题
     * @param title 标题
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 用户id
     * @return user_id 用户id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 用户id
     * @param userId 用户id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 手机号码
     * @return phone 手机号码
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 手机号码
     * @param phone 手机号码
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 用户名
     * @return user_name 用户名
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 用户名
     * @param userName 用户名
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 姓名
     * @return name 姓名
     */
    public String getName() {
        return name;
    }

    /**
     * 姓名
     * @param name 姓名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 身份证号码
     * @return id_number 身份证号码
     */
    public String getIdNumber() {
        return idNumber;
    }

    /**
     * 身份证号码
     * @param idNumber 身份证号码
     */
    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    /**
     * 信息状态（0：未读，1：已读）
     * @return status 信息状态（0：未读，1：已读）
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 信息状态（0：未读，1：已读）
     * @param status 信息状态（0：未读，1：已读）
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 类型（0：app消息，1：特别通知）
     * @return type 类型（0：app消息，1：特别通知）
     */
    public Integer getType() {
        return type;
    }

    /**
     * 类型（0：app消息，1：特别通知）
     * @param type 类型（0：app消息，1：特别通知）
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 创建时间
     * @return create_time 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 创建时间
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 处理时间
     * @return handle_time 处理时间
     */
    public Date getHandleTime() {
        return handleTime;
    }

    /**
     * 处理时间
     * @param handleTime 处理时间
     */
    public void setHandleTime(Date handleTime) {
        this.handleTime = handleTime;
    }

    /**
     * 内容
     * @return content 内容
     */
    public String getContent() {
        return content;
    }

    /**
     * 内容
     * @param content 内容
     */
    public void setContent(String content) {
        this.content = content;
    }

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}
	
}