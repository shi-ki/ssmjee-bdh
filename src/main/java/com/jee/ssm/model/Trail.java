package com.jee.ssm.model;

import com.jee.ssm.model.base.BaseModel;
import java.util.Date;

/**
 * 
 * 表名 b_trail
 * @author GaoXiang
 * @version 1.0
 * Create time 2018/05/09
 */
public class Trail extends BaseModel {
    /**
	 * 
	 */
	private static final long serialVersionUID = -5299054994953932471L;

	/**
     * 
     * 表字段 : b_trail.id
     * Create time 2018/05/09
     */
    private String id;

    /**
     * 车牌号码
     * 表字段 : b_trail.plate_number
     * Create time 2018/05/09
     */
    private String plateNumber;

    /**
     * 车牌颜色
     * 表字段 : b_trail.plate_colour
     * Create time 2018/05/09
     */
    private Integer plateColour;

    /**
     * 车辆类型
     * 表字段 : b_trail.plate_type
     * Create time 2018/05/09
     */
    private Integer plateType;

    /**
     * 轨迹类型（0：到达，1：离开）
     * 表字段 : b_trail.type
     * Create time 2018/05/09
     */
    private Integer type;

    /**
     * 状态（默认1）
     * 表字段 : b_trail.status
     * Create time 2018/05/09
     */
    private Integer status;

    /**
     * 到达或离开的时间
     * 表字段 : b_trail.execute_time
     * Create time 2018/05/09
     */
    private Date executeTime;

    /**
     * 停留时间（换算前的毫秒数）
     * 表字段 : b_trail.stay_time_mills
     * Create time 2018/05/09
     */
    private Long stayTimeMills;

    /**
     * 到达后停留的时间（计算后的显示时间）
     * 表字段 : b_trail.stay_time
     * Create time 2018/05/09
     */
    private String stayTime;

    /**
     * 维度
     * 表字段 : b_trail.lat
     * Create time 2018/05/09
     */
    private Double lat;

    /**
     * 经度
     * 表字段 : b_trail.lng
     * Create time 2018/05/09
     */
    private Double lng;

    /**
     * 地址描述
     * 表字段 : b_trail.address
     * Create time 2018/05/09
     */
    private String address;

    /**
     * 添加时间
     * 表字段 : b_trail.create_time
     * Create time 2018/05/09
     */
    private Date createTime;

    /**
     * app用户id
     * 表字段 : b_trail.user_id
     * Create time 2018/05/09
     */
    private String userId;

    /**
     * app昵称
     * 表字段 : b_trail.user_name
     * Create time 2018/05/09
     */
    private String userName;

    /**
     * 姓名
     * 表字段 : b_trail.name
     * Create time 2018/05/09
     */
    private String name;

    /**
     * 身份证号码
     * 表字段 : b_trail.id_number
     * Create time 2018/05/09
     */
    private String idNumber;

    /**
     * 联系电话
     * 表字段 : b_trail.user_phone
     * Create time 2018/05/09
     */
    private String userPhone;

    /**
     * 关联上次离开或到达的id
     * 表字段 : b_trail.super_id
     * Create time 2018/05/09
     */
    private String superId;
    
    private String executeDate;

	/**
     * 构造方法
     */
    public Trail(String id, String plateNumber, Integer plateColour, Integer plateType, Integer type, Integer status, Date executeTime, Long stayTimeMills, String stayTime, Double lat, Double lng, String address, Date createTime, String userId, String userName, String name, String idNumber, String userPhone, String superId) {
        this.id = id;
        this.plateNumber = plateNumber;
        this.plateColour = plateColour;
        this.plateType = plateType;
        this.type = type;
        this.status = status;
        this.executeTime = executeTime;
        this.stayTimeMills = stayTimeMills;
        this.stayTime = stayTime;
        this.lat = lat;
        this.lng = lng;
        this.address = address;
        this.createTime = createTime;
        this.userId = userId;
        this.userName = userName;
        this.name = name;
        this.idNumber = idNumber;
        this.userPhone = userPhone;
        this.superId = superId;
    }

    /**
     * 构造方法
     */
    public Trail() {
        super();
    }
    
    public Trail(String plateNumber, String executeDate) {
        this.plateNumber = plateNumber;
        this.executeDate = executeDate;
    }

    /**
     * 
     * @return id 
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 车牌号码
     * @return plate_number 车牌号码
     */
    public String getPlateNumber() {
        return plateNumber;
    }

    /**
     * 车牌号码
     * @param plateNumber 车牌号码
     */
    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    /**
     * 车牌颜色
     * @return plate_colour 车牌颜色
     */
    public Integer getPlateColour() {
        return plateColour;
    }

    /**
     * 车牌颜色
     * @param plateColour 车牌颜色
     */
    public void setPlateColour(Integer plateColour) {
        this.plateColour = plateColour;
    }

    /**
     * 车辆类型
     * @return plate_type 车辆类型
     */
    public Integer getPlateType() {
        return plateType;
    }

    /**
     * 车辆类型
     * @param plateType 车辆类型
     */
    public void setPlateType(Integer plateType) {
        this.plateType = plateType;
    }

    /**
     * 轨迹类型（0：到达，1：离开）
     * @return type 轨迹类型（0：到达，1：离开）
     */
    public Integer getType() {
        return type;
    }

    /**
     * 轨迹类型（0：到达，1：离开）
     * @param type 轨迹类型（0：到达，1：离开）
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 状态（默认1）
     * @return status 状态（默认1）
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 状态（默认1）
     * @param status 状态（默认1）
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 到达或离开的时间
     * @return execute_time 到达或离开的时间
     */
    public Date getExecuteTime() {
        return executeTime;
    }

    /**
     * 到达或离开的时间
     * @param executeTime 到达或离开的时间
     */
    public void setExecuteTime(Date executeTime) {
        this.executeTime = executeTime;
    }

    /**
     * 停留时间（换算前的毫秒数）
     * @return stay_time_mills 停留时间（换算前的毫秒数）
     */
    public Long getStayTimeMills() {
        return stayTimeMills;
    }

    /**
     * 停留时间（换算前的毫秒数）
     * @param stayTimeMills 停留时间（换算前的毫秒数）
     */
    public void setStayTimeMills(Long stayTimeMills) {
        this.stayTimeMills = stayTimeMills;
    }

    /**
     * 到达后停留的时间（计算后的显示时间）
     * @return stay_time 到达后停留的时间（计算后的显示时间）
     */
    public String getStayTime() {
        return stayTime;
    }

    /**
     * 到达后停留的时间（计算后的显示时间）
     * @param stayTime 到达后停留的时间（计算后的显示时间）
     */
    public void setStayTime(String stayTime) {
        this.stayTime = stayTime;
    }

    /**
     * 维度
     * @return lat 维度
     */
    public Double getLat() {
        return lat;
    }

    /**
     * 维度
     * @param lat 维度
     */
    public void setLat(Double lat) {
        this.lat = lat;
    }

    /**
     * 经度
     * @return lng 经度
     */
    public Double getLng() {
        return lng;
    }

    /**
     * 经度
     * @param lng 经度
     */
    public void setLng(Double lng) {
        this.lng = lng;
    }

    /**
     * 地址描述
     * @return address 地址描述
     */
    public String getAddress() {
        return address;
    }

    /**
     * 地址描述
     * @param address 地址描述
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 添加时间
     * @return create_time 添加时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 添加时间
     * @param createTime 添加时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * app用户id
     * @return user_id app用户id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * app用户id
     * @param userId app用户id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * app昵称
     * @return user_name app昵称
     */
    public String getUserName() {
        return userName;
    }

    /**
     * app昵称
     * @param userName app昵称
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 姓名
     * @return name 姓名
     */
    public String getName() {
        return name;
    }

    /**
     * 姓名
     * @param name 姓名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 身份证号码
     * @return id_number 身份证号码
     */
    public String getIdNumber() {
        return idNumber;
    }

    /**
     * 身份证号码
     * @param idNumber 身份证号码
     */
    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    /**
     * 联系电话
     * @return user_phone 联系电话
     */
    public String getUserPhone() {
        return userPhone;
    }

    /**
     * 联系电话
     * @param userPhone 联系电话
     */
    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    /**
     * 关联上次离开或到达的id
     * @return super_id 关联上次离开或到达的id
     */
    public String getSuperId() {
        return superId;
    }

    /**
     * 关联上次离开或到达的id
     * @param superId 关联上次离开或到达的id
     */
    public void setSuperId(String superId) {
        this.superId = superId;
    }
    
    public String getExecuteDate() {
		return executeDate;
	}

	public void setExecuteDate(String executeDate) {
		this.executeDate = executeDate;
	}
	
}