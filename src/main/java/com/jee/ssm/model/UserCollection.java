package com.jee.ssm.model;

import java.util.Date;

import com.jee.ssm.model.base.BaseModel;

/**
 * 
 * 表名 b_user_collection
 * @author GaoXiang
 * @version 1.0
 * Create time 2018/09/13
 */
public class UserCollection extends BaseModel {
    
	private static final long serialVersionUID = 3445134276669909330L;

	/**
     * 收藏标识
     * 表字段 : b_user_collection.id
     * Create time 2018/09/13
     */
    private String id;

    /**
     * 用户标识
     * 表字段 : b_user_collection.user_id
     * Create time 2018/09/13
     */
    private String userId;

    /**
     * 海康标识
     * 表字段 : b_user_collection.hik_id
     * Create time 2018/09/13
     */
    private String parkDepotId;
    
    /**
     * 添加时间
     * 表字段 : b_user_collection.create_time
     * Create time 2018/09/13
     */
    private Date createTime;
    
    /**
     * 停车预约及收费规则
     */
    private ParkDepot parkDepot;

    /**
     * 构造方法
     */
    public UserCollection(String id, String userId, String parkDepotId, Date createTime) {
    	super();
        this.id = id;
        this.userId = userId;
        this.parkDepotId = parkDepotId;
        this.createTime = createTime;
    }
    
    public UserCollection(String userId, String parkDepotId) {
    	super();
        this.userId = userId;
        this.parkDepotId = parkDepotId;
    }
    
    public UserCollection(String userId) {
    	super();
    	this.userId = userId;
    }

    /**
     * 构造方法
     */
    public UserCollection() {
        super();
    }

    /**
     * 收藏标识
     * @return id 收藏标识
     */
    public String getId() {
        return id;
    }

    /**
     * 收藏标识
     * @param id 收藏标识
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 用户标识
     * @return user_id 用户标识
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 用户标识
     * @param userId 用户标识
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 海康标识
     * @return hik_id 海康标识
     */
    public String getParkDepotId() {
        return parkDepotId;
    }

    /**
     * 海康标识
     * @param hikId 海康标识
     */
    public void setParkDepotId(String parkDepotId) {
        this.parkDepotId = parkDepotId;
    }
    
    /**
     * 添加时间
     * @return create_time 添加时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 添加时间
     * @param createTime 添加时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

	public ParkDepot getParkDepot() {
		return parkDepot;
	}

	public void setParkDepot(ParkDepot parkDepot) {
		this.parkDepot = parkDepot;
	}
    
}