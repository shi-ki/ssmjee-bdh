package com.jee.ssm.model.aipark;

public class AiparkReceive {
	
	/**
	 * 状态
	 */
	private String state;
	
	/**
	 * 描述
	 */
	private String desc;
	
	/**
	 * 回传数据
	 */
	private Object value;

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "AiparkReceive{" +
				"state='" + state + '\'' +
				", desc='" + desc + '\'' +
				", value=" + value +
				'}';
	}
}
