package com.jee.ssm.modules.monitor.dao;

import com.jee.ssm.model.Monitor;
import com.jee.ssm.modules.ssm.dao.LzDao;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;

/**
* 监控管理 Dao
* @author GaoXiang
* @version 1.0
*/
@Repository
public class MonitorDao extends LzDao<Monitor> {



    /**
    * 保存数据
    * @param monitor 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(Monitor monitor) throws Exception {
        return insert("MonitorMapper.insert",monitor);
    }

    /**
    * 根据 id 修改
    * @param monitor 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(Monitor monitor) throws Exception {
        return update("MonitorMapper.updateById",monitor);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return delete("MonitorMapper.deleteById",id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public Monitor selectById(String id) {
        return selectOne("MonitorMapper.selectById",id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return delete("MonitorMapper.deleteByIds",ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<Monitor> list(Map map) {
        return list("MonitorMapper.list",map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<Monitor> list(Map map,int page,int size) {
        return list("MonitorMapper.list",map,new RowBounds(page,size));
    }

	public List<Monitor> listCommonMoitors() {
		return arrayList("MonitorMapper.listCommonMoitors", null);
	}

}
