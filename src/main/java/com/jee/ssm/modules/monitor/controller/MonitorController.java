package com.jee.ssm.modules.monitor.controller;

import com.jee.ssm.model.Monitor;
import com.jee.ssm.modules.monitor.services.MonitorService;
import com.jee.ssm.common.log.AdminControllerLog;
import com.jee.ssm.modules.ssm.controller.AdminBaseController;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.model.param.ParamMap;
import com.jee.ssm.common.utils.UUIDFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
* 监控管理 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/monitor")
public class MonitorController extends AdminBaseController<Monitor> {


    /**
     * 进入监控添加页面 携带一个生成的id --> longId
     * @param model 返回的实体容器
     * @return 添加页面
     */
    @RequestMapping(value="/add")
    @RequiresPermissions("monitor:add")
    public String add(ModelMap model){

        model.put("longId", UUIDFactory.getStringId());
        return "manager/monitor/add";
    }


    /**
     * 进入监控编辑页面
     * @param model 返回监控的容器
     * @param id 监控id
     * @return 编辑页面
     */
    @RequestMapping(value="/edit")
    @RequiresPermissions("monitor:edit")
    public String edit(ModelMap model, @RequestParam String id) {

        model.put("data",monitorService.selectById(id));
        return "manager/monitor/edit";
    }


    /**
     * 监控添加
     * @param monitor 带id的监控对象
     * @return 成功状态
     */
    @RequestMapping(value="/save")
    @ResponseBody
    @RequiresPermissions("monitor:add")
    @AdminControllerLog(description = "添加监控" )
    public Tip save(Monitor monitor)  {

        try {
        	monitor.setRelateId(monitor.getNumber());
            monitorService.insert(monitor);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"添加失败！");
        }

    }


    /**
     * 根据 id 修改监控
     * @param monitor 带id的监控对象
     * @return 成功状态
     */
    @RequestMapping(value="/update")
    @ResponseBody
    @RequiresPermissions("monitor:edit")
    @AdminControllerLog(description = "修改监控" )
    public Tip update(Monitor monitor) {

        try {
            monitorService.updateById(monitor);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"修改失败！");
        }

    }


    /**
     * 根据 id 删除监控
     * @param id 监控id
     * @return 成功状态
     */
    @RequestMapping(value="/delete")
    @ResponseBody
    @RequiresPermissions("monitor:delete")
    @AdminControllerLog(description = "删除监控" )
    public Tip delete(@RequestParam String id) {

        try {
            monitorService.deleteById(id);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"删除失败！");
        }

    }


    /**
     * 根据 id 列表批量删除
     * @param ids 监控id List
     * @return 成功状态
     */
    @RequestMapping(value="/deleteByIds")
    @ResponseBody
    @RequiresPermissions("monitor:delete")
    @AdminControllerLog(description = "批量删除监控" )
    public Tip deleteByIds(@RequestParam("ids") List<String> ids) {

        try {
            monitorService.deleteByIds(ids);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"批量删除失败！");
        }

    }


    /**
     * 根据 id 查找监控
     * @param id 监控id
     * @return 监控对象 json
     */
    @RequestMapping(value="/findJson")
    @ResponseBody
    @RequiresPermissions("monitor:list")
    public Monitor find(@RequestParam String id) {

        return monitorService.selectById(id);
    }


    /**
     * 获取监控列表 获取全部 不分页
     * @param request 请求参数
     * @return 监控列表页面
     */
    @RequestMapping(value="/all")
    @RequiresPermissions("monitor:list")
    public String all(HttpServletRequest request,ModelMap modelMap) {

        return list(request,modelMap,1,0);
    }


    /**
     * 获取监控列表 分页
     * @param request 请求参数
     * @param page 第几页
     * @param size 每页大小
     * @return 监控列表页面
     */
    @RequestMapping(value="/list")
    @RequiresPermissions("monitor:list")
    public String list(HttpServletRequest request,ModelMap modelMap,Integer page,Integer size) {

        modelMap.put("pageInfo",monitorService.list(new ParamMap(request),page,size));
        modelMap.putAll(new ParamMap(request));
        return "manager/monitor/list";
    }


    //---------------------------- property -------------------------------

    @Resource
    private MonitorService monitorService;

}
