package com.jee.ssm.modules.hikBill.dao;

import com.jee.ssm.model.hik.receive.HikBill;
import com.jee.ssm.modules.ssm.dao.LzDao;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;

/**
* 海康账单记录管理 Dao
* @author GaoXiang
* @version 1.0
*/
@Repository
public class HikBillDao extends LzDao<HikBill> {



    /**
    * 保存数据
    * @param hikBill 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(HikBill hikBill) throws Exception {
        return insert("HikBillMapper.insert",hikBill);
    }

    /**
    * 根据 id 修改
    * @param hikBill 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(HikBill hikBill) throws Exception {
        return update("HikBillMapper.updateById",hikBill);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return delete("HikBillMapper.deleteById",id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public HikBill selectById(String id) {
        return selectOne("HikBillMapper.selectById",id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return delete("HikBillMapper.deleteByIds",ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<HikBill> list(Map map) {
        return list("HikBillMapper.list",map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<HikBill> list(Map map,int page,int size) {
        return list("HikBillMapper.list",map,new RowBounds(page,size));
    }

	public List<HikBill> findByPayStatus(HikBill hikBill) {
		return arrayList("HikBillMapper.findByPayStatus", hikBill);
	}
    public HikBill findByLane(HikBill hikBill) {
        return selectOne("HikBillMapper.findByLane",hikBill);
    }

    public Long countNoPayBill() {
        return count("HikBillMapper.countNoPayBill",null);
    }

    public Integer deleteNoPay() throws Exception {
        return delete("HikBillMapper.deleteNoPay",null);
    }
}
