package com.jee.ssm.modules.hikBill.controller;

import com.jee.ssm.model.hik.receive.HikBill;
import com.jee.ssm.modules.hikBill.services.HikBillService;
import com.jee.ssm.common.log.AdminControllerLog;
import com.jee.ssm.modules.ssm.controller.AdminBaseController;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.model.param.ParamMap;
import com.jee.ssm.common.utils.UUIDFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
* 海康账单记录管理 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/hikBill")
public class HikBillController extends AdminBaseController<HikBill> {


    /**
     * 进入海康账单记录添加页面 携带一个生成的id --> longId
     * @param model 返回的实体容器
     * @return 添加页面
     */
    @RequestMapping(value="/add")
    @RequiresPermissions("hikBill:add")
    public String add(ModelMap model){

        model.put("longId", UUIDFactory.getStringId());
        return "manager/hikBill/add";
    }


    /**
     * 进入海康账单记录编辑页面
     * @param model 返回海康账单记录的容器
     * @param id 海康账单记录id
     * @return 编辑页面
     */
    @RequestMapping(value="/edit")
    @RequiresPermissions("hikBill:edit")
    public String edit(ModelMap model, @RequestParam String id) {

        model.put("data",hikBillService.selectById(id));
        return "manager/hikBill/edit";
    }


    /**
     * 海康账单记录添加
     * @param hikBill 带id的海康账单记录对象
     * @return 成功状态
     */
    @RequestMapping(value="/save")
    @ResponseBody
    @RequiresPermissions("hikBill:add")
    @AdminControllerLog(description = "添加海康账单记录" )
    public Tip save(HikBill hikBill)  {

        try {
            hikBillService.insert(hikBill);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"添加失败！");
        }

    }


    /**
     * 根据 id 修改海康账单记录
     * @param hikBill 带id的海康账单记录对象
     * @return 成功状态
     */
    @RequestMapping(value="/update")
    @ResponseBody
    @RequiresPermissions("hikBill:edit")
    @AdminControllerLog(description = "修改海康账单记录" )
    public Tip update(HikBill hikBill) {

        try {
            hikBillService.updateById(hikBill);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"修改失败！");
        }

    }


    /**
     * 根据 id 删除海康账单记录
     * @param id 海康账单记录id
     * @return 成功状态
     */
    @RequestMapping(value="/delete")
    @ResponseBody
    @RequiresPermissions("hikBill:delete")
    @AdminControllerLog(description = "删除海康账单记录" )
    public Tip delete(@RequestParam String id) {

        try {
            hikBillService.deleteById(id);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"删除失败！");
        }

    }


    /**
     * 根据 id 列表批量删除
     * @param ids 海康账单记录id List
     * @return 成功状态
     */
    @RequestMapping(value="/deleteByIds")
    @ResponseBody
    @RequiresPermissions("hikBill:delete")
    @AdminControllerLog(description = "批量删除海康账单记录" )
    public Tip deleteByIds(@RequestParam("ids") List<String> ids) {

        try {
            hikBillService.deleteByIds(ids);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"批量删除失败！");
        }

    }


    /**
     * 根据 id 查找海康账单记录
     * @param id 海康账单记录id
     * @return 海康账单记录对象 json
     */
    @RequestMapping(value="/findJson")
    @ResponseBody
    @RequiresPermissions("hikBill:list")
    public HikBill find(@RequestParam String id) {

        return hikBillService.selectById(id);
    }


    /**
     * 获取海康账单记录列表 获取全部 不分页
     * @param request 请求参数
     * @return 海康账单记录列表页面
     */
    @RequestMapping(value="/all")
    @RequiresPermissions("hikBill:list")
    public String all(HttpServletRequest request,ModelMap modelMap) {

        return list(request,modelMap,1,0);
    }


    /**
     * 获取海康账单记录列表 分页
     * @param request 请求参数
     * @param page 第几页
     * @param size 每页大小
     * @return 海康账单记录列表页面
     */
    @RequestMapping(value="/list")
    @RequiresPermissions("hikBill:list")
    public String list(HttpServletRequest request,ModelMap modelMap,Integer page,Integer size) {

        modelMap.put("pageInfo",hikBillService.list(new ParamMap(request),page,size));
        modelMap.putAll(new ParamMap(request));
        return "manager/hikBill/list";
    }


    //---------------------------- property -------------------------------

    @Resource
    private HikBillService hikBillService;

}
