package com.jee.ssm.modules.trail.services;

import com.jee.ssm.modules.trail.dao.TrailDao;
import com.jee.ssm.model.Trail;
import com.jee.ssm.modules.ssm.services.BaseService;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
* 轨迹管理 Service
* @author GaoXiang
* @version 1.0
*/
@Service
public class TrailService extends BaseService<Trail> {

    @Resource
    private TrailDao trailDao;


    /**
    * 保存数据
    * @param trail 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(Trail trail) throws Exception {
        return trailDao.insert(trail);
    }

    /**
    * 根据 id 修改
    * @param trail 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(Trail trail) throws Exception {
        return trailDao.updateById(trail);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return trailDao.deleteById(id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public Trail selectById(String id) {
        return trailDao.selectById(id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return trailDao.deleteByIds(ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<Trail> list(Map map) {
        return trailDao.list(map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<Trail> list(Map map,int page,int size) {
        return trailDao.list(map,page,size);
    }

	public List<Trail> findTrailByPlate(Trail trail) {
		return trailDao.findTrailByPlate(trail);
	}


}
