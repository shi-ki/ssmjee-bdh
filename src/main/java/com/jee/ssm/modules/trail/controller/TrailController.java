package com.jee.ssm.modules.trail.controller;

import com.jee.ssm.model.Trail;
import com.jee.ssm.modules.trail.services.TrailService;
import com.jee.ssm.common.log.AdminControllerLog;
import com.jee.ssm.modules.ssm.controller.AdminBaseController;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.model.param.ParamMap;
import com.jee.ssm.common.utils.TimeUtils;
import com.jee.ssm.common.utils.UUIDFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
* 轨迹管理 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/trail")
public class TrailController extends AdminBaseController<Trail> {


    /**
     * 进入轨迹添加页面 携带一个生成的id --> longId
     * @param model 返回的实体容器
     * @return 添加页面
     */
    @RequestMapping(value="/add")
    @RequiresPermissions("trail:add")
    public String add(ModelMap model){

        model.put("longId", UUIDFactory.getStringId());
        return "manager/trail/add";
    }


    /**
     * 进入轨迹编辑页面
     * @param model 返回轨迹的容器
     * @param id 轨迹id
     * @return 编辑页面
     */
    @RequestMapping(value="/edit")
    @RequiresPermissions("trail:edit")
    public String edit(ModelMap model, @RequestParam String id) {

        model.put("data",trailService.selectById(id));
        return "manager/trail/edit";
    }


    /**
     * 轨迹添加
     * @param trail 带id的轨迹对象
     * @return 成功状态
     */
    @RequestMapping(value="/save")
    @ResponseBody
    @RequiresPermissions("trail:add")
    @AdminControllerLog(description = "添加轨迹" )
    public Tip save(Trail trail)  {

        try {
        	trail.setStayTimeMills(TimeUtils.getMilliSecond(trail.getStayTime()));
            trailService.insert(trail);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"添加失败！");
        }

    }


    /**
     * 根据 id 修改轨迹
     * @param trail 带id的轨迹对象
     * @return 成功状态
     */
    @RequestMapping(value="/update")
    @ResponseBody
    @RequiresPermissions("trail:edit")
    @AdminControllerLog(description = "修改轨迹" )
    public Tip update(Trail trail) {

        try {
            trailService.updateById(trail);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"修改失败！");
        }

    }


    /**
     * 根据 id 删除轨迹
     * @param id 轨迹id
     * @return 成功状态
     */
    @RequestMapping(value="/delete")
    @ResponseBody
    @RequiresPermissions("trail:delete")
    @AdminControllerLog(description = "删除轨迹" )
    public Tip delete(@RequestParam String id) {

        try {
            trailService.deleteById(id);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"删除失败！");
        }

    }


    /**
     * 根据 id 列表批量删除
     * @param ids 轨迹id List
     * @return 成功状态
     */
    @RequestMapping(value="/deleteByIds")
    @ResponseBody
    @RequiresPermissions("trail:delete")
    @AdminControllerLog(description = "批量删除轨迹" )
    public Tip deleteByIds(@RequestParam("ids") List<String> ids) {

        try {
            trailService.deleteByIds(ids);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"批量删除失败！");
        }

    }


    /**
     * 根据 id 查找轨迹
     * @param id 轨迹id
     * @return 轨迹对象 json
     */
    @RequestMapping(value="/findJson")
    @ResponseBody
    @RequiresPermissions("trail:list")
    public Trail find(@RequestParam String id) {

        return trailService.selectById(id);
    }


    /**
     * 获取轨迹列表 获取全部 不分页
     * @param request 请求参数
     * @return 轨迹列表页面
     */
    @RequestMapping(value="/all")
    @RequiresPermissions("trail:list")
    public String all(HttpServletRequest request,ModelMap modelMap) {

        return list(request,modelMap,1,0);
    }


    /**
     * 获取轨迹列表 分页
     * @param request 请求参数
     * @param page 第几页
     * @param size 每页大小
     * @return 轨迹列表页面
     */
    @RequestMapping(value="/list")
    @RequiresPermissions("trail:list")
    public String list(HttpServletRequest request,ModelMap modelMap,Integer page,Integer size) {

        modelMap.put("pageInfo",trailService.list(new ParamMap(request),page,size));
        modelMap.putAll(new ParamMap(request));
        return "manager/trail/list";
    }


    //---------------------------- property -------------------------------

    @Resource
    private TrailService trailService;

}
