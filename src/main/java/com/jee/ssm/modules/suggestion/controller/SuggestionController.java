package com.jee.ssm.modules.suggestion.controller;

import com.jee.ssm.model.Suggestion;
import com.jee.ssm.modules.suggestion.services.SuggestionService;
import com.jee.ssm.common.log.AdminControllerLog;
import com.jee.ssm.modules.ssm.controller.AdminBaseController;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.model.param.ParamMap;
import com.jee.ssm.common.utils.UUIDFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
* 意见反馈管理 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/suggestion")
public class SuggestionController extends AdminBaseController<Suggestion> {


    /**
     * 进入意见反馈添加页面 携带一个生成的id --> longId
     * @param model 返回的实体容器
     * @return 添加页面
     */
    @RequestMapping(value="/add")
    @RequiresPermissions("suggestion:add")
    public String add(ModelMap model){

        model.put("longId", UUIDFactory.getStringId());
        return "manager/suggestion/add";
    }


    /**
     * 进入意见反馈编辑页面
     * @param model 返回意见反馈的容器
     * @param id 意见反馈id
     * @return 编辑页面
     */
    @RequestMapping(value="/edit")
    @RequiresPermissions("suggestion:edit")
    public String edit(ModelMap model, @RequestParam String id) {

        model.put("data",suggestionService.selectById(id));
        return "manager/suggestion/edit";
    }


    /**
     * 意见反馈添加
     * @param suggestion 带id的意见反馈对象
     * @return 成功状态
     */
    @RequestMapping(value="/save")
    @ResponseBody
    @RequiresPermissions("suggestion:add")
    @AdminControllerLog(description = "添加意见反馈" )
    public Tip save(Suggestion suggestion)  {

        try {
            suggestionService.insert(suggestion);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"添加失败！");
        }

    }


    /**
     * 根据 id 修改意见反馈
     * @param suggestion 带id的意见反馈对象
     * @return 成功状态
     */
    @RequestMapping(value="/update")
    @ResponseBody
    @RequiresPermissions("suggestion:edit")
    @AdminControllerLog(description = "修改意见反馈" )
    public Tip update(Suggestion suggestion) {

        try {
            suggestionService.updateById(suggestion);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"修改失败！");
        }

    }


    /**
     * 根据 id 删除意见反馈
     * @param id 意见反馈id
     * @return 成功状态
     */
    @RequestMapping(value="/delete")
    @ResponseBody
    @RequiresPermissions("suggestion:delete")
    @AdminControllerLog(description = "删除意见反馈" )
    public Tip delete(@RequestParam String id) {

        try {
            suggestionService.deleteById(id);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"删除失败！");
        }

    }


    /**
     * 根据 id 列表批量删除
     * @param ids 意见反馈id List
     * @return 成功状态
     */
    @RequestMapping(value="/deleteByIds")
    @ResponseBody
    @RequiresPermissions("suggestion:delete")
    @AdminControllerLog(description = "批量删除意见反馈" )
    public Tip deleteByIds(@RequestParam("ids") List<String> ids) {

        try {
            suggestionService.deleteByIds(ids);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"批量删除失败！");
        }

    }


    /**
     * 根据 id 查找意见反馈
     * @param id 意见反馈id
     * @return 意见反馈对象 json
     */
    @RequestMapping(value="/findJson")
    @ResponseBody
    @RequiresPermissions("suggestion:list")
    public Suggestion find(@RequestParam String id) {

        return suggestionService.selectById(id);
    }


    /**
     * 获取意见反馈列表 获取全部 不分页
     * @param request 请求参数
     * @return 意见反馈列表页面
     */
    @RequestMapping(value="/all")
    @RequiresPermissions("suggestion:list")
    public String all(HttpServletRequest request,ModelMap modelMap) {

        return list(request,modelMap,1,0);
    }


    /**
     * 获取意见反馈列表 分页
     * @param request 请求参数
     * @param page 第几页
     * @param size 每页大小
     * @return 意见反馈列表页面
     */
    @RequestMapping(value="/list")
    @RequiresPermissions("suggestion:list")
    public String list(HttpServletRequest request,ModelMap modelMap,Integer page,Integer size) {

        modelMap.put("pageInfo",suggestionService.list(new ParamMap(request),page,size));
        modelMap.putAll(new ParamMap(request));
        return "manager/suggestion/list";
    }


    //---------------------------- property -------------------------------

    @Resource
    private SuggestionService suggestionService;

}
