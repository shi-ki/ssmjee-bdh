package com.jee.ssm.modules.suggestion.app;

import java.io.File;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.github.pagehelper.PageInfo;
import com.jee.ssm.common.config.Logger;
import com.jee.ssm.common.utils.TokenUtils;
import com.jee.ssm.common.utils.UUIDFactory;
import com.jee.ssm.model.Suggestion;
import com.jee.ssm.model.SuggestionImage;
import com.jee.ssm.model.User;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.modules.suggestion.services.SuggestionService;
import com.jee.ssm.modules.suggestionImage.services.SuggestionImageService;
import com.jee.ssm.modules.user.services.UserService;

/**
* 意见反馈管理 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/app/suggestion")
public class AppSuggestionController {

	/**
	 * 反馈意见保存
	 */
	@RequestMapping(value="/saveSuggestion", method = RequestMethod.POST)
    @ResponseBody
	public Tip saveSuggestion(HttpServletRequest request, long timestamp, String token, String userId, String content, MultipartFile[] files) {
		Tip tip = TokenUtils.verifyToken(timestamp, token);
        if(tip.getSuccess()) {
        	User user = userService.selectById(userId);
        	if(user != null) {
        		String id = UUIDFactory.getStringId();
            	if(files.length > 0) {
            		for(MultipartFile file : files) {
            			String url = "/images/suggestion/" + userId + "/";
            			String path = request.getServletContext().getRealPath(url);
            			File d = new File(path);
            			if(!d.exists() && !d.isDirectory()) {
            				d.mkdirs();
            			}
            			String fileName = userId + "_" + System.currentTimeMillis() + ".jpg";
            			path = path + fileName;
            			File image = new File(path);
            			try {
        					file.transferTo(image);
        					SuggestionImage si = new SuggestionImage(UUIDFactory.getStringId(), 1, 0, file.getName(), path, id);
        					suggestionImageService.insert(si);
        				} catch (Exception e) {
        					Logger.info("图片保存失败");
        				}
            		}
            	}
        		
        		try {
        			Suggestion suggestion = new Suggestion(id, new Date(), 0, 0, userId, user.getUserName(), user.getPhone(), user.getName(), user.getIdNumber(), content);
					suggestionService.insert(suggestion);
					tip = new Tip("提交成功");
				} catch (Exception e) {
					tip = new Tip(1, "提交失败");
				}
        	} else {
        		tip = new Tip(2, "未查找到该用户");
        	}
        }
        return tip;
	}
	
	/**
	 * 根据用户id查询自己的反馈意见列表
	 */
	@RequestMapping(value = "/findByUserId", method = RequestMethod.GET)
	@ResponseBody
	public Tip findByUserId(long timestamp, String token, String userId, Integer page, Integer size) {
		Tip tip = TokenUtils.verifyToken(timestamp, token);
        if(tip.getSuccess()) {
        	PageInfo<Suggestion> p = suggestionService.findByUserId(userId, page, size);
        	tip = new Tip(p.getList());
        }
        return tip;
	}
	
	/**
	 * 根据id查询反馈意见详情
	 */
	@RequestMapping(value = "/findDetailById", method = RequestMethod.GET)
	@ResponseBody
	public Tip findDetailById(long timestamp, String token, String id) {
		Tip tip = TokenUtils.verifyToken(timestamp, token);
        if(tip.getSuccess()) {
        	Suggestion s = suggestionService.selectById(id);
        	List<SuggestionImage> list = suggestionImageService.findBySuggestionId(id);
        	s.setList(list);
        	tip = new Tip(s);
        }
        return tip;
	}

    //---------------------------- property -------------------------------

    @Resource
    private SuggestionService suggestionService;
    
    @Resource
    private SuggestionImageService suggestionImageService;
    
    @Resource
    private UserService userService;

}
