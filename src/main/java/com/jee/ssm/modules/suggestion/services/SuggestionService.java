package com.jee.ssm.modules.suggestion.services;

import com.jee.ssm.modules.suggestion.dao.SuggestionDao;
import com.jee.ssm.model.Suggestion;
import com.jee.ssm.modules.ssm.services.BaseService;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
* 意见反馈管理 Service
* @author GaoXiang
* @version 1.0
*/
@Service
public class SuggestionService extends BaseService<Suggestion> {

    @Resource
    private SuggestionDao suggestionDao;


    /**
    * 保存数据
    * @param suggestion 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(Suggestion suggestion) throws Exception {
        return suggestionDao.insert(suggestion);
    }

    /**
    * 根据 id 修改
    * @param suggestion 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(Suggestion suggestion) throws Exception {
        return suggestionDao.updateById(suggestion);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return suggestionDao.deleteById(id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public Suggestion selectById(String id) {
        return suggestionDao.selectById(id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return suggestionDao.deleteByIds(ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<Suggestion> list(Map map) {
        return suggestionDao.list(map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<Suggestion> list(Map map,int page,int size) {
        return suggestionDao.list(map,page,size);
    }

	public PageInfo<Suggestion> findByUserId(String userId, Integer page, Integer size) {
		return suggestionDao.findByUserId(userId, page, size);
	}

}
