package com.jee.ssm.modules.guideScreen.dao;

import com.jee.ssm.model.GuideScreen;
import com.jee.ssm.modules.ssm.dao.LzDao;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;

/**
* 诱导屏管理 Dao
* @author GaoXiang
* @version 1.0
*/
@Repository
public class GuideScreenDao extends LzDao<GuideScreen> {



    /**
    * 保存数据
    * @param guideScreen 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(GuideScreen guideScreen) throws Exception {
        return insert("GuideScreenMapper.insert",guideScreen);
    }

    /**
    * 根据 id 修改
    * @param guideScreen 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(GuideScreen guideScreen) throws Exception {
        return update("GuideScreenMapper.updateById",guideScreen);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return delete("GuideScreenMapper.deleteById",id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public GuideScreen selectById(String id) {
        return selectOne("GuideScreenMapper.selectById",id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return delete("GuideScreenMapper.deleteByIds",ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<GuideScreen> list(Map map) {
        return list("GuideScreenMapper.list",map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<GuideScreen> list(Map map,int page,int size) {
        return list("GuideScreenMapper.list",map,new RowBounds(page,size));
    }

	public List<GuideScreen> findByNumber(String number) {
		return arrayList("GuideScreenMapper.findByNumber", number);
	}

	public List<GuideScreen> listScreen() {
		return arrayList("GuideScreenMapper.listScreen", null);
	}

	public Integer refreshLastTime(GuideScreen gs) throws Exception {
		return update("GuideScreenMapper.refreshLastTime", gs);
	}

	public Integer clean(String number) throws Exception {
		return update("GuideScreenMapper.clean", number);
	}

	public Integer editResult(GuideScreen gs) throws Exception {
		return update("GuideScreenMapper.editResult", gs);
	}

}
