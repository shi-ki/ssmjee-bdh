package com.jee.ssm.modules.guideScreen.services;

import com.jee.ssm.modules.guideScreen.dao.GuideScreenDao;
import com.jee.ssm.model.GuideScreen;
import com.jee.ssm.modules.ssm.services.BaseService;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
* 诱导屏管理 Service
* @author GaoXiang
* @version 1.0
*/
@Service(value = "guideScreenService")
public class GuideScreenService extends BaseService<GuideScreen> {

    @Resource
    private GuideScreenDao guideScreenDao;


    /**
    * 保存数据
    * @param guideScreen 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(GuideScreen guideScreen) throws Exception {
        return guideScreenDao.insert(guideScreen);
    }

    /**
    * 根据 id 修改
    * @param guideScreen 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(GuideScreen guideScreen) throws Exception {
        return guideScreenDao.updateById(guideScreen);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return guideScreenDao.deleteById(id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public GuideScreen selectById(String id) {
        return guideScreenDao.selectById(id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return guideScreenDao.deleteByIds(ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<GuideScreen> list(Map map) {
        return guideScreenDao.list(map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<GuideScreen> list(Map map,int page,int size) {
        return guideScreenDao.list(map,page,size);
    }

	public List<GuideScreen> findByNumber(String number) {
		return guideScreenDao.findByNumber(number);
	}

	public List<GuideScreen> listScreen() {
		return guideScreenDao.listScreen();
	}

	public Integer refreshLastTime(GuideScreen gs) throws Exception {
		return guideScreenDao.refreshLastTime(gs);
	}

	public Integer clean(String number) throws Exception {
		return guideScreenDao.clean(number);
	}

	public Integer editResult(GuideScreen gs) throws Exception {
		return guideScreenDao.editResult(gs);
	}


}
