package com.jee.ssm.modules.clientVersion.controller;

import com.jee.ssm.common.config.Const;
import com.jee.ssm.common.utils.ImageHelper;
import com.jee.ssm.model.Admin;
import com.jee.ssm.model.ClientVersion;
import com.jee.ssm.modules.clientVersion.services.ClientVersionService;
import com.jee.ssm.common.log.AdminControllerLog;
import com.jee.ssm.modules.ssm.controller.AdminBaseController;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.model.param.ParamMap;
import com.jee.ssm.common.utils.UUIDFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
* 版本信息管理 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/clientVersion")
public class ClientVersionController extends AdminBaseController<ClientVersion> {


    /**
     * 进入版本信息添加页面 携带一个生成的id --> longId
     * @param model 返回的实体容器
     * @return 添加页面
     */
    @RequestMapping(value="/add")
    @RequiresPermissions("clientVersion:add")
    public String add(ModelMap model){

        model.put("longId", UUIDFactory.getStringId());
        return "manager/clientVersion/add";
    }


    /**
     * 进入版本信息编辑页面
     * @param model 返回版本信息的容器
     * @param id 版本信息id
     * @return 编辑页面
     */
    @RequestMapping(value="/edit")
    @RequiresPermissions("clientVersion:edit")
    public String edit(ModelMap model, @RequestParam String id) {

        model.put("data",clientVersionService.selectById(id));
        return "manager/clientVersion/edit";
    }


    /**
     * 版本信息添加
     * @param clientVersion 带id的版本信息对象
     * @return 成功状态
     */
    @RequestMapping(value="/save")
    @ResponseBody
    @RequiresPermissions("clientVersion:add")
    public Tip changeHead(ClientVersion clientVersion,HttpServletRequest request) {
        int code = 0;
        ClientVersion clientVersion1 = clientVersionService.selectNewCode();
        if (clientVersion1!=null){
            String newCode = clientVersion1.getVersionCode();
            code = Integer.valueOf(newCode);
        }
        if ("".equals(clientVersion.getUrl())) {
            return new Tip(1); //请选择图片
        }else if (Integer.parseInt(clientVersion.getVersionCode())<=code){
            return new Tip(1,"版本号需大于当前最新版本！"); //请选择图片
        }
        else {
            clientVersion.setCreateTime(new Date());
            clientVersion.setStatus(1);
            clientVersion.setType(1);
        }

        try {
            clientVersionService.insert(clientVersion);
            return new Tip();
        } catch (Exception e) {
            e.printStackTrace();
            return new Tip(1,"上传失败");
        }
    }


    /**
     * 根据 id 修改版本信息
     * @param clientVersion 带id的版本信息对象
     * @return 成功状态
     */
    @RequestMapping(value="/update")
    @ResponseBody
    @RequiresPermissions("clientVersion:edit")
    @AdminControllerLog(description = "修改版本信息" )
    public Tip update(ClientVersion clientVersion) {

        try {
            clientVersionService.updateById(clientVersion);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"修改失败！");
        }

    }


    /**
     * 根据 id 删除版本信息
     * @param id 版本信息id
     * @return 成功状态
     */
    @RequestMapping(value="/delete")
    @ResponseBody
    @RequiresPermissions("clientVersion:delete")
    @AdminControllerLog(description = "删除版本信息" )
    public Tip delete(@RequestParam String id,HttpServletRequest request) {
        ClientVersion clientVersion = clientVersionService.selectById(id);
        String fileName = clientVersion.getFileName();
        String filePath = "/download/version/";
        String path = request.getServletContext().getRealPath(filePath);
        File file = new File(path+fileName);
        file.delete();
        try {
            clientVersionService.deleteById(id);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"删除失败！");
        }

    }


    /**
     * 根据 id 列表批量删除
     * @param ids 版本信息id List
     * @return 成功状态
     */
    @RequestMapping(value="/deleteByIds")
    @ResponseBody
    @RequiresPermissions("clientVersion:delete")
    @AdminControllerLog(description = "批量删除版本信息" )
    public Tip deleteByIds(@RequestParam("ids") List<String> ids) {

        try {
            clientVersionService.deleteByIds(ids);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"批量删除失败！");
        }

    }


    /**
     * 根据 id 查找版本信息
     * @param id 版本信息id
     * @return 版本信息对象 json
     */
    @RequestMapping(value="/findJson")
    @ResponseBody
    @RequiresPermissions("clientVersion:list")
    public ClientVersion find(@RequestParam String id) {

        return clientVersionService.selectById(id);
    }


    /**
     * 获取版本信息列表 获取全部 不分页
     * @param request 请求参数
     * @return 版本信息列表页面
     */
    @RequestMapping(value="/all")
    @RequiresPermissions("clientVersion:list")
    public String all(HttpServletRequest request,ModelMap modelMap) {

        return list(request,modelMap,1,0);
    }


    /**
     * 获取版本信息列表 分页
     * @param request 请求参数
     * @param page 第几页
     * @param size 每页大小
     * @return 版本信息列表页面
     */
    @RequestMapping(value="/list")
    @RequiresPermissions("clientVersion:list")
    public String list(HttpServletRequest request,ModelMap modelMap,Integer page,Integer size) {

        modelMap.put("pageInfo",clientVersionService.list(new ParamMap(request),page,size));
        modelMap.putAll(new ParamMap(request));
        return "manager/clientVersion/list";
    }

    /**
     * 上传文件到临时文件目录，此目录清空不会影响程序运行和文件丢失。
     * @param file 文件流，单个文件
     * @return
     */
    @RequestMapping("/uploadVersionImage")
    @ResponseBody
    public Tip uploadVersionImage( @RequestParam(value = "file") MultipartFile file, HttpServletRequest request ){

        if (file != null) {
                String fileName = file.getOriginalFilename();
                String saveName = "约慧夏都" + fileName.substring(fileName.lastIndexOf("."));
                String filePath = "/download/version/";
                String path = request.getServletContext().getRealPath(filePath);
                File directory = new File(path);
                //验证地址
                if (!directory.isDirectory() && !directory.exists()){
                    directory.mkdirs();
                }
                path = path + saveName;
                File versionImg = new File(path);
            try {
                file.transferTo(versionImg);
                return new Tip(saveName);
            } catch (IOException e) {
                e.printStackTrace();
                return new Tip(2);
            }
        } else {
            return new Tip(1);
        }
    }



    //---------------------------- property -------------------------------

    @Resource
    private ClientVersionService clientVersionService;

}
