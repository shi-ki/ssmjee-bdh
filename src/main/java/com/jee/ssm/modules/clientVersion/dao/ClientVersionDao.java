package com.jee.ssm.modules.clientVersion.dao;

import com.jee.ssm.model.ClientVersion;
import com.jee.ssm.modules.ssm.dao.LzDao;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;

/**
* 版本信息管理 Dao
* @author GaoXiang
* @version 1.0
*/
@Repository
public class ClientVersionDao extends LzDao<ClientVersion> {



    /**
    * 保存数据
    * @param clientVersion 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(ClientVersion clientVersion) throws Exception {
        return insert("ClientVersionMapper.insert",clientVersion);
    }

    /**
    * 根据 id 修改
    * @param clientVersion 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(ClientVersion clientVersion) throws Exception {
        return update("ClientVersionMapper.updateById",clientVersion);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return delete("ClientVersionMapper.deleteById",id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public ClientVersion selectById(String id) {
        return selectOne("ClientVersionMapper.selectById",id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return delete("ClientVersionMapper.deleteByIds",ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<ClientVersion> list(Map map) {
        return list("ClientVersionMapper.list",map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<ClientVersion> list(Map map,int page,int size) {
        return list("ClientVersionMapper.list",map,new RowBounds(page,size));
    }

    /**
     * 查询列表 带分页
     * @param page 页码
     * @param size 每页带澳
     * @return 列表
     */
    public PageInfo<ClientVersion> queryAllClientVersion(Integer page, Integer size) {
        return list("ClientVersionMapper.queryAllClientVersion",new RowBounds(page,size));
    }

    /**
     * 查询上传时间最晚的版本
     * @return
     */
    public ClientVersion selectNewClientVersion() {
        return selectOne("ClientVersionMapper.selectNewClientVersion",null);
    }

    /**
     * 根据版本号查询版本信息
     * @param code 版本号（纯数字）
     * @return
     */
    public ClientVersion selectByCode(String code) {
        return selectOne("ClientVersionMapper.selectByCode",code);
    }

    /**
     * 查询版本号最新的版本信息
     * @return
     */
    public ClientVersion selectNewCode() {
        return selectOne("ClientVersionMapper.selectNewCode",null);
    }
}
