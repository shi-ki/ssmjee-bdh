package com.jee.ssm.modules.clientVersion.services;

import com.jee.ssm.modules.clientVersion.dao.ClientVersionDao;
import com.jee.ssm.model.ClientVersion;
import com.jee.ssm.modules.ssm.services.BaseService;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
* 版本信息管理 Service
* @author GaoXiang
* @version 1.0
*/
@Service
public class ClientVersionService extends BaseService<ClientVersion> {

    @Resource
    private ClientVersionDao clientVersionDao;


    /**
    * 保存数据
    * @param clientVersion 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(ClientVersion clientVersion) throws Exception {
        return clientVersionDao.insert(clientVersion);
    }

    /**
    * 根据 id 修改
    * @param clientVersion 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(ClientVersion clientVersion) throws Exception {
        return clientVersionDao.updateById(clientVersion);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return clientVersionDao.deleteById(id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public ClientVersion selectById(String id) {
        return clientVersionDao.selectById(id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return clientVersionDao.deleteByIds(ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<ClientVersion> list(Map map) {
        return clientVersionDao.list(map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<ClientVersion> list(Map map,int page,int size) {
        return clientVersionDao.list(map,page,size);
    }


    /**
     * 查询列表 带分页
     * @param page 页码
     * @param size 每页带澳
     * @return 列表
     */
    public PageInfo<ClientVersion> queryAllClientVersion(Integer page, Integer size) {
        return clientVersionDao.queryAllClientVersion(page,size);
    }

    /**
     * 查询上传时间最晚的版本
     * @return
     */
    public ClientVersion selectNewClientVersion() {
        return clientVersionDao.selectNewClientVersion();
    }

    /**
     *  根据版本号查询版本信息
     * @param code
     * @return
     */
    public ClientVersion selectByCode(String code) {
        return clientVersionDao.selectByCode(code);
    }

    /**
     * 查询版本号最新的版本信息
     * @return
     */
    public ClientVersion selectNewCode() {
        return clientVersionDao.selectNewCode();
    }
}
