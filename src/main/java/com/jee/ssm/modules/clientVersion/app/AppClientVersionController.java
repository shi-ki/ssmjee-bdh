package com.jee.ssm.modules.clientVersion.app;

import com.github.pagehelper.PageInfo;
import com.jee.ssm.common.utils.TokenUtils;
import com.jee.ssm.model.ClientVersion;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.modules.clientVersion.services.ClientVersionService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;

/**
* 版本信息管理 客户端 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/app/clientVersion")
public class AppClientVersionController {
    /**
     * 查询所有app版本，带分页
     * @param timestamp 时间
     * @param token 令牌
     * @return
     */
    @RequestMapping(value ="/versionDownload")
    @ResponseBody
    public Tip versionList (long timestamp, String token, String code){
        Tip tip = TokenUtils.verifyToken(timestamp, token);
        if(tip.getSuccess()) {
            ClientVersion clientVersion = clientVersionService.selectNewCode(); //查询版本号最新的版本
            int codeInt = Integer.parseInt(code);
            if (codeInt<Integer.parseInt(clientVersion.getVersionCode())){//当前版本不是最新版
                return new Tip("正在更新",clientVersion);
            }else if  (codeInt==Integer.parseInt(clientVersion.getVersionCode())){ //当前已是最新版本
                return new Tip(1,"当前已是最新版本");
            }else {
                return new Tip(1,"更新失败");
            }
        }
        return tip;
    }

    /**
     *  根据版本id下载对应版本
     * @param timestamp 时间
     * @param token 令牌
     * @param request
     * @param response
     * @throws IOException
     */
   /* @RequestMapping(value = "/versionDownload")
    public void versionDownLoad(String code , long timestamp, String token, HttpServletRequest request, HttpServletResponse response)throws IOException {
        Tip tip = TokenUtils.verifyToken(timestamp,token);
        if (tip.getSuccess()){
            ClientVersion clientVersion = clientVersionService.selectByCode(code);
            String url = clientVersion.getUrl();
            String fileName = url.substring(18);
            String path = request.getSession().getServletContext().getRealPath("download/version/")+fileName;
            InputStream bis = new BufferedInputStream(new FileInputStream(new File(path)));
            fileName = URLEncoder.encode(fileName,"UTF-8");
            response.addHeader("Content-Disposition","attachment;filename="+fileName);
            response.setContentType("multipart/form-data");
            BufferedOutputStream out = new BufferedOutputStream(response.getOutputStream());
            int len = 0;
            while((len = bis.read())!= -1){
                out.write(len);
                out.flush();
            }
            out.close();
            ClientVersion clientVersion1 = clientVersion;
            clientVersion1.setDownloadPath(path);
        }
    }*/


    //---------------------------- property -------------------------------

    @Resource
    private ClientVersionService clientVersionService;

}
