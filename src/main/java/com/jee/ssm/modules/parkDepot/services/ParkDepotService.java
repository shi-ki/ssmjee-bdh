package com.jee.ssm.modules.parkDepot.services;

import com.jee.ssm.modules.parkDepot.dao.ParkDepotDao;
import com.jee.ssm.model.ParkDepot;
import com.jee.ssm.modules.ssm.services.BaseService;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
* 停车场管理 Service
* @author GaoXiang
* @version 1.0
*/
@Service
public class ParkDepotService extends BaseService<ParkDepot> {

    @Resource
    private ParkDepotDao parkDepotDao;


    /**
    * 保存数据
    * @param parkDepot 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(ParkDepot parkDepot) throws Exception {
        return parkDepotDao.insert(parkDepot);
    }

    /**
    * 根据 id 修改
    * @param parkDepot 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(ParkDepot parkDepot) throws Exception {
        return parkDepotDao.updateById(parkDepot);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return parkDepotDao.deleteById(id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public ParkDepot selectById(String id) {
        return parkDepotDao.selectById(id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return parkDepotDao.deleteByIds(ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<ParkDepot> list(Map map) {
        return parkDepotDao.list(map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<ParkDepot> list(Map map,int page,int size) {
        return parkDepotDao.list(map,page,size);
    }


    public List<ParkDepot> getParkDepots() {
        return parkDepotDao.getParkDepots();
    }

    /**
     * 根据海康平台停车场编号查询停车场
     * @param parkCode 停车场编号
     * @return
     */
    public ParkDepot findByHikId(String parkCode) {
        return parkDepotDao.findByHikId(parkCode);
    }

    public Integer relateOneSelf(ParkDepot parkDepot) throws Exception {
        return parkDepotDao.relateOneSelf(parkDepot);
    }

	public List<ParkDepot> nearByParkDepot(ParkDepot parkDepot) {
		return parkDepotDao.nearByParkDepot(parkDepot);
	}

    public List<ParkDepot> searchNearByParkDepot(ParkDepot parkDepot) {
        return parkDepotDao.searchNearByParkDepot(parkDepot);
    }

    public List<ParkDepot> getParkDepotsByType(int type) {
        return parkDepotDao.getParkDepotsByType(type);
    }

    public List<ParkDepot> getParkDepotsAll() {
        return parkDepotDao.getParkDepotsAll();
    }
}
