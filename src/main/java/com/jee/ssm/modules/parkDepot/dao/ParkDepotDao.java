package com.jee.ssm.modules.parkDepot.dao;

import com.jee.ssm.model.ParkDepot;
import com.jee.ssm.modules.ssm.dao.LzDao;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;

/**
* 停车场管理 Dao
* @author GaoXiang
* @version 1.0
*/
@Repository
public class ParkDepotDao extends LzDao<ParkDepot> {



    /**
    * 保存数据
    * @param parkDepot 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(ParkDepot parkDepot) throws Exception {
        return insert("ParkDepotMapper.insert",parkDepot);
    }

    /**
    * 根据 id 修改
    * @param parkDepot 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(ParkDepot parkDepot) throws Exception {
        return update("ParkDepotMapper.updateById",parkDepot);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return delete("ParkDepotMapper.deleteById",id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public ParkDepot selectById(String id) {
        return selectOne("ParkDepotMapper.selectById",id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return delete("ParkDepotMapper.deleteByIds",ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<ParkDepot> list(Map map) {
        return list("ParkDepotMapper.list",map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<ParkDepot> list(Map map,int page,int size) {
        return list("ParkDepotMapper.list",map,new RowBounds(page,size));
    }

    public List<ParkDepot> getParkDepots() {
        return arrayList("ParkDepotMapper.getParkDepots", null);
    }

    public ParkDepot findByHikId(String parkCode) {
        return selectOne("ParkDepotMapper.findByHikId", parkCode);
    }

    public Integer relateOneSelf(ParkDepot parkDepot) throws Exception {
        return insert("ParkDepotMapper.relateOneSelf", parkDepot);
    }

	public List<ParkDepot> nearByParkDepot(ParkDepot parkDepot) {
		return arrayList("ParkDepotMapper.nearByParkDepot", parkDepot);
	}

    public List<ParkDepot> searchNearByParkDepot(ParkDepot parkDepot) {
        return arrayList("ParkDepotMapper.searchNearByParkDepot",parkDepot);
    }

    public List<ParkDepot> getParkDepotsByType(int type) {
        return arrayList("ParkDepotMapper.getParkDepotsByType",type);
    }

    public List<ParkDepot> getParkDepotsAll() {
        return arrayList("ParkDepotMapper.getParkDepotsAll",null);
    }
}
