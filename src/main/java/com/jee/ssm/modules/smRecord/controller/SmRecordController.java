package com.jee.ssm.modules.smRecord.controller;

import com.jee.ssm.model.SmRecord;
import com.jee.ssm.modules.smRecord.services.SmRecordService;
import com.jee.ssm.common.log.AdminControllerLog;
import com.jee.ssm.modules.ssm.controller.AdminBaseController;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.model.param.ParamMap;
import com.jee.ssm.common.utils.UUIDFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
* 短信记录管理 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/smRecord")
public class SmRecordController extends AdminBaseController<SmRecord> {


    /**
     * 进入短信记录添加页面 携带一个生成的id --> longId
     * @param model 返回的实体容器
     * @return 添加页面
     */
    @RequestMapping(value="/add")
    @RequiresPermissions("smRecord:add")
    public String add(ModelMap model){

        model.put("longId", UUIDFactory.getStringId());
        return "manager/smRecord/add";
    }


    /**
     * 进入短信记录编辑页面
     * @param model 返回短信记录的容器
     * @param id 短信记录id
     * @return 编辑页面
     */
    @RequestMapping(value="/edit")
    @RequiresPermissions("smRecord:edit")
    public String edit(ModelMap model, @RequestParam String id) {

        model.put("data",smRecordService.selectById(id));
        return "manager/smRecord/edit";
    }


    /**
     * 短信记录添加
     * @param smRecord 带id的短信记录对象
     * @return 成功状态
     */
    @RequestMapping(value="/save")
    @ResponseBody
    @RequiresPermissions("smRecord:add")
    @AdminControllerLog(description = "添加短信记录" )
    public Tip save(SmRecord smRecord)  {

        try {
            smRecordService.insert(smRecord);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"添加失败！");
        }

    }


    /**
     * 根据 id 修改短信记录
     * @param smRecord 带id的短信记录对象
     * @return 成功状态
     */
    @RequestMapping(value="/update")
    @ResponseBody
    @RequiresPermissions("smRecord:edit")
    @AdminControllerLog(description = "修改短信记录" )
    public Tip update(SmRecord smRecord) {

        try {
            smRecordService.updateById(smRecord);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"修改失败！");
        }

    }


    /**
     * 根据 id 删除短信记录
     * @param id 短信记录id
     * @return 成功状态
     */
    @RequestMapping(value="/delete")
    @ResponseBody
    @RequiresPermissions("smRecord:delete")
    @AdminControllerLog(description = "删除短信记录" )
    public Tip delete(@RequestParam String id) {

        try {
            smRecordService.deleteById(id);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"删除失败！");
        }

    }


    /**
     * 根据 id 列表批量删除
     * @param ids 短信记录id List
     * @return 成功状态
     */
    @RequestMapping(value="/deleteByIds")
    @ResponseBody
    @RequiresPermissions("smRecord:delete")
    @AdminControllerLog(description = "批量删除短信记录" )
    public Tip deleteByIds(@RequestParam("ids") List<String> ids) {

        try {
            smRecordService.deleteByIds(ids);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"批量删除失败！");
        }

    }


    /**
     * 根据 id 查找短信记录
     * @param id 短信记录id
     * @return 短信记录对象 json
     */
    @RequestMapping(value="/findJson")
    @ResponseBody
    @RequiresPermissions("smRecord:list")
    public SmRecord find(@RequestParam String id) {

        return smRecordService.selectById(id);
    }


    /**
     * 获取短信记录列表 获取全部 不分页
     * @param request 请求参数
     * @return 短信记录列表页面
     */
    @RequestMapping(value="/all")
    @RequiresPermissions("smRecord:list")
    public String all(HttpServletRequest request,ModelMap modelMap) {

        return list(request,modelMap,1,0);
    }


    /**
     * 获取短信记录列表 分页
     * @param request 请求参数
     * @param page 第几页
     * @param size 每页大小
     * @return 短信记录列表页面
     */
    @RequestMapping(value="/list")
    @RequiresPermissions("smRecord:list")
    public String list(HttpServletRequest request,ModelMap modelMap,Integer page,Integer size) {

        modelMap.put("pageInfo",smRecordService.list(new ParamMap(request),page,size));
        modelMap.putAll(new ParamMap(request));
        return "manager/smRecord/list";
    }


    //---------------------------- property -------------------------------

    @Resource
    private SmRecordService smRecordService;

}
