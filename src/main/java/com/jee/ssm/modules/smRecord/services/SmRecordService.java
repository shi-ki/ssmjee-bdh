package com.jee.ssm.modules.smRecord.services;

import com.jee.ssm.modules.smRecord.dao.SmRecordDao;
import com.jee.ssm.model.SmRecord;
import com.jee.ssm.modules.ssm.services.BaseService;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
* 短信记录管理 Service
* @author GaoXiang
* @version 1.0
*/
@Service
public class SmRecordService extends BaseService<SmRecord> {

    @Resource
    private SmRecordDao smRecordDao;


    /**
    * 保存数据
    * @param smRecord 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(SmRecord smRecord) throws Exception {
        return smRecordDao.insert(smRecord);
    }

    /**
    * 根据 id 修改
    * @param smRecord 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(SmRecord smRecord) throws Exception {
        return smRecordDao.updateById(smRecord);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return smRecordDao.deleteById(id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public SmRecord selectById(String id) {
        return smRecordDao.selectById(id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return smRecordDao.deleteByIds(ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<SmRecord> list(Map map) {
        return smRecordDao.list(map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<SmRecord> list(Map map,int page,int size) {
        return smRecordDao.list(map,page,size);
    }


    public void updateSuccessById(String id) throws Exception {
        smRecordDao.updateSuccessById(id);
    }

    public List<SmRecord> findByPhone(SmRecord sr) {
        return smRecordDao.findByPhone(sr);
    }
}
