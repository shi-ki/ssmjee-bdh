package com.jee.ssm.modules.clearCity.app;

import com.jee.ssm.common.utils.TokenUtils;
import com.jee.ssm.model.ClearCity;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.modules.clearCity.services.ClearCityService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
* 开通城市管理 客户端 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/app/clearCity")
public class AppClearCityController {

    @RequestMapping(value = "/clearCity")
    @ResponseBody
    public Tip clearCity(long timestamp, String token){
        Tip tip = TokenUtils.verifyToken(timestamp,token);
        if (tip.getSuccess()){
            List<ClearCity> clearCities = clearCityService.selectAll();
            tip.setData(clearCities);
        }
        return tip;
    }





    //---------------------------- property -------------------------------

    @Resource
    private ClearCityService clearCityService;

}
