package com.jee.ssm.modules.clearCity.services;

import com.jee.ssm.modules.clearCity.dao.ClearCityDao;
import com.jee.ssm.model.ClearCity;
import com.jee.ssm.modules.ssm.services.BaseService;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
* 开通城市管理 Service
* @author GaoXiang
* @version 1.0
*/
@Service
public class ClearCityService extends BaseService<ClearCity> {

    @Resource
    private ClearCityDao clearCityDao;


    /**
    * 保存数据
    * @param clearCity 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(ClearCity clearCity) throws Exception {
        return clearCityDao.insert(clearCity);
    }

    /**
    * 根据 id 修改
    * @param clearCity 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(ClearCity clearCity) throws Exception {
        return clearCityDao.updateById(clearCity);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return clearCityDao.deleteById(id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public ClearCity selectById(String id) {
        return clearCityDao.selectById(id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return clearCityDao.deleteByIds(ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<ClearCity> list(Map map) {
        return clearCityDao.list(map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<ClearCity> list(Map map,int page,int size) {
        return clearCityDao.list(map,page,size);
    }


    public List<ClearCity> selectAll() {
        return clearCityDao.selectAll();
    }
}
