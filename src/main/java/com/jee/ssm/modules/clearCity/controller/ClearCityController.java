package com.jee.ssm.modules.clearCity.controller;

import com.jee.ssm.model.City;
import com.jee.ssm.model.ClearCity;
import com.jee.ssm.modules.clearCity.services.ClearCityService;
import com.jee.ssm.common.log.AdminControllerLog;
import com.jee.ssm.modules.ssm.controller.AdminBaseController;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.model.param.ParamMap;
import com.jee.ssm.common.utils.UUIDFactory;
import com.jee.ssm.modules.ssm.services.CityService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
* 开通城市管理 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/clearCity")
public class ClearCityController extends AdminBaseController<ClearCity> {


    /**
     * 进入开通城市添加页面 携带一个生成的id --> longId
     * @param model 返回的实体容器
     * @return 添加页面
     */
    @RequestMapping(value="/add")
    @RequiresPermissions("clearCity:add")
    public String add(ModelMap model){

        List<City> cityList = cityService.selectCity();
        model.put("city",cityList);
        model.put("longId", UUIDFactory.getStringId());

        return "manager/clearCity/add";
    }


    /**
     * 进入开通城市编辑页面
     * @param model 返回开通城市的容器
     * @param id 开通城市id
     * @return 编辑页面
     */
    @RequestMapping(value="/edit")
    @RequiresPermissions("clearCity:edit")
    public String edit(ModelMap model, @RequestParam String id) {

        model.put("data",clearCityService.selectById(id));
        return "manager/clearCity/edit";
    }


    /**
     * 开通城市添加
     * @param clearCity 带id的开通城市对象
     * @return 成功状态
     */
    @RequestMapping(value="/save")
    @ResponseBody
    @RequiresPermissions("clearCity:add")
    @AdminControllerLog(description = "添加开通城市" )
    public Tip save(ClearCity clearCity)  {

        try {
            clearCityService.insert(clearCity);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"添加失败！");
        }

    }


    /**
     * 根据 id 修改开通城市
     * @param clearCity 带id的开通城市对象
     * @return 成功状态
     */
    @RequestMapping(value="/update")
    @ResponseBody
    @RequiresPermissions("clearCity:edit")
    @AdminControllerLog(description = "修改开通城市" )
    public Tip update(ClearCity clearCity) {

        try {
            clearCityService.updateById(clearCity);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"修改失败！");
        }

    }


    /**
     * 根据 id 删除开通城市
     * @param id 开通城市id
     * @return 成功状态
     */
    @RequestMapping(value="/delete")
    @ResponseBody
    @RequiresPermissions("clearCity:delete")
    @AdminControllerLog(description = "删除开通城市" )
    public Tip delete(@RequestParam String id) {

        try {
            clearCityService.deleteById(id);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"删除失败！");
        }

    }


    /**
     * 根据 id 列表批量删除
     * @param ids 开通城市id List
     * @return 成功状态
     */
    @RequestMapping(value="/deleteByIds")
    @ResponseBody
    @RequiresPermissions("clearCity:delete")
    @AdminControllerLog(description = "批量删除开通城市" )
    public Tip deleteByIds(@RequestParam("ids") List<String> ids) {

        try {
            clearCityService.deleteByIds(ids);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"批量删除失败！");
        }

    }


    /**
     * 根据 id 查找开通城市
     * @param id 开通城市id
     * @return 开通城市对象 json
     */
    @RequestMapping(value="/findJson")
    @ResponseBody
    @RequiresPermissions("clearCity:list")
    public ClearCity find(@RequestParam String id) {

        return clearCityService.selectById(id);
    }


    /**
     * 获取开通城市列表 获取全部 不分页
     * @param request 请求参数
     * @return 开通城市列表页面
     */
    @RequestMapping(value="/all")
    @RequiresPermissions("clearCity:list")
    public String all(HttpServletRequest request,ModelMap modelMap) {

        return list(request,modelMap,1,0);
    }


    /**
     * 获取开通城市列表 分页
     * @param request 请求参数
     * @param page 第几页
     * @param size 每页大小
     * @return 开通城市列表页面
     */
    @RequestMapping(value="/list")
    @RequiresPermissions("clearCity:list")
    public String list(HttpServletRequest request,ModelMap modelMap,Integer page,Integer size) {

        modelMap.put("pageInfo",clearCityService.list(new ParamMap(request),page,size));
        modelMap.putAll(new ParamMap(request));
        return "manager/clearCity/list";
    }


    //---------------------------- property -------------------------------

    @Resource
    private ClearCityService clearCityService;

    @Resource
    private CityService cityService;

}
