package com.jee.ssm.modules.systemMessage.app;

import com.github.pagehelper.PageInfo;
import com.jee.ssm.common.utils.TokenUtils;
import com.jee.ssm.model.SystemMessage;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.modules.systemMessage.services.SystemMessageService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
* 系统消息管理 客户端 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/app/systemMessage")
public class AppSystemMessageController {

	/**
	 * 分页查询系统消息
	 * @param timestamp 时间
	 * @param token 令牌
	 * @param page 当前页码
	 * @param size 每页条数
	 * @return
	 */
	@RequestMapping(value = "/findAllByPage", method = RequestMethod.GET)
	@ResponseBody
	public Tip findAllByPage(long timestamp, String token, Integer page, Integer size) {
		Tip tip = TokenUtils.verifyToken(timestamp, token);
        if(tip.getSuccess()) {
        	PageInfo<SystemMessage> list = systemMessageService.findAllByPage(page, size);
        	tip = new Tip(list.getList());
        }
        return tip;
	}

	/**
	 * 根据id查询消息详情
	 * @param timestamp 时间
	 * @param token 令牌
	 * @param id 消息id
	 * @return
	 */
	@RequestMapping(value = "/checkDetailById", method = RequestMethod.GET)
	@ResponseBody
	public Tip checkDetailById(long timestamp, String token, String id) {
		Tip tip = TokenUtils.verifyToken(timestamp, token);
        if(tip.getSuccess()) {
        	SystemMessage pm = systemMessageService.selectById(id);
        	tip = new Tip(pm);
        }
        return tip;
	}

    //---------------------------- property -------------------------------

    @Resource
    private SystemMessageService systemMessageService;

}
