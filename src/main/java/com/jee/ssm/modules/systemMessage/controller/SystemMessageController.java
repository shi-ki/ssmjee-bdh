package com.jee.ssm.modules.systemMessage.controller;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jee.ssm.common.config.Const;
import com.jee.ssm.common.log.AdminControllerLog;
import com.jee.ssm.common.utils.PushUtils;
import com.jee.ssm.common.utils.UUIDFactory;
import com.jee.ssm.model.Account;
import com.jee.ssm.model.SystemMessage;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.model.param.ParamMap;
import com.jee.ssm.modules.ssm.controller.AdminBaseController;
import com.jee.ssm.modules.systemMessage.services.SystemMessageService;

/**
* 系统消息管理 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/systemMessage")
public class SystemMessageController extends AdminBaseController<SystemMessage> {


    /**
     * 进入系统消息添加页面 携带一个生成的id --> longId
     * @param model 返回的实体容器
     * @return 添加页面
     */
    @RequestMapping(value="/add")
    @RequiresPermissions("systemMessage:add")
    public String add(ModelMap model){

        model.put("longId", UUIDFactory.getStringId());
        return "manager/systemMessage/add";
    }


    /**
     * 进入系统消息编辑页面
     * @param model 返回系统消息的容器
     * @param id 系统消息id
     * @return 编辑页面
     */
    @RequestMapping(value="/edit")
    @RequiresPermissions("systemMessage:edit")
    public String edit(ModelMap model, @RequestParam String id) {

        model.put("data",systemMessageService.selectById(id));
        return "manager/systemMessage/edit";
    }


    /**
     * 系统消息添加
     * @param systemMessage 带id的系统消息对象
     * @return 成功状态
     */
    @RequestMapping(value="/save")
    @ResponseBody
    @RequiresPermissions("systemMessage:add")
    @AdminControllerLog(description = "添加系统消息" )
    public Tip save(SystemMessage systemMessage, HttpServletRequest request)  {

        try {
        	Account account = (Account) request.getSession().getAttribute(Const.ACCOUNT);
        	systemMessage.setCreateTime(new Date());
        	systemMessage.setStatus(0);
        	systemMessage.setType(0);
        	systemMessage.setUserId(account.getId());
        	systemMessage.setUserName(account.getUserName());
            systemMessageService.insert(systemMessage);
            PushUtils.pushSystemMessage(systemMessage.getTitle());
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"添加失败！");
        }

    }


    /**
     * 根据 id 修改系统消息
     * @param systemMessage 带id的系统消息对象
     * @return 成功状态
     */
    @RequestMapping(value="/update")
    @ResponseBody
    @RequiresPermissions("systemMessage:edit")
    @AdminControllerLog(description = "修改系统消息" )
    public Tip update(SystemMessage systemMessage) {

        try {
            systemMessageService.updateById(systemMessage);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"修改失败！");
        }

    }


    /**
     * 根据 id 删除系统消息
     * @param id 系统消息id
     * @return 成功状态
     */
    @RequestMapping(value="/delete")
    @ResponseBody
    @RequiresPermissions("systemMessage:delete")
    @AdminControllerLog(description = "删除系统消息" )
    public Tip delete(@RequestParam String id) {

        try {
            systemMessageService.deleteById(id);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"删除失败！");
        }

    }


    /**
     * 根据 id 列表批量删除
     * @param ids 系统消息id List
     * @return 成功状态
     */
    @RequestMapping(value="/deleteByIds")
    @ResponseBody
    @RequiresPermissions("systemMessage:delete")
    @AdminControllerLog(description = "批量删除系统消息" )
    public Tip deleteByIds(@RequestParam("ids") List<String> ids) {

        try {
            systemMessageService.deleteByIds(ids);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"批量删除失败！");
        }

    }


    /**
     * 根据 id 查找系统消息
     * @param id 系统消息id
     * @return 系统消息对象 json
     */
    @RequestMapping(value="/findJson")
    @ResponseBody
    @RequiresPermissions("systemMessage:list")
    public SystemMessage find(@RequestParam String id) {

        return systemMessageService.selectById(id);
    }


    /**
     * 获取系统消息列表 获取全部 不分页
     * @param request 请求参数
     * @return 系统消息列表页面
     */
    @RequestMapping(value="/all")
    @RequiresPermissions("systemMessage:list")
    public String all(HttpServletRequest request,ModelMap modelMap) {

        return list(request,modelMap,1,0);
    }


    /**
     * 获取系统消息列表 分页
     * @param request 请求参数
     * @param page 第几页
     * @param size 每页大小
     * @return 系统消息列表页面
     */
    @RequestMapping(value="/list")
    @RequiresPermissions("systemMessage:list")
    public String list(HttpServletRequest request,ModelMap modelMap,Integer page,Integer size) {

        modelMap.put("pageInfo",systemMessageService.list(new ParamMap(request),page,size));
        modelMap.putAll(new ParamMap(request));
        return "manager/systemMessage/list";
    }


    //---------------------------- property -------------------------------

    @Resource
    private SystemMessageService systemMessageService;

}
