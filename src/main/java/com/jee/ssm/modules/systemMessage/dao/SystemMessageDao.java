package com.jee.ssm.modules.systemMessage.dao;

import com.jee.ssm.model.SystemMessage;
import com.jee.ssm.modules.ssm.dao.LzDao;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;

/**
* 系统消息管理 Dao
* @author GaoXiang
* @version 1.0
*/
@Repository
public class SystemMessageDao extends LzDao<SystemMessage> {



    /**
    * 保存数据
    * @param systemMessage 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(SystemMessage systemMessage) throws Exception {
        return insert("SystemMessageMapper.insert",systemMessage);
    }

    /**
    * 根据 id 修改
    * @param systemMessage 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(SystemMessage systemMessage) throws Exception {
        return update("SystemMessageMapper.updateById",systemMessage);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return delete("SystemMessageMapper.deleteById",id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public SystemMessage selectById(String id) {
        return selectOne("SystemMessageMapper.selectById",id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return delete("SystemMessageMapper.deleteByIds",ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<SystemMessage> list(Map map) {
        return list("SystemMessageMapper.list",map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<SystemMessage> list(Map map,int page,int size) {
        return list("SystemMessageMapper.list",map,new RowBounds(page,size));
    }

	public PageInfo<SystemMessage> findAllByPage(Integer page, Integer size) {
		return list("SystemMessageMapper.findAllByPage", null, new RowBounds(page,size));
	}

}
