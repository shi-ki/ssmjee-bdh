package com.jee.ssm.modules.reservationRecord.app;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.jee.ssm.common.utils.TokenUtils;
import com.jee.ssm.model.ReservationRecord;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.modules.reservationRecord.services.ReservationRecordService;

/**
* 停车预约记录管理 客户端 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/app/reservationRecord")
public class AppReservationRecordController {


	
	@RequestMapping(value = "/findByUserId", method = RequestMethod.GET)
	@ResponseBody
	public Tip findByUserId(long timestamp, String token, String userId, Integer page, Integer size) {
		Tip tip = TokenUtils.verifyToken(timestamp, token);
		if(tip.getSuccess()) {
			PageInfo<ReservationRecord> p = reservationRecordService.findByUserId(userId, page, size);
			tip = new Tip(p.getList());
		}
		return tip;
	}
	
	/**
	 * 查找用户当前预约
	 * @param userId 用户标识
	 * @return 预约记录
	 */
	@RequestMapping(value = "/findReservingByUserId", method = RequestMethod.GET)
	@ResponseBody
	public Tip findReservingByUserId(long timestamp, String token, String userId) {
		Tip tip = TokenUtils.verifyToken(timestamp, token);
		if(tip.getSuccess()) {
			ReservationRecord rr = new ReservationRecord(userId, 1);
			List<ReservationRecord> list = reservationRecordService.findByUserIdAndStatus(rr);
			tip = new Tip(list);
		}
		return tip;
	}



    //---------------------------- property -------------------------------

    @Resource
    private ReservationRecordService reservationRecordService;

}
