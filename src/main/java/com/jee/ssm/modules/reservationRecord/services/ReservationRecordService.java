package com.jee.ssm.modules.reservationRecord.services;

import com.jee.ssm.modules.reservationRecord.dao.ReservationRecordDao;
import com.jee.ssm.model.ReservationRecord;
import com.jee.ssm.modules.ssm.services.BaseService;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
* 停车预约记录管理 Service
* @author GaoXiang
* @version 1.0
*/
@Service
public class ReservationRecordService extends BaseService<ReservationRecord> {

    @Resource
    private ReservationRecordDao reservationRecordDao;


    /**
    * 保存数据
    * @param reservationRecord 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(ReservationRecord reservationRecord) throws Exception {
        return reservationRecordDao.insert(reservationRecord);
    }

    /**
    * 根据 id 修改
    * @param reservationRecord 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(ReservationRecord reservationRecord) throws Exception {
        return reservationRecordDao.updateById(reservationRecord);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return reservationRecordDao.deleteById(id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public ReservationRecord selectById(String id) {
        return reservationRecordDao.selectById(id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return reservationRecordDao.deleteByIds(ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<ReservationRecord> list(Map map) {
        return reservationRecordDao.list(map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<ReservationRecord> list(Map map,int page,int size) {
        return reservationRecordDao.list(map,page,size);
    }

	public PageInfo<ReservationRecord> findByUserId(String userId, Integer page, Integer size) {
		return reservationRecordDao.findByUserId(userId, page, size);
	}

	public List<ReservationRecord> findByUserIdAndStatus(ReservationRecord rr) {
		return reservationRecordDao.findByUserIdAndStatus(rr);
	}

	public List<ReservationRecord> listReservationRecordWithStatus(ReservationRecord rr) {
		return reservationRecordDao.listReservationRecordWithStatus(rr);
	}

	public void editListStatus(List<String> sl) throws Exception {
		reservationRecordDao.editListStatus(sl);
	}


}
