package com.jee.ssm.modules.reservationRecord.controller;

import com.jee.ssm.model.ReservationRecord;
import com.jee.ssm.modules.reservationRecord.services.ReservationRecordService;
import com.jee.ssm.common.log.AdminControllerLog;
import com.jee.ssm.modules.ssm.controller.AdminBaseController;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.model.param.ParamMap;
import com.jee.ssm.common.utils.UUIDFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
* 停车预约记录管理 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/reservationRecord")
public class ReservationRecordController extends AdminBaseController<ReservationRecord> {


    /**
     * 进入停车预约记录添加页面 携带一个生成的id --> longId
     * @param model 返回的实体容器
     * @return 添加页面
     */
    @RequestMapping(value="/add")
    @RequiresPermissions("reservationRecord:add")
    public String add(ModelMap model){

        model.put("longId", UUIDFactory.getStringId());
        return "manager/reservationRecord/add";
    }


    /**
     * 进入停车预约记录编辑页面
     * @param model 返回停车预约记录的容器
     * @param id 停车预约记录id
     * @return 编辑页面
     */
    @RequestMapping(value="/edit")
    @RequiresPermissions("reservationRecord:edit")
    public String edit(ModelMap model, @RequestParam String id) {

        model.put("data",reservationRecordService.selectById(id));
        return "manager/reservationRecord/edit";
    }


    /**
     * 停车预约记录添加
     * @param reservationRecord 带id的停车预约记录对象
     * @return 成功状态
     */
    @RequestMapping(value="/save")
    @ResponseBody
    @RequiresPermissions("reservationRecord:add")
    @AdminControllerLog(description = "添加停车预约记录" )
    public Tip save(ReservationRecord reservationRecord)  {

        try {
            reservationRecordService.insert(reservationRecord);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"添加失败！");
        }

    }


    /**
     * 根据 id 修改停车预约记录
     * @param reservationRecord 带id的停车预约记录对象
     * @return 成功状态
     */
    @RequestMapping(value="/update")
    @ResponseBody
    @RequiresPermissions("reservationRecord:edit")
    @AdminControllerLog(description = "修改停车预约记录" )
    public Tip update(ReservationRecord reservationRecord) {

        try {
            reservationRecordService.updateById(reservationRecord);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"修改失败！");
        }

    }


    /**
     * 根据 id 删除停车预约记录
     * @param id 停车预约记录id
     * @return 成功状态
     */
    @RequestMapping(value="/delete")
    @ResponseBody
    @RequiresPermissions("reservationRecord:delete")
    @AdminControllerLog(description = "删除停车预约记录" )
    public Tip delete(@RequestParam String id) {

        try {
            reservationRecordService.deleteById(id);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"删除失败！");
        }

    }


    /**
     * 根据 id 列表批量删除
     * @param ids 停车预约记录id List
     * @return 成功状态
     */
    @RequestMapping(value="/deleteByIds")
    @ResponseBody
    @RequiresPermissions("reservationRecord:delete")
    @AdminControllerLog(description = "批量删除停车预约记录" )
    public Tip deleteByIds(@RequestParam("ids") List<String> ids) {

        try {
            reservationRecordService.deleteByIds(ids);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"批量删除失败！");
        }

    }


    /**
     * 根据 id 查找停车预约记录
     * @param id 停车预约记录id
     * @return 停车预约记录对象 json
     */
    @RequestMapping(value="/findJson")
    @ResponseBody
    @RequiresPermissions("reservationRecord:list")
    public ReservationRecord find(@RequestParam String id) {

        return reservationRecordService.selectById(id);
    }


    /**
     * 获取停车预约记录列表 获取全部 不分页
     * @param request 请求参数
     * @return 停车预约记录列表页面
     */
    @RequestMapping(value="/all")
    @RequiresPermissions("reservationRecord:list")
    public String all(HttpServletRequest request,ModelMap modelMap) {

        return list(request,modelMap,1,0);
    }


    /**
     * 获取停车预约记录列表 分页
     * @param request 请求参数
     * @param page 第几页
     * @param size 每页大小
     * @return 停车预约记录列表页面
     */
    @RequestMapping(value="/list")
    @RequiresPermissions("reservationRecord:list")
    public String list(HttpServletRequest request,ModelMap modelMap,Integer page,Integer size) {

        modelMap.put("pageInfo",reservationRecordService.list(new ParamMap(request),page,size));
        modelMap.putAll(new ParamMap(request));
        return "manager/reservationRecord/list";
    }


    //---------------------------- property -------------------------------

    @Resource
    private ReservationRecordService reservationRecordService;

}
