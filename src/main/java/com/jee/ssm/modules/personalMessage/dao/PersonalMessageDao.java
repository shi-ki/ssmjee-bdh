package com.jee.ssm.modules.personalMessage.dao;

import com.jee.ssm.model.PersonalMessage;
import com.jee.ssm.modules.ssm.dao.LzDao;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;

/**
* 用户消息管理 Dao
* @author GaoXiang
* @version 1.0
*/
@Repository
public class PersonalMessageDao extends LzDao<PersonalMessage> {



    /**
    * 保存数据
    * @param personalMessage 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(PersonalMessage personalMessage) throws Exception {
        return insert("PersonalMessageMapper.insert",personalMessage);
    }

    /**
    * 根据 id 修改
    * @param personalMessage 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(PersonalMessage personalMessage) throws Exception {
        return update("PersonalMessageMapper.updateById",personalMessage);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return delete("PersonalMessageMapper.deleteById",id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public PersonalMessage selectById(String id) {
        return selectOne("PersonalMessageMapper.selectById",id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return delete("PersonalMessageMapper.deleteByIds",ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<PersonalMessage> list(Map map) {
        return list("PersonalMessageMapper.list",map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<PersonalMessage> list(Map map,int page,int size) {
        return list("PersonalMessageMapper.list",map,new RowBounds(page,size));
    }

	public PageInfo<PersonalMessage> findByUserId(String userId, Integer page, Integer size) {
		return list("PersonalMessageMapper.findByUserId", userId, new RowBounds(page,size));
	}

	public Integer editStatusById(PersonalMessage pm) throws Exception {
		return update("PersonalMessageMapper.editStatusById", pm);
	}

	public Integer editAllStatusByUserId(PersonalMessage pm) throws Exception {
		return update("PersonalMessageMapper.editAllStatusByUserId", pm);
	}

	public Integer findUnreadNumByUserId(String id) {
		return count("PersonalMessageMapper.findUnreadNumByUserId", id).intValue();
	}

}
