package com.jee.ssm.modules.personalMessage.services;

import com.jee.ssm.modules.personalMessage.dao.PersonalMessageDao;
import com.jee.ssm.model.PersonalMessage;
import com.jee.ssm.modules.ssm.services.BaseService;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
* 用户消息管理 Service
* @author GaoXiang
* @version 1.0
*/
@Service
public class PersonalMessageService extends BaseService<PersonalMessage> {

    @Resource
    private PersonalMessageDao personalMessageDao;


    /**
    * 保存数据
    * @param personalMessage 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(PersonalMessage personalMessage) throws Exception {
        return personalMessageDao.insert(personalMessage);
    }

    /**
    * 根据 id 修改
    * @param personalMessage 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(PersonalMessage personalMessage) throws Exception {
        return personalMessageDao.updateById(personalMessage);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return personalMessageDao.deleteById(id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public PersonalMessage selectById(String id) {
        return personalMessageDao.selectById(id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return personalMessageDao.deleteByIds(ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<PersonalMessage> list(Map map) {
        return personalMessageDao.list(map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<PersonalMessage> list(Map map,int page,int size) {
        return personalMessageDao.list(map,page,size);
    }

	public PageInfo<PersonalMessage> findByUserId(String userId, Integer page, Integer size) {
		return personalMessageDao.findByUserId(userId, page, size);
	}

	public Integer editStatusById(PersonalMessage pm) throws Exception {
		return personalMessageDao.editStatusById(pm);
	}

	public Integer editAllStatusByUserId(PersonalMessage pm) throws Exception {
		return personalMessageDao.editAllStatusByUserId(pm);
	}

	public Integer findUnreadNumByUserId(String id) {
		return personalMessageDao.findUnreadNumByUserId(id);
	}


}
