package com.jee.ssm.modules.personalMessage.app;

import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.jee.ssm.common.config.Logger;
import com.jee.ssm.common.utils.TokenUtils;
import com.jee.ssm.model.PersonalMessage;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.modules.personalMessage.services.PersonalMessageService;

/**
* 用户消息管理 客户端 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/app/personalMessage")
public class AppPersonalMessageController {

	/**
	 * 根据用户id分页查询消息列表
	 * @param timestamp 时间
	 * @param token 令牌
	 * @param userId 用户id
	 * @param page 当前页码
	 * @param size 每页条数
	 * @return
	 */
	@RequestMapping(value = "/findByUserId", method = RequestMethod.GET)
	@ResponseBody
	public Tip findByUserId(long timestamp, String token, String userId, Integer page, Integer size) {
		Tip tip = TokenUtils.verifyToken(timestamp, token);
        if(tip.getSuccess()) {
        	PageInfo<PersonalMessage> list = personalMessageService.findByUserId(userId, page, size);
        	tip = new Tip(list.getList());
        }
        return tip;
	}
	
	/**
	 * 根据id查询消息详情
	 * @param timestamp 时间
	 * @param token 令牌
	 * @param id 消息id
	 * @return
	 */
	@RequestMapping(value = "/checkDetailById", method = RequestMethod.GET)
	@ResponseBody
	public Tip checkDetailById(long timestamp, String token, String id) {
		Tip tip = TokenUtils.verifyToken(timestamp, token);
        if(tip.getSuccess()) {
        	PersonalMessage pm = personalMessageService.selectById(id);
        	if(pm != null) {
        		tip = new Tip(pm);
        		if(pm.getStatus() == 0) {
        			try {
            			pm.setStatus(1);
            			pm.setHandleTime(new Date());
                		personalMessageService.editStatusById(pm);
                	} catch(Exception e) {
                		Logger.info("修改已读状态失败");
                	}
        		}
        	} else {
        		tip = new Tip(1, "未查找到该条消息，请刷新");
        	}
        }
        return tip;
	}
	
	/**
	 * 根据用户id修改全部状态为已读
	 * @param timestamp
	 * @param token
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/editAllStatusByUserId", method = RequestMethod.GET)
	@ResponseBody
	public Tip editAllStatusByUserId(long timestamp, String token, String userId) {
		Tip tip = TokenUtils.verifyToken(timestamp, token);
        if(tip.getSuccess()) {
        	try {
        		PersonalMessage pm = new PersonalMessage();
        		pm.setStatus(1);
        		pm.setHandleTime(new Date());
        		personalMessageService.editAllStatusByUserId(pm);
        		tip = new Tip("修改成功");
        	} catch(Exception e) {
        		Logger.info("修改已读状态失败");
        		tip = new Tip("修改失败");
        	}
        }
        return tip;
	}
	
    //---------------------------- property -------------------------------

    @Resource
    private PersonalMessageService personalMessageService;

}
