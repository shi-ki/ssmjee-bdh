package com.jee.ssm.modules.personalMessage.controller;

import com.jee.ssm.common.utils.PushUtils;
import com.jee.ssm.model.PersonalMessage;
import com.jee.ssm.model.User;
import com.jee.ssm.modules.personalMessage.services.PersonalMessageService;
import com.jee.ssm.common.log.AdminControllerLog;
import com.jee.ssm.modules.ssm.controller.AdminBaseController;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.model.param.ParamMap;
import com.jee.ssm.common.utils.UUIDFactory;
import com.jee.ssm.modules.user.services.UserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
* 用户消息管理 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/personalMessage")
public class PersonalMessageController extends AdminBaseController<PersonalMessage> {


    /**
     * 进入用户消息添加页面 携带一个生成的id --> longId
     * @param model 返回的实体容器
     * @return 添加页面
     */
    @RequestMapping(value="/add")
    @RequiresPermissions("personalMessage:add")
    public String add(ModelMap model, String ids){
        model.put("ids", ids);
        return "manager/personalMessage/add";
    }


    /**
     * 进入用户消息编辑页面
     * @param model 返回用户消息的容器
     * @param id 用户消息id
     * @return 编辑页面
     */
    @RequestMapping(value="/edit")
    @RequiresPermissions("personalMessage:edit")
    public String edit(ModelMap model, @RequestParam String id) {

        model.put("data",personalMessageService.selectById(id));
        return "manager/personalMessage/edit";
    }


    /**
     * 用户消息添加
     * @param personalMessage 带id的用户消息对象
     * @return 成功状态
     */
    @RequestMapping(value="/save")
    @ResponseBody
    @RequiresPermissions("personalMessage:add")
    @AdminControllerLog(description = "添加用户消息" )
    public Tip save(PersonalMessage personalMessage)  {

        Tip tip = new Tip();
        String ids = personalMessage.getIds();
        if(ids != null && !"".equals(ids)) {
            String[] id = ids.split(",");
            Integer num = id.length;
            Integer s = 0;
            List<PersonalMessage> list = new ArrayList<>();
            for(String d : id) {
                User user = userService.selectById(d);
                if(user != null) {
                    PersonalMessage pm = new PersonalMessage(UUIDFactory.getStringId(), personalMessage.getTitle(), d, user.getPhone(), user.getUserName(), user.getName(), user.getIdNumber(), 0, personalMessage.getType(), new Date(), null, personalMessage.getContent());
                    list.add(pm);
                    try {
                        personalMessageService.insert(pm);
                        s ++;
                    } catch(Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            PushUtils.pushPersonalMessage(list);
            if(!num.equals(s)) {
                tip = new Tip(2, "发送成功数： " + s + "，发送失败数： " + (num - s));
            }

        } else {
            tip = new Tip(1, "选择用户失效，请刷新重试");
        }
        return tip;
    }


    /**
     * 根据 id 修改用户消息
     * @param personalMessage 带id的用户消息对象
     * @return 成功状态
     */
    @RequestMapping(value="/update")
    @ResponseBody
    @RequiresPermissions("personalMessage:edit")
    @AdminControllerLog(description = "修改用户消息" )
    public Tip update(PersonalMessage personalMessage) {

        try {
            personalMessageService.updateById(personalMessage);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"修改失败！");
        }

    }


    /**
     * 根据 id 删除用户消息
     * @param id 用户消息id
     * @return 成功状态
     */
    @RequestMapping(value="/delete")
    @ResponseBody
    @RequiresPermissions("personalMessage:delete")
    @AdminControllerLog(description = "删除用户消息" )
    public Tip delete(@RequestParam String id) {

        try {
            personalMessageService.deleteById(id);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"删除失败！");
        }

    }


    /**
     * 根据 id 列表批量删除
     * @param ids 用户消息id List
     * @return 成功状态
     */
    @RequestMapping(value="/deleteByIds")
    @ResponseBody
    @RequiresPermissions("personalMessage:delete")
    @AdminControllerLog(description = "批量删除用户消息" )
    public Tip deleteByIds(@RequestParam("ids") List<String> ids) {

        try {
            personalMessageService.deleteByIds(ids);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"批量删除失败！");
        }

    }


    /**
     * 根据 id 查找用户消息
     * @param id 用户消息id
     * @return 用户消息对象 json
     */
    @RequestMapping(value="/findJson")
    @ResponseBody
    @RequiresPermissions("personalMessage:list")
    public PersonalMessage find(@RequestParam String id) {

        return personalMessageService.selectById(id);
    }


    /**
     * 获取用户消息列表 获取全部 不分页
     * @param request 请求参数
     * @return 用户消息列表页面
     */
    @RequestMapping(value="/all")
    @RequiresPermissions("personalMessage:list")
    public String all(HttpServletRequest request,ModelMap modelMap) {

        return list(request,modelMap,1,0);
    }


    /**
     * 获取用户消息列表 分页
     * @param request 请求参数
     * @param page 第几页
     * @param size 每页大小
     * @return 用户消息列表页面
     */
    @RequestMapping(value="/list")
    @RequiresPermissions("personalMessage:list")
    public String list(HttpServletRequest request,ModelMap modelMap,Integer page,Integer size) {

        modelMap.put("pageInfo",personalMessageService.list(new ParamMap(request),page,size));
        modelMap.putAll(new ParamMap(request));
        return "manager/personalMessage/list";
    }


    //---------------------------- property -------------------------------

    @Resource
    private PersonalMessageService personalMessageService;

    @Resource
    private UserService userService;

}
