package com.jee.ssm.modules.userPlate.controller;

import com.jee.ssm.model.UserPlate;
import com.jee.ssm.modules.userPlate.services.UserPlateService;
import com.jee.ssm.common.log.AdminControllerLog;
import com.jee.ssm.modules.ssm.controller.AdminBaseController;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.model.param.ParamMap;
import com.jee.ssm.common.utils.UUIDFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
* 用户车牌信息管理 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/userPlate")
public class UserPlateController extends AdminBaseController<UserPlate> {


    /**
     * 进入用户车牌信息添加页面 携带一个生成的id --> longId
     * @param model 返回的实体容器
     * @return 添加页面
     */
    @RequestMapping(value="/add")
    @RequiresPermissions("userPlate:add")
    public String add(ModelMap model){

        model.put("longId", UUIDFactory.getStringId());
        return "manager/user-plate/add";
    }


    /**
     * 进入用户车牌信息编辑页面
     * @param model 返回用户车牌信息的容器
     * @param id 用户车牌信息id
     * @return 编辑页面
     */
    @RequestMapping(value="/edit")
    @RequiresPermissions("userPlate:edit")
    public String edit(ModelMap model, @RequestParam String id) {

        model.put("data",userPlateService.selectById(id));
        return "manager/user-plate/edit";
    }


    /**
     * 用户车牌信息添加
     * @param userPlate 带id的用户车牌信息对象
     * @return 成功状态
     */
//    @RequestMapping(value="/save")
//    @ResponseBody
//    @RequiresPermissions("userPlate:add")
//    @AdminControllerLog(description = "添加用户车牌信息" )
//    public Tip save(UserPlate userPlate)  {
//
//        try {
//            userPlateService.insert(userPlate);
//            return new Tip();
//        } catch (Exception e) {
//            //e.printStackTrace();
//            return new Tip(1,"添加失败！");
//        }
//
//    }


    /**
     * 根据 id 修改用户车牌信息
     * @param userPlate 带id的用户车牌信息对象
     * @return 成功状态
     */
    @RequestMapping(value="/update")
    @ResponseBody
    @RequiresPermissions("userPlate:edit")
    @AdminControllerLog(description = "修改用户车牌信息" )
    public Tip update(UserPlate userPlate) {

        try {
            userPlateService.updateById(userPlate);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"修改失败！");
        }

    }


    /**
     * 根据 id 删除用户车牌信息
     * @param id 用户车牌信息id
     * @return 成功状态
     */
    @RequestMapping(value="/delete")
    @ResponseBody
    @RequiresPermissions("userPlate:delete")
    @AdminControllerLog(description = "删除用户车牌信息" )
    public Tip delete(@RequestParam String id) {

        try {
            userPlateService.deleteById(id);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"删除失败！");
        }

    }


    /**
     * 根据 id 列表批量删除
     * @param ids 用户车牌信息id List
     * @return 成功状态
     */
    @RequestMapping(value="/deleteByIds")
    @ResponseBody
    @RequiresPermissions("userPlate:delete")
    @AdminControllerLog(description = "批量删除用户车牌信息" )
    public Tip deleteByIds(@RequestParam("ids") List<String> ids) {

        try {
            userPlateService.deleteByIds(ids);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"批量删除失败！");
        }

    }


    /**
     * 根据 id 查找用户车牌信息
     * @param id 用户车牌信息id
     * @return 用户车牌信息对象 json
     */
    @RequestMapping(value="/findJson")
    @ResponseBody
    @RequiresPermissions("userPlate:list")
    public UserPlate find(@RequestParam String id) {

        return userPlateService.selectById(id);
    }


    /**
     * 获取用户车牌信息列表 获取全部 不分页
     * @param request 请求参数
     * @return 用户车牌信息列表页面
     */
    @RequestMapping(value="/all")
    @RequiresPermissions("userPlate:list")
    public String all(HttpServletRequest request,ModelMap modelMap) {

        return list(request,modelMap,1,0);
    }


    /**
     * 获取用户车牌信息列表 分页
     * @param request 请求参数
     * @param page 第几页
     * @param size 每页大小
     * @return 用户车牌信息列表页面
     */
    @RequestMapping(value="/list")
    @RequiresPermissions("userPlate:list")
    public String list(HttpServletRequest request,ModelMap modelMap,Integer page,Integer size) {

        modelMap.put("pageInfo",userPlateService.list(new ParamMap(request),page,size));
        modelMap.putAll(new ParamMap(request));
        return "manager/user-plate/list";
    }


    //---------------------------- property -------------------------------

    @Resource
    private UserPlateService userPlateService;

}
