package com.jee.ssm.modules.userPlate.dao;

import com.jee.ssm.model.UserPlate;
import com.jee.ssm.modules.ssm.dao.LzDao;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;

/**
* 用户车牌信息管理 Dao
* @author GaoXiang
* @version 1.0
*/
@Repository
public class UserPlateDao extends LzDao<UserPlate> {



    /**
    * 保存数据
    * @param userPlate 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(UserPlate userPlate) throws Exception {
        return insert("UserPlateMapper.insert",userPlate);
    }

    /**
    * 根据 id 修改
    * @param userPlate 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(UserPlate userPlate) throws Exception {
        return update("UserPlateMapper.updateById",userPlate);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return delete("UserPlateMapper.deleteById",id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public UserPlate selectById(String id) {
        return selectOne("UserPlateMapper.selectById",id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return delete("UserPlateMapper.deleteByIds",ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<UserPlate> list(Map map) {
        return list("UserPlateMapper.list",map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<UserPlate> list(Map map,int page,int size) {
        return list("UserPlateMapper.list",map,new RowBounds(page,size));
    }

	public Integer editOtherPlate(UserPlate userPlate) throws Exception {
		return update("UserPlateMapper.editOtherPlate", userPlate);
	}

    public List<UserPlate> findByUserId(String userId) {
        return arrayList("UserPlateMapper.findByUserId", userId);
    }

	public Integer editUnDefault(String userId) throws Exception {
		return update("UserPlateMapper.editUnDefault", userId);
	}

	public List<UserPlate> findByPlateNumber(String number) {
		return arrayList("UserPlateMapper.findByPlateNumber", number);
	}

    public UserPlate findByNumberAndColour(UserPlate up) {
        return selectOne("UserPlateMapper.findByNumberAndColour", up);
    }
}
