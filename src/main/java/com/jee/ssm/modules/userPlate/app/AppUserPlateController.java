package com.jee.ssm.modules.userPlate.app;

import com.jee.ssm.common.utils.TokenUtils;
import com.jee.ssm.common.utils.UUIDFactory;
import com.jee.ssm.model.User;
import com.jee.ssm.model.UserPlate;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.modules.user.services.UserService;
import com.jee.ssm.modules.userPlate.services.UserPlateService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

/**
* 用户车牌信息管理 客户端 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/app/userPlate")
public class AppUserPlateController {

	/**
	 * 用户绑定车牌
	 * @param userId 用户id
	 * @param number 车牌号码
	 * @param plateColour 车牌颜色
	 * @param status 状态
	 * @return 提示信息
	 */
	@RequestMapping(value = "/saveUserPlate", method = RequestMethod.POST)
	@ResponseBody
	public Tip saveUserPlate(long timestamp, String token, String userId, String number, String plateColour, Integer status, Integer type, Integer autoPay) {
		Tip tip = TokenUtils.verifyToken(timestamp, token);
		if(tip.getSuccess()) {
			User user = userService.selectById(userId);
			if(user != null) {
				number = number.replaceAll("·", "").replaceAll(" ", "").toUpperCase();
				List<UserPlate> ups = userPlateService.findByPlateNumber(number);
				if(ups.size() == 0) {
					List<UserPlate> list = userPlateService.findByUserId(userId);
					if(list.size() < 5) {
						UserPlate up = new UserPlate(UUIDFactory.getStringId(), userId, number, plateColour, type, status, autoPay, new Date(), new Date(), user.getUserName(), user.getPhone(), user.getName(), user.getIdNumber());
						try {
							user.setPlateNumber(number);
							user.setCommonPlateColour(plateColour);
							userPlateService.insert(up, user);
							tip = new Tip("添加成功");
						} catch (Exception e) {
							e.printStackTrace();
							tip = new Tip(2, "系统异常，添加失败，请刷新重试");
						}
					} else {
						tip = new Tip(3, "每个用户最多只能添加五个车牌");
					}
				} else {
					tip = new Tip(4, "该车牌已经被用户绑定");
				}
				
			} else {
				tip = new Tip(1, "用户不存在");
			}
		}
		return tip;
	}

	/**
	 * 根据用户id查询车牌列表
	 * @param userId 用户id
	 * @return 车牌列表
	 */
	@RequestMapping(value = "/findPlateByUserId", method = RequestMethod.GET)
	@ResponseBody
	public Tip findPlateByUserId(long timestamp, String token, String userId) {
		Tip tip = TokenUtils.verifyToken(timestamp, token);
		if(tip.getSuccess()) {
			List<UserPlate> list = userPlateService.findByUserId(userId);
			tip = new Tip(list);
		}
		return tip;
	}
	
	@RequestMapping(value = "/deletePlateById", method = RequestMethod.GET)
	@ResponseBody
	public Tip deletePlateById(long timestamp, String token, String id) {
		Tip tip = TokenUtils.verifyToken(timestamp, token);
		if(tip.getSuccess()) {
			try {
				userPlateService.deleteById(id);
				tip = new Tip("删除成功");
			} catch (Exception e) {
				tip = new Tip(1, "系统异常，删除失败，请刷新 重试");
			}
		}
		return tip;
	}
	
	@RequestMapping(value = "/editPlateById", method = RequestMethod.GET)
	@ResponseBody
	public Tip editPlateById(long timestamp, String token, String id, String number, String colour, String status, Integer type, Integer autoPay, String userId) {
		Tip tip = TokenUtils.verifyToken(timestamp, token);
		if(tip.getSuccess()) {
			UserPlate up = new UserPlate(id, number, colour, type, autoPay, new Date(), userId);
			try {
				if(status != null && !"".equals(status) && "1".equals(status)) {
					User user = new User();
					user.setId(userId);
					user.setPlateNumber(number);
					user.setCommonPlateColour(colour);
					up.setStatus(Integer.valueOf(status));
					userPlateService.editDefaultPlate(up, user);
				} else {
					userPlateService.updateById(up);
				}
				tip = new Tip("修改成功");
			} catch (Exception e) {
				tip = new Tip(1, "系统异常，修改失败，请刷新重试");
			}
		}
		return tip;
	}



    //---------------------------- property -------------------------------

	@Resource
	private UserService userService;
	
    @Resource
    private UserPlateService userPlateService;

}
