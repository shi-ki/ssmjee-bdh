package com.jee.ssm.modules.userPlate.services;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.PageInfo;
import com.jee.ssm.model.User;
import com.jee.ssm.model.UserPlate;
import com.jee.ssm.modules.ssm.services.BaseService;
import com.jee.ssm.modules.user.dao.UserDao;
import com.jee.ssm.modules.userPlate.dao.UserPlateDao;

/**
* 用户车牌信息管理 Service
* @author GaoXiang
* @version 1.0
*/
@Service
public class UserPlateService extends BaseService<UserPlate> {

    @Resource
    private UserPlateDao userPlateDao;
    
    @Resource
    private UserDao userDao;


    /**
    * 保存数据
    * @param userPlate 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(UserPlate userPlate, User user) throws Exception {
    	if(userPlate.getStatus() == 1) {
    		userDao.updateById(user);
    		userPlateDao.editOtherPlate(userPlate);    		
    	}
    	userPlateDao.insert(userPlate);
        return 1;
    }

    /**
    * 根据 id 修改
    * @param userPlate 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(UserPlate userPlate) throws Exception {
        return userPlateDao.updateById(userPlate);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
    	UserPlate up = userPlateDao.selectById(id);
    	if(up != null && up.getStatus() == 1) {
    		userDao.deleteDefaultPlate(up.getUserId());
    	}
        return userPlateDao.deleteById(id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public UserPlate selectById(String id) {
        return userPlateDao.selectById(id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return userPlateDao.deleteByIds(ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<UserPlate> list(Map map) {
        return userPlateDao.list(map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<UserPlate> list(Map map,int page,int size) {
        return userPlateDao.list(map,page,size);
    }


    /**
     * 根据用户id查询所有车牌
     * @param id
     * @return
     */
    public List<UserPlate> findByUserId(String id) {
        return userPlateDao.findByUserId(id);
    }

	public Integer editDefaultPlate(UserPlate up, User user) throws Exception {
		userDao.updateById(user);
		userPlateDao.editOtherPlate(up);
		userPlateDao.updateById(up);
		return 1;
	}

	public List<UserPlate> findByPlateNumber(String number) {
		return userPlateDao.findByPlateNumber(number);
	}

    public UserPlate findByNumberAndColour(UserPlate up) {
        return userPlateDao.findByNumberAndColour(up);
    }
}
