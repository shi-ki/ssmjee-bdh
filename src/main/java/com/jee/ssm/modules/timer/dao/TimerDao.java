package com.jee.ssm.modules.timer.dao;

import com.jee.ssm.model.Timer;
import com.jee.ssm.modules.ssm.dao.LzDao;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;

/**
* 停车包夜类型管理 Dao
* @author GaoXiang
* @version 1.0
*/
@Repository
public class TimerDao extends LzDao<Timer> {



    /**
    * 保存数据
    * @param timer 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(Timer timer) throws Exception {
        return insert("TimerMapper.insert",timer);
    }

    /**
    * 根据 id 修改
    * @param timer 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(Timer timer) throws Exception {
        return update("TimerMapper.updateById",timer);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return delete("TimerMapper.deleteById",id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public Timer selectById(String id) {
        return selectOne("TimerMapper.selectById",id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return delete("TimerMapper.deleteByIds",ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<Timer> list(Map map) {
        return list("TimerMapper.list",map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<Timer> list(Map map,int page,int size) {
        return list("TimerMapper.list",map,new RowBounds(page,size));
    }

}
