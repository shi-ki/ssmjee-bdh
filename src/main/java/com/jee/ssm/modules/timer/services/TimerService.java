package com.jee.ssm.modules.timer.services;

import com.jee.ssm.modules.timer.dao.TimerDao;
import com.jee.ssm.model.Timer;
import com.jee.ssm.modules.ssm.services.BaseService;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
* 停车包夜类型管理 Service
* @author GaoXiang
* @version 1.0
*/
@Service
public class TimerService extends BaseService<Timer> {

    @Resource
    private TimerDao timerDao;


    /**
    * 保存数据
    * @param timer 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(Timer timer) throws Exception {
        return timerDao.insert(timer);
    }

    /**
    * 根据 id 修改
    * @param timer 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(Timer timer) throws Exception {
        return timerDao.updateById(timer);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return timerDao.deleteById(id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public Timer selectById(String id) {
        return timerDao.selectById(id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return timerDao.deleteByIds(ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<Timer> list(Map map) {
        return timerDao.list(map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<Timer> list(Map map,int page,int size) {
        return timerDao.list(map,page,size);
    }


}
