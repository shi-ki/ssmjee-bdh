package com.jee.ssm.modules.timer.controller;

import com.jee.ssm.model.Timer;
import com.jee.ssm.modules.timer.services.TimerService;
import com.jee.ssm.common.log.AdminControllerLog;
import com.jee.ssm.modules.ssm.controller.AdminBaseController;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.model.param.ParamMap;
import com.jee.ssm.common.utils.UUIDFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
* 停车包夜类型管理 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/timer")
public class TimerController extends AdminBaseController<Timer> {


    /**
     * 进入停车包夜类型添加页面 携带一个生成的id --> longId
     * @param model 返回的实体容器
     * @return 添加页面
     */
    @RequestMapping(value="/add")
    @RequiresPermissions("timer:add")
    public String add(ModelMap model){

        model.put("longId", UUIDFactory.getStringId());
        return "manager/timer/add";
    }


    /**
     * 进入停车包夜类型编辑页面
     * @param model 返回停车包夜类型的容器
     * @param id 停车包夜类型id
     * @return 编辑页面
     */
    @RequestMapping(value="/edit")
    @RequiresPermissions("timer:edit")
    public String edit(ModelMap model, @RequestParam String id) {

        model.put("data",timerService.selectById(id));
        return "manager/timer/edit";
    }


    /**
     * 停车包夜类型添加
     * @param timer 带id的停车包夜类型对象
     * @return 成功状态
     */
    @RequestMapping(value="/save")
    @ResponseBody
    @RequiresPermissions("timer:add")
    @AdminControllerLog(description = "添加停车包夜类型" )
    public Tip save(Timer timer)  {

        try {
            timerService.insert(timer);
            return new Tip();
        } catch (Exception e) {
            e.printStackTrace();
            return new Tip(1,"添加失败！");
        }

    }


    /**
     * 根据 id 修改停车包夜类型
     * @param timer 带id的停车包夜类型对象
     * @return 成功状态
     */
    @RequestMapping(value="/update")
    @ResponseBody
    @RequiresPermissions("timer:edit")
    @AdminControllerLog(description = "修改停车包夜类型" )
    public Tip update(Timer timer) {

        try {
            timerService.updateById(timer);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"修改失败！");
        }

    }


    /**
     * 根据 id 删除停车包夜类型
     * @param id 停车包夜类型id
     * @return 成功状态
     */
    @RequestMapping(value="/delete")
    @ResponseBody
    @RequiresPermissions("timer:delete")
    @AdminControllerLog(description = "删除停车包夜类型" )
    public Tip delete(@RequestParam String id) {

        try {
            timerService.deleteById(id);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"删除失败！");
        }

    }


    /**
     * 根据 id 列表批量删除
     * @param ids 停车包夜类型id List
     * @return 成功状态
     */
    @RequestMapping(value="/deleteByIds")
    @ResponseBody
    @RequiresPermissions("timer:delete")
    @AdminControllerLog(description = "批量删除停车包夜类型" )
    public Tip deleteByIds(@RequestParam("ids") List<String> ids) {

        try {
            timerService.deleteByIds(ids);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"批量删除失败！");
        }

    }


    /**
     * 根据 id 查找停车包夜类型
     * @param id 停车包夜类型id
     * @return 停车包夜类型对象 json
     */
    @RequestMapping(value="/findJson")
    @ResponseBody
    @RequiresPermissions("timer:list")
    public Timer find(@RequestParam String id) {

        return timerService.selectById(id);
    }


    /**
     * 获取停车包夜类型列表 获取全部 不分页
     * @param request 请求参数
     * @return 停车包夜类型列表页面
     */
    @RequestMapping(value="/all")
    @RequiresPermissions("timer:list")
    public String all(HttpServletRequest request,ModelMap modelMap) {

        return list(request,modelMap,1,0);
    }


    /**
     * 获取停车包夜类型列表 分页
     * @param request 请求参数
     * @param page 第几页
     * @param size 每页大小
     * @return 停车包夜类型列表页面
     */
    @RequestMapping(value="/list")
    @RequiresPermissions("timer:list")
    public String list(HttpServletRequest request,ModelMap modelMap,Integer page,Integer size) {

        modelMap.put("pageInfo",timerService.list(new ParamMap(request),page,size));
        modelMap.putAll(new ParamMap(request));
        return "manager/timer/list";
    }


    //---------------------------- property -------------------------------

    @Resource
    private TimerService timerService;

}
