package com.jee.ssm.modules.user.services;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.PageInfo;
import com.jee.ssm.common.utils.UUIDFactory;
import com.jee.ssm.model.Account;
import com.jee.ssm.model.BillRecord;
import com.jee.ssm.model.User;
import com.jee.ssm.modules.billRecord.dao.BillRecordDao;
import com.jee.ssm.modules.ssm.dao.AccountDao;
import com.jee.ssm.modules.ssm.services.BaseService;
import com.jee.ssm.modules.user.dao.UserDao;

/**
* 注册用户管理 Service
* @author GaoXiang
* @version 1.0
*/
@Service
public class UserService extends BaseService<User> {

    @Resource
    private UserDao userDao;

    @Resource
    private AccountDao accountDao;

    @Resource
    private BillRecordDao billRecordDao;


    /**
    * 保存数据
    * @param user 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(User user) throws Exception {
        return userDao.insert(user);
    }

    public Integer register(Account account, User user) throws Exception {
        accountDao.insert(account);
        userDao.insert(user);
        return 1;
    }

    /**
    * 根据 id 修改
    * @param user 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(User user) throws Exception {
        return userDao.updateById(user);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return userDao.deleteById(id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public User selectById(String id) {
        return userDao.selectById(id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return userDao.deleteByIds(ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<User> list(Map map) {
        return userDao.list(map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<User> list(Map map,int page,int size) {
        return userDao.list(map,page,size);
    }


    public List<User> findByPhone(String phone) {
        return userDao.findByPhone(phone);
    }

    /**
     * 根据用户id修改余额
     * @param user 用户
     */
    public Integer updateBalanceById(User user) throws Exception {
        return userDao.updateBalanceById(user);
    }
    
    public Integer updateWalletBalanceById(User user, String addOrSub, BigDecimal price, String content, Integer status, Integer type, String relateId, String intro, String method, Date inDateTime,Date outDateTime,Integer parkTime,String  plateNo) throws Exception {
    	BillRecord billRecord = new BillRecord(UUIDFactory.getStringId(), UUIDFactory.getBillNUmber(user.getPhone()), price, new Date(), content, addOrSub, 1, status, type, relateId, intro, user.getId(), user.getUserName(), user.getPhone(), user.getName(), user.getIdNumber(), method,inDateTime,outDateTime,parkTime,plateNo);
    	billRecordDao.insert(billRecord);
    	userDao.updateWalletBalanceById(user);
    	return 1;
    }

	public List<User> listUserToCount(User user) {
		return userDao.listUserToCount(user);
	}

	public Integer editPhoneByUserId(Account account, User user) throws Exception {
		userDao.updateById(user);
		accountDao.updateByInfoId(account);
		return 1;
	}

	public Integer editPasswordByUserName(Account account) throws Exception {
		return accountDao.editPasswordByUserName(account);
	}

}
