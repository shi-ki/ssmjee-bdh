package com.jee.ssm.modules.user.controller;

import com.jee.ssm.model.User;
import com.jee.ssm.modules.user.services.UserService;
import com.jee.ssm.common.log.AdminControllerLog;
import com.jee.ssm.modules.ssm.controller.AdminBaseController;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.model.param.ParamMap;
import com.jee.ssm.common.utils.UUIDFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
* 注册用户管理 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/user")
public class UserController extends AdminBaseController<User> {


    /**
     * 进入注册用户添加页面 携带一个生成的id --> longId
     * @param model 返回的实体容器
     * @return 添加页面
     */
    @RequestMapping(value="/add")
    @RequiresPermissions("user:add")
    public String add(ModelMap model){

        model.put("longId", UUIDFactory.getStringId());
        return "manager/user/add";
    }


    /**
     * 进入注册用户编辑页面
     * @param model 返回注册用户的容器
     * @param id 注册用户id
     * @return 编辑页面
     */
    @RequestMapping(value="/edit")
    @RequiresPermissions("user:edit")
    public String edit(ModelMap model, @RequestParam String id) {

        model.put("data",userService.selectById(id));
        return "manager/user/edit";
    }


    /**
     * 注册用户添加
     * @param user 带id的注册用户对象
     * @return 成功状态
     */
    @RequestMapping(value="/save")
    @ResponseBody
    @RequiresPermissions("user:add")
    @AdminControllerLog(description = "添加注册用户" )
    public Tip save(User user)  {

        try {
            userService.insert(user);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"添加失败！");
        }

    }


    /**
     * 根据 id 修改注册用户
     * @param user 带id的注册用户对象
     * @return 成功状态
     */
    @RequestMapping(value="/update")
    @ResponseBody
    @RequiresPermissions("user:edit")
    @AdminControllerLog(description = "修改注册用户" )
    public Tip update(User user) {

        try {
            userService.updateById(user);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"修改失败！");
        }

    }


    /**
     * 根据 id 删除注册用户
     * @param id 注册用户id
     * @return 成功状态
     */
    @RequestMapping(value="/delete")
    @ResponseBody
    @RequiresPermissions("user:delete")
    @AdminControllerLog(description = "删除注册用户" )
    public Tip delete(@RequestParam String id) {

        try {
            userService.deleteById(id);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"删除失败！");
        }

    }


    /**
     * 根据 id 列表批量删除
     * @param ids 注册用户id List
     * @return 成功状态
     */
    @RequestMapping(value="/deleteByIds")
    @ResponseBody
    @RequiresPermissions("user:delete")
    @AdminControllerLog(description = "批量删除注册用户" )
    public Tip deleteByIds(@RequestParam("ids") List<String> ids) {

        try {
            userService.deleteByIds(ids);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"批量删除失败！");
        }

    }


    /**
     * 根据 id 查找注册用户
     * @param id 注册用户id
     * @return 注册用户对象 json
     */
    @RequestMapping(value="/findJson")
    @ResponseBody
    @RequiresPermissions("user:list")
    public User find(@RequestParam String id) {

        return userService.selectById(id);
    }


    /**
     * 获取注册用户列表 获取全部 不分页
     * @param request 请求参数
     * @return 注册用户列表页面
     */
    @RequestMapping(value="/all")
    @RequiresPermissions("user:list")
    public String all(HttpServletRequest request,ModelMap modelMap) {

        return list(request,modelMap,1,0);
    }


    /**
     * 获取注册用户列表 分页
     * @param request 请求参数
     * @param page 第几页
     * @param size 每页大小
     * @return 注册用户列表页面
     */
    @RequestMapping(value="/list")
    @RequiresPermissions("user:list")
    public String list(HttpServletRequest request,ModelMap modelMap,Integer page,Integer size) {

        modelMap.put("pageInfo",userService.list(new ParamMap(request),page,size));
        modelMap.putAll(new ParamMap(request));
        return "manager/user/list";
    }


    //---------------------------- property -------------------------------

    @Resource
    private UserService userService;

}
