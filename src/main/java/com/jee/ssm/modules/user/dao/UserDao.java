package com.jee.ssm.modules.user.dao;

import com.jee.ssm.model.User;
import com.jee.ssm.modules.ssm.dao.LzDao;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;

/**
* 注册用户管理 Dao
* @author GaoXiang
* @version 1.0
*/
@Repository
public class UserDao extends LzDao<User> {



    /**
    * 保存数据
    * @param user 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(User user) throws Exception {
        return insert("UserMapper.insert",user);
    }

    /**
    * 根据 id 修改
    * @param user 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(User user) throws Exception {
        return update("UserMapper.updateById",user);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return delete("UserMapper.deleteById",id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public User selectById(String id) {
        return selectOne("UserMapper.selectById",id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return delete("UserMapper.deleteByIds",ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<User> list(Map map) {
        return list("UserMapper.list",map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<User> list(Map map,int page,int size) {
        return list("UserMapper.list",map,new RowBounds(page,size));
    }

    public List<User> findByPhone(String phone) {
        return arrayList("UserMapper.findByPhone", phone);
    }

    /**
     * 修改可用余额与冻结余额
     * @param user 用户两种余额
     * @return
     * @throws Exception
     */
    public Integer updateBalanceById(User user) throws Exception {
        return update("UserMapper.updateBalanceById", user);
    }

    /**
     * 修改可用余额
     * @param user 可用余额
     */
    public Integer updateWalletBalanceById(User user) throws Exception {
        return update("UserMapper.updateWalletBalanceById", user);
    }

	public List<User> listUserToCount(User user) {
		return arrayList("UserMapper.listUserToCount", user);
	}

	public Integer deleteDefaultPlate(String userId) throws Exception {
		return update("UserMapper.deleteDefaultPlate", userId);
	}

}
