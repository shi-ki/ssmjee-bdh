package com.jee.ssm.modules.image.app;

import com.jee.ssm.common.utils.TokenUtils;
import com.jee.ssm.model.Image;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.modules.image.services.ImageService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
* 上传轮播图管理 客户端 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/app/image")
public class AppImageController {

    @RequestMapping(value ="getImage")
    @ResponseBody
    public Tip getImage (long timestamp, String token){
        Tip tip = TokenUtils.verifyToken(timestamp, token);
        if (tip.getSuccess()){
            List<Image> imageList = imageService.queryImage();
            return new Tip(imageList);
        }else {
            return tip;
        }
    }





    //---------------------------- property -------------------------------

    @Resource
    private ImageService imageService;

}
