package com.jee.ssm.modules.image.services;

import com.jee.ssm.common.utils.StringUtils;
import com.jee.ssm.modules.image.dao.ImageDao;
import com.jee.ssm.model.Image;
import com.jee.ssm.modules.ssm.services.BaseService;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
* 上传轮播图管理 Service
* @author GaoXiang
* @version 1.0
*/
@Service
public class ImageService extends BaseService<Image> {

    @Resource
    private ImageDao imageDao;


    /**
    * 保存数据
    * @param image 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(Image image) throws Exception {
        image.setType("0");
        image.setCreateTime(new Date());
        return imageDao.insert(image);
    }

    /**
    * 根据 id 修改
    * @param image 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(Image image) throws Exception {
        return imageDao.updateById(image);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return imageDao.deleteById(id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public Image selectById(String id) {
        return imageDao.selectById(id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return imageDao.deleteByIds(ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<Image> list(Map map) {
        return imageDao.list(map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<Image> list(Map map,int page,int size) {
        return imageDao.list(map,page,size);
    }


    public List<Image> queryImage() {
        return imageDao.queryImage();
    }
}
