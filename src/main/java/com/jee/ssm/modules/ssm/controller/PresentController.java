package com.jee.ssm.modules.ssm.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.jee.ssm.model.*;
import com.jee.ssm.modules.mapPoint.services.MapPointService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.jee.ssm.common.utils.TimeUtils;
import com.jee.ssm.common.utils.hik.HikUtils;
import com.jee.ssm.model.hik.receive.ParkDepotInfo;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.modules.monitor.services.MonitorService;
import com.jee.ssm.modules.parkDepot.services.ParkDepotService;
import com.jee.ssm.modules.trail.services.TrailService;
import com.jee.ssm.modules.user.services.UserService;

@Controller
@RequestMapping(value = "/present")
public class PresentController {

    @RequestMapping(value = "/getParkDepots", method = RequestMethod.GET)
    @ResponseBody
    public List<ParkDepotInfo> getParkDepots() {
        List<ParkDepotInfo> list = new ArrayList<>();
        Tip tip = HikUtils.getParkingInfos(1, 400);
        if(tip.getSuccess()) {
        	List<ParkDepotInfo> l = JSON.parseArray(JSONArray.toJSONString(tip.getData()), ParkDepotInfo.class);
            for(ParkDepotInfo p : l) {
                ParkDepot parkDepot = parkDepotService.findByHikId(p.getParkCode());
                if(parkDepot != null) {
                    p.setParkDepot(parkDepot);
                    list.add(p);
                }
            }
            List<ParkDepot> parkDepotList = parkDepotService.getParkDepotsByType(2);
            List<ParkDepot> parkDepotList2 = parkDepotService.getParkDepotsByType(3);
            for (ParkDepot parkDepot : parkDepotList){
                ParkDepotInfo pinfo = new ParkDepotInfo();
                pinfo.setParkDepot(parkDepot);
                pinfo.setParkLatitude(BigDecimal.valueOf(parkDepot.getLat()));
                pinfo.setParkLongitude(BigDecimal.valueOf(parkDepot.getLng()));
                pinfo.setParkName(parkDepot.getName());
                pinfo.setParkType(4);
                list.add(pinfo);
            }
            for (ParkDepot parkDepot : parkDepotList2){
                ParkDepotInfo pinfo1 = new ParkDepotInfo();
                pinfo1.setParkDepot(parkDepot);
                pinfo1.setParkLatitude(BigDecimal.valueOf(parkDepot.getLat()));
                pinfo1.setParkLongitude(BigDecimal.valueOf(parkDepot.getLng()));
                pinfo1.setParkName(parkDepot.getName());
                pinfo1.setParkType(5);
                list.add(pinfo1);
            }

        }else {
            List<ParkDepot> parkDepotList = parkDepotService.getParkDepotsAll();
            for (ParkDepot parkDepot : parkDepotList){
                ParkDepotInfo pinfo = new ParkDepotInfo();
                pinfo.setParkDepot(parkDepot);
                pinfo.setParkLatitude(BigDecimal.valueOf(parkDepot.getLat()));
                pinfo.setParkLongitude(BigDecimal.valueOf(parkDepot.getLng()));
                pinfo.setParkName(parkDepot.getName());
                pinfo.setParkType(4);
                list.add(pinfo);
            }
        }
        return list;
    }

    @RequestMapping(value = "/getMapPoint", method = RequestMethod.GET)
    @ResponseBody
    public List<MapPoint> getMapPoint() {
        List<MapPoint> list = mapPointService.selectAllPoint();
        return list;
    }

    /**
     * 摄像头列表
     * @return
     */
    @RequestMapping(value = "/getMonitors", method = RequestMethod.GET)
    @ResponseBody
    public List<Monitor> getMonitors() {
    	return monitorService.listCommonMoitors();
    }

    @RequestMapping(value = "/parkDepotData", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> parkDepotData() {
    	Map<String, Object> map = new HashMap<>();
    	List<ParkDepotInfo> list = new ArrayList<>();
    	List<ParkDepotInfo> l = HikUtils.getParkingList();
    	if(l.size() > 0) {
    		for(ParkDepotInfo p : l) {
                ParkDepot parkDepot = parkDepotService.findByHikId(p.getParkCode());
                if(parkDepot != null) {
                	list.add(p);
                }
            }
    	}
    	if(list.size() > 0) {
    		String[] names = new String[list.size()];
    		Integer[] totalParklot = new Integer[list.size()];
    		Integer[] parklotLeft = new Integer[list.size()];
    		for(int i=0;i<list.size();i ++) {
    			String parkName = list.get(i).getParkName();
    			Integer total = list.get(i).getTotalPlot();
    			Integer left = list.get(i).getLeftPlot();
//    			names[i] = parkName.length() > 3 ? parkName.substring(0, 3) : parkName;
    			names[i] = parkName;
    			totalParklot[i] = total;
    			parklotLeft[i] = left;
    		}
    		map.put("names", names);
    		map.put("totalParklot", totalParklot);
    		map.put("parklotLeft", parklotLeft);
    	}
        return map;
    }
    
    @RequestMapping(value = "/userData", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> userData(Integer num) {
    	Map<String, Object> map = new HashMap<>();
    	String[] weeks = new String[num];
    	Integer[] numbers = new Integer[num];
    	for(int i=0;i<num;i++) {
    		Date date = TimeUtils.getWantedTime(i - num + 1);
    		List<User> users = userService.listUserToCount(new User(TimeUtils.getStringDay(date)));
    		weeks[i] = TimeUtils.getDayOfweek(date);
    		numbers[i] = users.size();
    	}
    	map.put("weeks", weeks);
    	map.put("numbers", numbers);
    	
    	return map;
    }

    @RequestMapping(value = "/leftParkDepotNum", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Integer> leftParkDepotNum() {
    	Map<String, Integer> map = new HashMap<>();
    	Integer totalParklot = 0;
    	Integer parklotLeft = 0;
    	Integer parklotUsing = 0;
    	List<ParkDepotInfo> list = HikUtils.getParkingList();
    	if(list.size() > 0) {
    		for(ParkDepotInfo p : list) {
                ParkDepot parkDepot = parkDepotService.findByHikId(p.getParkCode());
                if(parkDepot != null) {
                	totalParklot += p.getTotalPlot();
                	parklotLeft += p.getLeftPlot();
                }
            }
    	}
    	parklotUsing = totalParklot - parklotLeft;
    	map.put("parklotLeft", parklotLeft);
    	map.put("parklotUsing", parklotUsing);
    	return map;
    }
    
    @RequestMapping(value = "/depot", method = RequestMethod.GET)
    public String depot() {
    	return "manager/present/depot";
    }
    
    @RequestMapping(value = "/traffic", method = RequestMethod.GET)
    public String traffic() {
    	return "manager/present/traffic";
    }
    
    @RequestMapping(value = "/monitor", method = RequestMethod.GET)
    public String monitor() {
    	return "manager/present/monitor";
    }
    
    @RequestMapping(value = "/guide", method = RequestMethod.GET)
    public String guide() {
    	return "manager/present/guide";
    }
    
    @RequestMapping(value = "/findTrailByPlate", method = RequestMethod.GET)
    @ResponseBody
    public Tip findTrailByPlate(String plateNumber, String executeDate) {
    	List<Trail> list = trailService.findTrailByPlate(new Trail(plateNumber, executeDate));
    	return new Tip(list);
    }
    
    @RequestMapping(value = "/trail", method = RequestMethod.GET)
    public String trail(String plateNumber, String executeDate, ModelMap model) {
    	model.addAttribute("plateNumber", plateNumber);
    	model.addAttribute("executeDate", executeDate);
    	return "manager/present/trail";
    }
    
    @Resource
    private ParkDepotService parkDepotService;
    
    @Resource
    private UserService userService;
    
    @Resource
    private TrailService trailService;
    
    @Resource
    private MonitorService monitorService;
    @Resource
    private MapPointService mapPointService;

}
