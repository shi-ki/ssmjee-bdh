package com.jee.ssm.modules.suggestionImage.services;

import com.jee.ssm.modules.suggestionImage.dao.SuggestionImageDao;
import com.jee.ssm.model.SuggestionImage;
import com.jee.ssm.modules.ssm.services.BaseService;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
* 意见图片管理 Service
* @author GaoXiang
* @version 1.0
*/
@Service
public class SuggestionImageService extends BaseService<SuggestionImage> {

    @Resource
    private SuggestionImageDao suggestionImageDao;


    /**
    * 保存数据
    * @param suggestionImage 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(SuggestionImage suggestionImage) throws Exception {
        return suggestionImageDao.insert(suggestionImage);
    }

    /**
    * 根据 id 修改
    * @param suggestionImage 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(SuggestionImage suggestionImage) throws Exception {
        return suggestionImageDao.updateById(suggestionImage);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return suggestionImageDao.deleteById(id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public SuggestionImage selectById(String id) {
        return suggestionImageDao.selectById(id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return suggestionImageDao.deleteByIds(ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<SuggestionImage> list(Map map) {
        return suggestionImageDao.list(map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<SuggestionImage> list(Map map,int page,int size) {
        return suggestionImageDao.list(map,page,size);
    }

	public List<SuggestionImage> findBySuggestionId(String id) {
		return suggestionImageDao.findBySuggestionId(id);
	}


}
