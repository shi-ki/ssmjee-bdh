package com.jee.ssm.modules.suggestionImage.controller;

import com.jee.ssm.model.SuggestionImage;
import com.jee.ssm.modules.suggestionImage.services.SuggestionImageService;
import com.jee.ssm.common.log.AdminControllerLog;
import com.jee.ssm.modules.ssm.controller.AdminBaseController;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.model.param.ParamMap;
import com.jee.ssm.common.utils.UUIDFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
* 意见图片管理 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/suggestionImage")
public class SuggestionImageController extends AdminBaseController<SuggestionImage> {


    /**
     * 进入意见图片添加页面 携带一个生成的id --> longId
     * @param model 返回的实体容器
     * @return 添加页面
     */
    @RequestMapping(value="/add")
    @RequiresPermissions("suggestionImage:add")
    public String add(ModelMap model){

        model.put("longId", UUIDFactory.getStringId());
        return "manager/suggestionImage/add";
    }


    /**
     * 进入意见图片编辑页面
     * @param model 返回意见图片的容器
     * @param id 意见图片id
     * @return 编辑页面
     */
    @RequestMapping(value="/edit")
    @RequiresPermissions("suggestionImage:edit")
    public String edit(ModelMap model, @RequestParam String id) {

        model.put("data",suggestionImageService.selectById(id));
        return "manager/suggestionImage/edit";
    }


    /**
     * 意见图片添加
     * @param suggestionImage 带id的意见图片对象
     * @return 成功状态
     */
    @RequestMapping(value="/save")
    @ResponseBody
    @RequiresPermissions("suggestionImage:add")
    @AdminControllerLog(description = "添加意见图片" )
    public Tip save(SuggestionImage suggestionImage)  {

        try {
            suggestionImageService.insert(suggestionImage);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"添加失败！");
        }

    }


    /**
     * 根据 id 修改意见图片
     * @param suggestionImage 带id的意见图片对象
     * @return 成功状态
     */
    @RequestMapping(value="/update")
    @ResponseBody
    @RequiresPermissions("suggestionImage:edit")
    @AdminControllerLog(description = "修改意见图片" )
    public Tip update(SuggestionImage suggestionImage) {

        try {
            suggestionImageService.updateById(suggestionImage);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"修改失败！");
        }

    }


    /**
     * 根据 id 删除意见图片
     * @param id 意见图片id
     * @return 成功状态
     */
    @RequestMapping(value="/delete")
    @ResponseBody
    @RequiresPermissions("suggestionImage:delete")
    @AdminControllerLog(description = "删除意见图片" )
    public Tip delete(@RequestParam String id) {

        try {
            suggestionImageService.deleteById(id);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"删除失败！");
        }

    }


    /**
     * 根据 id 列表批量删除
     * @param ids 意见图片id List
     * @return 成功状态
     */
    @RequestMapping(value="/deleteByIds")
    @ResponseBody
    @RequiresPermissions("suggestionImage:delete")
    @AdminControllerLog(description = "批量删除意见图片" )
    public Tip deleteByIds(@RequestParam("ids") List<String> ids) {

        try {
            suggestionImageService.deleteByIds(ids);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"批量删除失败！");
        }

    }


    /**
     * 根据 id 查找意见图片
     * @param id 意见图片id
     * @return 意见图片对象 json
     */
    @RequestMapping(value="/findJson")
    @ResponseBody
    @RequiresPermissions("suggestionImage:list")
    public SuggestionImage find(@RequestParam String id) {

        return suggestionImageService.selectById(id);
    }


    /**
     * 获取意见图片列表 获取全部 不分页
     * @param request 请求参数
     * @return 意见图片列表页面
     */
    @RequestMapping(value="/all")
    @RequiresPermissions("suggestionImage:list")
    public String all(HttpServletRequest request,ModelMap modelMap) {

        return list(request,modelMap,1,0);
    }


    /**
     * 获取意见图片列表 分页
     * @param request 请求参数
     * @param page 第几页
     * @param size 每页大小
     * @return 意见图片列表页面
     */
    @RequestMapping(value="/list")
    @RequiresPermissions("suggestionImage:list")
    public String list(HttpServletRequest request,ModelMap modelMap,Integer page,Integer size) {

        modelMap.put("pageInfo",suggestionImageService.list(new ParamMap(request),page,size));
        modelMap.putAll(new ParamMap(request));
        return "manager/suggestionImage/list";
    }


    //---------------------------- property -------------------------------

    @Resource
    private SuggestionImageService suggestionImageService;

}
