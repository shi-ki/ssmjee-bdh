package com.jee.ssm.modules.screenImage.controller;

import com.jee.ssm.model.ScreenImage;
import com.jee.ssm.modules.screenImage.services.ScreenImageService;
import com.jee.ssm.common.log.AdminControllerLog;
import com.jee.ssm.modules.ssm.controller.AdminBaseController;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.model.param.ParamMap;
import com.jee.ssm.common.utils.UUIDFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
* 诱导屏播放文件管理 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/screenImage")
public class ScreenImageController extends AdminBaseController<ScreenImage> {


    /**
     * 进入诱导屏播放文件添加页面 携带一个生成的id --> longId
     * @param model 返回的实体容器
     * @return 添加页面
     */
    @RequestMapping(value="/add")
    @RequiresPermissions("screenImage:add")
    public String add(ModelMap model){

        model.put("longId", UUIDFactory.getStringId());
        return "manager/screenImage/add";
    }


    /**
     * 进入诱导屏播放文件编辑页面
     * @param model 返回诱导屏播放文件的容器
     * @param id 诱导屏播放文件id
     * @return 编辑页面
     */
    @RequestMapping(value="/edit")
    @RequiresPermissions("screenImage:edit")
    public String edit(ModelMap model, @RequestParam String id) {

        model.put("data",screenImageService.selectById(id));
        return "manager/screenImage/edit";
    }


    /**
     * 诱导屏播放文件添加
     * @param screenImage 带id的诱导屏播放文件对象
     * @return 成功状态
     */
    @RequestMapping(value="/save")
    @ResponseBody
    @RequiresPermissions("screenImage:add")
    @AdminControllerLog(description = "添加诱导屏播放文件" )
    public Tip save(ScreenImage screenImage)  {

        try {
            screenImageService.insert(screenImage);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"添加失败！");
        }

    }


    /**
     * 根据 id 修改诱导屏播放文件
     * @param screenImage 带id的诱导屏播放文件对象
     * @return 成功状态
     */
    @RequestMapping(value="/update")
    @ResponseBody
    @RequiresPermissions("screenImage:edit")
    @AdminControllerLog(description = "修改诱导屏播放文件" )
    public Tip update(ScreenImage screenImage) {

        try {
            screenImageService.updateById(screenImage);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"修改失败！");
        }

    }


    /**
     * 根据 id 删除诱导屏播放文件
     * @param id 诱导屏播放文件id
     * @return 成功状态
     */
    @RequestMapping(value="/delete")
    @ResponseBody
    @RequiresPermissions("screenImage:delete")
    @AdminControllerLog(description = "删除诱导屏播放文件" )
    public Tip delete(@RequestParam String id) {

        try {
            screenImageService.deleteById(id);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"删除失败！");
        }

    }


    /**
     * 根据 id 列表批量删除
     * @param ids 诱导屏播放文件id List
     * @return 成功状态
     */
    @RequestMapping(value="/deleteByIds")
    @ResponseBody
    @RequiresPermissions("screenImage:delete")
    @AdminControllerLog(description = "批量删除诱导屏播放文件" )
    public Tip deleteByIds(@RequestParam("ids") List<String> ids) {

        try {
            screenImageService.deleteByIds(ids);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"批量删除失败！");
        }

    }


    /**
     * 根据 id 查找诱导屏播放文件
     * @param id 诱导屏播放文件id
     * @return 诱导屏播放文件对象 json
     */
    @RequestMapping(value="/findJson")
    @ResponseBody
    @RequiresPermissions("screenImage:list")
    public ScreenImage find(@RequestParam String id) {

        return screenImageService.selectById(id);
    }


    /**
     * 获取诱导屏播放文件列表 获取全部 不分页
     * @param request 请求参数
     * @return 诱导屏播放文件列表页面
     */
    @RequestMapping(value="/all")
    @RequiresPermissions("screenImage:list")
    public String all(HttpServletRequest request,ModelMap modelMap) {

        return list(request,modelMap,1,0);
    }


    /**
     * 获取诱导屏播放文件列表 分页
     * @param request 请求参数
     * @param page 第几页
     * @param size 每页大小
     * @return 诱导屏播放文件列表页面
     */
    @RequestMapping(value="/list")
    @RequiresPermissions("screenImage:list")
    public String list(HttpServletRequest request,ModelMap modelMap,Integer page,Integer size) {

        modelMap.put("pageInfo",screenImageService.list(new ParamMap(request),page,size));
        modelMap.putAll(new ParamMap(request));
        return "manager/screenImage/list";
    }


    //---------------------------- property -------------------------------

    @Resource
    private ScreenImageService screenImageService;

}
