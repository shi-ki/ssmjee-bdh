package com.jee.ssm.modules.screenImage.services;

import com.jee.ssm.modules.screenImage.dao.ScreenImageDao;
import com.jee.ssm.model.ScreenImage;
import com.jee.ssm.modules.ssm.services.BaseService;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
* 诱导屏播放文件管理 Service
* @author GaoXiang
* @version 1.0
*/
@Service
public class ScreenImageService extends BaseService<ScreenImage> {

    @Resource
    private ScreenImageDao screenImageDao;


    /**
    * 保存数据
    * @param screenImage 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(ScreenImage screenImage) throws Exception {
        return screenImageDao.insert(screenImage);
    }

    /**
    * 根据 id 修改
    * @param screenImage 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(ScreenImage screenImage) throws Exception {
        return screenImageDao.updateById(screenImage);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return screenImageDao.deleteById(id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public ScreenImage selectById(String id) {
        return screenImageDao.selectById(id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return screenImageDao.deleteByIds(ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<ScreenImage> list(Map map) {
        return screenImageDao.list(map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<ScreenImage> list(Map map,int page,int size) {
        return screenImageDao.list(map,page,size);
    }

}
