package com.jee.ssm.modules.screenImage.dao;

import com.jee.ssm.model.ScreenImage;
import com.jee.ssm.modules.ssm.dao.LzDao;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;

/**
* 诱导屏播放文件管理 Dao
* @author GaoXiang
* @version 1.0
*/
@Repository
public class ScreenImageDao extends LzDao<ScreenImage> {



    /**
    * 保存数据
    * @param screenImage 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(ScreenImage screenImage) throws Exception {
        return insert("ScreenImageMapper.insert",screenImage);
    }

    /**
    * 根据 id 修改
    * @param screenImage 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(ScreenImage screenImage) throws Exception {
        return update("ScreenImageMapper.updateById",screenImage);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return delete("ScreenImageMapper.deleteById",id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public ScreenImage selectById(String id) {
        return selectOne("ScreenImageMapper.selectById",id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return delete("ScreenImageMapper.deleteByIds",ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<ScreenImage> list(Map map) {
        return list("ScreenImageMapper.list",map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<ScreenImage> list(Map map,int page,int size) {
        return list("ScreenImageMapper.list",map,new RowBounds(page,size));
    }

}
