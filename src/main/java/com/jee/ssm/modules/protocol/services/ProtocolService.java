package com.jee.ssm.modules.protocol.services;

import com.jee.ssm.modules.protocol.dao.ProtocolDao;
import com.jee.ssm.model.Protocol;
import com.jee.ssm.modules.ssm.services.BaseService;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
* 协议管理 Service
* @author GaoXiang
* @version 1.0
*/
@Service
public class ProtocolService extends BaseService<Protocol> {

    @Resource
    private ProtocolDao protocolDao;


    /**
    * 保存数据
    * @param protocol 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(Protocol protocol) throws Exception {
        return protocolDao.insert(protocol);
    }

    /**
    * 根据 id 修改
    * @param protocol 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(Protocol protocol) throws Exception {
        return protocolDao.updateById(protocol);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return protocolDao.deleteById(id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public Protocol selectById(String id) {
        return protocolDao.selectById(id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return protocolDao.deleteByIds(ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<Protocol> list(Map map) {
        return protocolDao.list(map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<Protocol> list(Map map,int page,int size) {
        return protocolDao.list(map,page,size);
    }

    /**
     * 根据类型查询协议
     * @param type 类型
     * @return
     */
    public Protocol selectByType(String type) {
        return protocolDao.selectByType(type);
    }
}
