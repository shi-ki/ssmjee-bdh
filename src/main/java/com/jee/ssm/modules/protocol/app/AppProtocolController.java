package com.jee.ssm.modules.protocol.app;

import com.jee.ssm.model.Protocol;
import com.jee.ssm.modules.protocol.services.ProtocolService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.management.modelmbean.ModelMBean;

/**
* 协议管理 客户端 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/app/protocol")
public class AppProtocolController {

    @RequestMapping(value = "/protocolContent")
    public String protocolContent(ModelMap  modelMap,String type){
        Protocol protocol = protocolService.selectByType(type);
        modelMap.put("protocol",protocol);
        return "manager/protocol/protocolContext";
    }




    //---------------------------- property -------------------------------

    @Resource
    private ProtocolService protocolService;

}
