package com.jee.ssm.modules.protocol.dao;

import com.jee.ssm.model.Protocol;
import com.jee.ssm.modules.ssm.dao.LzDao;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;

/**
* 协议管理 Dao
* @author GaoXiang
* @version 1.0
*/
@Repository
public class ProtocolDao extends LzDao<Protocol> {



    /**
    * 保存数据
    * @param protocol 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(Protocol protocol) throws Exception {
        return insert("ProtocolMapper.insert",protocol);
    }

    /**
    * 根据 id 修改
    * @param protocol 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(Protocol protocol) throws Exception {
        return update("ProtocolMapper.updateById",protocol);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return delete("ProtocolMapper.deleteById",id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public Protocol selectById(String id) {
        return selectOne("ProtocolMapper.selectById",id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return delete("ProtocolMapper.deleteByIds",ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<Protocol> list(Map map) {
        return list("ProtocolMapper.list",map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<Protocol> list(Map map,int page,int size) {
        return list("ProtocolMapper.list",map,new RowBounds(page,size));
    }

    /**
     * 根据类型查询协议
     * @param type 类型0 app协议 1钱包协议
     * @return
     */
    public Protocol selectByType(String type) {
        return selectOne("ProtocolMapper.selectByType",type);
    }
}
