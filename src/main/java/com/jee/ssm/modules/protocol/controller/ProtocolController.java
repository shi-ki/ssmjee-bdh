package com.jee.ssm.modules.protocol.controller;

import com.jee.ssm.model.Protocol;
import com.jee.ssm.modules.protocol.services.ProtocolService;
import com.jee.ssm.common.log.AdminControllerLog;
import com.jee.ssm.modules.ssm.controller.AdminBaseController;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.model.param.ParamMap;
import com.jee.ssm.common.utils.UUIDFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
* 协议管理 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/protocol")
public class ProtocolController extends AdminBaseController<Protocol> {


    /**
     * 进入协议添加页面 携带一个生成的id --> longId
     * @param model 返回的实体容器
     * @return 添加页面
     */
    @RequestMapping(value="/add")
    @RequiresPermissions("protocol:add")
    public String add(ModelMap model){

        model.put("longId", UUIDFactory.getStringId());
        return "manager/protocol/add";
    }


    /**
     * 进入协议编辑页面
     * @param model 返回协议的容器
     * @param id 协议id
     * @return 编辑页面
     */
    @RequestMapping(value="/edit")
    @RequiresPermissions("protocol:edit")
    public String edit(ModelMap model, @RequestParam String id) {

        model.put("data",protocolService.selectById(id));
        return "manager/protocol/edit";
    }


    /**
     * 协议添加
     * @param protocol 带id的协议对象
     * @return 成功状态
     */
    @RequestMapping(value="/save")
    @ResponseBody
    @RequiresPermissions("protocol:add")
    @AdminControllerLog(description = "添加协议" )
    public Tip save(Protocol protocol)  {

        try {
            protocolService.insert(protocol);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"添加失败！");
        }

    }


    /**
     * 根据 id 修改协议
     * @param protocol 带id的协议对象
     * @return 成功状态
     */
    @RequestMapping(value="/update")
    @ResponseBody
    @RequiresPermissions("protocol:edit")
    @AdminControllerLog(description = "修改协议" )
    public Tip update(Protocol protocol) {

        try {
            protocolService.updateById(protocol);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"修改失败！");
        }

    }


    /**
     * 根据 id 删除协议
     * @param id 协议id
     * @return 成功状态
     */
    @RequestMapping(value="/delete")
    @ResponseBody
    @RequiresPermissions("protocol:delete")
    @AdminControllerLog(description = "删除协议" )
    public Tip delete(@RequestParam String id) {

        try {
            protocolService.deleteById(id);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"删除失败！");
        }

    }


    /**
     * 根据 id 列表批量删除
     * @param ids 协议id List
     * @return 成功状态
     */
    @RequestMapping(value="/deleteByIds")
    @ResponseBody
    @RequiresPermissions("protocol:delete")
    @AdminControllerLog(description = "批量删除协议" )
    public Tip deleteByIds(@RequestParam("ids") List<String> ids) {

        try {
            protocolService.deleteByIds(ids);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"批量删除失败！");
        }

    }


    /**
     * 根据 id 查找协议
     * @param id 协议id
     * @return 协议对象 json
     */
    @RequestMapping(value="/findJson")
    @ResponseBody
    @RequiresPermissions("protocol:list")
    public Protocol find(@RequestParam String id) {

        return protocolService.selectById(id);
    }


    /**
     * 获取协议列表 获取全部 不分页
     * @param request 请求参数
     * @return 协议列表页面
     */
    @RequestMapping(value="/all")
    @RequiresPermissions("protocol:list")
    public String all(HttpServletRequest request,ModelMap modelMap) {

        return list(request,modelMap,1,0);
    }


    /**
     * 获取协议列表 分页
     * @param request 请求参数
     * @param page 第几页
     * @param size 每页大小
     * @return 协议列表页面
     */
    @RequestMapping(value="/list")
    @RequiresPermissions("protocol:list")
    public String list(HttpServletRequest request,ModelMap modelMap,Integer page,Integer size) {

        modelMap.put("pageInfo",protocolService.list(new ParamMap(request),page,size));
        modelMap.putAll(new ParamMap(request));
        return "manager/protocol/list";
    }


    //---------------------------- property -------------------------------

    @Resource
    private ProtocolService protocolService;

}
