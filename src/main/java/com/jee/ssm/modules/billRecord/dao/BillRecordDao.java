package com.jee.ssm.modules.billRecord.dao;

import com.jee.ssm.model.BillRecord;
import com.jee.ssm.modules.ssm.dao.LzDao;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;

/**
* 订单记录管理 Dao
* @author GaoXiang
* @version 1.0
*/
@Repository
public class BillRecordDao extends LzDao<BillRecord> {



    /**
    * 保存数据
    * @param billRecord 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(BillRecord billRecord) throws Exception {
        return insert("BillRecordMapper.insert",billRecord);
    }

    /**
    * 根据 id 修改
    * @param billRecord 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(BillRecord billRecord) throws Exception {
        return update("BillRecordMapper.updateById",billRecord);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return delete("BillRecordMapper.deleteById",id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public BillRecord selectById(String id) {
        return selectOne("BillRecordMapper.selectById",id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return delete("BillRecordMapper.deleteByIds",ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<BillRecord> list(Map map) {
        return list("BillRecordMapper.list",map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<BillRecord> list(Map map,int page,int size) {
        return list("BillRecordMapper.list",map,new RowBounds(page,size));
    }

    public Integer updateFinishStatus(String id) throws Exception {
        return update("BillRecordMapper.updateFinishStatus", id);
    }

    public BillRecord findByNumber(String number) {
        return selectOne("BillRecordMapper.findByNumber", number);
    }

	public List<BillRecord> findByUserId(String userId) {
		return arrayList("BillRecordMapper.findByUserId", userId);
	}
}
