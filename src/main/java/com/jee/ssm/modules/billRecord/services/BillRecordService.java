package com.jee.ssm.modules.billRecord.services;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.jee.ssm.common.utils.hik.HikUtils;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageInfo;
import com.jee.ssm.common.utils.CalculateUtils;
import com.jee.ssm.model.BillRecord;
import com.jee.ssm.model.User;
import com.jee.ssm.model.hik.receive.HikBill;
import com.jee.ssm.modules.billRecord.dao.BillRecordDao;
import com.jee.ssm.modules.hikBill.dao.HikBillDao;
import com.jee.ssm.modules.ssm.services.BaseService;
import com.jee.ssm.modules.user.dao.UserDao;

/**
* 订单记录管理 Service
* @author GaoXiang
* @version 1.0
*/
@Service
public class BillRecordService extends BaseService<BillRecord> {

    @Resource
    private BillRecordDao billRecordDao;

    @Resource
    private UserDao userDao;
    
    @Resource
    private HikBillDao hikBillDao;

    /**
    * 保存数据
    * @param billRecord 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(BillRecord billRecord) throws Exception {
        return billRecordDao.insert(billRecord);
    }

    /**
    * 根据 id 修改
    * @param billRecord 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(BillRecord billRecord) throws Exception {
        return billRecordDao.updateById(billRecord);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return billRecordDao.deleteById(id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public BillRecord selectById(String id) {
        return billRecordDao.selectById(id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return billRecordDao.deleteByIds(ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<BillRecord> list(Map map) {
        return billRecordDao.list(map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<BillRecord> list(Map map,int page,int size) {
        return billRecordDao.list(map,page,size);
    }


    public Integer updateFinishStatus(BillRecord billRecord) throws Exception {
        billRecordDao.updateFinishStatus(billRecord.getNumber());
        User user = userDao.selectById(billRecord.getUserId());
        if(user != null) {
            user.setWalletBalance(CalculateUtils.add(user.getWalletBalance(), billRecord.getPayAmount()));
            userDao.updateWalletBalanceById(user);
        }
        return 1;
    }

    /**
     * 根据订单编号查询订单
     * @param number 订单编号
     * @return 订单详情
     */
    public BillRecord findByNumber(String number) {
        return billRecordDao.findByNumber(number);
    }

	public List<BillRecord> findByUserId(String userId) {
		return billRecordDao.findByUserId(userId);
	}

	public void updateHikBill(BillRecord billRecord, int payMethod) throws Exception {
		billRecordDao.updateFinishStatus(billRecord.getNumber());
		HikBill hb = hikBillDao.selectById(billRecord.getRelateId());
		if(hb != null) {
			hb.setPayStatus(1);
			hb.setPayMethod(payMethod);
			hb.setUpdateTime(new Date());
			hikBillDao.updateById(hb);
            if(hb.getPayMethod()==3){
                System.out.println(hb.getBillNo());
                System.out.println(hb.getNeedPay());
                HikUtils.payParkingFee(hb.getBillNo(),hb.getNeedPay(),"5");

            }else if(hb.getPayMethod()==2){
                System.out.println(hb.getBillNo());
                System.out.println(hb.getNeedPay());
                HikUtils.payParkingFee(hb.getBillNo(),hb.getNeedPay(),"4");
            }
		}
	}
}
