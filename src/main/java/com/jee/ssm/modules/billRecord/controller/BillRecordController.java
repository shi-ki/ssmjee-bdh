package com.jee.ssm.modules.billRecord.controller;

import com.jee.ssm.model.BillRecord;
import com.jee.ssm.modules.billRecord.services.BillRecordService;
import com.jee.ssm.common.log.AdminControllerLog;
import com.jee.ssm.modules.ssm.controller.AdminBaseController;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.model.param.ParamMap;
import com.jee.ssm.common.utils.UUIDFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
* 订单记录管理 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/billRecord")
public class BillRecordController extends AdminBaseController<BillRecord> {


    /**
     * 进入订单记录添加页面 携带一个生成的id --> longId
     * @param model 返回的实体容器
     * @return 添加页面
     */
    @RequestMapping(value="/add")
    @RequiresPermissions("billRecord:add")
    public String add(ModelMap model){

        model.put("longId", UUIDFactory.getStringId());
        return "manager/billRecord/add";
    }


    /**
     * 进入订单记录编辑页面
     * @param model 返回订单记录的容器
     * @param id 订单记录id
     * @return 编辑页面
     */
    @RequestMapping(value="/edit")
    @RequiresPermissions("billRecord:edit")
    public String edit(ModelMap model, @RequestParam String id) {

        model.put("data",billRecordService.selectById(id));
        return "manager/billRecord/edit";
    }


    /**
     * 订单记录添加
     * @param billRecord 带id的订单记录对象
     * @return 成功状态
     */
    @RequestMapping(value="/save")
    @ResponseBody
    @RequiresPermissions("billRecord:add")
    @AdminControllerLog(description = "添加订单记录" )
    public Tip save(BillRecord billRecord)  {

        try {
            billRecordService.insert(billRecord);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"添加失败！");
        }

    }


    /**
     * 根据 id 修改订单记录
     * @param billRecord 带id的订单记录对象
     * @return 成功状态
     */
    @RequestMapping(value="/update")
    @ResponseBody
    @RequiresPermissions("billRecord:edit")
    @AdminControllerLog(description = "修改订单记录" )
    public Tip update(BillRecord billRecord) {

        try {
            billRecordService.updateById(billRecord);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"修改失败！");
        }

    }


    /**
     * 根据 id 删除订单记录
     * @param id 订单记录id
     * @return 成功状态
     */
    @RequestMapping(value="/delete")
    @ResponseBody
    @RequiresPermissions("billRecord:delete")
    @AdminControllerLog(description = "删除订单记录" )
    public Tip delete(@RequestParam String id) {

        try {
            billRecordService.deleteById(id);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"删除失败！");
        }

    }


    /**
     * 根据 id 列表批量删除
     * @param ids 订单记录id List
     * @return 成功状态
     */
    @RequestMapping(value="/deleteByIds")
    @ResponseBody
    @RequiresPermissions("billRecord:delete")
    @AdminControllerLog(description = "批量删除订单记录" )
    public Tip deleteByIds(@RequestParam("ids") List<String> ids) {

        try {
            billRecordService.deleteByIds(ids);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"批量删除失败！");
        }

    }


    /**
     * 根据 id 查找订单记录
     * @param id 订单记录id
     * @return 订单记录对象 json
     */
    @RequestMapping(value="/findJson")
    @ResponseBody
    @RequiresPermissions("billRecord:list")
    public BillRecord find(@RequestParam String id) {

        return billRecordService.selectById(id);
    }


    /**
     * 获取订单记录列表 获取全部 不分页
     * @param request 请求参数
     * @return 订单记录列表页面
     */
    @RequestMapping(value="/all")
    @RequiresPermissions("billRecord:list")
    public String all(HttpServletRequest request,ModelMap modelMap) {

        return list(request,modelMap,1,0);
    }


    /**
     * 获取订单记录列表 分页
     * @param request 请求参数
     * @param page 第几页
     * @param size 每页大小
     * @return 订单记录列表页面
     */
    @RequestMapping(value="/list")
    @RequiresPermissions("billRecord:list")
    public String list(HttpServletRequest request,ModelMap modelMap,Integer page,Integer size) {

        modelMap.put("pageInfo",billRecordService.list(new ParamMap(request),page,size));
        modelMap.putAll(new ParamMap(request));
        return "manager/billRecord/list";
    }


    //---------------------------- property -------------------------------

    @Resource
    private BillRecordService billRecordService;

}
