package com.jee.ssm.modules.rescue.services;

import com.jee.ssm.modules.rescue.dao.RescueDao;
import com.jee.ssm.model.Rescue;
import com.jee.ssm.modules.ssm.services.BaseService;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
* 救援管理 Service
* @author GaoXiang
* @version 1.0
*/
@Service
public class RescueService extends BaseService<Rescue> {

    @Resource
    private RescueDao rescueDao;


    /**
    * 保存数据
    * @param rescue 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(Rescue rescue) throws Exception {
        return rescueDao.insert(rescue);
    }

    /**
    * 根据 id 修改
    * @param rescue 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(Rescue rescue) throws Exception {
        return rescueDao.updateById(rescue);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return rescueDao.deleteById(id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public Rescue selectById(String id) {
        return rescueDao.selectById(id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return rescueDao.deleteByIds(ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<Rescue> list(Map map) {
        return rescueDao.list(map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<Rescue> list(Map map,int page,int size) {
        return rescueDao.list(map,page,size);
    }


}
