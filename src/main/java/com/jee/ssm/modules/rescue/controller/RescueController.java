package com.jee.ssm.modules.rescue.controller;

import com.jee.ssm.model.Rescue;
import com.jee.ssm.modules.rescue.services.RescueService;
import com.jee.ssm.common.log.AdminControllerLog;
import com.jee.ssm.modules.ssm.controller.AdminBaseController;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.model.param.ParamMap;
import com.jee.ssm.common.utils.UUIDFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
* 救援管理 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/rescue")
public class RescueController extends AdminBaseController<Rescue> {


    /**
     * 进入救援添加页面 携带一个生成的id --> longId
     * @param model 返回的实体容器
     * @return 添加页面
     */
    @RequestMapping(value="/add")
    @RequiresPermissions("rescue:add")
    public String add(ModelMap model){

        model.put("longId", UUIDFactory.getStringId());
        return "manager/rescue/add";
    }


    /**
     * 进入救援编辑页面
     * @param model 返回救援的容器
     * @param id 救援id
     * @return 编辑页面
     */
    @RequestMapping(value="/edit")
    @RequiresPermissions("rescue:edit")
    public String edit(ModelMap model, @RequestParam String id) {

        model.put("data",rescueService.selectById(id));
        return "manager/rescue/edit";
    }


    /**
     * 救援添加
     * @param rescue 带id的救援对象
     * @return 成功状态
     */
    @RequestMapping(value="/save")
    @ResponseBody
    @RequiresPermissions("rescue:add")
    @AdminControllerLog(description = "添加救援" )
    public Tip save(Rescue rescue)  {

        try {
            rescueService.insert(rescue);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"添加失败！");
        }

    }


    /**
     * 根据 id 修改救援
     * @param rescue 带id的救援对象
     * @return 成功状态
     */
    @RequestMapping(value="/update")
    @ResponseBody
    @RequiresPermissions("rescue:edit")
    @AdminControllerLog(description = "修改救援" )
    public Tip update(Rescue rescue) {

        try {
            rescueService.updateById(rescue);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"修改失败！");
        }

    }


    /**
     * 根据 id 删除救援
     * @param id 救援id
     * @return 成功状态
     */
    @RequestMapping(value="/delete")
    @ResponseBody
    @RequiresPermissions("rescue:delete")
    @AdminControllerLog(description = "删除救援" )
    public Tip delete(@RequestParam String id) {

        try {
            rescueService.deleteById(id);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"删除失败！");
        }

    }


    /**
     * 根据 id 列表批量删除
     * @param ids 救援id List
     * @return 成功状态
     */
    @RequestMapping(value="/deleteByIds")
    @ResponseBody
    @RequiresPermissions("rescue:delete")
    @AdminControllerLog(description = "批量删除救援" )
    public Tip deleteByIds(@RequestParam("ids") List<String> ids) {

        try {
            rescueService.deleteByIds(ids);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"批量删除失败！");
        }

    }


    /**
     * 根据 id 查找救援
     * @param id 救援id
     * @return 救援对象 json
     */
    @RequestMapping(value="/findJson")
    @ResponseBody
    @RequiresPermissions("rescue:list")
    public Rescue find(@RequestParam String id) {

        return rescueService.selectById(id);
    }


    /**
     * 获取救援列表 获取全部 不分页
     * @param request 请求参数
     * @return 救援列表页面
     */
    @RequestMapping(value="/all")
    @RequiresPermissions("rescue:list")
    public String all(HttpServletRequest request,ModelMap modelMap) {

        return list(request,modelMap,1,0);
    }


    /**
     * 获取救援列表 分页
     * @param request 请求参数
     * @param page 第几页
     * @param size 每页大小
     * @return 救援列表页面
     */
    @RequestMapping(value="/list")
    @RequiresPermissions("rescue:list")
    public String list(HttpServletRequest request,ModelMap modelMap,Integer page,Integer size) {

        modelMap.put("pageInfo",rescueService.list(new ParamMap(request),page,size));
        modelMap.putAll(new ParamMap(request));
        return "manager/rescue/list";
    }


    //---------------------------- property -------------------------------

    @Resource
    private RescueService rescueService;

}
