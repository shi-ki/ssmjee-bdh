package com.jee.ssm.modules.mapPoint.controller;

import com.jee.ssm.model.MapPoint;
import com.jee.ssm.modules.mapPoint.services.MapPointService;
import com.jee.ssm.common.log.AdminControllerLog;
import com.jee.ssm.modules.ssm.controller.AdminBaseController;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.model.param.ParamMap;
import com.jee.ssm.common.utils.UUIDFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
* 点位管理 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/mapPoint")
public class MapPointController extends AdminBaseController<MapPoint> {


    /**
     * 进入点位添加页面 携带一个生成的id --> longId
     * @param model 返回的实体容器
     * @return 添加页面
     */
    @RequestMapping(value="/add")
    @RequiresPermissions("mapPoint:add")
    public String add(ModelMap model){

        model.put("longId", UUIDFactory.getStringId());
        return "manager/mapPoint/add";
    }


    /**
     * 进入点位编辑页面
     * @param model 返回点位的容器
     * @param id 点位id
     * @return 编辑页面
     */
    @RequestMapping(value="/edit")
    @RequiresPermissions("mapPoint:edit")
    public String edit(ModelMap model, @RequestParam String id) {

        model.put("data",mapPointService.selectById(id));
        return "manager/mapPoint/edit";
    }


    /**
     * 点位添加
     * @param mapPoint 带id的点位对象
     * @return 成功状态
     */
    @RequestMapping(value="/save")
    @ResponseBody
    @RequiresPermissions("mapPoint:add")
    @AdminControllerLog(description = "添加点位" )
    public Tip save(MapPoint mapPoint)  {

        try {
            mapPointService.insert(mapPoint);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"添加失败！");
        }

    }


    /**
     * 根据 id 修改点位
     * @param mapPoint 带id的点位对象
     * @return 成功状态
     */
    @RequestMapping(value="/update")
    @ResponseBody
    @RequiresPermissions("mapPoint:edit")
    @AdminControllerLog(description = "修改点位" )
    public Tip update(MapPoint mapPoint) {

        try {
            mapPointService.updateById(mapPoint);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"修改失败！");
        }

    }


    /**
     * 根据 id 删除点位
     * @param id 点位id
     * @return 成功状态
     */
    @RequestMapping(value="/delete")
    @ResponseBody
    @RequiresPermissions("mapPoint:delete")
    @AdminControllerLog(description = "删除点位" )
    public Tip delete(@RequestParam String id) {

        try {
            mapPointService.deleteById(id);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"删除失败！");
        }

    }


    /**
     * 根据 id 列表批量删除
     * @param ids 点位id List
     * @return 成功状态
     */
    @RequestMapping(value="/deleteByIds")
    @ResponseBody
    @RequiresPermissions("mapPoint:delete")
    @AdminControllerLog(description = "批量删除点位" )
    public Tip deleteByIds(@RequestParam("ids") List<String> ids) {

        try {
            mapPointService.deleteByIds(ids);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"批量删除失败！");
        }

    }


    /**
     * 根据 id 查找点位
     * @param id 点位id
     * @return 点位对象 json
     */
    @RequestMapping(value="/findJson")
    @ResponseBody
    @RequiresPermissions("mapPoint:list")
    public MapPoint find(@RequestParam String id) {

        return mapPointService.selectById(id);
    }


    /**
     * 获取点位列表 获取全部 不分页
     * @param request 请求参数
     * @return 点位列表页面
     */
    @RequestMapping(value="/all")
    @RequiresPermissions("mapPoint:list")
    public String all(HttpServletRequest request,ModelMap modelMap) {

        return list(request,modelMap,1,0);
    }


    /**
     * 获取点位列表 分页
     * @param request 请求参数
     * @param page 第几页
     * @param size 每页大小
     * @return 点位列表页面
     */
    @RequestMapping(value="/list")
    @RequiresPermissions("mapPoint:list")
    public String list(HttpServletRequest request,ModelMap modelMap,Integer page,Integer size) {

        modelMap.put("pageInfo",mapPointService.list(new ParamMap(request),page,size));
        modelMap.putAll(new ParamMap(request));
        return "manager/mapPoint/list";
    }


    //---------------------------- property -------------------------------

    @Resource
    private MapPointService mapPointService;

}
