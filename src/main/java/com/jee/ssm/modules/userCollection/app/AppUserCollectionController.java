package com.jee.ssm.modules.userCollection.app;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jee.ssm.common.utils.TokenUtils;
import com.jee.ssm.common.utils.UUIDFactory;
import com.jee.ssm.common.utils.hik.HikUtils;
import com.jee.ssm.model.UserCollection;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.modules.userCollection.services.UserCollectionService;

/**
* 我的收藏管理 客户端 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/app/userCollection")
public class AppUserCollectionController {

	/**
	 * 添加收藏
	 * @param userId 用户id
	 * @param parkDepotId 停车场标识
	 * @return 提示
	 */
	@RequestMapping(value = "/saveUserCollection", method = RequestMethod.GET)
	@ResponseBody
	public Tip saveUserCollection(long timestamp, String token, String userId, String parkDepotId) {
		Tip tip = TokenUtils.verifyToken(timestamp, token);
        if(tip.getSuccess()) {
        	try {
        		UserCollection uc = new UserCollection(userId, parkDepotId);
        		List<UserCollection> list = userCollectionService.findByUserCollection(uc);
        		if(list.size() == 0) {
        			list = userCollectionService.findByUserCollection(uc);
            		if(list.size() < 20) {
            			uc = new UserCollection(UUIDFactory.getStringId(), userId, parkDepotId, new Date());
            			userCollectionService.insert(uc);
            			tip = new Tip("收藏成功");
            		} else {
            			tip = new Tip(3, "最多只能收藏20个停车场");
            		}
        		} else {
        			tip = new Tip(2, "您已收藏此停车场");
        		}
			} catch (Exception e) {
				e.printStackTrace();
				tip = new Tip(1, "系统异常，收藏失败，请刷新重试");
			}
        }
        return tip;
	}
	
	/**
	 * 取消收藏
	 * @param id 收藏标识
	 * @return
	 */
	@RequestMapping(value = "/deleteUserCollection", method = RequestMethod.GET)
	@ResponseBody
	public Tip deleteUserCollection(long timestamp, String token, String id) {
		Tip tip = TokenUtils.verifyToken(timestamp, token);
        if(tip.getSuccess()) {
        	try {
				userCollectionService.deleteById(id);
				tip = new Tip("取消收藏成功");
			} catch (Exception e) {
				e.printStackTrace();
				tip = new Tip(1, "系统异常，取消收藏失败，请刷新重试");
			}
        }
        return tip;
	}
	
	/**
	 * 根据用户id查询收藏列表
	 * @param userId 用户id
	 * @return 收藏列表
	 */
	@RequestMapping(value = "/findCollectionByUserId", method = RequestMethod.GET)
	@ResponseBody
	public Tip findCollectionByUserId(long timestamp, String token, String userId) {
		Tip tip = TokenUtils.verifyToken(timestamp, token);
        if(tip.getSuccess()) {
        	List<UserCollection> list = userCollectionService.findParkDepotByUserId(userId);
        	for(UserCollection uc : list) {
        		uc.setParkDepot(HikUtils.handleParkDepot(uc.getParkDepot()));
        	}
        	tip = new Tip(list);
        }
        return tip;
	}
	
    //---------------------------- property -------------------------------

    @Resource
    private UserCollectionService userCollectionService;

}
