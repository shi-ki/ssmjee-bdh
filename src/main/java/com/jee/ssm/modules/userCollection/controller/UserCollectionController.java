package com.jee.ssm.modules.userCollection.controller;

import com.jee.ssm.model.UserCollection;
import com.jee.ssm.modules.userCollection.services.UserCollectionService;
import com.jee.ssm.common.log.AdminControllerLog;
import com.jee.ssm.modules.ssm.controller.AdminBaseController;
import com.jee.ssm.model.json.Tip;
import com.jee.ssm.model.param.ParamMap;
import com.jee.ssm.common.utils.UUIDFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
* 我的收藏管理 Controller
* @author GaoXiang
* @version 1.0
*/
@Controller
@RequestMapping("/userCollection")
public class UserCollectionController extends AdminBaseController<UserCollection> {


    /**
     * 进入我的收藏添加页面 携带一个生成的id --> longId
     * @param model 返回的实体容器
     * @return 添加页面
     */
    @RequestMapping(value="/add")
    @RequiresPermissions("userCollection:add")
    public String add(ModelMap model){

        model.put("longId", UUIDFactory.getStringId());
        return "manager/userCollection/add";
    }


    /**
     * 进入我的收藏编辑页面
     * @param model 返回我的收藏的容器
     * @param id 我的收藏id
     * @return 编辑页面
     */
    @RequestMapping(value="/edit")
    @RequiresPermissions("userCollection:edit")
    public String edit(ModelMap model, @RequestParam String id) {

        model.put("data",userCollectionService.selectById(id));
        return "manager/userCollection/edit";
    }


    /**
     * 我的收藏添加
     * @param userCollection 带id的我的收藏对象
     * @return 成功状态
     */
    @RequestMapping(value="/save")
    @ResponseBody
    @RequiresPermissions("userCollection:add")
    @AdminControllerLog(description = "添加我的收藏" )
    public Tip save(UserCollection userCollection)  {

        try {
            userCollectionService.insert(userCollection);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"添加失败！");
        }

    }


    /**
     * 根据 id 修改我的收藏
     * @param userCollection 带id的我的收藏对象
     * @return 成功状态
     */
    @RequestMapping(value="/update")
    @ResponseBody
    @RequiresPermissions("userCollection:edit")
    @AdminControllerLog(description = "修改我的收藏" )
    public Tip update(UserCollection userCollection) {

        try {
            userCollectionService.updateById(userCollection);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"修改失败！");
        }

    }


    /**
     * 根据 id 删除我的收藏
     * @param id 我的收藏id
     * @return 成功状态
     */
    @RequestMapping(value="/delete")
    @ResponseBody
    @RequiresPermissions("userCollection:delete")
    @AdminControllerLog(description = "删除我的收藏" )
    public Tip delete(@RequestParam String id) {

        try {
            userCollectionService.deleteById(id);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"删除失败！");
        }

    }


    /**
     * 根据 id 列表批量删除
     * @param ids 我的收藏id List
     * @return 成功状态
     */
    @RequestMapping(value="/deleteByIds")
    @ResponseBody
    @RequiresPermissions("userCollection:delete")
    @AdminControllerLog(description = "批量删除我的收藏" )
    public Tip deleteByIds(@RequestParam("ids") List<String> ids) {

        try {
            userCollectionService.deleteByIds(ids);
            return new Tip();
        } catch (Exception e) {
            //e.printStackTrace();
            return new Tip(1,"批量删除失败！");
        }

    }


    /**
     * 根据 id 查找我的收藏
     * @param id 我的收藏id
     * @return 我的收藏对象 json
     */
    @RequestMapping(value="/findJson")
    @ResponseBody
    @RequiresPermissions("userCollection:list")
    public UserCollection find(@RequestParam String id) {

        return userCollectionService.selectById(id);
    }


    /**
     * 获取我的收藏列表 获取全部 不分页
     * @param request 请求参数
     * @return 我的收藏列表页面
     */
    @RequestMapping(value="/all")
    @RequiresPermissions("userCollection:list")
    public String all(HttpServletRequest request,ModelMap modelMap) {

        return list(request,modelMap,1,0);
    }


    /**
     * 获取我的收藏列表 分页
     * @param request 请求参数
     * @param page 第几页
     * @param size 每页大小
     * @return 我的收藏列表页面
     */
    @RequestMapping(value="/list")
    @RequiresPermissions("userCollection:list")
    public String list(HttpServletRequest request,ModelMap modelMap,Integer page,Integer size) {

        modelMap.put("pageInfo",userCollectionService.list(new ParamMap(request),page,size));
        modelMap.putAll(new ParamMap(request));
        return "manager/userCollection/list";
    }


    //---------------------------- property -------------------------------

    @Resource
    private UserCollectionService userCollectionService;

}
