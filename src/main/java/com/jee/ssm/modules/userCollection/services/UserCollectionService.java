package com.jee.ssm.modules.userCollection.services;

import com.jee.ssm.modules.userCollection.dao.UserCollectionDao;
import com.jee.ssm.model.UserCollection;
import com.jee.ssm.modules.ssm.services.BaseService;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
* 我的收藏管理 Service
* @author GaoXiang
* @version 1.0
*/
@Service
public class UserCollectionService extends BaseService<UserCollection> {

    @Resource
    private UserCollectionDao userCollectionDao;


    /**
    * 保存数据
    * @param userCollection 实体对象
    * @return 实体id
    * @throws Exception 数据保存异常
    */
    public Integer insert(UserCollection userCollection) throws Exception {
        return userCollectionDao.insert(userCollection);
    }

    /**
    * 根据 id 修改
    * @param userCollection 带id的实体对象
    * @return 受影响的行数
    * @throws Exception 数据修改异常
    */
    public Integer updateById(UserCollection userCollection) throws Exception {
        return userCollectionDao.updateById(userCollection);
    }

    /**
    * 根据 id 删除
    * @param id 数据id
    * @return 受影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteById(String id) throws Exception {
        return userCollectionDao.deleteById(id);
    }

    /**
    * 根据 id 查找
    * @param id 实体id
    * @return 实体
    */
    public UserCollection selectById(String id) {
        return userCollectionDao.selectById(id);
    }

    /**
    * 根据 id 批量删除
    * @param ids 要删除的id
    * @return 影响的行数
    * @throws Exception 数据删除异常
    */
    public Integer deleteByIds(List<String> ids) throws Exception {
        return userCollectionDao.deleteByIds(ids);
    }

    /**
    * 查询列表
    * @param map 参数
    * @return 列表
    */
    public PageInfo<UserCollection> list(Map map) {
        return userCollectionDao.list(map);
    }

    /**
    * 查询列表 带分页
    * @param map 参数
    * @param page 页码
    * @param size 每页大小
    * @return 列表
    */
    public PageInfo<UserCollection> list(Map map,int page,int size) {
        return userCollectionDao.list(map,page,size);
    }

	public List<UserCollection> findParkDepotByUserId(String userId) {
		return userCollectionDao.findParkDepotByUserId(userId);
	}

	public List<UserCollection> findByUserCollection(UserCollection uc) {
		return userCollectionDao.findByUserCollection(uc);
	}


}
