package com.jee.ssm.common.socket.guideScreen;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

public class SocketStart implements ApplicationListener<ContextRefreshedEvent> {
	
	@Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        new SocketServer().start();
    }

}
