package com.jee.ssm.common.socket.rescue;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.web.socket.WebSocketSession;

public class RescueUtil {
	
	private static Map<String, WebSocketSession> map = Collections.synchronizedMap(new HashMap<>());
	
	public static WebSocketSession get(String id) {
		return map.get(id);
	}
	
	public static void put(String id, WebSocketSession session) {
		map.put(id, session);
	}
	
	public static void remove(String id) {
		map.remove(id);
	}

}
