package com.jee.ssm.common.socket.guideScreen;

import java.util.ArrayList;
import java.util.List;

import com.jee.ssm.common.utils.CRC16;

public class HandleUtils {
	
	private static String hexStr = "0123456789ABCDEF";
	
	public static String get16Str(int paramStr) {
		return String.valueOf(hexStr.charAt((paramStr & 0xF0) >> 4)) + String.valueOf(hexStr.charAt(paramStr & 0x0F));
	}
	
	public static List<Byte> getHead() {
		List<Byte> bytes = new ArrayList<>();
		bytes.add((byte)0xAA);
        bytes.add((byte)0xFF);
        bytes.add((byte)0xFF);
		return bytes;
	}
	
	public static List<Byte> getFoot(List<Byte> bytes) {
		
		bytes.add((byte)0xCC);
        byte[] bs = new byte[bytes.size()];
        for(int i = 0;i < bytes.size(); i ++) {
        	bs[i] = bytes.get(i);
        }
        int crc = CRC16.pubCalcCRC(bs);
        bytes.add((byte) (crc % 256));
        bytes.add((byte) (crc / 256));
		
		return bytes;
	}
	
}
