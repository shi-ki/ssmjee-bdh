package com.jee.ssm.common.exception;

/**
 */
public class ApiException extends RuntimeException{


    private Integer errorCode;

    public ApiException() {
        super();
    }

    public ApiException(String errorMessage) {
        super(errorMessage);
    }

    public ApiException(int errorCode, String errorMessage) {
        super(errorMessage);
        this.errorCode = errorCode;
    }


    public ApiException(Throwable cause) {
        super(cause);
    }

    public ApiException(String errorMessage, Throwable cause) {
        super(errorMessage, cause);
    }

    public ApiException(int errorCode, String errorMessage, Throwable cause) {
        super(errorMessage, cause);
        this.errorCode = errorCode;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

}
