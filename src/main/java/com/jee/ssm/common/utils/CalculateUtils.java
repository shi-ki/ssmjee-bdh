package com.jee.ssm.common.utils;

import java.math.BigDecimal;

import com.jee.ssm.model.ParkDepot;

public class CalculateUtils {

    /**
     * 其他类型 加法
     * @param a 被加数
     * @param b 加数
     * @return
     */
    public static BigDecimal add(Object a, Object b) {
        BigDecimal c = BigDecimal.valueOf(null2Double(a));
        BigDecimal d = BigDecimal.valueOf(null2Double(b));
        return add(c, d);
    }

    /**
     * 其他类型 减法
     * @param a 被减数
     * @param b 减数
     * @return
     */
    public static BigDecimal subtract(Object a, Object b) {
        BigDecimal c = BigDecimal.valueOf(null2Double(a));
        BigDecimal d = BigDecimal.valueOf(null2Double(b));
        return subtract(c, d);
    }

    /**
     * 其他类型 乘法
     * @param a 被乘数
     * @param b 乘数
     * @return
     */
    public static BigDecimal multiply(Object a, Object b) {
        BigDecimal c = BigDecimal.valueOf(null2Double(a));
        BigDecimal d = BigDecimal.valueOf(null2Double(b));
        return multiply(c, d);
    }

    /**
     * 其他类型 除法
     * @param a 被除数
     * @param b 除数
     * @return
     */
    public static BigDecimal divide(Object a, Object b) {
        BigDecimal c = BigDecimal.valueOf(null2Double(a));
        BigDecimal d = BigDecimal.valueOf(null2Double(b));
        return divide(c, d);
    }

    /**
     * 加法
     * @param a 被加数
     * @param b 加数
     * @return
     */
    public static BigDecimal add(BigDecimal a, BigDecimal b) {
        return a.add(b);
    }

    /**
     * 减法
     * @param a 被减数
     * @param b 减数
     * @return
     */
    public static BigDecimal subtract(BigDecimal a, BigDecimal b) {
        return a.subtract(b);
    }

    /**
     * 乘法
     * @param a 被乘数
     * @param b 乘数
     * @return
     */
    public static BigDecimal multiply(BigDecimal a, BigDecimal b) {
        return a.multiply(b);
    }

    /**
     * 除法
     * @param a 被除数
     * @param b 除数
     * @return
     */
    public static BigDecimal divide(BigDecimal a, BigDecimal b) {
        return a.divide(b);
    }

    public static BigDecimal null2Decimal(Object o) {
        BigDecimal b = BigDecimal.valueOf(null2Double(o));
        b = b.setScale(2, BigDecimal.ROUND_HALF_DOWN);
        return b;
    }

    public static double null2Double(Object o) {
        double d = 0.00D;
        try {
            d = Double.parseDouble(null2String(o));
        } catch(Exception e) { }
        return d;
    }

    public static String null2String(Object o) {
        return o == null ? "" : String.valueOf(o);
    }
    
    public static ParkDepot findNeighPosition(double latitude, double longitude, ParkDepot pd){
        //先计算查询点的经纬度范围
        double r = 6371;//地球半径千米
        double dis = 2.0;//2.0千米距离
        double dlng =  2*Math.asin(Math.sin(dis/(2*r))/Math.cos(latitude*Math.PI/180));
        dlng = dlng*180/Math.PI;//角度转为弧度
        double dlat = dis/r;
        dlat = dlat*180/Math.PI;
        double minlat = latitude - dlat;
        double maxlat = latitude + dlat;
        double minlng = longitude - dlng;
        double maxlng = longitude + dlng;

        pd.setMinlng(minlng);
        pd.setMaxlng(maxlng);
        pd.setMinlat(minlat);
        pd.setMaxlat(maxlat);
        
        return pd;
    }

    public static void main(String[] args) {
//        String a = "10";
//        System.out.println(a);
//        BigDecimal b = null2Decimal(a);
//        System.out.println(b);
//        BigDecimal c = multiply(b, 100);
//        System.out.println(c);
//        Integer d = c.intValue();
//        System.out.println(d);
//        Map<String, Object> map = new HashMap<>();
//        map.put("abce", String.valueOf(d));
//        String e = (String)map.get("abce");
//        System.out.println(e);
//        Integer f = Integer.valueOf(e);
//        System.out.println(f);

        System.out.println(divide(4000, 100));

    }

}
