package com.jee.ssm.common.utils.mq.jms;

public class SimpleTestMain {
	
	/** 
     * @param args 
     */  
    public static void main(String[] args) throws Exception{  
        SimpleSubscriber consumer = new SimpleSubscriber("TestClientId");  
        consumer.start();  
          
        SimplePublisher productor = new SimplePublisher();  
        for(int i=0; i<10; i++){  
            productor.send("message content:" + i);  
        }  
        productor.close();  
        //consumer.close();  
    }  

    
    public static void jsm() throws Exception {
    	SimpleSubscriber consumer = new SimpleSubscriber("TestClientId");  
        consumer.start();  
          
        SimplePublisher productor = new SimplePublisher();  
        for(int i=0; i<10; i++){  
            productor.send("message content:" + i);  
        }  
        productor.close();  
        //consumer.close();  
    }
}
