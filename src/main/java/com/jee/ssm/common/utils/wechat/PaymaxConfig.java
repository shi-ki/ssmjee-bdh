package com.jee.ssm.common.utils.wechat;

public class PaymaxConfig {
    
	//微信appid
	private String appId;
	
	//微信商户号
	private String mchId;
	
	//商户密钥
	private String key;
	
	//异步通知地址
	private String notifyUrl;
	
	//微信支付方式
	private String tradeType;
	
	//微信接口地址
	private String url;
	
	//微信提交方式
	private String method;
	
	public PaymaxConfig(String appId, String mchId, String key, String notifyUrl, String tradeType, String url, String method) {
		super();
		this.appId = appId;
		this.mchId = mchId;
		this.key = key;
		this.notifyUrl = notifyUrl;
		this.tradeType = tradeType;
		this.url = url;
		this.method = method;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getMchId() {
		return mchId;
	}

	public void setMchId(String mchId) {
		this.mchId = mchId;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	public String getTradeType() {
		return tradeType;
	}

	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}
	
}
