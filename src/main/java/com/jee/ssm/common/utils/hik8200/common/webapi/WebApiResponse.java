package com.jee.ssm.common.utils.hik8200.common.webapi;

import com.jee.ssm.common.utils.hik8200.common.constant.VideoConstant;

/**
 * @author zhangtuo
 * @Date 2017/5/8
 */
public class WebApiResponse<T> {

    private String	code;
    private String	msg;
    private T		data;

    public WebApiResponse() {
    }

    public WebApiResponse(String code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public static <T> WebApiResponse<T> success(T data) {
        return new WebApiResponse<T>(VideoConstant.WebApiResDict.SUCCESS_CODE, VideoConstant.HttpMsgDict.SUCCESS_MSG, data);
    }

    public static <T> WebApiResponse<T> error(String msg) {
        return new WebApiResponse<T>(VideoConstant.WebApiResDict.ERROR_CODE, msg, null);
    }
}
