package com.jee.ssm.common.utils;

import java.util.Map;

public class SingUtils {
	
	public static String sing(Map<String, String> param, String secret){
		String orgnalStr = ParamFormatUtil.formatParameterMap(param, false, false); //步骤一：参数排序
		orgnalStr += secret; //步骤二：后面追加accessSecret
		String signedsignature = SHA1.encode(orgnalStr); //步骤三：进行SHA1签名，并转换为16进制小写字符串
		return signedsignature;
	}
	
	public static void main(String[] args) {
		String signedsignature = SHA1.encode("accessKey=5051B42F23C993C2"
				+ "&autoCheck=1&berthNumber=PA20160714&carType=0&harCode=1232125"
				+ "&noncestr=2245447845&parkCode=20160317125733&plateColor=黑&plateNumber=京A88888&reportTime=20160301143322&type=1adfdcdfdffdfdf"); //步骤三：进行SHA1签名
		System.out.println(signedsignature);
	}
	
}
