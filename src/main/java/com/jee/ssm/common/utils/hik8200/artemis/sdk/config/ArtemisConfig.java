package com.jee.ssm.common.utils.hik8200.artemis.sdk.config;

/**
 * artemis host、appKey、appSecret配置
 * @author zhangtuo
 * @Date 2017/4/26
 */
public class ArtemisConfig {
    private String host;
    private String appKey;
    private String appSecret;

    public ArtemisConfig(String host, String appKey, String appSecret) {
		super();
		this.host = host;
		this.appKey = appKey;
		this.appSecret = appSecret;
	}

	public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }
}
