package com.jee.ssm.common.utils.hik8200.conf;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * SpringMVC 扩展配置
 * @author zhangtuo
 * @Date 2017/3/27
 */
@Configuration
public class MvcConfiguration extends WebMvcConfigurerAdapter{
    /**
     * springmvc ROOT域指定的页面跳转
     */
    @Override
    public  void addViewControllers(ViewControllerRegistry registry){
        registry.addViewController("/").setViewName("playview");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/views/**").addResourceLocations("classpath:/views/");
        super.addResourceHandlers(registry);
    }

    /**
     * null值不返回json
     * @return
     */
    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(Include.NON_NULL);
        return objectMapper;
    }
}
