package com.jee.ssm.common.utils.hik8200.common.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * 对model的一些工具操作
 * @author zhangtuo
 * @Date 2017/4/25
 */
public class ModelUtil {

    /**
     * 将model转换成querysMap集合
     * @param obj model
     * @return
     */
    public static Map<String, String> modelToQuerysMap(Object obj) {

        Field[] field = obj.getClass().getDeclaredFields();
        Map<String, String> map = new HashMap<String, String>();
        try {
            for (int i = 0; i < field.length; i++) {
                String name = field[i].getName().substring(0, 1).toUpperCase()
                        + field[i].getName().substring(1);//属性名, 并将属性名的首字母大写，方便构造get函数名。
                Method m = obj.getClass().getMethod("get" + name);
                String type = field[i].getGenericType().toString();//获取属性类型
                if (type.equals("class java.lang.String")) {
                    map.put(field[i].getName(), (String) m.invoke(obj));
                } else {
                    map.put(field[i].getName(), m.invoke(obj).toString());
                }
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return map;
    }
}
