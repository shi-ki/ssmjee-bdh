package com.jee.ssm.common.utils;

import com.jee.ssm.model.json.Tip;

public class TokenUtils {

    public static Tip verifyToken(long timestamp, String t) {
        Tip tip = new Tip();
        if((System.currentTimeMillis() - timestamp) > 300000) {
            tip = new Tip(1, "请同步系统时间");
        } else {
            String token = MD5.md5(timestamp + "app").toLowerCase();
            if(!t.equals(token)) {
                tip = new Tip(1, "无效认证");
            }
        }
        return tip;
    }

}
