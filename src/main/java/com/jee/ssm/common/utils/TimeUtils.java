package com.jee.ssm.common.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimeUtils {
	
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	private static final SimpleDateFormat sdf_second = new SimpleDateFormat("yyyyMMddHHmmss");
	
	private static final SimpleDateFormat sdf_week = new SimpleDateFormat("EEEE");
	
	private static final SimpleDateFormat sdf_year = new SimpleDateFormat("yyyy-MM-dd");
	
	/** 
	 * 毫秒转化 
	 */  
	public static String formatTime(long ms) {
		int ss = 1000;
		int mi = ss * 60;
		int hh = mi * 60;
		int dd = hh * 24;
		long day = ms / dd;
		long hour = (ms - day * dd) / hh;
		
//		long hour_left = (ms - day * dd) % hh;
		
		long minute = (ms - day * dd - hour * hh) / mi;
		long second = (ms - day * dd - hour * hh - minute * mi) / ss;
		long milliSecond = ms - day * dd - hour * hh - minute * mi - second * ss;
		String strDay = day < 10 ? "0" + day : "" + day; //天
		String strHour = hour < 10 ? "0" + hour : "" + hour;//小时
		String strMinute = minute < 10 ? "0" + minute : "" + minute;//分钟
		String strSecond = second < 10 ? "0" + second : "" + second;//秒
		String strMilliSecond = milliSecond < 10 ? "0" + milliSecond : "" + milliSecond;//毫秒
		strMilliSecond = milliSecond < 100 ? "0" + strMilliSecond : "" + strMilliSecond;
		return strDay + " 天 " + strHour + " 小时 " + strMinute + " 分钟 " + strSecond + " 秒 " + strMilliSecond + " 毫秒 ";
	}
	
	/**
	 * 毫秒数转小时数
	 */
	public static int getHours(long ms) {
		int ss = 1000;
		int mi = ss * 60;
		int hh = mi * 60;
		int dd = hh * 24;
		long day = ms / dd;
		long day_hour = day * 24;
		long hour = (ms - day * dd) / hh;
		hour += day_hour;
		long ms_left = (ms - day * dd) % hh;
		if(ms_left > 0) {
			hour += 1;
		}
		return new Long(hour).intValue();  
	}
	
	public static int getMinutes(long ms) {
		int ss = 1000;
		int mi = ss * 60;
		int hh = mi * 60;
		int dd = hh * 24;
		long day = ms / dd;
		long day_hour = day * 24;
		long hour = (ms - day * dd) / hh;
		long minute_left = (ms - day * dd - hour * hh) / mi;
		hour += day_hour;
		long minutes = hour * 60;
		minutes += minute_left;
		return new Long(minutes).intValue();
	}
	
	/**
	 * 时分秒转毫秒数
	 */
	public static long getMilliSecond(String time) {
		String[] t = time.split(":");
		return ((60 * 60 * Long.valueOf(t[0])) + (60 * Long.valueOf(t[1]) + Long.valueOf(t[2]))) * 1000;
	}
	
	/**
	 * 得到i天后的日期
	 * @param i 可为负数
	 * @return i天后的日期
	 */
	public static Date getWantedTime(Integer i) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, i);
		return cal.getTime();
	}

	/*当前日期的*/
	public static Date getWantedDateWithoutOmit(Integer i) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, i);
		cal.add(Calendar.HOUR, i);
		cal.add(Calendar.MINUTE, i);
		cal.add(Calendar.SECOND, i);
		return cal.getTime();
	}
	
	public static Date getWantedHour(Integer i) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR, i);
		return cal.getTime();
	}
	
	public static Date getWantedHourWithoutOmit(Integer i) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR, i);
		cal.add(Calendar.MINUTE, i);
		cal.add(Calendar.SECOND, i);
		return cal.getTime();
	}
	
	/**
	 * 获取指定日期在一星期中的数字并把星期替换为周，比如2018/08/27转为周一
	 */
	public static String getDayOfweek(Date date){
		return sdf_week.format(date).replaceAll("星期", "周");
	}
	
	/**
	 * 获取指定日期的年月日时分秒，格式如：2018-08-27 00:00:00
	 */
	public static String getStringDayTime(Date date) {
		return sdf.format(date);
	}
	
	/**
	 * 获取指定日期的年月日，格式如：2018-08-27
	 */
	public static String getStringDay(Date date) {
		return sdf_year.format(date);
	}
	
	/**
	 * 获取指定日期的年月日时分秒，格式如：20180827142639
	 */
	public static String getStringSecond(Date date) {
		return sdf_second.format(date);
	}
	
	/**
	 * 字符串转日期，字符串格式如：2018-08-27 00:00:00(此方法需捕获异常)
	 */
	public static Date getDateByString(String dateTime) throws ParseException {
		return sdf.parse(dateTime);
	}

	/**
	 * 字符串转日期，字符串格式如：2018-08-27 00:00:00(此方法不需捕获异常，有异常则返回当前时间)
	 */
	public static Date getDateByStringNoException(String dateTime) {
		Date d;
		try {
			d = sdf.parse(dateTime);
		} catch (ParseException e) {
			d = new Date();
		}
		return d;
	}
	
	/**
	 * 年月日转换为日期类型，格式如：2018-08-27 ，可带 时分秒，但时分秒会被忽略
	 */
	public static Date getDateByYear(String date) throws ParseException {
		return sdf_year.parse(date);
	}
	
	/**
	 * 同上，不抛出异常
	 */
	public static Date getDateByYearNoException(String date) {
		Date d;
		try {
			d = sdf_year.parse(date);
		} catch (ParseException e) {
			d = new Date();
		}
		return d;
	}
	
	/**
	 * 获取今天开始时的毫秒数
	 */
	public static long getTodayStartTime() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime().getTime();
    }
	
	public static String getStringDateByMillionSeconds(long millionSeconds) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(millionSeconds); 
		return sdf.format(c.getTime());
	}
	
	public static Date getDateByMillionSeconds(long millionSeconds) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(millionSeconds); 
		return c.getTime();
	}
	
	public static void main(String[] args) throws ParseException {
		
//		System.out.println(getHours(46554451532115l));
		
//		System.out.println(getMinutes(3000000));
		System.out.println(getWantedDateWithoutOmit(-1));


	}

}
