package com.jee.ssm.common.utils.hik;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jee.ssm.common.utils.TimeUtils;
import com.jee.ssm.common.utils.UUIDFactory;
import com.jee.ssm.model.ParkDepot;
import com.jee.ssm.model.hik.*;
import com.jee.ssm.model.hik.receive.*;
import com.jee.ssm.model.json.Tip;
import org.springframework.mail.MailParseException;

public class HikUtils {
	
	private static String ip = "117.132.167.10";
    private static String port = "8086";

    private static String appkey = "t99ibcy5";
    private static String secret = "2xf3E9rX19+l0AF6p/ZUSJTB7BCRkuBN+r+setsTcwkAQlEweJTOrRWUNg9oqyHQ";
    
    public static Tip getParkingPage(Integer pageNo, Integer pageSize) {
    	Tip tip = new Tip();
    	String url = "http://{0}:{1}/cloud/service/getParkingInfos?token={2}";
        GetParkingInfosRequest request = new GetParkingInfosRequest();
        request.setAppkey(appkey);
        request.setTime(System.currentTimeMillis());
        request.setNonce(CommonUtil.getNonce());
        request.setPageNo(pageNo);
        request.setPageSize(pageSize);
        String data = JSONObject.toJSONString(request);
        url = getUrl(url, data);

        try {
			String rs = HttpUtil.doPost(url, data);
        	HikTip h = JSONObject.parseObject(rs, HikTip.class);
        	if(h.getErrorCode() == 0) {
        		tip = new Tip(h.getData());
        	} else {
        		tip.setSuccess(false);
        		tip.setCode(h.getErrorCode());
        		tip.setMessage(h.getErrorMessage());
        	}
        } catch(Exception e) {
        	tip = new Tip(2, "请求数据异常");
        }
        return tip;
    }
    
    /**
     * 获取停车场列表
     * @param pageNo
     * @param pageSize
     * @return
     */
    public static Tip getParkingInfos(Integer pageNo, Integer pageSize) {
    	Tip tip = getParkingPage(pageNo, pageSize);
    	if(tip.getSuccess()) {
    		HikPage p = JSONObject.parseObject(String.valueOf(tip.getData()), HikPage.class);
    		tip = new Tip(p.getList());
    	}
        return tip;
    }
    
    public static Tip getParkingInfoByParkCode(String id) {
    	Tip tip = new Tip();
    	String url = "http://{0}:{1}/cloud/service/getParkingInfosByParkCodes?token={2}";
    	GetParkingInfosByParkCodeRequest request = new GetParkingInfosByParkCodeRequest();
        request.setAppkey(appkey);
        request.setTime(System.currentTimeMillis());
        request.setNonce(CommonUtil.getNonce());
        request.setParkCodes(id);
        String data = JSONObject.toJSONString(request);
        url = getUrl(url, data);
        try {
			String rs = HttpUtil.doPost(url, data);
        	HikTip h = JSONObject.parseObject(rs, HikTip.class);
        	if(h.getErrorCode() == 0) {
        		List<ParkDepotInfo> list = JSON.parseArray(JSONArray.toJSONString(h.getData()), ParkDepotInfo.class);
        		tip = new Tip(list.size() > 0 ? list.get(0) : null);
        	} else {
        		tip.setSuccess(false);
        		tip.setCode(h.getErrorCode());
                tip.setMessage(h.getErrorMessage());
        	}
        } catch(Exception e) {
        	e.printStackTrace();
        	tip = new Tip(2, "请求数据异常");
        }
    	return tip;
    }

    /**
     * 车位预约
     * @param parkCode
     * @param plateNo
     * @param plateColor
     * @param payTime
     * @param bookDuration
     * @param telephone
     * @param plotNum
     * @return
     */
	public static Tip parkReservation(String parkCode, String plateNo, Integer plateColor, String payTime, Integer bookDuration, String telephone, String plotNum) {
		Tip tip = new Tip();
		String url = "http://{0}:{1}/cloud/service/parkingReservation?token={2}";
        ParkingReservationRequestBO baseRequest = new ParkingReservationRequestBO();
        baseRequest.setPlateColor(plateColor);
        baseRequest.setPlateNo(plateNo);
        baseRequest.setBookDuration(bookDuration);
        baseRequest.setParkCode(parkCode);
        baseRequest.setPayAmount(2000);
        baseRequest.setPayTime(payTime);
        baseRequest.setPlotNum(plotNum == null ? "" : plotNum);
        baseRequest.setTelephone(telephone);
        baseRequest.setAppkey(appkey);
        baseRequest.setTime(System.currentTimeMillis());
        baseRequest.setNonce(CommonUtil.getNonce());
        String data = JSONObject.toJSONString(baseRequest);
        url = getUrl(url, data);
        try {
			String rs = HttpUtil.doPost(url, data);
        	HikTip h = JSONObject.parseObject(rs, HikTip.class);
        	if(h.getErrorCode() == 0) {
        		tip = new Tip(h.getData());
        	} else {
        		tip.setSuccess(false);
        		tip.setCode(h.getErrorCode());
                tip.setMessage(h.getErrorMessage());
        	}
        } catch(Exception e) {
        	tip = new Tip(2, "请求数据异常");
        }
		Map map = new HashMap();
        String billNo = UUIDFactory.getStringId();
        map.put("billNo",billNo);
		map.put("plotNum","");
		String jiaData = JSONObject.toJSONString(map);
		Tip jiatip = new Tip("预约成功！",jiaData);
		return jiatip;
	}

	public static Tip getGateInfosByParkCodes(String id){
		Tip tip = new Tip();
		String url = "http://{0}:{1}/cloud/service/getGateInfosByParkCodes?token={2}";
		GetParkingInfosByParkCodeRequest baseRequest = new GetParkingInfosByParkCodeRequest();
		baseRequest.setAppkey(appkey);
		baseRequest.setTime(System.currentTimeMillis());
		baseRequest.setNonce(CommonUtil.getNonce());
		baseRequest.setParkCodes(id);
		String data = JSONObject.toJSONString(baseRequest);
		url = getUrl(url, data);
		try {
			String rs = HttpUtil.doPost(url, data);
			System.out.println(rs);

			HikTip h = JSONObject.parseObject(rs, HikTip.class);
			if(h.getErrorCode() == 0) {

				List<GateCodeInfo> list = JSON.parseArray(JSONArray.toJSONString(h.getData()), GateCodeInfo.class);
				System.out.println(list.size());
				tip = new Tip(list);
			} else {
				tip.setSuccess(false);
				tip.setCode(h.getErrorCode());
				tip.setMessage(h.getErrorMessage());
			}
		} catch(Exception e) {
			e.printStackTrace();
			tip = new Tip(2, "请求数据异常");
		}
		return tip;

	}
	/*
	* 根据出入口编号获取车道编号
	* */
	public static Tip getLaneInfosByGateCodes(String id,String parkCode){
		Tip tip = new Tip();
		String url = "http://{0}:{1}/cloud/service/getLaneInfosByGateCodes?token={2}";
		GetLaneInfosByGateCodesRequest baseRequest = new GetLaneInfosByGateCodesRequest();
		baseRequest.setAppkey(appkey);
		baseRequest.setTime(System.currentTimeMillis());
		baseRequest.setNonce(CommonUtil.getNonce());
		baseRequest.setGateCodes(id);
		String data = JSONObject.toJSONString(baseRequest);
		url = getUrl(url, data);
		try {
			String rs = HttpUtil.doPost(url, data);
			System.out.println(rs);

			HikTip h = JSONObject.parseObject(rs, HikTip.class);
			if(h.getErrorCode() == 0) {

				List<LaneInfo> list = JSON.parseArray(JSONArray.toJSONString(h.getData()), LaneInfo.class);
				List<LaneInfo> removeS = new ArrayList<>();
				for(LaneInfo laneInfo :list){
					System.out.println(laneInfo.getParkCode());
					System.out.println(parkCode);
					if(laneInfo.getParkCode().equals(parkCode)){

					}else{
						removeS.add(laneInfo);
					}
				}
				list.removeAll(removeS);
				tip = new Tip(list);
			} else {
				tip.setSuccess(false);
				tip.setCode(h.getErrorCode());
				tip.setMessage(h.getErrorMessage());
			}
		} catch(Exception e) {
			e.printStackTrace();
			tip = new Tip(2, "请求数据异常");
		}
		return tip;

	}
	/**
	 * 获取收益列表
	 * @param pageNo
	 * @param pageSize
	 * @param start
	 * @param end
	 * @return
	 */
	public static Tip getTempCarChargeRecords(Integer pageNo, Integer pageSize, Long start, Long end, String parkCode, String plateNumber, Integer plateColor, Integer payType) {
		Tip tip = new Tip();
		String url = "http://{0}:{1}/cloud/service/getTempCarChargeRecords?token={2}";
        GetTempCarChargeRecordsRequest request = new GetTempCarChargeRecordsRequest();
        request.setAppkey(appkey);
        request.setTime(System.currentTimeMillis());
        request.setNonce(CommonUtil.getNonce());
        request.setPageNo(pageNo);
        request.setPageSize(pageSize);
        request.setParkCode(parkCode);
        request.setPlateColor(plateColor);
        request.setPlateNo(plateNumber);
        request.setPayType(payType);
        request.setStartTime(start);
        request.setEndTime(end);
        String data = JSONObject.toJSONString(request);
        url = getUrl(url, data);
        try {
			String rs = HttpUtil.doPost(url, data);
        	HikTip h = JSONObject.parseObject(rs, HikTip.class);
        	if(h.getErrorCode() == 0) {
        		HikPage p = JSONObject.parseObject(String.valueOf(h.getData()), HikPage.class);
        		tip = new Tip(p.getList());
        	} else {
        		tip.setSuccess(false);
        		tip.setCode(h.getErrorCode());
        		tip.setMessage(h.getErrorMessage());
        	}
        } catch(Exception e) {
        	tip = new Tip(2, "请求数据异常");
        }
        return tip;
	}
	public static Tip getParkingPaymentInfoRequestBO(Integer plateColor,String plateNumber){
		Tip tip = new Tip();
		String url = "http://{0}:{1}/cloud/service/getParkingPaymentInfo?token={2}";
		GetParkingPaymentInfoRequestBO request = new GetParkingPaymentInfoRequestBO();
		request.setAppkey(appkey);
		request.setTime(System.currentTimeMillis());
		request.setNonce(CommonUtil.getNonce());
		request.setPlateColor(plateColor);
		request.setPlateNo(plateNumber);
		String data = JSONObject.toJSONString(request);
		url = getUrl(url, data);
		try {
			String rs = HttpUtil.doPost(url, data);
			System.out.println(rs  );
			HikTip h = JSONObject.parseObject(rs, HikTip.class);
			if(h.getErrorCode() == 0) {
				HikBill hb = JSONObject.parseObject(h.getData().toString(), HikBill.class);
				tip = new Tip(hb);
			} else {
				tip.setSuccess(false);
				tip.setCode(h.getErrorCode());
				tip.setMessage(h.getErrorMessage());
			}
		} catch(Exception e) {
			tip = new Tip(2, "请求数据异常");
		}
		return tip;
	}
	/**
	 * 获取过车记录
	 * @param pageNumber
	 * @param pageSize
	 * @param parkCode
	 * @param gateCode
	 * @param plateNumber
	 * @param start
	 * @param end
	 * @param type
	 * @param direct
	 * @return
	 */
	public static Tip getVehicleRecords(Integer pageNumber, Integer pageSize, String parkCode, String gateCode, String plateNumber, Long start, Long end, Integer type, Integer direct) {
		Tip tip = new Tip();
		String url = "http://{0}:{1}/cloud/service/getVehicleRecords?token={2}";
        GetVehicleRecordsRequest request = new GetVehicleRecordsRequest();
        request.setAppkey(appkey);
        request.setTime(System.currentTimeMillis());
        request.setNonce(CommonUtil.getNonce());
        request.setPageNo(pageNumber);
        request.setPageSize(pageSize);
        request.setParkCode(parkCode);
        request.setGateCode(gateCode);
        request.setPlateNo(plateNumber);
        request.setStartTime(start);
        request.setEndTime(end);
        request.setCarType(type);
        request.setDirect(direct);
        String data = JSONObject.toJSONString(request);
        url = getUrl(url, data);
		try {
			String rs = HttpUtil.doPost(url, data);
			HikTip h = JSONObject.parseObject(rs, HikTip.class);
			if(h.getErrorCode() == 0) {
				tip = new Tip(h.getData());
			} else {
				tip.setSuccess(false);
				tip.setCode(h.getErrorCode());
				tip.setMessage(h.getErrorMessage());
			}
		} catch(Exception e) {
			tip = new Tip(2, "请求数据异常");
		}
		return tip;
	}
	
	public static Tip payParkingFee(String billNo, Integer payAmount, String payType) {
		Tip tip = new Tip();
		String url = "http://{0}:{1}/cloud/service/payParkingFee?token={2}";
        PayParkingFeeRequestBO baseRequest = new PayParkingFeeRequestBO();

        baseRequest.setBillNo(billNo);
        baseRequest.setPayAmount(payAmount);
        baseRequest.setPayTime(System.currentTimeMillis());
        baseRequest.setPayType(payType);
        
        baseRequest.setAppkey(appkey);
        baseRequest.setTime(System.currentTimeMillis());
        baseRequest.setNonce(CommonUtil.getNonce());
        String data = JSONObject.toJSONString(baseRequest);
        url = getUrl(url, data);
        try {
			String rs = HttpUtil.doPost(url, data);
			System.out.println(rs);
        	HikTip h = JSONObject.parseObject(rs, HikTip.class);
        	if(h.getErrorCode() == 0) {
        		tip = new Tip();
        	} else {
        		tip.setSuccess(false);
        		tip.setCode(h.getErrorCode());
        		tip.setMessage(h.getErrorMessage());
        	}
        } catch(Exception e) {
        	tip = new Tip(2, "请求数据异常");
        }
		return tip;
	}
	
	public static String getUrl(String url, String data) {
		String token = CommonUtil.getToken(data, secret);
        url = MessageFormat.format(url, ip, port, token);
		return url;
	}
	
	public static List<ParkDepotInfo> getParkingList() {
		List<ParkDepotInfo> list = new ArrayList<>();
		Tip tip = getParkingInfos(1, 400);
    	if(tip.getSuccess()) {
			list = JSON.parseArray(JSONArray.toJSONString(tip.getData()), ParkDepotInfo.class);
		}
    	return list;
	}
	
	public static List<ChargeRecord> getChargeRecords(String parkCode) {
		List<ChargeRecord> list = new ArrayList<>();
		Tip tip = getTempCarChargeRecords(1, 1000, TimeUtils.getTodayStartTime(), System.currentTimeMillis(), parkCode, null, null, null);
		if(tip.getSuccess()) {
			list = JSON.parseArray(JSONArray.toJSONString(tip.getData()), ChargeRecord.class); 
		}
		return list;
	}
	
	public static List<ChargeRecord> getChargeByPlateNo(Long start, Long end, String parkCode, String plateNumber, Integer plateColor) {
		List<ChargeRecord> list = new ArrayList<>();
		Tip tip = getTempCarChargeRecords(1, 400, start, end, parkCode, plateNumber, plateColor, 1);
		if(tip.getSuccess()) {
			list = JSON.parseArray(JSONArray.toJSONString(tip.getData()), ChargeRecord.class);
		}
		return list;
	}
	
	public static Tip getLeftPlot(String hikId) {
		Tip tip = new Tip();
		String url = "http://{0}:{1}/cloud/service/getLeftPlot?token={2}";
        ParkInfoRequest request = new ParkInfoRequest();
        request.setAppkey(appkey);
        request.setTime(System.currentTimeMillis());
        request.setNonce(CommonUtil.getNonce());
        request.setParkCode(hikId);
        String data = JSONObject.toJSONString(request);
        url = getUrl(url, data);
        try {
			String rs = HttpUtil.doPost(url, data);
        	HikTip h = JSONObject.parseObject(rs, HikTip.class);
        	if(h.getErrorCode() == 0) {
        		ParkLeftPlot p = JSONObject.parseObject(String.valueOf(h.getData()), ParkLeftPlot.class);
        		tip = new Tip(p.getLeftPlot());
        	} else {
        		tip.setSuccess(false);
        		tip.setCode(h.getErrorCode());
        		tip.setMessage(h.getErrorMessage());
        	}
        } catch(Exception e) {
        	tip = new Tip(2, "请求数据异常");
        }
		return tip;
	}

	public static Tip getPassPicByUuid(String uuid, String parkCode){
		Tip tip = new Tip();
		String url = "http://{0}:{1}/cloud/service/getPassPicByUuid?token={2}";
        GetPassPicByUuidRequest request = new GetPassPicByUuidRequest();
        request.setAppkey(appkey);
        request.setTime(System.currentTimeMillis());
        request.setNonce(CommonUtil.getNonce());
        request.setUnid(uuid);
        request.setParkCode(parkCode);
        String data = JSONObject.toJSONString(request);
        url = getUrl(url, data);
        try {
			String rs = HttpUtil.doPost(url, data);
			HikTip h = JSONObject.parseObject(rs, HikTip.class);
			if (h.getErrorCode() == 0){
				tip = new Tip(h.getData());
			}else {
				tip.setSuccess(false);
				tip.setCode(h.getErrorCode());
				tip.setMessage(h.getErrorMessage());
			}
		}catch (Exception e){
			tip = new Tip(2, "请求数据异常");
		}
		return tip;
	}
	
	/**
	 * 根据海康编号查询各种车位数
	 * @param pd 停车场信息
	 * @return 各种车位数
	 */
	public static ParkDepot handleParkDepot(ParkDepot pd) {
		Tip tip = getParkingInfoByParkCode(pd.getHikId());
		if(tip.getSuccess() && tip.getData() != null) {
			ParkDepotInfo p = JSONObject.parseObject(JSONArray.toJSONString(tip.getData()), ParkDepotInfo.class);
			pd.setLeftFixedPlot(p.getLeftFiexdPlot());
			pd.setLeftPlot(p.getLeftPlot());
			pd.setTotalFixedPlot(p.getTotalFixedPlot());
			pd.setTotalPlot(p.getTotalPlot());
		} else {
			pd.setLeftFixedPlot(0);
			pd.setLeftPlot(0);
			pd.setTotalFixedPlot(0);
			pd.setTotalPlot(0);
		}
		return pd;
	}
	
	public static void main(String[] args) {

//		payParkingFee("22418102510264604000", 300, "4");
		getChargeByPlateNo(null,null,null,"蒙FQ9039",null);
	}

}
