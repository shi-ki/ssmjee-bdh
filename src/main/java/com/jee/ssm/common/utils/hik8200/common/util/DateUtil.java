package com.jee.ssm.common.utils.hik8200.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 时间和日期处理
 * @author zhangtuo
 * @Date 2017/4/25
 */
public class DateUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(DateUtil.class);

    private static final String FORMAT_TYPE = "yyyy-MM-dd HH:mm:ss";

    private static final String FORMAT_TIMESTAMP = "yyyyMMddHHmmss";

    /**
     * 日期转换成秒
     * @param strTime
     * @return
     */
    public static long timeToSec(String strTime) {
        long time_sec = 0;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat(FORMAT_TYPE);
            Date date = formatter.parse(strTime);
            time_sec = date.getTime() / 1000;
        } catch (Exception e) {
            LOGGER.error("日期转化出错，错误信息：" + e);
        }
        return time_sec;
    }

    /**
     * 事件转换成时间戳字符串
     * @param date
     * @return
     */
    public static String dateToString(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_TIMESTAMP);
        String str = sdf.format(date);
        return str;
    }
}
