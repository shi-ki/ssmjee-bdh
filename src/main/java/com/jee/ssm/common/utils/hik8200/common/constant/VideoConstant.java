package com.jee.ssm.common.utils.hik8200.common.constant;

/**
 * 数据字典/常量
 * @author zhangtuo
 * @Date 2017/3/27
 */
public class VideoConstant {

    /**
     * WEB API code码
     */
    public static class WebApiResDict {
        public static final String SUCCESS_CODE = "0";//success
        public static final String ERROR_CODE = "1";//error
    }


    /**
     * 构造Tree常量
     */
    public static class TreeConstant {
        public static final String ROOT_NODE_INDEX_CODE = "0";
        public static final String CONTROL_UNIT = "controlUnit"; // 组织
        public static final String CAMERA = "camera"; // 通道
    }

    /**
     * Http状态数据字典
     */
    public static class HttpStatusDict {
        public static final String SUCCESS_CODE ="200";
        public static final String ERROR_CODE = "500";
        public static final String BAD_REQUEST = "400";
        public static final String NOT_FOUND = "404";
        public static final String EXCEPTION = "450";//调用统计异常，记录请求信息时发生错误。
        public static final String APPKEY_NULL = "460";//请求appkey为空
        public static final String SIGN_NULL= "461";//请求签名为空
        public static final String CONSUMER_IS_NOT_EXIST = "462";//appkey对应的合作方不存在
        public static final String UN_AUTH = "463";//未授权合作方
        public static final String SIGN_ERROR = "464";//签名错误
        public static final String AUTH_EXCEPTION = "465";
        public static final String PARM_CONVERT_EXCEPTION = "466";//参数转换异常，具体错误见异常信息
    }

    /**
     * Http消息MSG数据字典
     */
    public static class HttpMsgDict {
        public static final String SUCCESS_MSG ="Success";
        public static final String ERROR_MSG ="Error";
        public static final String BAD_REQUEST_MSG = "Bad request";
        public static final String NOT_FOUND_MSG ="Not found";
        public static final String EXCEPTION_MSG ="Call exception";
        public static final String APPKEY_NULL_MSG ="the appkey is null";
        public static final String SIGN_NULL_MSG= "The sign is null";//请求签名为空
        public static final String CONSUMER_IS_NOT_EXIST_MSG = "The consumer is not exist";//appkey对应的合作方不存在
        public static final String UN_AUTH_MSG = "Partner unauthorized";//未授权合作方
        public static final String SIGN_ERROR_MSG = "Signature error";//签名错误
        public static final String AUTH_EXCEPTION_MSG = "Authority exception";
        public static final String PARM_CONVERT_EXCEPTION_MSG = "Parameter conversion exception";//参数转换异常，具体错误见异常信息
    }

    /**
     * artemis 接口
     */
    public static class ArtemisFacade {

        public static final String ARTEMIS_APPOLLO_ORG_BY_PARENT_PATH =
                "/artemis/api/common/v1/remoteControlUnitRestService/findControlUnitByUnitCode";
        public static final String ARTEMIS_APPOLLO_CAMERA_BY_PARENT_PATH =
                "/artemis/api/common/v1/remoteControlUnitRestService/findCameraInfoPageByTreeNode";
        public static final String ARTMEIS_BASIC_AGRREMNT =
                "/artemis/api/artemis/v1/agreementService/securityParam/appKey";
    }

    /**
     * videoClient数据字典
     */
    public static class VideoClientConstant {
        public static final String REQTYPE_PLAYVIEW = "PlayReal";
        public static final String REQTYPE_PLAYBACK = "PlayBack";
        public static final String VIDEO_CLIENT_NAME = "hikvideoclient";
        public static final String AGREEMENT_TAG = "VersionTag:artemis";//为开放平台专用Tag
        public static final String APPTYPE_VAS = "vas";
        public static final String APPTYPE_VDS = "vds";
        public static final String APPTYPE_ICS = "ics";
        public static final String CLEAR_CLOSE_ALL = "1";//1, 关闭所有播放窗口，清空列表
        public static final String CLEAR_CLOSE_WND = "2";//2,关闭所有播放窗口，不清空列表
        public static final String CLEAR_LIST = "3";//3,只往列表追加点位，不做任何清理
        public static final String ENABLE_AUTO = "0";//自动轮询
        public static final String DISABLE_AUTO = "1";//手动轮询
        public static final String WND_ONE = "1";//1个窗口
        public static final String WND_FOUR = "4";//4个窗口
        public static final String WND_NINE = "9";//9个窗口
        public static final String WND_SELECT_ZERO = "0";//0 是否使用选中窗口进行播放，偌没有该字段，则使用空闲窗口播放
        public static final String WND_SELECT_ONE = "1";//1，优先选择选中窗口进行播放
        public static final String WND_SELECT_TWO = "2";//2，使用空闲窗口播放, 没有空闲窗口时则使用选中窗口播放
    }

    /**
     * 云台控制数据字典
     */
    public static class PtzDict {
        public static final String REQTYPE_PTZ = "PtzControl";
        public static final String PTZ_ACTION_STOP = "0";//停止
        public static final String PTZ_ACTION_START = "1";//启动

        public static final String MULTI_ZOOM_IN = "11";			/* 焦距减小*/
        public static final String MULTI_ZOOM_OUT = "12";			/* 焦距放大*/
        public static final String MULTI_FOCUS_NEAR = "13";			/* 焦点拉近*/
        public static final String MULTI_FOCUS_FAR = "14";			/* 焦点拉远*/
        public static final String MULTI_IRIS_OPEN = "15";			/* 光圈打开*/
        public static final String MULTI_IRIS_CLOSE = "16";			/* 光圈关闭*/
        public static final String MULTI_TILE_UP = "21";			/* 云台向上*/
        public static final String MULTI_TILT_DOWN = "22";			/* 云台向下*/
        public static final String MULTI_PAN_LEFT = "23";    		/* 云台向左*/
        public static final String MULTI_PAN_RIGHT = "24";			/* 云台向右*/
        public static final String MULTI_UP_LEFT = "25";			/* 云台左上*/
        public static final String MULTI_UP_RIGHT = "26";			/* 云台右上*/
        public static final String MULTI_DOWN_LEFT = "27";			/* 云台左下*/
        public static final String  MULTI_DOWN_RIGHT = "28";		/* 云台右下*/
        public static final String MULTI_DOWN_CONFIGPRESET = "8";	/* 设置预置点*/
        public static final String MULTI_DOWN_GOTOPRESET = "39";	/* 调用预置点*/
        public static final String MULTI_RUN_SEQ = "37";         	/* 开始巡航*/
        public static final String MULTI_STOP_SEQ = "38";	    	/* 停止巡航*/
        public static final String MULTI_PTZ_LOCK = "200";      	/* 云台锁定*/
        public static final String MULTI_PTZ_UNLOCK = "201";    	/* 云台解锁*/
        public static final String MULTI_PAN_AUTO = "29";	    	/* 云台以SS的速度左右自动扫描*/
        public static final String MULTI_PAN_AUTO_STOP = "-29";		/* 停止云台以SS的速度左右自动扫描*/
        public static final String MULTI_STA_MEM_CRUISE = "34";		/* 开始记录轨迹*/
        public static final String MULTI_STO_MEM_CRUISE = "35";		/* 停止记录轨迹*/
        public static final String MULTI_RUN_CRUISE = "36";	   	    /* 开始轨迹*/
        public static final String MULTI_TILT_UP_STOP = "-21";		/* 停止云台向上*/
        public static final String MULTI_WIPER_PWRON = "3";			/* 接通雨刷开关*/
        public static final String MULTI_WIPER_PWRON_STOP = "-3"; 	/* 停止接通雨刷开关*/
        public static final String MULTI_LIGHT_PWRON = "2";			/* 接通灯光电源*/
        public static final String MULTI_LIGHT_PWRON_STOP = "-2"; 	/* 停止接通灯光电源*/
    }

    /**
     * RSA加密常量
     */
    public static class RSAConstant {
        public static final String KEY_ALGORITHM = "RSA";
        public static final String SIGNATURE_ALGORITHM = "MD5withRSA";
    }

    /**
     * 异常MSG数据字典
     */
    public static class ExceptionConstant {
        public static final String LIST_NULL = "list集合为null";
        public static final String PARAM_BAD_REQUEST = "请求参数异常";
    }

    /**
     *
     */
    public static class LogConstant {
        public static final String HANDLE_START = "is started";
        public static final String HANDLE_SUCCESS = "is success";
        public static final String HANDLE_FAILED = "is failed";
        public static final String PALYBACK_TIME_ERROR = "录像搜索时间的间隔不能超过3天";
        public static final String JSON_CONVERT_ERROR = "Json转换失败";
        public static final String HANDLE_API_GATEWAY = "从API网关操作";
        public static final String HANDLE_GET_ORG_BY_PARENT = "根据父节点获取组织";
        public static final String HANDLE_GET_CAM_BY_PARENT = "根据父节点获取监控点";

    }

}
