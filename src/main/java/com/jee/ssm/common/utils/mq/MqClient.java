package com.jee.ssm.common.utils.mq;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import com.ahjxlm.mq.proto.PassVehicleMsg.PassVehicle;
import com.jee.ssm.common.utils.UUIDFactory;

import javax.jms.*;

public class MqClient {
	
    private static Connection connection = null;

    public MqClient() {
    	ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_USER, ActiveMQConnection.DEFAULT_PASSWORD, "tcp://117.132.167.10:61616");
    
    	String id = UUIDFactory.getStringId();
    	 try {
             connection = connectionFactory.createConnection();
             connection.setClientID(id);
             connection.start();
             Session session = connection.createSession(Boolean.FALSE, 1);
             Topic topic = session.createTopic("topic.pass");
             MessageConsumer consumer = session.createDurableSubscriber(topic, id);
//             MessageUtil.writeReceiveMsg("mq连接启动成功!");

             while(true) {
                 BytesMessage bytesMessage = (BytesMessage)consumer.receive(100000000L);
                 if (null != bytesMessage) {
                     byte[] byteMsg = new byte[(int)bytesMessage.getBodyLength()];
                     bytesMessage.readBytes(byteMsg);
                     PassVehicle passVehicle = PassVehicle.parseFrom(byteMsg);
//                     String messagePattern = "plateNo【{0}】 plateColor【{1}】 passTime【{2}】 parkCode【{3}】 direct【{4}】";
//                     String message = MessageFormat.format(messagePattern, passVehicle.getPlateNo(), passVehicle.getPlateColor(), passVehicle.getPassTime(), passVehicle.getParkCode(), passVehicle.getDirect());
//                     System.out.println(message);
                     System.out.print(passVehicle.getPlateNo() + ",");
                     System.out.print(passVehicle.getBerthCode() + ",");
                     System.out.print(passVehicle.getDirect() + ",");
                     System.out.print(passVehicle.getGateCode() + ",");
                     System.out.print(passVehicle.getLaneNo() + ",");
                     System.out.print(passVehicle.getParkCode() + ",");
                     System.out.print(passVehicle.getParkType() + ",");
                     System.out.print(passVehicle.getPassTime() + ",");
                     System.out.print(passVehicle.getPlateColor() + ",");
                     System.out.print(passVehicle.getPlateType() + ",");
                     System.out.print(passVehicle.getUuid());
                     System.out.println();
//                     MessageUtil.writeReceiveMsg(message);
                 }
             }
         } catch (Exception var13) {
        	 var13.printStackTrace();
             System.out.println("监听异常");
//             Config.isConnect = false;
//             MainStart.getMainFrame().getOpenBtn().setText("连接");
//             MessageUtil.writeReceiveMsg("连接mq失败");
         }
    }
    
    public static void main(String[] args) {
    	new MqClient();
    }
}
