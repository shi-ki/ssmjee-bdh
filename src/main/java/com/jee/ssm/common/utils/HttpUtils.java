package com.jee.ssm.common.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;

public class HttpUtils {

        public static String doPost(String url, NameValuePair[] data) throws IOException {
                HttpClient client = new HttpClient();
                client.getHttpConnectionManager().getParams().setConnectionTimeout(2000);//设置连接超时时间为2秒（连接初始化时间）
                PostMethod post = new PostMethod(url);
                post.addRequestHeader("Content-Type","application/x-www-form-urlencoded;charset=utf-8");//在头文件中设置转码
//        NameValuePair[] data ={
//                new NameValuePair("username",username),
//                new NameValuePair("passwd",passwd),
//                new NameValuePair("phone",phone),
//                new NameValuePair("msg",msg),
//                new NameValuePair("needstatus",needstatus),
//                new NameValuePair("port",port),
//                new NameValuePair("sendtime",sendtime)
//        };
                post.setRequestBody(data);
                client.executeMethod(post);
                int statusCode = post.getStatusCode();
                String result = new String(post.getResponseBodyAsString().getBytes());
                if(statusCode == 200) {

                }
                post.releaseConnection();
                return result;
        }
        /**
         * Get方式
         */
        public static String doGet(String url) {
                String result = "";
                //        try {
                //            // 当出现乱码时，可能是由于编码错误引起，尝试改变编码格式，使用URLEncoder.encode()方法
                //            url = url + "?username=" + URLEncoder.encode(name, "UTF-8") + "&pwd=" + URLEncoder.encode(password, "UTF-8");
                //        } catch (UnsupportedEncodingException e1) {
                //            e1.printStackTrace();
                //        }
                InputStream is = null;
                InputStreamReader isr = null;
                BufferedReader bufferedReader = null;
                try {
                        URL httpUrl = new URL(url);
                        HttpURLConnection connection = (HttpURLConnection) httpUrl.openConnection();
                        // 设置读取超时
                        connection.setReadTimeout(5000);
                        // 设置链接超时时间
                        connection.setConnectTimeout(5000);
                        connection.setRequestMethod("GET");
                        // 获得链接请求码
                        if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                                is = connection.getInputStream();
                                isr = new InputStreamReader(is);
                                bufferedReader = new BufferedReader(isr);
                                String str = null;
                                StringBuffer sb = new StringBuffer();
                                while ((str = bufferedReader.readLine()) != null) {
                                        sb.append(str);
                                }
                                System.out.println("Get方式的result:" + sb.toString());
                                result = sb.toString();
                        }
                } catch (MalformedURLException e) {
                        e.printStackTrace();
                } catch (IOException e) {
                        e.printStackTrace();
                } finally {
                        try {
                                if (is != null)
                                        is.close();
                                if (isr != null)
                                        isr.close();
                                if (bufferedReader != null)
                                        bufferedReader.close();
                        } catch (IOException e) {
                                e.printStackTrace();
                        }
                }
                return result;
        }


}
