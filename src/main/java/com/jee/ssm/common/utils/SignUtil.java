package com.jee.ssm.common.utils;

import org.apache.commons.codec.digest.DigestUtils;

import cn.jiguang.common.utils.Base64;

public class SignUtil {
    public static String sign(String param){
        byte[] signature = DigestUtils.sha1(param);
        return new String(Base64.encode(signature));
    }
}