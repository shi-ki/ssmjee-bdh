package com.jee.ssm.common.utils.hik8200.common.exception;

import java.util.Arrays;

/**
 * 自定义运行期异常，非检查型运行期异常通常不用try-catch包裹。
 * @author zhangtuo
 * @Date 2017/4/26
 */
public class VideoRuntimeException extends RuntimeException{
    private String code;
    private String msg;
    private String[] args;

    public VideoRuntimeException() {
    }

    public VideoRuntimeException(String code) {
        this.code = code;
    }

    public VideoRuntimeException(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public VideoRuntimeException(String code, String msg, String[] args) {
        this.code = code;
        this.msg = msg;
        this.args = args;
    }

    public VideoRuntimeException(Throwable cause, String code) {
        super(cause);
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String[] getArgs() {
        return args;
    }

    public void setArgs(String[] args) {
        this.args = args;
    }

    @Override
    public String toString() {
        return "VideoRuntimeException{" +
                "code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                ", args=" + Arrays.toString(args) +
                '}';
    }
}
