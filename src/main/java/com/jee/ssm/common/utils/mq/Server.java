package com.jee.ssm.common.utils.mq;

import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.ActiveMQConnectionFactory;

import java.util.Scanner;

import javax.jms.*;

public class Server implements MessageListener {

    private static int ackMode;
    private static String messageQueueName;
    private static String messageBrokerUrl;

    private Session session;
    private boolean transacted = false;
    private MessageProducer replyProducer;
    private MessageProtocol messageProtocol;

    static {
        messageBrokerUrl = "tcp://localhost:61616";
        messageQueueName = "client.messages";
        ackMode = Session.AUTO_ACKNOWLEDGE;
    }

    public Server() {
        try {
            //This message broker is embedded
            BrokerService broker = new BrokerService();
            broker.setPersistent(false);
            broker.setUseJmx(false);
            broker.addConnector(messageBrokerUrl);
            broker.start();
        } catch (Exception e) {
            //Handle the exception appropriately
        }

        //Delegating the handling of messages to another class, instantiate it before setting up JMS so it
        //is ready to handle messages
        this.messageProtocol = new MessageProtocol();
        this.setupMessageQueueConsumer();
    }

    private void setupMessageQueueConsumer() {
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(messageBrokerUrl);
        Connection connection;
        try {
            connection = connectionFactory.createConnection();
            connection.start();
            this.session = connection.createSession(this.transacted, ackMode);
            Destination adminQueue = this.session.createQueue(messageQueueName);

            //Setup a message producer to respond to messages from clients, we will get the destination
            //to send to from the JMSReplyTo header field from a Message
            this.replyProducer = this.session.createProducer(null);
            this.replyProducer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

            //Set up a consumer to consume messages off of the admin queue
            MessageConsumer consumer = this.session.createConsumer(adminQueue);
            consumer.setMessageListener(this);
        } catch (JMSException e) {
            //Handle the exception appropriately
        }
    }

    public void onMessage(Message message) {
        try {
            TextMessage response = this.session.createTextMessage();
//            if (message instanceof TextMessage) {
//                TextMessage txtMsg = (TextMessage) message;
//                String messageText = txtMsg.getText();
//                System.out.println(messageText);
//                response.setText(this.messageProtocol.handleProtocolMessage(messageText));
                while(true) {
                	Scanner scan = new Scanner(System.in);    //声明Scanner对象
                    System.out.println("请输入字符串：");                    //提示用户输入字符串
            		String result = scan.next();                          //定义一个result接受输入的语句
            		response.setText(result);
                    response.setJMSCorrelationID(message.getJMSCorrelationID());
                    this.replyProducer.send(message.getJMSReplyTo(), response);
//                }
            }
            
//            for(int i=0;i< 1000;i++) {
//                response.setText(String.valueOf(i));
//                response.setJMSCorrelationID(message.getJMSCorrelationID());
//
//                this.replyProducer.send(message.getJMSReplyTo(), response);
//            }

        } catch (JMSException e) {
            //Handle the exception appropriately
        }
    }

    public static void main(String[] args) {
        new Server();
    }
}
