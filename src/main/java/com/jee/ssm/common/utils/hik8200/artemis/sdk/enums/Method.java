package com.jee.ssm.common.utils.hik8200.artemis.sdk.enums;

/**
 * @author zhangtuo
 * @Date 2017/4/26
 */
public enum Method {
    GET, POST_FORM, POST_STRING, POST_BYTES, PUT_FORM, PUT_STRING, PUT_BYTES, DELETE;
}
