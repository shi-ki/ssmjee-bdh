package com.jee.ssm.common.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Sha1Police {
	
	/**
     * 修复sha1加密算法，修改完成，SHA1Digest有问题，已废除；
     * @author 
     * @param text
     * @return
     */
    public static String SHA1Digest(String text) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA1");
			try {
				md.update(text.getBytes("utf-8"));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			byte[] digest = md.digest();
			StringBuffer sb = new StringBuffer();
			for(int i=0;i<digest.length;i++){
				String hexStr = Integer.toHexString(digest[i] & 0xff);
				if(hexStr.length()<2){
					sb.append("0");
				}
				sb.append(hexStr);
			}
			String signature = sb.toString();
			return signature;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
    }

}
