package com.jee.ssm.common.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Created by GaoXiang
 */
public class UUIDFactory {

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static SimpleDateFormat sdf_close = new SimpleDateFormat("yyyyMMddHHmmss");

    /**
     * 生成一个Long类型的id
     * @return
     */
    public static String getStringId(){

        Long random = UUID.randomUUID().getMostSignificantBits();
        Long id = Math.abs(random);
        return id.toString();
    }

    public static String get32Id() {
        return UUID.randomUUID().toString().trim().replaceAll("-", "");
    }
    
    public static String get36Id() {
        return UUID.randomUUID().toString();
    }

    public static String get12Id() {
        return getStringId().substring(6, 18);
    }

    public static void main(String[] args) {

//    	System.out.println(UUID.randomUUID().toString());
        System.out.println(get32Id().toUpperCase());
//        System.out.println(getCurrentCloseTime());

    }

    public static String getBillNUmber(String phone) {
        String number;
        try {
            String currentTime = getCurrentCloseTime();
            phone = phone.substring(5, 11);
            number = currentTime + phone + get12Id();
        } catch (Exception e) {
            number = get32Id();
        }

        return number;
    }

    public static String getCurrentTime() {
        return sdf.format(new Date());
    }

    private static String getCurrentCloseTime() {
        return sdf_close.format(new Date());
    }

}
