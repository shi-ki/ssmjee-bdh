package com.jee.ssm.common.utils.wechat;

import com.alibaba.fastjson.JSONObject;
import com.jee.ssm.common.config.ShopInfo;
import com.jee.ssm.common.utils.HttpUtils;

import java.util.Map;

public class WxUtils {
    /**
     * 将Map转成json方便前端解析
     */
    public static String getOpenId(String code) {
       String url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid="+ShopInfo.WX_GZ_APPID
+ "&secret="+ ShopInfo.WX_GZ_AppSecret
 + "&code="+code
 + "&grant_type=authorization_code";
        String json1  = HttpUtils.doGet(url);
        JSONObject json =  JSONObject.parseObject(json1);


        return json.getString("openid");
    }
}
