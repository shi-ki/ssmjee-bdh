package com.jee.ssm.common.utils;

import java.security.MessageDigest;

public final class SHA1 {
	
	private static final char[] HEX_DIGITS = {'0', '1', '2', '3', '4', '5',
            '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

	/**
	* Takes the raw bytes from the digest and formats them correct.
	*
	* @param bytes the raw bytes from the digest.
	* @return the formatted bytes.
	*/
	private static String getFormattedText(byte[] bytes) {
		int len = bytes.length;
		StringBuilder buf = new StringBuilder(len * 2);
		// 把密文转换成十六进制的字符串形式
		for (int j = 0; j < len; j++) {
			buf.append(HEX_DIGITS[(bytes[j] >> 4) & 0x0f]);
			buf.append(HEX_DIGITS[bytes[j] & 0x0f]);
		}
		return buf.toString();
	}

	public static String encode(String str) {
		if (str == null) {
			return null;
		}
		try {
			MessageDigest messageDigest = MessageDigest.getInstance("SHA1");
			messageDigest.update(str.getBytes("utf-8"));
			return getFormattedText(messageDigest.digest());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public static void main(String[] args) {
		String a = encode("accessKey=57EF312FEDAD8CE5&actualPay=0.00&agioMoney=0.00&berthNumber=null&carType=1&entryTime=1533262071000&exitFeaturePic=暂无图片&exitPlatePic=暂无图片&exitTime=1533262671000&oprNum=427d45bc-9266-46f7-9d04-b50ede2ecf08&parkCode=100320180802112419&plateColor=1&plateNumber=冀G77G01&recordCode=cc576c63d0064834a60071ab74ff138d&recordType=0&shouldPay=0.00&tenantCode=1003&timestamp=1533262801081U2FsdGVkX18uWXs4C3evsGHcQbGYuUFxchjQYkDJfkgb5UI45SSeC8d4evm6v0Rd");
		System.out.println(a);
	}

}
