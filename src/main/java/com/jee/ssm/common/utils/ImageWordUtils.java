package com.jee.ssm.common.utils;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.font.TextLayout;
import java.awt.image.BufferedImage;

public class ImageWordUtils {
	
	public static BufferedImage getPictureByWord(String text, int width) {
		int bold = 1; // 是否加粗
		int italic = 0; // 是否斜体
		int size = 15;
		int rgb = 0xffffffff; // 颜色
		// 设置字体
		Font font = new Font("宋体", Font.BOLD, 30);
		font = deriveFont(font, bold, italic, size);
		BufferedImage img = CreateFontImgWithGraphics(text, rgb, false, font, width);
		return img;
	}
	 
	private static Font deriveFont(Font font, int bold, int italic, int size) {
		int style = Font.PLAIN;
		if (bold > 0) {
			style = style | Font.BOLD;
		}
		if (italic > 0) {
			style = style | Font.ITALIC;
		}
		return font.deriveFont(style, size);
	}
	
	private static BufferedImage CreateFontImgWithGraphics(String text, int rgb, boolean isVertical, Font font, int width) {
		// 字体大小
		int fontSize = font.getSize();
		// 高、宽比例
		float radio = 1.4f;
		// 文字图片边框
		float border = (float) (fontSize * 0.01);
		// 设置每行的固定高度，用于横排
		int line_height = Math.round(fontSize * radio);
		// 设置每行的固定宽度度，用于竖排
		int line_width = Math.round(fontSize * radio);
		// 文字
		String lines[] = text.split(";");
		String line;
		TextLayout layout;
		// 计算图片的width,height
		BufferedImage tmp = new BufferedImage(1, 1, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics2D gtmp = (Graphics2D) tmp.getGraphics();
		// dwh用于根据实际文件来计算图片某一边的长度,dx用于对图片水平方向的空白补齐,dy用于对图片垂直方向的空白补齐
		float dwh = 0, dx = 0, dy = 0;
		for (int i = 0; i < lines.length; i++) {
			line = lines[i];
			
			if (StringUtils.isNotBlank(line)) {
				font.deriveFont(Font.BOLD, fontSize);
				layout = new TextLayout(line, font, gtmp.getFontRenderContext());
				dwh = Math.max(layout.getAdvance(), dwh);
				dy = (float) Math.min(-((isVertical ? fontSize : line_height) - layout.getBounds().getHeight()) / 2, dy);
			}
		}
		// 横排文字:width不固定,height固定; 竖排文字:width固定,height不固定
		// 文字图片的宽
//		int width = Math.round((isVertical ? line_width * lines.length : dwh) + 2 * border);
//		int width = 300;
		// 文字图片的高
		int height = Math.round((isVertical ? dwh : line_height * lines.length) + 2 * border);
		// 创建文字图片
		BufferedImage image = new BufferedImage(width < 1 ? 1 : width, height < 1 ? 1 : height, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics2D g = (Graphics2D) image.getGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setColor(new Color(rgb));
		g.setFont(font);
		// 初始化第一个字的坐标
		float xpos = isVertical ? width : border + dx, ypos = border + dy;
		// 每行字
		for (int i = 0; i < lines.length; i++) {
			line = lines[i];
			if (isVertical) {
				xpos -= line_width;
				ypos = border + dy;
			} else {
				xpos = border + dx;
				ypos += line_height;
			}
			// 如果该行为空行，直接跳过
			if (StringUtils.isBlank(lines[i])) {
				continue;
			}
			// 每个字符
			
			for (int j = 0; j < line.length(); j++) {
				char c = line.charAt(j);
				// 用于获取字的该advance
				layout = new TextLayout(String.valueOf(c), font, g.getFontRenderContext());
				g.scale(1.0, 1.0); // 比例
				if (c > 32 && c < 126 && isVertical) {
					g.rotate(Math.PI / 2, xpos, ypos + layout.getAdvance());
					g.drawString(String.valueOf(c),   xpos  , ypos+ layout.getAdvance());
					g.rotate(-Math.PI / 2, xpos, ypos + layout.getAdvance());
				} else {
					g.drawString(String.valueOf(c), xpos, isVertical ? ypos + layout.getAdvance() : ypos);
				}
//				System.out.println(c + ", xy:xpos =" + xpos + ",ypos=" + (ypos + layout.getAdvance()));

				if (isVertical) {
					ypos += layout.getAdvance();
				} else {
					xpos += layout.getAdvance();
				}
			}
		}
//		g.drawString(String.valueOf("a"), 160, 81);
//		System.out.println("width:" + width + ", height:" + height);
		// g.setStroke(new BasicStroke(4.0f));// 线条粗细
		// g.setColor(Color.blue);// 线条颜色
		// g.drawLine(440, 0, 440, 580);// 线条起点及终点位置
		// g.setStroke(new BasicStroke(4.0f));// 线条粗细
		// g.setColor(Color.red);// 线条颜色
		// g.drawLine(0, 110, 620, 110);// 线条起点及终点位置
		g.dispose();
		return image;
	}

}
