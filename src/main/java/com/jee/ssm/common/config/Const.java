package com.jee.ssm.common.config;


/**
 * 项目session字典
 * @author GaoXiang
 * @version 1.0
*/
public class Const {

	/**
	 * 不对匹配该值的访问路径拦截（正则）
	 */
	public static final String NO_INTERCEPTOR_PATH = ".*/((login)|(logout)|(code)|(app)|(weixin)|(static)|(main)|(websocket)).*";

	/**
	 * 登录界面
	 */
	public static final String LOGIN = "/login/";

	/**
	 * 登录验证码
	 */
	public static final String SECURITY_CODE = "secCode";

	/**
	 * 账号
	 */
	public static final String ACCOUNT = "account";

	/**
	 * 管理员
	 */
	public static final String ADMIN = "admin";

	/**
	 * 用户
	 */
	public static final String USER = "user";

	/**
	 * 权限集合 用于界面遍历
	 */
	public static final String POWER_LIST = "powerList";

	/**
	 * 某用户的所有权限列表，用于判断权限
	 */
	public static final String ACCOUNT_POWER_LIST = "accountPowerList";

	/**
	 * 某角色的权限列表
	 */
	public static final String ROLE_POWER_LIST = "rolePowerList";

	/**
	 * socket 会话 accountid
	 */
    public static final String WEBSOCKET_ID = "websocketId";
    
    /**
     * 正则表达式：验证手机号
     */
    public static final String REGEX_MOBILE = "^((17[0-9])|(14[0-9])|(13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$";


}
