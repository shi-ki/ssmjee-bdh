package com.jee.ssm.common.config;

public class ShopInfo {
	//支付宝appId
	public static final String APP_ID = "2018041760033023";

	//支付宝商户私钥
    public static final String PRIVATE_KEY = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCWoebsqGNpB7Ux1s5sqx3NQ16e+petkmWKcNJJCu/oxpmAW4mjTCuGkcYpHnOqbecp6Uc/9y3HF8zPOSwK+FODHiRxTcSC7rvR5hyPjnkh0cn7qZojLWt15A0Y2sjjgnTGKTUOBAmrbxU42Nn+X+gLzjzseuq6AHF5JYznLsKHEUiF0ivWGYRznrNPRMVTpVPFxawmZBlBvcEZdsAKkT55J7dJhkK3fFe65V00P582LM0nBHdWREJdQkID5x1MVL7eM31X53mZwA6ZY6VOKNKHmoC3QX0S2fvROL2wZ1tNMfauFrJGasQFHqbr1gotFdeANN1diaHHCB+f3BgDoEJnAgMBAAECggEAAP7G2M9wALSlVNP3fy5OPxnv2RsMv3/VHeoK6aKh3SbawsdYDHUnYHc8GO9zki+xtEwvRVPrwIH+v2vAR75GRvp/ASSjZL4RDZz4mVY2pKuPKxN+yja1nAzKrVxcsWweO2TwTcxnB/Pu7vY30n9tuA4/xsw1Gs9/8G34FVrwvBW1fhJX7RhlVxvQK53JVowXxp1/7F8+VGYoZg/aZ0fZ+iGboaxRnlm2dpk9Mhcym5H/OcFmyesJBgHhGJULfY9pZ1qoxg3Z+q6/+i3JhiAvVK6KSKCOTIplgvgCVqxan2v9m9fU7j6AuGJtNtbzZ40N/ESpfrpVgbSGnVkiztuYMQKBgQDzyGxTP+rLPdmYL8nj8JijfR2mvh59g6ql6vRNw0OT2YWMJerzPPg2mNjxLCyAAc1/SVrJEy4QoXJiAWBLv70oTP9sxcCTXuZqRuaNNNx99Lzad+VfvqY4N+6CCNtGB1TXd/jySYZAAylP+SoKUmIBA1KE3uKqXSdXIurHIfZnqQKBgQCeLmsrGT5t04k6NDLHLP5O54heKq+dIuK+0iE15QJtvNlK5iYTXohgzRaYLa4zcYCIixyly8N1G6XWRwZ632vb4Ps7Tvla+fnE4bRA8du6V9HYZxESoT3ItC2sLMEeRheij7jIz7QOa7NcHI0chxGGe1Giwicfl/ls7D190XZjjwKBgDIYHkJhIQNQm1UNhOQE/aiiviQUbzmFg9yCUl9kX4+TJEEvTX9cKPk3IWU1mZNYS3z7et9UxKOAOXvwInSOWvVcFSSp4i+mUZzHIvsjQKfVUbVm68hn6colnCo2XbCfMwzvx0UqXJ7sx2F6puvmD+N7ZkqNbnBpofri3vG6heK5AoGASJPXzECa6l1i23fTzJuSW7XUVhCT3y+Ahjl3z3iyMpOxx7HtynlXbcLsGQrsoOH1LbFWhaQGnJcA8l5IVNxqasrBuyazNtD3jyRYRmwyez+MIxS3OqsM5Cf3xMYMHc3fTlFPYaQbY3p7I0peXLaEjgXLITigJtjeMuH6HN6cyXUCgYBe8altRjiikFxnz7t3qYJZJsG44mTLEC157VGIV+XMbRd0q92lVlulkGGKrbaY4zzZ5SiwdYp7CHnZrqpVsV+6fQoaGBQWY6tn0pHqo6QI+kNkqBmJvz0RR/8b8b/vzMECxrpIgJEn/ZEI+v9ehh40uYeQTut1ubH5mYVELQp1Ew==";

    //支付宝公钥
    public static final String PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkwrCqWS8v/t6FneNPOalZl85bFldoXzeqZaZE88WmubzgVGEhL/V3qEtuTFy0TnkO/Vs+TJh8mq/tPLDVce0iQhNvpXQOkO1YgM4S0ZNIPiroxzwVHvKpxtMWIPFfcUM+iUHHuCBFKIDwFMibq3GwXQbJSkbWAdH9ltUx5HkgTKTj2HEo+KlGxKFoFK4YhUuW55MVIaED750wGOT46Be/NALT8nmQKFSk72TmlaJObuOywkvHMor5pJMG6hPG2+3NBnpY/a2AG7Hs9HI6z2eCnGMBe3Ou/bgLlJ0wA9igj6beZXLGJ4YzBzwMbau3LUJXJIACyYpRB4eeSV2CVFJtwIDAQAB";

    public static final String ALI_RECHARGE_NOTIFYURL = "https://www.yhxiadu.com/app/billRecord/alipayCallBack575872E59B8F4499BC18867904071FC5";

	/**微信公众号appid**/
	public static final String WX_GZ_APPID = "wx63be143bda8e2503";
	/**微信公众号appSerct**/
	public static final String WX_GZ_AppSecret = "4abbdffba8205ab95f4d5c4f24da3f7d";
    /**微信appid**/
	public static final String WX_APPID = "wxed7c2646cbc93e99";
	
	public static final String WX_PROGRAM_ID = "wxa8b41277339d53db";
	
	/**微信商户号**/
	public static final String WX_MCHID = "1502986011";
	
	public static final String WX_PROGRAM_MECHID = "1482930542";

	//商户密钥APIkey
	public static final String WX_KEY = "ba4b73917be94f70b9556f2607d1abd5";

    public static final String WX_PROGRAM_KEY = "d3cb1d23236c4f0ab1bba8c8434e33e3";
	
	/**异步通知地址**/
	public static final String WX_RECHARGE_NOTIFYURL ="https://www.yhxiadu.com/app/billRecord/WXpayCallBackF0D56C07947C4D3E874EED3DCCDA6DF7";
	
	/**微信支付方式**/
	public static final String WX_TRADETYPE_APP = "APP";
	public static final String WX_TRADETYPE_WEB = "NATIVE";
	public static final String WX_TRADETYPE_PROGRAM = "JSAPI";
	
	/**微信接口地址**/
	public static final String WX_URL = "https://api.mch.weixin.qq.com/pay/unifiedorder";
	
	/**微信提交方式**/
	public static final String POST = "POST";
	
	public static final String GET = "GET";

}
