﻿<%--
  Created by IntelliJ IDEA.
  User: gaoxiang
  Date: 2017/3/14
  Time: 10:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="ssm" uri="http://ssm.elangzhi.com/jsp/tag/functions" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="zh" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="zh" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="zh">
<head>
<title>慧若停车订单支付确认</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes"/>
<meta name="full-screen" content="yes">
<meta name="browsermode" content="application">
<meta name="x5-fullscreen" content="true">
<meta name="x5-page-mode" content="app">
	<link href="/static/css/lxs_index.css" rel="stylesheet" type="text/css"/>
	<link href="/static/css/lxsHeadFoot.css" rel="stylesheet" type="text/css"/>
	<link href="/static/css/order_new.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="/static/css/animate.min.css"/> <!-- 动画效果 -->
	<link rel="stylesheet" href="/static/css/common.css"/><!-- 页面基本样式 -->
	<script src="/static/metronic/global/plugins/jquery.min.js" type="text/javascript"></script>
	<script src="http://res.wx.qq.com/open/js/jweixin-1.2.0.js" type="text/javascript"></script>

</head>
<body>

<div class="content">
	<div class="headTop">
		<span>停车确认</span>
	</div>
</div>


<div class="j_main m-main">
	<div class="o_logo">
		<span>下载APP更快捷</span><a href="" rel="nofollow">马上下载</a>
	</div>
	<form action="" method="post" name="form_1">
		
		<div class="tit">
			<i></i>确认停车信息
		</div>
		<div class="txt">
			<dl>
				<dt>停车场名称</dt>
				<dd class="line30">${hikBill.parkName}</dd>
			</dl>
			<dl>
				<dt>车牌号码</dt>
				<dd class="line30">${hikBill.plateNo}</dd>
			</dl>
			<dl>
				<dt>车牌颜色</dt>
				<c:if test="${hikBill.plateColor == 0}"><dd class="line30">其他</dd></c:if>
				<c:if test="${hikBill.plateColor == 1}"><dd class="line30">蓝色</dd></c:if>
				<c:if test="${hikBill.plateColor == 2}"><dd class="line30">黄色</dd></c:if>
				<c:if test="${hikBill.plateColor == 3}"><dd class="line30">黑色</dd></c:if>
				<c:if test="${hikBill.plateColor == 4}"><dd class="line30">白色</dd></c:if>
				<c:if test="${hikBill.plateColor == 5}"><dd class="line30">绿色</dd></c:if>
			</dl>
			<dl>
				<dt>入场时间</dt>
				<dd><fmt:formatDate value="${hikBill.inDateTime}" pattern="yyyy年MM月dd日HH点mm分ss秒" /></dd>
			</dl>
			<dl>
				<dt>离场时间时间</dt>
				<dd><fmt:formatDate value="${hikBill.outDateTime}" pattern="yyyy年MM月dd日HH点mm分ss秒" /></dd>
			</dl>
			<dl>
				<dt>停车分钟数</dt>
				<dd>${hikBill.parkTime}</dd>
			</dl>
		</div>
		<div class="tit">
			<i></i>提示信息<br>
			<span>注：支付前请确认展示信息为您本人车辆信息</span>
		</div>
		<div class="txt txt2 J_baoxian">
		</div>
		<script type="text" id="j_baoxian_con"> <dl> <dt> <a href="javascript:;" class="j_baoxian_tit J_baoxian_info">*title*</a> <input type="hidden" name="*name1*" value="*id*" /> <input type="hidden" name="*name2*" value="*price*" /> </dt> <dd> <font><span class="j_baoxian_c">*price_c*</span><i class="more"></i></font> </dd> </dl> </script>
		<script></script>
	</form>
	<div class="submintFix">
		<dl>
			<dt>
			<div class="price">
				订单总额 <span>￥<em class="j_all_money">${money}</em></span>
			</div>
			</dt>
			<dd class="sbmFix"><button type="button" id="save" onclick="onBridgeReady()">提交订单</button></dd>
		</dl>
	</div>
</div>
<script src="/static/js/min_com.js"></script>
<script src="/static/js/jquery.hDialog.min.js"></script>
<script>
var is_dijie = '0'; /*预定须知弹窗*/
 var cart_type_num = 0;
 var myScroll;
 var mobiletel_regexp = /^1[3|4|5|7|8|9][0-9]\d{8}$/;

function onBridgeReady(){
    WeixinJSBridge.invoke(
        'getBrandWCPayRequest', {
            "appId":"${data.appId}",     //公众号名称，由商户传入
            "timeStamp":"${data.timeStamp}",         //时间戳，自1970年以来的秒数
            "nonceStr":"${data.nonceStr}", //随机串
            "package":"${data.packageValue}",
            "signType":"${data.signType}",         //微信签名方式：
            "paySign":"${data.sign}", //微信签名
        },
        function(res){
            if(res.err_msg == "get_brand_wcpay_request:ok" ){
                $.dialog('alert','提示','您已经支付成功');
                // 使用以上方式判断前端返回,微信团队郑重提示：
                //res.err_msg将在用户支付成功后返回ok，但并不保证它绝对可靠。
            }
        });
}


 function bodyscroll(e) {
 	e.preventDefault();
 }

 $('.btn_notes').click(function() {
 	$('.notes_con').show();
 	setTimeout(loaded, 300);
 	document.addEventListener('touchmove', bodyscroll, false);
 });
 $('.notes_con').click(function() {
 	$(this).hide();
 	document.removeEventListener('touchmove', bodyscroll, false);
 }); /*60秒倒计时*/
 var wait = 60;

 function time_d(t) {
 	if (wait == 0) {
 		$(t).removeAttr("disabled").html("获取验证码");
 		wait = 60;
 	} else {
 		$(t).attr("disabled", "disabled").html(wait + '秒后重新发送').addClass('disable');
 		wait--;
 		setTimeout(function() {
 			time_d(t);
 		}, 1000);
 	}
 } /*游客信息检测*/


 function tourist_check() {
 	var tourist_list = $(".j_kehu_open"),
 		type = 1;
 	for (var j = 0; j < tourist_list.length; j++) {
 		var tr = tourist_list.eq(j);
 		if (!tr.data('full')) {
 			alert('请填写<b style="color:#FFF000">游客' + (j + 1) + '</b>的信息');
 			type = 0;
 			break;
 		}
 	};
 	return type ? true : false;
 }; /*异步核对验证码*/


 function mobiletel_code_check() {
 	var ajax_url = '/order/checkCode',
 		code = $('input[name="code"]').val(),
 		mobiletel = $('input[name="mobiletel"]').val();
 	if (mobiletel == '') {
 		alert('请输入手机号码');
 		return false;
 	} else if (!checkMobile(mobiletel)) {
 		alert('请输入正确的手机号码');
 		return false;
 	} else if (code == '' || code.length != 6) {
 		alert('请输入6位验证码');
 		return false;
 	}
 	$('#save').addClass('not');
 	$.ajax({
 		url: ajax_url,
 		type: 'post',
 		data: {
 			mobiletel: mobiletel,
 			code: code,
 			inajax: 1
 		},
 		dataType: 'json',
 		success: function(data) { /*console.log(data);*/
 			if (data == '1') {
 				alert('手机验证完毕');
 				document.form_1.submit();
 			} else {
 				$('#save').removeClass('not');
 				if (data == '-1') {
 					alert('手机号码错误');
 					return false;
 				} else if (data == '-2') {
 					alert('验证码错误');
 					return false;
 				} else {
 					alert('意外错误');
 					return false;
 				}
 			}
 		},
 		error: function() {
 			$('#save').removeClass('not');
 			alert('意外错误');
 			return false
 		}
 	});
 }
 $(function() { /*表单提交*/
 	$('#save').click(function(e) {
 		e.stopPropagation();
 		if ($('#save').hasClass('not')) return false;
 		var uid = parseInt($("#uid").val()); /*检测游客填写的身份信息*/
 		if (!tourist_check()) {
 			return false;
 		}
 		var true_name = $('input[name="truename"]').val(),
 			mobiletel = $('input[name="mobiletel"]').val();
 		if (true_name == '') {
 			alert('联系人为必须填写项');
 			return false;
 		} else if (true_name.length < 2) {
 			alert('联系人过短，请重新输入');
 			return false;
 		} else if (true_name.length > 10) {
 			alert('联系人长度仅限10个字符，请重新输入');
 			return false;
 		} else if (mobiletel == '') {
 			alert('手机号码为必须填写项');
 			return false;
 		} else if (mobiletel.length != 11 || !mobiletel_regexp.test(mobiletel)) {
 			alert('手机号码不正确，请重新输入');
 			return false;
 		}
 		if (!$('.booking_notes i').hasClass('on')) {
 			alert('请阅读并同意此产品的预订须知');
 			return false;
 		}
 		if (uid == 0) {
 			mobiletel_code_check();
 		} else {
 			$('#save').addClass('not');
 			document.form_1.submit();
 		}
 	}); /*发送手机验证码*/
 	$(".mobile_code").click(function() {
 		var th = $(this),
 			tel = $("#n_mobiletel").val(),
 			r_url = '/account/getcode?inajax=1&mobiletel=' + tel + '&idtype=4';
 		if (tel == '') {
 			alert('请先输入手机号码');
 			return false;
 		}
 		if (tel.length != 11 || !mobiletel_regexp.test(tel)) {
 			alert('手机号码不正确，请您重新输入');
 			return false
 		}
 		if (th.hasClass('not')) {
 			return false;
 		}
 		th.addClass('not');
 		setTimeout(function() {
 			th.removeClass('not');
 		}, 60000);
 		$.get(r_url, function(data) {
 			if (data == '1') {
 				alert('短信已发送，请查看');
 			} else if (data == '-1') {
 				alert('获取失败，手机号码不能为空');
 			} else if (data == '-2') {
 				alert('获取失败，手机号码错误');
 			} else if (data == '-3') {
 				alert('获取失败，该手机已被注册');
 			} else if (data == '-4') {
 				alert('您的操作太频繁，请稍候再试');
 			} else if (data == '-8') {
 				alert('同一ip一天最多10条短信');
 			} else if (data == '-5') {
 				alert('同一手机一个月最多5条短信');
 			} else if (data == '-6') {
 				alert('获取失败，获取验证时间间隔60秒');
 			} else {
 				alert('获取失败');
 			}
 		});
 	}); /*改变证件类型事件*/
 	$('#j_kehu_list').on('change', '.tourist_box .certificate_type', function() {
 		
 		placeholder = mark + '号码（必填）';
 		cur.closest('dl').next('dl').find('dt').html(mark).siblings('dd').find('input[type="text"]').attr('placeholder', placeholder);
 	});
 });

 function guoqing_yh() {

 }
</script>
		</body>
		</html>