<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>微信二维码支付</title>
</head>
<body>
	<div>
		<c:if test="${tip.success}">
			<img alt="" src="${tip.message}" style="width: 500px;">
		</c:if>
		<c:if test="${!tip.success}">
			<div style="width: 500px;">${tip.message}</div>
		</c:if>
	</div>
</body>
</html>