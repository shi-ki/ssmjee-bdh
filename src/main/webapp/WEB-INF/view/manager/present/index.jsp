<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>北戴河智慧静态交通综合管理平台</title>
    <style type="text/css">
        body{
            margin: 0px 0px 0px 0px;
        }

        #loading{
            background-color: #2980b9;
            height: 100%;
            width: 100%;
            position: fixed;
            z-index: 1;
            margin-top: 0px;
            top: 0px;
        }
        #loading-center{
            width: 100%;
            height: 100%;
            position: relative;
        }
        #loading-center-absolute {
            position: absolute;
            left: 50%;
            top: 50%;
            height: 118px;
            width: 118px;
            margin-top: -59px;
            margin-left: -59px;
        }

        .object{
            width: 20px;
            height: 20px;
            background-color: #FFF;
            margin-right: 20px;
            float: left;
            margin-bottom: 20px;
        }
        .object:nth-child(3n+0) {
            margin-right: 0px;
        }


        #object_one {
            -webkit-animation: animate 1s -0.9s ease-in-out infinite ;
            animation: animate 1s -0.9s ease-in-out infinite ;
        }
        #object_two {
            -webkit-animation: animate 1s -0.8s ease-in-out infinite ;
            animation: animate 1s -0.8s ease-in-out infinite ;
        }
        #object_three {
            -webkit-animation: animate 1s -0.7s ease-in-out infinite ;
            animation: animate 1s -0.7s ease-in-out infinite ;
        }
        #object_four {
            -webkit-animation: animate 1s -0.6s ease-in-out infinite ;
            animation: animate 1s -0.6s ease-in-out infinite ;
        }
        #object_five {
            -webkit-animation: animate 1s -0.5s ease-in-out infinite ;
            animation: animate 1s -0.5s ease-in-out infinite ;
        }
        #object_six {
            -webkit-animation: animate 1s -0.4s ease-in-out infinite ;
            animation: animate 1s -0.4s ease-in-out infinite ;
        }
        #object_seven {
            -webkit-animation: animate 1s -0.3s ease-in-out infinite ;
            animation: animate 1s -0.3s ease-in-out infinite ;
        }
        #object_eight {
            -webkit-animation: animate 1s -0.2s ease-in-out infinite ;
            animation: animate 1s -0.2s ease-in-out infinite ;
        }
        #object_nine {
            -webkit-animation: animate 1s -0.1s ease-in-out infinite ;
            animation: animate 1s -0.1s ease-in-out infinite ;
        }

        @-webkit-keyframes animate {


            50% {
                -ms-transform: scale(1.5,1.5);
                -webkit-transform: scale(1.5,1.5);
                transform: scale(1.5,1.5);
            }

            100% {
                -ms-transform: scale(1,1);
                -webkit-transform: scale(1,1);
                transform: scale(1,1);
            }

        }

        @keyframes animate {
            50% {
                -ms-transform: scale(1.5,1.5);
                -webkit-transform: scale(1.5,1.5);
                transform: scale(1.5,1.5);
            }

            100% {
                -ms-transform: scale(1,1);
                -webkit-transform: scale(1,1);
                transform: scale(1,1);
            }

        }

        #title{
            position: absolute;
            z-index: 1;
            text-align: center;
        }

        .banner-span{
            font-size: xx-large;
            color: greenyellow;
        }

        .slibar{
            position: absolute;
            z-index: 1;
            top: 10%;

        }

        .replace-new{
            margin-bottom: 20px;
            background-color: #1c1c31;
            filter:alpha(opacity:30);
            opacity:0.7;
            width: 280px;
            height: 50px;
            text-align: center;
            padding-top: 13px;
            font-weight: bold;
            font-size: 16px;
            color: greenyellow;
            border: 2px solid #000000;
        }

        .anchorBL{display:none}

    </style>

    <link href="https://api.map.baidu.com/library/TrafficControl/1.4/src/TrafficControl_min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="/static/present/index/css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="/static/present/index/css/default.css">
    <link href="https://cdn.bootcss.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="/static/present/index/css/Icomoon/style.css" rel="stylesheet" type="text/css" />

</head>
<body>
    <div id="title"></div>

    <div class="slibar">
        <a href="javascript:void(0);" onclick="location.reload();"><div class="replace-new"><span id="time"></span></div></a>
        <a href="javascript:void(0);" onclick="showTraffic(this);"><div class="replace-new"><span>实时路况监测（开启）</span></div></a>
        <a href="javascript:void(0);" onclick="showParkDepot(this);"><div class="replace-new"><span>停车场分布(开启)</span></div></a>
        <a href="javascript:void(0);"><div class="replace-new"><span>城市三级诱导系统</span></div></a>
        <a href="javascript:void(0);"><div class="replace-new"><span>实时监控系统</span></div></a>
        <a href="javascript:void(0);"><div class="replace-new"><span>智能停车管理设备</span></div></a>
    </div>

    <div id="loading">
        <div id="loading-center">
            <div id="loading-center-absolute">
                <div class="object" id="object_one"></div>
                <div class="object" id="object_two"></div>
                <div class="object" id="object_three"></div>
                <div class="object" id="object_four"></div>
                <div class="object" id="object_five"></div>
                <div class="object" id="object_six"></div>
                <div class="object" id="object_seven"></div>
                <div class="object" id="object_eight"></div>
                <div class="object" id="object_nine"></div>
            </div>
        </div>
    </div>

    <div id="container"></div>

    <div id="charts" style="position: absolute;bottom: 0px;"></div>
</body>
<footer>

    <script type="text/javascript" src="/static/present/index/js/jquery-2.0.2.min.js"></script>

    <script type="text/javascript" src="https://api.map.baidu.com/api?v=3.0&ak=KV0oCIEGZ6UQHUZKkjRXGz6Du6WUzBpo"></script>
    <script type="text/javascript" src="https://api.map.baidu.com/library/TrafficControl/1.4/src/TrafficControl_min.js"></script>
    <script type="text/javascript" src="/static/present/jqplot/jqplot.js"></script>
    <script type="text/javascript" src="/static/plugins/layer/layer.js"></script>
    <script type="text/javascript">

        var map;

        var point = new BMap.Point(119.491737, 39.840167);  // 创建点坐标
        var zoom = 15;

        var traffic;

        //实时路况是否打开
        var trafficFlag = false;

        var parkFlag = false;

        var parkArray = [];

        var  mapStyle = {
            features: ["road", "building","water","land"],//隐藏地图上的poi
            style : "midnight"  //设置地图风格为高端黑
        };

        $(document).ready(function(){

            $("#container").width(window.innerWidth).height(window.innerHeight);
            $("#title").width(window.innerWidth).html("<span class=\"banner-span\">北戴河智慧静态交通综合管理平台</span>");

            map = new BMap.Map("container");          // 创建地图实例

            map.centerAndZoom(point, zoom);                 // 初始化地图，设置中心点坐标和地图级别
            map.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
            // map.addEventListener("click", showInfo);
            map.setMapStyle(mapStyle);
            map.addEventListener("tilesloaded",function(){
                $("#loading").fadeOut(2000);
            });

            showtime();

            showCharts();

        });

        $(window).resize(function() {
            $("#container").width(window.innerWidth).height(window.innerHeight);
            $("#title").width(window.innerWidth);
        });

        function showTraffic(obj){
            if(trafficFlag){
                map.removeTileLayer(traffic);
                $($(obj).find("span")).html("实时路况监测（开启)");
            } else {
                traffic = new BMap.TrafficLayer();
                map.addTileLayer(traffic);
                $($(obj).find("span")).html("实时路况监测（关闭)");
            }
            trafficFlag = !trafficFlag;
        }

        function showInfo(e){}

        function showtime() {
            var today,hour,second,minute,year,month,date;
            var strDate ;
            today = new Date();
            var n_day = today.getDay();
            switch (n_day) {
                case 0:{
                    strDate = "星期日"
                } break;
                case 1:{
                    strDate = "星期一"
                } break;
                case 2:{
                    strDate ="星期二"
                } break;
                case 3:{
                    strDate = "星期三"
                } break;
                case 4:{
                    strDate = "星期四"
                } break;
                case 5:{
                    strDate = "星期五"
                } break;
                case 6:{
                    strDate = "星期六"
                } break;
                case 7:{
                    strDate = "星期日"
                } break;
            }
            year = today.getFullYear();
            month = today.getMonth() + 1;
            date = today.getDate();
            hour = today.getHours();
            minute = today.getMinutes();
            second = today.getSeconds();
            month = month < 10 ? "0" + month : month;
            date = date < 10 ? "0" + date : date;
            hour = hour < 10 ? "0" + hour : hour;
            minute = minute < 10 ? "0" + minute : minute;
            second = second < 10 ? "0" + second : second;
            $("#time").html(year + "年" + month + "月" + date + "日 " + "  " + hour + ":" + minute + ":" + second + "  " + strDate); //显示时间
            setTimeout(showtime, 1000); //设定函数自动执行时间为 1000 ms(1 s)
        }

        function showParkDepot(obj){
            if(!parkFlag){
                $($(obj).find("span")).html("停车场分布（关闭)");
                $.ajax({
                    url:"/present/getParkDepots",
                    type:"POST",
                    data:null,
                    dataType:"JSON",
                    success:function(data){
                        if(data.length > 0){
                            for(var d in data){
                                var parkMark = new BMap.Marker(new BMap.Point(data[d].parkLongitude, data[d].parkLatitude), {icon: new BMap.Icon("http://api.map.baidu.com/img/markers.png", new BMap.Size(23, 25))});
                                parkMark.setLabel(new BMap.Label(data[d].parkName,{offset:new BMap.Size(20,-10)}));
                                parkArray.push(parkMark);
                                parkClick(data[d].parkCode, parkMark);
                                map.addOverlay(parkMark);
                            }
                        }
                    }
                });
            } else {
                $($(obj).find("span")).html("停车场分布（开启)");
                for(var pa in parkArray){
                    parkArray[pa].hide();
                }
            }
            parkFlag = !parkFlag;
        }

        function parkClick(code, park){
            park.addEventListener("click", function(e){
                console.log(code);
            });
        }

        function showCharts(){
            $.ajax({url:"/present/parkDepotData",type:"POST",data:null,dataType:"JSON",
                success:function(data){
                    // console.log(data);
                }
            });
            var data = [[1,2,3,4,5,6,7,8,9],[3,6,8,1,11,22,4,21,6]];
            var data_max = 30; //Y轴最大刻度
            var line_title = ["A","B"]; //曲线名称
            var y_label = "这是Y轴"; //Y轴标题
            var x_label = "这是X轴"; //X轴标题
            var x = [1,2,3,4,5,6,7,8,9]; //定义X轴刻度值
            var title = "这是标题"; //统计图标标题
            // j.jqplot.diagram.base("chart1", data, line_title, "这是统计标题", x, x_label, y_label, data_max, 1);
            j.jqplot.diagram.base("charts", data, line_title, "这是统计标题", x, x_label, y_label, data_max, 2);
        }

    </script>
</footer>
</html>
