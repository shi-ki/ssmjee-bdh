<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>行车轨迹</title>
<style type="text/css">
	.anchorBL{display:none}
</style>
</head>
<body>
	<input type="hidden" id="plate-number" value="${plateNumber}"/>
	<input type="hidden" id="execute-date" value="${executeDate}"/>
	<div id="map"></div>
</body>
<footer>
	<script type="text/javascript" src="/static/present/index/js/jquery-2.0.2.min.js"></script>
    <script type="text/javascript" src="https://api.map.baidu.com/api?v=3.0&ak=KV0oCIEGZ6UQHUZKkjRXGz6Du6WUzBpo"></script>
    <script type="text/javascript" src="/static/present/trail/trail.js"></script>
</footer>
</html>