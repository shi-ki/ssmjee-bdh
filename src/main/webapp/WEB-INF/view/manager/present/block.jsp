<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
    <meta http-equiv="X-UA-Compatible" content="IE=10">
    <title>北戴河智慧静态交通综合管理平台</title>

    <link href="https://api.map.baidu.com/library/TrafficControl/1.4/src/TrafficControl_min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="/static/present/index/css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="/static/present/index/css/default.css">
    <link href="https://cdn.bootcss.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="/static/present/index/css/Icomoon/style.css" rel="stylesheet" type="text/css" />
    <link href="/static/present/block/block.css" rel="stylesheet" type="text/css" />

</head>
<body>

    <div id="loading">
        <div id="loading-center">
            <div id="loading-center-absolute">
                <div class="object" id="object_one"></div>
                <div class="object" id="object_two"></div>
                <div class="object" id="object_three"></div>
                <div class="object" id="object_four"></div>
                <div class="object" id="object_five"></div>
                <div class="object" id="object_six"></div>
                <div class="object" id="object_seven"></div>
                <div class="object" id="object_eight"></div>
                <div class="object" id="object_nine"></div>
            </div>
        </div>
    </div>

    <div id="container">
    	<div id="title">
    		<span class="banner-span">北戴河智慧静态交通综合管理平台</span>
    		<div style="float: right;margin-top: 10px">
    			<input type="text" class="trail-input" placeholder="请输入车牌号码" onfocus="deleteBorder(this);" />&nbsp;
    			<input type="text" class="bs-date trail-input" readonly onfocus="deleteBorder(this);" placeholder="请选择查询日期" />&nbsp;
				<a href="javascript:void(0);" onclick="findTrailByPlate(this);" class="button gray bigrounded">查询</a>
    		</div>
    	</div>
    	<div>
    		<div class="four-block first-of-all">
	   			<div id="map-park-depot" class="four-oracle" onclick="showDepot();"></div>
	   			<div class="bottom-word top-middle-word">停车场分布</div>
	    	</div>
	    	<div class="four-block second-of-all">
	    		<div id="map-traffic" class="four-oracle" onclick="showTraffic();"></div>
	    		<div class="bottom-word top-middle-word">实时路况监测</div>
	    	</div>
	    	<div class="four-block third-of-all">
	    		<div id="map-monitor" class="four-oracle" onclick="showMonitor();"></div>
	    		<div class="bottom-word top-middle-word">实时监控系统</div>
	    	</div>
	    	<div class="four-block fourth-of-all">
	    		<div id="map-guide" class="four-oracle" onclick="showGuide();"></div>
	    		<div class="bottom-word top-middle-word">城市三级诱导系统</div>
	    	</div>
    	</div>
    	<div id="project-data" class="top-middle-word">
	    	<span class="span-data">停车场数量：${parkDepotNumbers}</span>
	    	<span class="span-data">停车位总量：${totalParklot}</span>
	    	<span class="span-data">已使用停车位：${parklotUsing}</span>
	    	<span class="span-data">空余：${parklotLeft}</span>
	    	<span class="span-data">已注册用户：${userNumbers}</span>
	    	<span class="span-data">今日收益：${totalFee}（元）</span>
	    </div>
    </div>

    <div id="charts" class="chart-div first-chart"></div>
    <div id="charts2" class="chart-div second-chart"></div>
    <div id="pie" class="chart-div third-chart"></div>
    <div class="copyright">  &copy; 2017-2020 yhxiadu.com 版权所有 鲁ICP备16035191号-2 </div>
    <!-- <div id="oracle" class="chart-div fourth-chart"></div> -->
</body>
<footer>

    <script type="text/javascript" src="/static/present/index/js/jquery-2.0.2.min.js"></script>
    <script type="text/javascript" src="https://api.map.baidu.com/api?v=2.0&ak=KV0oCIEGZ6UQHUZKkjRXGz6Du6WUzBpo"></script>
    <script type="text/javascript" src="https://api.map.baidu.com/library/TrafficControl/1.4/src/TrafficControl_min.js"></script>

    <script type="text/javascript" src="/static/plugins/echarts/echarts.js"></script>

    <script type="text/javascript" src="/static/plugins/layer/layer.js"></script>
	<script type="text/javascript" src="/static/plugins/laydate/laydate.js"></script>
    <script type="text/javascript" src="/static/present/block/block.js"></script>
</footer>
</html>
