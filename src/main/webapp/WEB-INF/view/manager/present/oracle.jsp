<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>北戴河智慧静态交通综合管理平台</title>
    <style type="text/css">
        body{
            margin: 0px 0px 0px 0px;
        }

        #loading{
            background-color: #2980b9;
            height: 100%;
            width: 100%;
            position: fixed;
            z-index: 1;
            margin-top: 0px;
            top: 0px;
        }
        #loading-center{
            width: 100%;
            height: 100%;
            position: relative;
        }
        #loading-center-absolute {
            position: absolute;
            left: 50%;
            top: 50%;
            height: 118px;
            width: 118px;
            margin-top: -59px;
            margin-left: -59px;
        }

        .object{
            width: 20px;
            height: 20px;
            background-color: #FFF;
            margin-right: 20px;
            float: left;
            margin-bottom: 20px;
        }
        .object:nth-child(3n+0) {
            margin-right: 0px;
        }


        #object_one {
            -webkit-animation: animate 1s -0.9s ease-in-out infinite ;
            animation: animate 1s -0.9s ease-in-out infinite ;
        }
        #object_two {
            -webkit-animation: animate 1s -0.8s ease-in-out infinite ;
            animation: animate 1s -0.8s ease-in-out infinite ;
        }
        #object_three {
            -webkit-animation: animate 1s -0.7s ease-in-out infinite ;
            animation: animate 1s -0.7s ease-in-out infinite ;
        }
        #object_four {
            -webkit-animation: animate 1s -0.6s ease-in-out infinite ;
            animation: animate 1s -0.6s ease-in-out infinite ;
        }
        #object_five {
            -webkit-animation: animate 1s -0.5s ease-in-out infinite ;
            animation: animate 1s -0.5s ease-in-out infinite ;
        }
        #object_six {
            -webkit-animation: animate 1s -0.4s ease-in-out infinite ;
            animation: animate 1s -0.4s ease-in-out infinite ;
        }
        #object_seven {
            -webkit-animation: animate 1s -0.3s ease-in-out infinite ;
            animation: animate 1s -0.3s ease-in-out infinite ;
        }
        #object_eight {
            -webkit-animation: animate 1s -0.2s ease-in-out infinite ;
            animation: animate 1s -0.2s ease-in-out infinite ;
        }
        #object_nine {
            -webkit-animation: animate 1s -0.1s ease-in-out infinite ;
            animation: animate 1s -0.1s ease-in-out infinite ;
        }

        @-webkit-keyframes animate {


            50% {
                -ms-transform: scale(1.5,1.5);
                -webkit-transform: scale(1.5,1.5);
                transform: scale(1.5,1.5);
            }

            100% {
                -ms-transform: scale(1,1);
                -webkit-transform: scale(1,1);
                transform: scale(1,1);
            }

        }

        @keyframes animate {
            50% {
                -ms-transform: scale(1.5,1.5);
                -webkit-transform: scale(1.5,1.5);
                transform: scale(1.5,1.5);
            }

            100% {
                -ms-transform: scale(1,1);
                -webkit-transform: scale(1,1);
                transform: scale(1,1);
            }

        }
        
        #container{
        	background-color: #021019;
        }

        #title{
            top: 10px;
            position: absolute;
            text-align: center;
        }

        .banner-span{
            font-size: xx-large;
            color: #a8d3ec;
        }

        .slibar{
            position: absolute;
            z-index: 1;
            top: 10%;

        }

        .replace-new{
            margin-bottom: 20px;
            background-color: #1c1c31;
            filter:alpha(opacity:30);
            opacity:0.7;
            width: 280px;
            height: 50px;
            text-align: center;
            padding-top: 13px;
            font-weight: bold;
            font-size: 16px;
            color: greenyellow;
            border: 2px solid #000000;
        }

        .anchorBL{display:none}
        
        .four-block{
        	top: 70px;
        	width: 25%;
        	height:50%;
        	float: left;
        	position: absolute;
        }
        
        .first-of-all{
        	left: 0px;
        }
        
        .second-of-all{
        	left: 25%;
        }
        
        .third-of-all{
        	left: 50%;
        }
        
        .fourth-of-all{
        	left: 75%;
        }
        
        .four-oracle{
        	width:100%;
        	height: 90%;
        	border-radius:100%;
        	position: absolute;
        	/* border-left:1px solid #3b5061;
        	border-right:1px solid #3b5061; */
        }
        
        .bottom-word{
        	bottom: 0px;
        	width: 100%;
        	position: absolute;
        	text-align: center;
        	font-family: Microsoft YaHei;
        	font-size: 30px;
        }
        
        #project-data{
        	color: #3b5667;
        	position: absolute;
        	text-align: center;
        	font-family: Microsoft YaHei;
        	font-size: 24px;
        	top: 60%;
        }
        
        .span-data{
        	padding-left: 1%;
        }

    </style>

    <link href="https://api.map.baidu.com/library/TrafficControl/1.4/src/TrafficControl_min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="/static/present/index/css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="/static/present/index/css/default.css">
    <link href="https://cdn.bootcss.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="/static/present/index/css/Icomoon/style.css" rel="stylesheet" type="text/css" />

</head>
<body>

    <div id="loading">
        <div id="loading-center">
            <div id="loading-center-absolute">
                <div class="object" id="object_one"></div>
                <div class="object" id="object_two"></div>
                <div class="object" id="object_three"></div>
                <div class="object" id="object_four"></div>
                <div class="object" id="object_five"></div>
                <div class="object" id="object_six"></div>
                <div class="object" id="object_seven"></div>
                <div class="object" id="object_eight"></div>
                <div class="object" id="object_nine"></div>
            </div>
        </div>
    </div>

    <div id="container">
    	<div id="title"></div>
    	<div>
    		<div class="four-block first-of-all">
	   			<div id="map-park-depot" class="four-oracle"></div>
	   			<div class="bottom-word">停车场分布</div>
	    	</div>
	    	<div class="four-block second-of-all">
	    		<div id="map-traffic" class="four-oracle"></div>
	    		<div class="bottom-word">实时路况监测</div>
	    	</div>
	    	<div class="four-block third-of-all">
	    		<div id="map-monitor" class="four-oracle"></div>
	    		<div class="bottom-word">实时监控系统</div>
	    	</div>
	    	<div class="four-block fourth-of-all">
	    		<div id="map-guide" class="four-oracle"></div>
	    		<div class="bottom-word">城市三级诱导系统</div>
	    	</div>
    	</div>
    	<div id="project-data">
	    	<span class="span-data">停车场数量：17</span>
	    	<span class="span-data">停车位总量：17000</span>
	    	<span class="span-data">已使用停车位：10169</span>
	    	<span class="span-data">已注册用户：47681</span>
	    	<span class="span-data">今日收益：123648（元）</span>
	    </div>
    </div>

    <div id="charts" style="position: absolute;bottom: 0px;width: 25%;height: 30%;"></div>
    <div id="charts2" style="position: absolute;bottom: 0px;left: 25%;width: 25%;height: 30%;"></div>
    <div id="pie" style="position: absolute;bottom: 0px;left: 50%;width: 25%;height: 30%;"></div>
    <div id="oracle" style="position: absolute;bottom: 0px;left: 75%;width: 25%;height: 30%;"></div>
</body>
<footer>

    <script type="text/javascript" src="/static/present/index/js/jquery-2.0.2.min.js"></script>

    <script type="text/javascript" src="https://api.map.baidu.com/api?v=3.0&ak=KV0oCIEGZ6UQHUZKkjRXGz6Du6WUzBpo"></script>
    <script type="text/javascript" src="https://api.map.baidu.com/library/TrafficControl/1.4/src/TrafficControl_min.js"></script>
    <!-- <script type="text/javascript" src="/static/present/jqplot/jqplot.js"></script> -->
    <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts/echarts.min.js"></script>
	<script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts-gl/echarts-gl.min.js"></script>
	<script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts-stat/ecStat.min.js"></script>
	<script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts/extension/dataTool.min.js"></script>
	<script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts/map/js/china.js"></script>
	<script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts/map/js/world.js"></script>
	<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=ZUONbpqGBsYGXNIYHicvbAbM"></script>
	<script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts/extension/bmap.min.js"></script>
	<script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/simplex.js"></script>
	
    <script type="text/javascript" src="/static/plugins/layer/layer.js"></script>
    <script type="text/javascript">

        var map_park_depot;
        var map_traffic;
        var map_monitor;
        var map_guide;

        var point = new BMap.Point(119.491737, 39.840167);  // 创建点坐标
        var zoom = 13;

        var traffic;

        //实时路况是否打开
        var trafficFlag = false;

        var parkFlag = false;

        var parkArray = [];

        var  mapStyle = {
            features: ["road", "building","water","land"],//隐藏地图上的poi
            style : "midnight"  //设置地图风格为高端黑
        };

        $(document).ready(function(){

            $("#container").width(window.innerWidth).height(window.innerHeight);
            $("#map-park-depot").css("width", $("#map-park-depot").height()).css("left", ($("#map-park-depot").parent().width() - $("#map-park-depot").width()) / 2);
            $("#map-traffic").css("width", $("#map-traffic").height()).css("left", ($("#map-traffic").parent().width() - $("#map-traffic").width()) / 2);
            $("#map-monitor").css("width", $("#map-monitor").height()).css("left", ($("#map-monitor").parent().width() - $("#map-monitor").width()) / 2);
            $("#map-guide").css("width", $("#map-guide").height()).css("left", ($("#map-guide").parent().width() - $("#map-guide").width()) / 2);
            $("#title").width(window.innerWidth).html("<span class=\"banner-span\">北戴河智慧静态交通综合管理平台</span>");
            $("#project-data").width(window.innerWidth);
            //$("#loading").fadeOut(2000);

            showCharts();
            showCharts2();
            showPie();
            showOracle();
            map_park_depot = new BMap.Map("map-park-depot");          // 创建地图实例
            map_park_depot.centerAndZoom(point, zoom);                 // 初始化地图，设置中心点坐标和地图级别
            map_park_depot.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
            map_park_depot.setMapStyle(mapStyle);
            $.ajax({
                url:"/present/getParkDepots",
                type:"POST",
                data:null,
                dataType:"JSON",
                success:function(data){
                    if(data.length > 0){
                        for(var d in data){
                            var parkMark = new BMap.Marker(new BMap.Point(data[d].parkLongitude, data[d].parkLatitude), {icon: new BMap.Icon("http://api.map.baidu.com/img/markers.png", new BMap.Size(23, 25))});
                            parkMark.setLabel(new BMap.Label(data[d].parkName,{offset:new BMap.Size(20,-10)}));
                            //parkArray.push(parkMark);
                            //parkClick(data[d].parkCode, parkMark);
                            map_park_depot.addOverlay(parkMark);
                        }
                    }
                }
            });
            
            map_traffic = new BMap.Map("map-traffic");          // 创建地图实例
            map_traffic.centerAndZoom(point, zoom);                 // 初始化地图，设置中心点坐标和地图级别
            map_traffic.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
            map_traffic.setMapStyle(mapStyle);
            map_traffic.addTileLayer(new BMap.TrafficLayer());
            
            map_monitor = new BMap.Map("map-monitor");          // 创建地图实例
            map_monitor.centerAndZoom(point, zoom);                 // 初始化地图，设置中心点坐标和地图级别
            map_monitor.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
            map_monitor.setMapStyle(mapStyle);
            
            map_guide = new BMap.Map("map-guide");          // 创建地图实例
            map_guide.centerAndZoom(point, zoom);                 // 初始化地图，设置中心点坐标和地图级别
            map_guide.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
            map_guide.setMapStyle(mapStyle);
            map_guide.addEventListener("tilesloaded",function(){
                $("#loading").fadeOut(2000);
            });

        });

        $(window).resize(function() {
            $("#container").width(window.innerWidth).height(window.innerHeight);
            $("#map-park-depot").css("width", $("#map-park-depot").height()).css("left", ($("#map-park-depot").parent().width() - $("#map-park-depot").width()) / 2);
            $("#map-traffic").css("width", $("#map-traffic").height()).css("left", ($("#map-traffic").parent().width() - $("#map-traffic").width()) / 2);
            $("#map-monitor").css("width", $("#map-monitor").height()).css("left", ($("#map-monitor").parent().width() - $("#map-monitor").width()) / 2);
            $("#map-guide").css("width", $("#map-guide").height()).css("left", ($("#map-guide").parent().width() - $("#map-guide").width()) / 2);
            $("#title").width(window.innerWidth);
            $("#project-data").width(window.innerWidth);
        });
        
        function showCharts(){
        	
        	
        	var dom = document.getElementById("charts");
        	var myChart = echarts.init(dom);
        	var app = {};
        	option = null;
        	option = {
        	    title : {
        	        text: '某地区蒸发量和降水量',
        	        subtext: '纯属虚构'
        	    },
        	    tooltip : {
        	        trigger: 'axis'
        	    },
        	    legend: {
        	        data:['蒸发量','降水量']
        	    },
        	    toolbox: {
        	        show : true,
        	        feature : {
        	            dataView : {show: true, readOnly: false},
        	            magicType : {show: true, type: ['line', 'bar']},
        	            restore : {show: true},
        	            saveAsImage : {show: true}
        	        }
        	    },
        	    calculable : true,
        	    xAxis : [
        	        {
        	            type : 'category',
        	            data : ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月']
        	        }
        	    ],
        	    yAxis : [
        	        {
        	            type : 'value'
        	        }
        	    ],
        	    series : [
        	        {
        	            name:'蒸发量',
        	            type:'bar',
        	            data:[2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3],
        	            markPoint : {
        	                data : [
        	                    {type : 'max', name: '最大值'},
        	                    {type : 'min', name: '最小值'}
        	                ]
        	            },
        	            markLine : {
        	                data : [
        	                    {type : 'average', name: '平均值'}
        	                ]
        	            }
        	        },
        	        {
        	            name:'降水量',
        	            type:'bar',
        	            data:[2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3],
        	            markPoint : {
        	                data : [
        	                    {name : '年最高', value : 182.2, xAxis: 7, yAxis: 183},
        	                    {name : '年最低', value : 2.3, xAxis: 11, yAxis: 3}
        	                ]
        	            },
        	            markLine : {
        	                data : [
        	                    {type : 'average', name : '平均值'}
        	                ]
        	            }
        	        }
        	    ]
        	};
        	;
        	if (option && typeof option === "object") {
        	    myChart.setOption(option, true);
        	}
        }
        
        function showCharts2(){
        	var dom = document.getElementById("charts2");
        	var myChart = echarts.init(dom);
        	var app = {};
        	option = null;
        	option = {
        	    title: {
        	        text: '折线图堆叠'
        	    },
        	    tooltip: {
        	        trigger: 'axis'
        	    },
        	    legend: {
        	        data:['邮件营销','联盟广告','视频广告','直接访问','搜索引擎']
        	    },
        	    grid: {
        	        left: '3%',
        	        right: '4%',
        	        bottom: '3%',
        	        containLabel: true
        	    },
        	    toolbox: {
        	        feature: {
        	            saveAsImage: {}
        	        }
        	    },
        	    xAxis: {
        	        type: 'category',
        	        boundaryGap: false,
        	        data: ['周一','周二','周三','周四','周五','周六','周日']
        	    },
        	    yAxis: {
        	        type: 'value'
        	    },
        	    series: [
        	        {
        	            name:'邮件营销',
        	            type:'line',
        	            stack: '总量',
        	            data:[120, 132, 101, 134, 90, 230, 210]
        	        },
        	        {
        	            name:'联盟广告',
        	            type:'line',
        	            stack: '总量',
        	            data:[220, 182, 191, 234, 290, 330, 310]
        	        },
        	        {
        	            name:'视频广告',
        	            type:'line',
        	            stack: '总量',
        	            data:[150, 232, 201, 154, 190, 330, 410]
        	        },
        	        {
        	            name:'直接访问',
        	            type:'line',
        	            stack: '总量',
        	            data:[320, 332, 301, 334, 390, 330, 320]
        	        },
        	        {
        	            name:'搜索引擎',
        	            type:'line',
        	            stack: '总量',
        	            data:[820, 932, 901, 934, 1290, 1330, 1320]
        	        }
        	    ]
        	};
        	;
        	if (option && typeof option === "object") {
        	    myChart.setOption(option, true);
        	}
        }
        
        function showPie(){
        	var dom = document.getElementById("pie");
        	var myChart = echarts.init(dom);
        	var app = {};
        	option = null;
        	option = {
        	    title : {
        	        text: '某站点用户访问来源',
        	        subtext: '纯属虚构',
        	        x:'center'
        	    },
        	    tooltip : {
        	        trigger: 'item',
        	        formatter: "{a} <br/>{b} : {c} ({d}%)"
        	    },
        	    legend: {
        	        orient: 'vertical',
        	        left: 'left',
        	        data: ['直接访问','邮件营销','联盟广告','视频广告','搜索引擎']
        	    },
        	    series : [
        	        {
        	            name: '访问来源',
        	            type: 'pie',
        	            radius : '55%',
        	            center: ['50%', '60%'],
        	            data:[
        	                {value:335, name:'直接访问'},
        	                {value:310, name:'邮件营销'},
        	                {value:234, name:'联盟广告'},
        	                {value:135, name:'视频广告'},
        	                {value:1548, name:'搜索引擎'}
        	            ],
        	            itemStyle: {
        	                emphasis: {
        	                    shadowBlur: 10,
        	                    shadowOffsetX: 0,
        	                    shadowColor: 'rgba(0, 0, 0, 0.5)'
        	                }
        	            }
        	        }
        	    ]
        	};
        	;
        	if (option && typeof option === "object") {
        	    myChart.setOption(option, true);
        	}
        }
        
        function showOracle(){
        	var dom = document.getElementById("oracle");
        	var myChart = echarts.init(dom);
        	var app = {};
        	option = null;
        	app.title = '环形图';

        	option = {
        	    tooltip: {
        	        trigger: 'item',
        	        formatter: "{a} <br/>{b}: {c} ({d}%)"
        	    },
        	    legend: {
        	        orient: 'vertical',
        	        x: 'left',
        	        data:['直接访问','邮件营销','联盟广告','视频广告','搜索引擎']
        	    },
        	    series: [
        	        {
        	            name:'访问来源',
        	            type:'pie',
        	            radius: ['50%', '70%'],
        	            avoidLabelOverlap: false,
        	            label: {
        	                normal: {
        	                    show: false,
        	                    position: 'center'
        	                },
        	                emphasis: {
        	                    show: true,
        	                    textStyle: {
        	                        fontSize: '30',
        	                        fontWeight: 'bold'
        	                    }
        	                }
        	            },
        	            labelLine: {
        	                normal: {
        	                    show: false
        	                }
        	            },
        	            data:[
        	                {value:335, name:'直接访问'},
        	                {value:310, name:'邮件营销'},
        	                {value:234, name:'联盟广告'},
        	                {value:135, name:'视频广告'},
        	                {value:1548, name:'搜索引擎'}
        	            ]
        	        }
        	    ]
        	};
        	;
        	if (option && typeof option === "object") {
        	    myChart.setOption(option, true);
        	}
        }

    </script>
</footer>
</html>
