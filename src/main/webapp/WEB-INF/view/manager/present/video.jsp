<%@ page language="java" contentType="text/html; utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="/static/present/monitor/ocx/ocx.css" />
</head>

<body>
    <!-- <div class="func-point">&nbsp;&nbsp;监控画面</div> -->
    <div class="video-position" style="width:775px; height:480px">
        <input type="hidden" name="config" id="config" value="ReqType:PlayReal;wndcount:1;" />
        <!-- 添加预览控件（需要先在windows下注册） -->
        <object classid="CLSID:7E393848-7238-4CE3-82EE-44AF444B240A" id="PlayViewOCX" wmode="opaque" width="0" height="0" name="PlayViewOCX">
        </object>
    </div>



    <div class="form" style="display: none;">
        <label for="PalyType">PalyType:</label>
        <br />
        <input type="text" class="PalyType text" value="PlayReal" />
        <br />
        <label for="SvrIp">SvrIp:</label>
        <br />
        <input type="text" class="SvrIp text" value="60.208.57.118" />
        <br />
        <label for="SvrPort">SvrPort:</label>
        <br />
        <input type="text" class="SvrPort text" value="9999" />
        <br />
        <label for="appkey">appkey:</label>
        <br />
        <input type="text" class="appkey text" value="${monitor.data.appKey}" />
        <br />
        <label for="appSecret">appSecret:</label>
        <br />
        <input type="text" class="appSecret text" value="${monitor.data.appSecret}"/>
        <br />
        <label for="time">time:</label>
        <br />
        <input type="text" class="time text" value="${monitor.data.time}" />
        <br />
        <label for="timeSecret">timeSecret:</label>
        <br />
        <input type="text" class="timeSecret text" value="${monitor.data.timeSecret}" />
        <br />
        <label for="httpsflag">httpsflag:</label>
        <br />
        <input type="text" class="httpsflag text" value="1" />
        <br />
        <label for="CamList">CamList:</label>
        <br />
        <input type="text" class="CamList text" value="${number}" />
        <br />
        <input id="submit-click" type="submit" class="submit" value = "视频预览播放"/>
		<input type="submit" class="exe-submit" value = "exe"/>
    </div>
    <!-- <div>
        <p>appSecret, time, timeSecret去
            <a href="https://60.208.57.118:9999/artemis" target="_blank">api网关</a>获取
        </p>
    </div> -->

    <script type="text/javascript" src="/static/present/monitor/ocx/jquery-1.12.1.js"></script>
    <script type="text/javascript" src="/static/present/monitor/ocx/ocx.js"></script>
    <script type="text/javascript">
    	$(document).ready(function(){
    		setTimeout(function(){
    			$("#submit-click").click();
    		}, 500);
    	})
    </script>

    

</body>

</html>