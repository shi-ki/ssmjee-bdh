<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div style="width: 900px; height: 508px;">
    <div id="map" style="width: 100%; height: 100%;"></div>
</div>

<script type="text/javascript">
    var map = new BMap.Map("map");          // 创建地图实例

    var lat, lng;
    if($("#lat").val() != ""){
        lat = $("#lat").val();
        lng = $("#lng").val();
    } else {
        lat = 39.840167;
        lng = 119.491737;
    }

    map.centerAndZoom(new BMap.Point(lng, lat), 15);                 // 初始化地图，设置中心点坐标和地图级别
    map.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
    var marker = new BMap.Marker(new BMap.Point(lng, lat)); // 创建点
    var geoc = new BMap.Geocoder();
    map.addEventListener("tilesloaded",function(){
        map.addOverlay(marker);
        marker.enableDragging();
    });

    function getMarkInfo(){
        $("#lat").val(marker.getPosition().lat);
        $("#lng").val(marker.getPosition().lng);
        geoc.getLocation(marker.getPosition(), function(rs){
            var addComp = rs.addressComponents;
            $("#map-address").val(addComp.province + addComp.city + addComp.district + addComp.street + addComp.streetNumber);
            $("#address").val(addComp.province + addComp.city + addComp.district + addComp.street + addComp.streetNumber);
        });
        layer.close(layer_map);
    }
</script>