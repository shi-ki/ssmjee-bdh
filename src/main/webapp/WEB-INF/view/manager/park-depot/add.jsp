<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--
Created by GaoXiang
Date: 2018-04-14 15:34:14 周六
Version: 1.0
停车场添加页面
--%>

<div class="portlet light">

    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="#" class="form-horizontal" id="save-module">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-6 hide">
                        <div class="form-group">
                            <label class="control-label col-md-3">ID</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="id" value="${longId}" placeholder="默认生成，ID">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">名称</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="name" placeholder="名称">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">层数</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required number="true" name="floorNum" placeholder="层数">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">所在位置</label>
                            <div class="col-md-9">
                                <input  id="lng" name="lng" placeholder="经度" />
                                <input  id="lat" name="lat" placeholder="纬度"/>

                                <%--<input type="text" id="map-address" name="mapAddress" class="form-control" required readonly="readonly" style="cursor: not-allowed;" onclick="showMap();" placeholder="所在位置">--%>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">详细地址</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="address" required name="address" placeholder="详细地址">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">出口数</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required number="true" name="exitNum" placeholder="出口数">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">入口数</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required number="true" name="entranceNum" placeholder="入口数">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">小车车位数</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required number="true" name="carNum" placeholder="小车车位数">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">大车车位数</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required number="true" name="trunkNum" placeholder="大车车位数">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">计费标准</label>
                            <div class="col-md-9">
                                <select class="form-control" required name="feeScale">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>
<!-- script 开始 -->
<script>

    $(function(){
        //初始化页面
        initPage();
    });

    /**
     * 保存停车场
     */
    function save(){
        var param = tools.formParams("save-module");
        if(tools.valid("save-module")){
            tools.post("/parkDepot/save",param,function(data){
                if(data.success){
                    layer.msg('停车场保存成功', {icon: 1,time:1000},function(){
                        //刷新列表页面
                        toPage(null);
                        //关闭弹窗
                        layer.close(layer_addModule);
                    });
                }else{
                    tools.errorTip(data.code,data.message);
                }
            });
        }
    }

    function showMap(){
        $.post("/parkDepot/map", null, function(html){
            window.layer_map = layer.open({
                id:"layer-map",
                type: 1,
                title:"停车场位置",
                area:["900px", "600px"],
                content: html,
                anim:1,
                shadeClose:false,
                btn:["确定", "取消"],
                yes:function(){
                    getMarkInfo();
                },
                btn2:function(index){layer.close(index);},
                cancel: function(){}
            });
        });
    }

</script>
<!-- script 结束 -->