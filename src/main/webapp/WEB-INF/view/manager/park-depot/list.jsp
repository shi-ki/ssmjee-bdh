<%@ page contentType="text/html;charset=UTF-8" language="java"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="ssm" uri="http://ssm.elangzhi.com/jsp/tag/functions" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%--
Created by GaoXiang
Date: 2018-04-14 15:34:14 周六
Version: 1.0
停车场列表
--%>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="zh" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="zh" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="zh">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>停车场管理 - 后台服务中心</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="停车场管理列表" name="description"/>
    <meta content="GaoXiang" name="author"/>
    <jsp:include page="../body/link-page.jsp"/>
</head>
<!-- END HEAD -->
<body class="page-content-white  fade-in-up">

<!-- BEGIN CONTAINER -->
<div class="page-container">


    <!-- BEGIN PAGE TOOLS-->
    <!--查询条件示例 使用时取消hide样式-->
    <div class="portlet light hide">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-paper-plane font-green-haze"></i>
                <span class="caption-subject bold font-green-haze uppercase">搜索</span>
                <span class="caption-helper">点击右侧搜索按钮开始检索</span>
            </div>
            <div class="tools">
                <a href="javascript:void(0);" class="collapse" data-original-title="收起" title="收起"></a>
                <a href="javascript:void(0);" class="fullscreen" data-original-title="全屏" title="全屏"></a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="form-inline" role="form" id="table-param">
                <div class="form-group form-md-line-input has-success">
                    <input type="text" class="form-control" name="name" value="${name}" autofocus placeholder="名称">
                    <div class="form-control-focus"></div>
                </div>
                <button class="btn btn-success btn-tools-search"><i class="icon-magnifier"></i> 搜索</button>
                <button class="btn btn-danger btn-tools-reset" data-url-param="&page=1&size=10"><i class="icon-reload"></i> 重置
                </button>
            </div>
        </div>
    </div>
    <!-- END PAGE TOOLS-->

    <!-- BEGIN PAGE TABLE-->
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-speech  font-blue-hoki"></i>
                <span class="caption-subject font-blue-hoki">数据表</span>
                <span class="caption-helper">
                    当前查询条件下有：<span class="show-page-total">${pageInfo.total}</span> 条数据，
                    总计：<span class="show-page-total">${pageInfo.pages}</span> 页，
                    当前显示第：<span class="show-page-total">${pageInfo.pageNum}</span> 页，
                    首行为第：<span class="show-page-total">${(pageInfo.pageNum - 1) * pageInfo.pageSize + 1}</span> 条数据。
                </span>
            </div>

            <div class="tools">
                <a href="" class="collapse" data-original-title="" title=""> </a>
                <a href="" class="reload btn-tools-refresh" data-original-title="" title=""> </a>
                <a href="" class="fullscreen" data-original-title="" title=""> </a>
            </div>

        </div>
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">

                    <div class="btn-group">
                        <shiro:hasPermission name="parkDepot:add">
                        <a href="javascript:void(0);" class="btn green btn-module-add">
                            添加 <i class="fa fa-plus"></i>
                        </a>
                        </shiro:hasPermission>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="btn-group pull-right">
                        <shiro:hasPermission name="parkDepot:delete">
                        <button class="btn btn-danger btn-module-delete-all"> 批量删除 <i class="fa fa-times"></i></button>
                        </shiro:hasPermission>
                    </div>
                </div>
            </div>
        </div>
        <div class="portlet-body table-responsive">
            <div class="table-scrollable">
                <table data-current-page="${pageInfo.pageNum}" data-page-size="${pageInfo.pageSize}"
                       data-total-counts="${pageInfo.total}" data-page-counts="${pageInfo.pages}"
                       data-visible-pages="10" class="table table-bordered table-hover" id="table">
                    <thead>
                    <tr>
                        <th class="table-checkbox">
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                <input type="checkbox" class="checkbox-all" title="全选"><span></span>
                            </label>
                        </th>
                        <th>编号</th>
                        <th>名称</th>
                        <th>总车位数</th>
                        <th>剩余车位数</th>
                        <th>固定车总车位数</th>
                        <th>固定车剩余车位数</th>
                        <th>停车场描述</th>
                        <th>停车点类型</th>
                        <th>停车点等级</th>
                        <th>预约费用（元/时）</th>
                        <!-- <th>收费规则</th> -->
                        <th>互通编号</th>
                        <th>状态</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="item" items="${pageInfo.list}" varStatus="status">
                        <tr>
                            <td class="center">
                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                    <input type="checkbox" class="checkbox-child" title="选择此条数据" value="${item.parkCode}"><span></span>
                                </label>
                            </td>
                            <td>${item.parkCode}</td>
                            <td>${item.parkName}</td>
                            <td>${item.totalPlot}</td>
                            <td>${item.leftPlot}</td>
                            <td>${item.totalFixedPlot}</td>
                            <td>${item.leftFixedPlot}</td>
                            <td>${item.description}</td>
                            <td>
                                <c:if test="${item.parkType == 1}">封闭式场库</c:if>
                                <c:if test="${item.parkType == 2}">路边停车场</c:if>
                            </td>
                            <td>${item.parkLevel}</td>
                            <td>${item.parkDepot.orderFee}</td>
                            <%-- <td style="width:600px;word-break: break-all;white-space: normal;">${item.parkDepot.chargeRules}</td> --%>
                            <td>
	                            <c:choose>
                                    <c:when test="${item.parkDepot.aiparkStatus == 1}">
                                    	${item.parkDepot.aiparkCode}
                                    </c:when>
                                    <c:otherwise>
                                    	<div class="btn-group btn-group-xs btn-group-solid">
                                            <button data-code="${item.parkCode}" class="btn btn-success" onclick="uploadToAipark(this);"> 上传 </button>
                                        </div>
                                    </c:otherwise>
	                            </c:choose>
                            </td>
                            <td>
                                <c:choose>
                                    <c:when test="${item.parkDepot != null}">
                                        	已关联
                                    </c:when>
                                    <c:otherwise>
                                        	未关联
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <td>
                                <c:choose>
                                    <c:when test="${item.parkDepot != null}">
                                        <div class="btn-group btn-group-xs btn-group-solid">

                                            <button data-id="${item.parkDepot.id}" class="btn btn-warning" onclick="editRelate(this);"> 修改 </button>
                                        </div>
                                    <div class="btn-group btn-group-xs btn-group-solid">
                                        <button data-id="${item.parkDepot.hikId}" class="btn btn-danger" onclick="viewParkCodes(this);"> 出入口 </button>

                                    </div>
                                    <%-- <div class="btn-group btn-group-xs btn-group-solid">
                                            <button data-id="${item.parkDepot.id}" class="btn btn-danger" onclick="deleteRelate(this);"> 取关 </button>
                                        </div> --%>
                                    </c:when>
                                    <c:otherwise>
                                        <div class="btn-group btn-group-xs btn-group-solid">
                                            <button data-code="${item.parkCode}" class="btn btn-success" onclick="addParkDepot(this);"> 关联 </button>
                                        </div>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            <ul class="pagination">
                <li>
                    <select class="form-control" id="pageSize">
                        <optgroup label="每页显示行数"></optgroup>
                        <option ${pageInfo.pageSize==3?"selected":""} value="3">3</option>
                        <option ${pageInfo.pageSize==5?"selected":""} value="5">5</option>
                        <option ${pageInfo.pageSize==10?"selected":""} value="10">10</option>
                        <option ${pageInfo.pageSize==20?"selected":""} value="20">20</option>
                        <option ${pageInfo.pageSize==50?"selected":""} value="50">50</option>
                        <option ${pageInfo.pages==1?"selected":""} value="all">全部</option>
                    </select>
                </li>
            </ul>
            <ul class="pagination" id="pagination"></ul>
        </div>
    </div>
    <!-- END PAGE TABLE-->
</div>
<!-- END CONTAINER -->

<!-- BEGIN PAGE JAVASCRIPT-->
<jsp:include page="../body/javascript-page.jsp"/>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=3.0&ak=KV0oCIEGZ6UQHUZKkjRXGz6Du6WUzBpo"></script>
<script>

    $(document).ready(function () {

        //初始化页面
        initList({
            table: "table",                                                 //表格ID
            url: "/parkDepot/list",                                      //表格分页url
            ajax: true                                                      //为true时伪静态刷新指定ID的table
        });

    });

    function addParkDepot(obj){
        $.post("/parkDepot/relate", {hikId: $(obj).data("code")}, function(html){
            window.layer_relate = layer.open({
                id:"relate",
                type: 1,
                title:"关联停车场",
                area:'900px',
                content: html,
                anim:1,
                shadeClose:false,
                btn:["保存", "取消"],
                yes:function(){
                    save();
                },
                btn2:function(index){layer.close(index);},
                cancel: function(){}
            });
        });
    }

    function editRelate(obj){
        $.post("/parkDepot/editRelate", {id: $(obj).data("id")}, function(html){
            window.layer_editRelate = layer.open({
                id:"editRelate",
                type: 1,
                title:"修改停车场信息",
                area:'900px',
                content: html,
                anim:1,
                shadeClose:false,
                btn:["保存", "取消"],
                yes:function(){
                    save();
                },
                btn2:function(index){layer.close(index);},
                cancel: function(){}
            });
        });
    }
    function viewParkCodes(obj){
        $.post("/parkDepot/viewParkCodes", {id: $(obj).data("id")}, function(html){
            window.layer_editRelate = layer.open({
                id:"editRelate",
                type: 1,
                title:"修改停车场信息",
                area:'900px',
                content: html,
                anim:1,
                shadeClose:false,
                btn:["保存", "取消"],
                yes:function(){
                    save();
                },
                btn2:function(index){layer.close(index);},
                cancel: function(){}
            });
        });
    }
    
    function deleteRelate(obj){
    	layer.confirm("确定取关么？",function() {
    		$.post("/parkDepot/delete",{id:$(obj).data("id")},function(data){
        		if(data.success){
        			layer.msg('取关成功！', {icon: 1,time:1000},function(){
                        //跳转到第一页
                        toPage(1);
                    });
        		} else {
        			tools.errorTip(data.code,data.message);
        		}
        	});
    	});
    }
    
    function uploadToAipark(obj){
    	$.ajax({
    		url:"/parkDepot/sendToAipark",type:"POST",data:{code:$(obj).data("code")},dataType:"JSON",
    		beforeSend: function () {
                layer.load();
            },
    		success:function(data){
                layer.closeAll("loading");
    			if(data.success){
    				toPage(null);
    			} else {
    				tools.errorTip(data.code,data.message);
    			}
    		}
    	});
    }


    /**
     * 添加停车场
     */
    function addModule(){
        $.post("/parkDepot/add", null, function(html){
            window.layer_addModule = layer.open({
                id:"addModule",
                type: 1,
                title:"停车场添加",
                area:'900px',
                content: html,
                anim:1,
                shadeClose:false,
                btn:["保存", "取消"],
                yes:function(){
                    save();
                },
                btn2:function(index){layer.close(index);},
                cancel: function(){}
            });
        });
    }


    /**
     * 编辑停车场
     * @param id 停车场id
     */
    function editModule(id){
        $.post("/parkDepot/edit", {"id":id}, function(html){
            window.layer_editModule = layer.open({
                id:"editModule",
                type: 1,
                title:"停车场编辑",
                area:'900px',
                content: html,
                anim:1,
                shadeClose:false,
                btn:["保存", "取消"],
                yes:function(){
                    edit();
                },
                btn2:function(index){layer.close(index);},
                cancel: function(){}
            });
        });
    }

    /**
     * 删除单个停车场
     * @param id 要删除的停车场id
     */
    function deleteById(id){
        layer.confirm("确定删除么？",function() {
            tools.post("/parkDepot/delete", {"id":id}, function (data) {
                if(data.success){
                    layer.msg('停车场删除成功！', {icon: 1,time:1000},function(){
                        //跳转到第一页
                        toPage(1);
                    });
                }else{
                    tools.errorTip(data.code,data.message);
                }
            });
        });
    }


    /**
     * 批量删除停车场
     */
    function deleteByIds(){
        var ids = getIds($('#table').find(".checkbox-child:checked"));
        if(ids.length === 0){
            layer.msg("请先选择要删除的停车场！", {icon: 2,time:1000});
            return;
        }
        layer.confirm("确定删除选中停车场信息么？",function(){
            tools.post("/parkDepot/deleteByIds",{"ids":ids.join(",")},function(data){
                if(data.success){
                    layer.msg('停车场批量删除成功！', {icon: 1,time:1000},function(){
                        //跳转到第一页
                        toPage(1);
                    });
                }else{
                    tools.errorTip(data.code,data.message);
                }
            });
        });
    }





</script>
<!-- END PAGE JAVASCRIPT-->
</body>
</html>
