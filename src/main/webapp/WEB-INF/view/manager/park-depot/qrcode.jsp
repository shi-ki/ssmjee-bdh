<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/10/24
  Time: 11:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script src="/static/js/jqueryqr.js"></script>
<script src="/static/js/qrcode.js"></script>

<div class="portlet light">

    <div class="portlet-body table-responsive">
        <img id="imgLogo" src="/images/headImage/h2.jpg" hidden>
        <div id="container">
            <canvas width="400" height="400"></canvas>
        </div>
    </div>
</div>
<script>

    $(document).ready(function () {
        var str = "${parkCode}"+","+"${laneNo}";
        var url =  "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx63be143bda8e2503&redirect_uri=http://www.yhxiadu.com/scanToPay&response_type=code&scope=snsapi_userinfo&state="+str+"&connect_redirect=1#wechat_redirect"
        //初始化页面
        $("#container").erweima({
                text:url,
                mode: 0,
                mSize: 40,
        }
        );

    });
</script>