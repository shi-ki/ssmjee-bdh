<%@ page contentType="text/html;charset=UTF-8" language="java"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>

<div class="portlet light">

<div class="portlet-body table-responsive">
    <div class="table-scrollable">
        <table  class="table table-bordered table-hover" id="table">
            <thead>
            <tr>
                <th class="table-checkbox">
                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                        <input type="checkbox" class="checkbox-all" title="全选"><span></span>
                    </label>
                </th>
                <th>出入口编号</th>
                <th>包含车道数</th>
                <th>出入口名称</th>
                <th>终端编号</th>
                <th>终端名称</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="item" items="${data}" varStatus="status">
                <tr>
                    <td class="center">
                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                            <input type="checkbox" class="checkbox-child" title="选择此条数据" value="${item.parkCode}"><span></span>
                        </label>
                    </td>
                    <td>${item.gateCode}</td>
                    <td>${item.laneNum}</td>
                    <td>${item.gateName}</td>

                    <td>${item.terminalCode}</td>
                    <td>${item.terminalName}</td>

                    <td>
                        <div class="btn-group btn-group-xs btn-group-solid">

                            <button data-id="${item.gateCode}" data-parkCode="${item.parkCode}" class="btn btn-warning" onclick="viewParkCodeInfos(this);"> 查看车道 </button>
                        </div>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>

</div>
</div>
<script>
    function viewParkCodeInfos(obj){
        console.log($(obj).attr("data-parkCode"))
        $.post("/parkDepot/viewParkCodeInfos", {id: $(obj).data("id"),parkCode:$(obj).attr("data-parkCode")}, function(html){
            window.layer_CodeInfos = layer.open({
                id:"codeInfos",
                type: 1,
                title:"查看出入口信息",
                area:'900px',
                content: html,
                anim:1,
                shadeClose:false,
            });
        });
    }

</script>