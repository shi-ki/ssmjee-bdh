<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="ssm" uri="http://ssm.elangzhi.com/jsp/tag/functions" %>
<%--
Created by GaoXiang
Date: 2018-04-14 15:34:14 周六
Version: 1.0
停车场编辑页面
--%>

<div class="portlet light" >
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="#" class="form-horizontal" id="edit-module">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-6 hide">
                        <div class="form-group">
                            <label class="control-label col-md-3">ID</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="id" value="${data.id}" placeholder="默认生成，ID">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">名称</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="name" value="${data.name}" placeholder="名称">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">层数</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="floorNum" value="${data.floorNum}" placeholder="层数">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">所在位置</label>
                            <div class="col-md-9">
                                <input type="hidden" class="form-control" id="lat" required name="lat" value="${data.lat}" placeholder="纬度">
                                <input type="hidden" class="form-control" id="lng" required name="lng" value="${data.lng}" placeholder="经度">
                                <input type="text" class="form-control" required id="map-address" name="mapAddress" value="${data.mapAddress}" onclick="showMap();" readonly="readonly" placeholder="所在位置">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">详细地址</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="address" value="${data.address}" placeholder="详细地址">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">出口数</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="exitNum" value="${data.exitNum}" placeholder="出口数">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">入口数</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="entranceNum" value="${data.entranceNum}" placeholder="入口数">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">小车车位数</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="carNum" value="${data.carNum}" placeholder="小车车位数">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">小车车位数（被使用）</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="carUsedNum" value="${data.carUsedNum}" placeholder="小车车位数（被使用）">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">大车车位数</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="trunkNum" value="${data.trunkNum}" placeholder="大车车位数">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">大车车位数（被使用）</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="trunkUsedNum" value="${data.trunkUsedNum}" placeholder="大车车位数（被使用）">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">计费标准</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="feeScale" value="${data.feeScale}" placeholder="计费标准">
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </form>
        <!-- END FORM-->
    </div>
</div>
<!-- script 开始 -->
<script>

    $(function(){
        //初始化页面
        initPage();
    });

    /**
     * 保存编辑的停车场
     */
    function edit(){
        var param = tools.formParams("edit-module");
        if(tools.valid("edit-module")){
            tools.post("/parkDepot/update",param,function(data){
                if(data.success){
                    layer.msg('停车场修改成功', {icon: 1,time:1000},function(){
                        //刷新列表页面
                        toPage(null);
                        //关闭弹窗
                        layer.close(layer_editModule);
                    });
                }else{
                    tools.errorTip(data.code,data.message);
                }
            });
        }
    }

    function showMap(){
        $.post("/parkDepot/map", null, function(html){
            window.layer_map = layer.open({
                id:"layer-map",
                type: 1,
                title:"停车场位置",
                area:["900px", "600px"],
                content: html,
                anim:1,
                shadeClose:false,
                btn:["确定", "取消"],
                yes:function(){
                    getMarkInfo();
                },
                btn2:function(index){layer.close(index);},
                cancel: function(){}
            });
        });
    }

</script>
<!-- script 结束 -->