<%@ page contentType="text/html;charset=UTF-8" language="java"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>

<div class="portlet light">

<div class="portlet-body table-responsive">
    <div class="table-scrollable">
        <table  class="table table-bordered table-hover" id="table">
            <thead>
            <tr>
                <th class="table-checkbox">
                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                        <input type="checkbox" class="checkbox-all" title="全选"><span></span>
                    </label>
                </th>
                <th>车道编号</th>
                <th>车道名称</th>
                <th>车道类型</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="item" items="${data}" varStatus="status">
                <tr>
                    <td class="center">
                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                            <input type="checkbox" class="checkbox-child" title="选择此条数据" value="${item.parkCode}"><span></span>
                        </label>
                    </td>
                    <td>${item.laneNo}</td>
                    <td>${item.laneName}</td>
                    <td>
                        <c:if test="${item.laneType == -1}">其他</c:if>
                        <c:if test="${item.laneType == 0}">入口</c:if>
                        <c:if test="${item.laneType == 1}">出口不收费</c:if>
                        <c:if test="${item.laneType == 2}">出口岗亭收费</c:if>
                        <c:if test="${item.laneType == 3}">出口中央收费</c:if>
                    </td>

                    <td>
                        <div class="btn-group btn-group-xs btn-group-solid">

                            <button data-id="${item.parkCode}" data-laneNo="${item.laneNo}" class="btn btn-warning" onclick="qrcode(this);"> 查看二维码 </button>
                        </div>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>

</div>
</div>
<script>
    function qrcode(obj){
        $.post("/parkDepot/qrcode", {parkCode: $(obj).data("id"),laneNo:$(obj).attr("data-laneNo")}, function(html){
            window.layer_qrcode = layer.open({
                id:"qrcode",
                type: 1,
                title:"查看出入口信息",
                area:'900px',
                content: html,
                anim:1,
                shadeClose:false,
            });
        });
    }

</script>