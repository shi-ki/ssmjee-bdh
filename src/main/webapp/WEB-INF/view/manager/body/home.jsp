<%--
  Created by IntelliJ IDEA.
  User: gaoxiang
  Date: 2017/3/14
  Time: 11:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="ssm" uri="http://ssm.elangzhi.com/jsp/tag/functions" %>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="zh" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="zh" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="zh">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>后台服务中心</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="" name="后台服务中心"/>
    <meta content="" name="GaoXiang"/>
    <style>
        body{
        	margin: 0px;
        }
    </style>
</head>
<!-- END HEAD -->
<body class="page-content-white fade-in-up">

<div id="map"></div>

<input type="hidden" id="rescue-phone" value="${rescuePhone}">
<input type="hidden" id="lat" value="${lat}">
<input type="hidden" id="lng" value="${lng}">

<jsp:include page="../body/javascript-page.jsp"/>
<script>
	var map;
	var point;
	var zoom = 15;

	var  mapStyle = {
    	features: ["road", "building","water","land"],//隐藏地图上的poi
    	style : "midnight"  //设置地图风格为高端黑
	};

    $(document).ready(function () {
    	$("#map").width(window.innerWidth).height(window.innerHeight);
    	map = new BMap.Map("map");          // 创建地图实例
    	
    	var phone = $("#rescue-phone").val();
    	var lat = $("#lat").val();
        var lng = $("#lng").val();
        if(phone != "" && lat != "" && lng != ""){
        	point = new BMap.Point(lng, lat);
        	var address = "";
        	var geoc = new BMap.Geocoder();
        	geoc.getLocation(point, function(rs){
                var addComp = rs.addressComponents;
                address = addComp.province + addComp.city + addComp.district + addComp.street + addComp.streetNumber;
                if(address != undefined && address != ""){
                	address = "，地址：" + address;
                }
                var mark = new BMap.Marker(point, {icon: new BMap.Icon("http://api.map.baidu.com/img/markers.png", new BMap.Size(23, 25))});
            	map.addOverlay(mark);
            	var label = new BMap.Label("手机号码：" + phone + "，经纬度：" + lat + "," + lng + address,{offset:new BMap.Size(20,-10)});
            	label.setStyle({fontSize : "18px", fontFamily:"微软雅黑"});
            	mark.setLabel(label);
            });
        	
        } else {
        	point = new BMap.Point(119.491737, 39.840167);
        }

        map.centerAndZoom(point, zoom);                 // 初始化地图，设置中心点坐标和地图级别
        map.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
        //map.setMapStyle(mapStyle);
    });

</script>
<!-- END PAGE JAVASCRIPT-->
</body>
</html>








