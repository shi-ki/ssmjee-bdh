<%@ page contentType="text/html;charset=UTF-8" language="java"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="ssm" uri="http://ssm.elangzhi.com/jsp/tag/functions" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%--
Created by GaoXiang
Date: 2018-04-28 16:03:15 星期六
Version: 1.0
订单记录列表
--%>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="zh" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="zh" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="zh">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>订单记录管理 - 后台服务中心</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="订单记录管理列表" name="description"/>
    <meta content="GaoXiang" name="author"/>
    <jsp:include page="../body/link-page.jsp"/>
</head>
<!-- END HEAD -->
<body class="page-content-white  fade-in-up">

<!-- BEGIN CONTAINER -->
<div class="page-container">


    <!-- BEGIN PAGE TOOLS-->
    <!--查询条件示例 使用时取消hide样式-->
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-paper-plane font-green-haze"></i>
                <span class="caption-subject bold font-green-haze uppercase">搜索</span>
                <span class="caption-helper">点击右侧搜索按钮开始检索</span>
            </div>
            <div class="tools">
                <a href="javascript:void(0);" class="collapse" data-original-title="收起" title="收起"></a>
                <a href="javascript:void(0);" class="fullscreen" data-original-title="全屏" title="全屏"></a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="form-inline" role="form" id="table-param">
            
                <div class="form-group form-md-line-input has-success">
                    <input type="text" class="form-control" name="userPhone" value="${userPhone}" placeholder="手机号码">
                    <div class="form-control-focus"></div>
                </div>
                
                <div class="form-group form-md-line-input has-success">
                	<select class="form-control" name="isFinished">
                		<option value="">状态</option>
                		<option value="1">已支付</option>
                		<option value="0">未支付</option>
                	</select>
                    <div class="form-control-focus"></div>
                </div>
                
                <div class="form-group form-md-line-input has-success">
                	<select class="form-control" name="payMethod">
                		<option value="">支付方式</option>
                		<option value="0">余额</option>
                        <option value="1">冻结余额</option>
                        <option value="2">支付宝APP</option>
                        <option value="3">微信APP</option>
                        <option value="4">银联</option>
                        <option value="5">支付宝扫码</option>
                        <option value="6">微信扫码</option>
                        <option value="7">微信小程序</option>
                        <option value="8">无用户扫码支付</option>
                	</select>
                    <div class="form-control-focus"></div>
                </div>
                
                <div class="form-group form-md-line-input has-success">
                    <input type="text" class="form-control bs-date" name="createTime" value="${createTime}" placeholder="时间">
                    <div class="form-control-focus"></div>
                </div>
                
                <button class="btn btn-success btn-tools-search"><i class="icon-magnifier"></i> 搜索</button>
                <button class="btn btn-danger btn-tools-reset" data-url-param="&page=1&size=10"><i class="icon-reload"></i> 重置
                </button>
            </div>
        </div>
    </div>
    <!-- END PAGE TOOLS-->

    <!-- BEGIN PAGE TABLE-->
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-speech  font-blue-hoki"></i>
                <span class="caption-subject font-blue-hoki">数据表</span>
                <span class="caption-helper">
                    当前查询条件下有：<span class="show-page-total">${pageInfo.total}</span> 条数据，
                    总计：<span class="show-page-total">${pageInfo.pages}</span> 页，
                    当前显示第：<span class="show-page-total">${pageInfo.pageNum}</span> 页，
                    首行为第：<span class="show-page-total">${(pageInfo.pageNum - 1) * pageInfo.pageSize + 1}</span> 条数据。
                </span>
            </div>

            <div class="tools">
                <a href="" class="collapse" data-original-title="" title=""> </a>
                <a href="" class="reload btn-tools-refresh" data-original-title="" title=""> </a>
                <a href="" class="fullscreen" data-original-title="" title=""> </a>
            </div>

        </div>
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group pull-right">
                        <shiro:hasPermission name="billRecord:delete">
                        <button class="btn btn-danger btn-module-delete-all"> 批量删除 <i class="fa fa-times"></i></button>
                        </shiro:hasPermission>
                    </div>
                </div>
            </div>
        </div>
        <div class="portlet-body table-responsive">
            <div class="table-scrollable">
                <table data-current-page="${pageInfo.pageNum}" data-page-size="${pageInfo.pageSize}"
                       data-total-counts="${pageInfo.total}" data-page-counts="${pageInfo.pages}"
                       data-visible-pages="10" class="table table-bordered table-hover" id="table">
                    <thead>
                    <tr>
                        <th class="table-checkbox">
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                <input type="checkbox" class="checkbox-all" title="全选"><span></span>
                            </label>
                        </th>
                        <th>订单编号</th>
                        <th>用户名</th>
                        <th>订单金额</th>
                        <th>订单内容</th>
                        <th>订单状态</th>
                        <th>车牌号码</th>
                        <th>入场时间</th>
                        <th>出场时间</th>
                        <th>停车时长</th>
                        <th>支付方式</th>
                        <th>备注</th>
                        <%--<th>操作</th>--%>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="item" items="${pageInfo.list}" varStatus="status">
                        <tr>
                            <td class="center">
                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                    <input type="checkbox" class="checkbox-child" title="选择此条数据" value="${item.id}"><span></span>
                                </label>
                            </td>
                            <td>${item.number}</td>
                            <td>${item.userName}</td>
                            <td>${item.addOrSub}${item.payAmount}元</td>
                            <td>${item.content}</td>
                            <td>${item.isFinished == 1 ? "已支付" : "未支付"}</td>
                            <td>${item.plateNo}</td>
                            <td>${ssm:dateToStringTime(item.inDateTime)}</td>
                            <td>${ssm:dateToStringTime(item.outDateTime)}</td>
                            <td>${item.parkTime}</td>

                            <td>
                                <c:if test="${item.payMethod == 0}">余额</c:if>
                                <c:if test="${item.payMethod == 1}">冻结余额</c:if>
                                <c:if test="${item.payMethod == 2}">支付宝APP</c:if>
                                <c:if test="${item.payMethod == 3}">微信APP</c:if>
                                <c:if test="${item.payMethod == 4}">银联</c:if>
                                <c:if test="${item.payMethod == 5}">支付宝扫码</c:if>
                                <c:if test="${item.payMethod == 6}">微信扫码</c:if>
                                <c:if test="${item.payMethod == 7}">微信小程序</c:if>
                            </td>
                            <td>${item.intro}</td>
                            <%--<td>
                                <div class="btn-group btn-group-xs btn-group-solid">
                                    <shiro:hasPermission name="billRecord:edit">
                                    <button data-id="${item.id}" class="btn btn-success btn-module-edit"> 查看\编辑 </button>
                                    </shiro:hasPermission>
                                    <shiro:hasPermission name="billRecord:delete">
                                    <button data-id="${item.id}" class="btn btn-danger btn-module-delete"> 删除 </button>
                                    </shiro:hasPermission>
                                </div>
                            </td>--%>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            <ul class="pagination">
                <li>
                    <select class="form-control" id="pageSize">
                        <optgroup label="每页显示行数"></optgroup>
                        <option ${pageInfo.pageSize==3?"selected":""} value="3">3</option>
                        <option ${pageInfo.pageSize==5?"selected":""} value="5">5</option>
                        <option ${pageInfo.pageSize==10?"selected":""} value="10">10</option>
                        <option ${pageInfo.pageSize==20?"selected":""} value="20">20</option>
                        <option ${pageInfo.pageSize==50?"selected":""} value="50">50</option>
                        <option ${pageInfo.pages==1?"selected":""} value="all">全部</option>
                    </select>
                </li>
            </ul>
            <ul class="pagination" id="pagination"></ul>
        </div>
    </div>
    <!-- END PAGE TABLE-->
</div>
<!-- END CONTAINER -->

<!-- BEGIN PAGE JAVASCRIPT-->
<jsp:include page="../body/javascript-page.jsp"/>
<script>

    $(document).ready(function () {

        //初始化页面
        initList({
            table: "table",                                                 //表格ID
            url: "/billRecord/list",                                      //表格分页url
            ajax: true                                                      //为true时伪静态刷新指定ID的table
        });
        
        $(".bs-date").datetimepicker({
            format: "yyyy/mm/dd",
            language:  "zh-CN",
            autoclose: true,
            minView: 2,
            todayBtn: true,
            todayHighlight:true
        });

    });


    /**
     * 编辑订单记录
     * @param id 订单记录id
     */
    function editModule(id){
        $.post("/billRecord/edit", {"id":id}, function(html){
            window.layer_editModule = layer.open({
                id:"editModule",
                type: 1,
                title:"订单记录编辑",
                /* area:['900px','600px'],*/
                area:'900px',
                content: html,
                anim:1,
                shadeClose:false,
                cancel: function(){}
            });
        });
    }

    /**
     * 删除单个订单记录
     * @param id 要删除的订单记录id
     */
    function deleteById(id){
        layer.confirm("确定删除么？",function() {
            tools.post("/billRecord/delete", {"id":id}, function (data) {
                if(data.success){
                    layer.msg('订单记录删除成功！', {icon: 1,time:1000},function(){
                        //跳转到第一页
                        toPage(1);
                    });
                }else{
                    tools.errorTip(data.code,data.message);
                }
            });
        });
    }


    /**
     * 批量删除订单记录
     */
    function deleteByIds(){
        var ids = getIds($('#table').find(".checkbox-child:checked"));
        if(ids.length === 0){
            layer.msg("请先选择要删除的订单记录！", {icon: 2,time:1000});
            return;
        }
        layer.confirm("确定删除选中订单记录信息么？",function(){
            tools.post("/billRecord/deleteByIds",{"ids":ids.join(",")},function(data){
                if(data.success){
                    layer.msg('订单记录批量删除成功！', {icon: 1,time:1000},function(){
                        //跳转到第一页
                        toPage(1);
                    });
                }else{
                    tools.errorTip(data.code,data.message);
                }
            });
        });
    }





</script>
<!-- END PAGE JAVASCRIPT-->
</body>
</html>
