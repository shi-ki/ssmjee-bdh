<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="ssm" uri="http://ssm.elangzhi.com/jsp/tag/functions" %>
<%--
Created by GaoXiang
Date: 2018-04-28 16:03:15 星期六
Version: 1.0
订单记录编辑页面
--%>

<div class="portlet light" >
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="#" class="form-horizontal" id="edit-module">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-6 hide">
                        <div class="form-group">
                            <label class="control-label col-md-3">ID</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="id" value="${data.id}" placeholder="默认生成，ID">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">number</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="number" value="${data.number}" placeholder="number">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">payAmount</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="payAmount" value="${data.payAmount}" placeholder="payAmount">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">createTime</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="createTime" value="${data.createTime}" placeholder="createTime">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">content</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="content" value="${data.content}" placeholder="content">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">addOrSub</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="addOrSub" value="${data.addOrSub}" placeholder="addOrSub">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">isFinished</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="isFinished" value="${data.isFinished}" placeholder="isFinished">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">intro</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="intro" value="${data.intro}" placeholder="intro">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">userId</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="userId" value="${data.userId}" placeholder="userId">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">userName</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="userName" value="${data.userName}" placeholder="userName">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">userPhone</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="userPhone" value="${data.userPhone}" placeholder="userPhone">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">realName</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="realName" value="${data.realName}" placeholder="realName">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">idNumber</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="idNumber" value="${data.idNumber}" placeholder="idNumber">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">payMethod</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="payMethod" value="${data.payMethod}" placeholder="payMethod">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="button" class="btn green" onclick="edit();">修改订单记录</button>
                                <button type="button" class="btn default" onclick="layer.close(layer_editModule);">取消</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6"> </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>
<!-- script 开始 -->
<script>

    $(function(){
        //初始化页面
        initPage();
    });

    /**
     * 保存编辑的订单记录
     */
    function edit(){
        var param = tools.formParams("edit-module");
        if(tools.valid("edit-module")){
            tools.post("/billRecord/update",param,function(data){
                if(data.success){
                    layer.msg('订单记录修改成功', {icon: 1,time:1000},function(){
                        //刷新列表页面
                        toPage(null);
                        //关闭弹窗
                        layer.close(layer_editModule);
                    });
                }else{
                    tools.errorTip(data.code,data.message);
                }
            });
        }
    }
</script>
<!-- script 结束 -->