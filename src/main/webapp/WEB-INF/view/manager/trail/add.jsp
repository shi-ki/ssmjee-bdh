<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--
Created by GaoXiang
Date: 2018-05-09 17:52:09 星期三
Version: 1.0
轨迹添加页面
--%>

<div class="portlet light">

    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="#" class="form-horizontal" id="save-module">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-6 hide">
                        <div class="form-group">
                            <label class="control-label col-md-3">ID</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="id" value="${longId}" placeholder="默认生成，ID">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">车牌号码</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="plateNumber" placeholder="车牌号码">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">车牌颜色</label>
                            <div class="col-md-9">
                                <select class="form-control" required name="plateColour">
                                	<option value="1">蓝色</option>
                                	<option value="2">黄色</option>
                                	<option value="3">黑色</option>
                                	<option value="4">白色</option>
                                	<option value="5">绿色</option>
                                	<option value="0">其他</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">车辆类型</label>
                            <div class="col-md-9">
                                <select class="form-control" required name="plateType">
                                	<option value="1">小型汽车</option>
                                	<option value="2">大型汽车</option>
                                	<option value="0">其他</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">类型</label>
                            <div class="col-md-9">
                                <select class="form-control" required name="type">
                                	<option value="0">到达</option>
                                	<option value="1">离开</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">状态</label>
                            <div class="col-md-9">
                                <select class="form-control" required name="status">
                                	<option value="1">正常</option>
                                	<option value="0">异常</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">时间</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="executeTime" placeholder="时间">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">时长</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="stayTime" placeholder="时长">
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">经纬度</label>
                            <div class="col-md-9">
                                <input type="text" id="latlng" readonly onclick="showMap();" class="form-control" required placeholder="经纬度">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 hide">
                        <div class="form-group">
                            <label class="control-label col-md-3">纬度</label>
                            <div class="col-md-9">
                                <input type="text" id="lat" class="form-control" required name="lat" placeholder="纬度">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 hide">
                        <div class="form-group">
                            <label class="control-label col-md-3">经度</label>
                            <div class="col-md-9">
                                <input type="text" id="lng" class="form-control" required name="lng" placeholder="经度">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">地址描述</label>
                            <div class="col-md-9">
                                <input type="text" id="address" class="form-control" required name="address" placeholder="地址描述">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">用户id</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="userId" placeholder="用户id">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">用户昵称</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="userName" placeholder="用户昵称">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">姓名</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="name" placeholder="姓名">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">身份证号</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="idNumber" placeholder="身份证号">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">手机号码</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="userPhone" placeholder="手机号码">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">关联上次id</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="superId" placeholder="关联上次id">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>
<!-- script 开始 -->
<script>

    $(function(){
        //初始化页面
        initPage();
    });

    /**
     * 保存轨迹
     */
    function save(){
        var param = tools.formParams("save-module");
        if(tools.valid("save-module")){
            tools.post("/trail/save",param,function(data){
                if(data.success){
                    layer.msg('轨迹保存成功', {icon: 1,time:1000},function(){
                        //刷新列表页面
                        toPage(null);
                        //关闭弹窗
                        layer.close(layer_addModule);
                    });
                }else{
                    tools.errorTip(data.code,data.message);
                }
            });
        }
    }
    
    function showMap(){
        $.post("/parkDepot/map", null, function(html){
            window.layer_map = layer.open({
                id:"layer-map",
                type: 1,
                title:false,
                area:["900px", "600px"],
                content: html,
                anim:1,
                shadeClose:false,
                btn:["确定", "取消"],
                yes:function(){
                    getMarkInfo();
                    $("#latlng").val($("#lat").val() + "," + $("#lng").val());
                },
                btn2:function(index){layer.close(index);},
                cancel: function(){}
            });
        });
    }

</script>
<!-- script 结束 -->