<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--
Created by GaoXiang
Date: 2018-06-06 10:40:13 星期三
Version: 1.0
协议添加页面
--%>

<div class="portlet light">

    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="#" class="form-horizontal" id="save-module">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-6 hide">
                        <div class="form-group">
                            <label class="control-label col-md-3">ID</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="id" value="${longId}" placeholder="默认生成，ID">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">名称</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="name" placeholder="名称">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">版本号</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="version" placeholder="版本号">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">类型</label>
                            <div class="col-md-9">
                                <select class="form-control" name="type">
                                    <option value="0">app用户协议</option>
                                    <option value="1">钱包用户协议</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">状态</label>
                            <div class="col-md-9">
                                <select class="form-control" name="status">
                                	<option value="1">启用</option>
                                	<option value="0">不启用</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">内容</label>
                            <div class="col-md-9">
                                <!-- <input type="text" class="form-control" required name="context" placeholder="context"> -->
                                <!-- <textarea rows="12" cols="12"></textarea> -->
                                <div name="context" id="editor" style="width:850px;height:256px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="form-actions">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="button" class="btn green" onclick="save();">保存协议</button>
                                <button type="button" class="btn default" onclick="layer.close(layer_addModule);">取消</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6"> </div>
                </div>
            </div> -->
        </form>
        <!-- END FORM-->
    </div>
</div>
<!-- script 开始 -->

<script type="text/javascript" charset="utf-8" src="/static/plugins/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="/static/plugins/ueditor/ueditor.all.min.js"> </script>
<!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
<!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
<script type="text/javascript" charset="utf-8" src="/static/plugins/ueditor/lang/zh-cn/zh-cn.js"></script>

<script>

    $(function(){
        //初始化页面
        initPage();
        var ue = UE.getEditor('editor');
    });

    /**
     * 保存协议
     */
    function save(){
        var param = tools.formParams("save-module");
        if(tools.valid("save-module")){
            tools.post("/protocol/save",param,function(data){
                if(data.success){
                    layer.msg('协议保存成功', {icon: 1,time:1000},function(){
                        //刷新列表页面
                        toPage(null);
                        //关闭弹窗
                        layer.close(layer_addModule);
                    });
                }else{
                    tools.errorTip(data.code,data.message);
                }
            });
        }
    }

</script>
<!-- script 结束 -->