<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--
Created by GaoXiang
Date: 2018-07-07 14:01:01 星期六
Version: 1.0
诱导屏添加页面
--%>

<div class="portlet light">

    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="#" class="form-horizontal" id="save-module">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-6 hide">
                        <div class="form-group">
                            <label class="control-label col-md-3">ID</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="id" value="${longId}" placeholder="默认生成，ID">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">name</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="name" placeholder="name">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">number</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="number" placeholder="number">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">lat</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="lat" placeholder="lat">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">lng</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="lng" placeholder="lng">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">baiduNumber</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="baiduNumber" placeholder="baiduNumber">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">parkTopNumber</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="parkTopNumber" placeholder="parkTopNumber">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">parkBottomNumber</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="parkBottomNumber" placeholder="parkBottomNumber">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">imageName</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="imageName" placeholder="imageName">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">imageUrl</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="imageUrl" placeholder="imageUrl">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">playNumber</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="playNumber" placeholder="playNumber">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">playFileName</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="playFileName" placeholder="playFileName">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">playFileUrl</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="playFileUrl" placeholder="playFileUrl">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">topWord</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="topWord" placeholder="topWord">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">bottomWord</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="bottomWord" placeholder="bottomWord">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">pureWord</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="pureWord" placeholder="pureWord">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">status</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="status" placeholder="status">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">type</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="type" placeholder="type">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">createTime</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="createTime" placeholder="createTime">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">updateTime</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="updateTime" placeholder="updateTime">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="button" class="btn green" onclick="save();">保存诱导屏</button>
                                <button type="button" class="btn default" onclick="layer.close(layer_addModule);">取消</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6"> </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>
<!-- script 开始 -->
<script>

    $(function(){
        //初始化页面
        initPage();
    });

    /**
     * 保存诱导屏
     */
    function save(){
        var param = tools.formParams("save-module");
        if(tools.valid("save-module")){
            tools.post("/guideScreen/save",param,function(data){
                if(data.success){
                    layer.msg('诱导屏保存成功', {icon: 1,time:1000},function(){
                        //刷新列表页面
                        toPage(null);
                        //关闭弹窗
                        layer.close(layer_addModule);
                    });
                }else{
                    tools.errorTip(data.code,data.message);
                }
            });
        }
    }

</script>
<!-- script 结束 -->