<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="ssm" uri="http://ssm.elangzhi.com/jsp/tag/functions" %>
<%--
Created by GaoXiang
Date: 2018-07-07 14:01:02 星期六
Version: 1.0
诱导屏编辑页面
--%>

<div class="portlet light" >
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="#" class="form-horizontal" id="edit-module">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-6 hide">
                        <div class="form-group">
                            <label class="control-label col-md-3">ID</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="id" value="${data.id}" placeholder="默认生成，ID">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">名称</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="name" value="${data.name}" placeholder="名称">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">编号</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" readonly style="cursor: not-allowed;" required name="number" value="${data.number}" placeholder="编号">
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">经纬度</label>
                            <div class="col-md-9">
                                <input type="text" id="latlng" class="form-control" required readonly="readonly" style="cursor: not-allowed;" onclick="showMap();" placeholder="经纬度">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 hide">
                        <div class="form-group">
                            <label class="control-label col-md-3">lat</label>
                            <div class="col-md-9">
                                <input type="text" id="lat" class="form-control" required name="lat" value="${data.lat}" placeholder="lat">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 hide">
                        <div class="form-group">
                            <label class="control-label col-md-3">lng</label>
                            <div class="col-md-9">
                                <input type="text" id="lng" class="form-control" required name="lng" value="${data.lng}" placeholder="lng">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">百度编号</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="baiduNumber" value="${data.baiduNumber}" placeholder="百度编号">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">上行编号</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="parkTopNumber" value="${data.parkTopNumber}" placeholder="上行编号">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">下行编号</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="parkBottomNumber" value="${data.parkBottomNumber}" placeholder="下行编号">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">上行提示语</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="topWord" value="${data.topWord}" placeholder="上行提示语">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">下行提示语</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="bottomWord" value="${data.bottomWord}" placeholder="下行提示语">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">纯文字</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="pureWord" value="${data.pureWord}" placeholder="纯文字">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">类型</label>
                            <div class="col-md-9">
                            	<select class="form-control" name="type">
                                	<option value="1" >图片</option>
                                	<option value="0" >文字</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>
<!-- script 开始 -->
<script>

    $(function(){
        //初始化页面
        initPage();
        $("#latlng").val($("#lat").val() + "," + $("#lng").val());
    });

    /**
     * 保存编辑的诱导屏
     */
    function edit(){
        var param = tools.formParams("edit-module");
        if(tools.valid("edit-module")){
            tools.post("/guideScreen/update",param,function(data){
                if(data.success){
                    layer.msg('诱导屏修改成功', {icon: 1,time:1000},function(){
                        //刷新列表页面
                        toPage(null);
                        //关闭弹窗
                        layer.close(layer_editModule);
                    });
                }else{
                    tools.errorTip(data.code,data.message);
                }
            });
        }
    }
    
    function showMap(){
        $.post("/parkDepot/map", null, function(html){
            window.layer_map = layer.open({
                id:"layer-map",
                type: 1,
                title:"停车场位置",
                area:["900px", "600px"],
                content: html,
                anim:1,
                shadeClose:false,
                btn:["确定", "取消"],
                yes:function(){
                    getMarkInfo();
                    $("#latlng").val($("#lat").val() + "," + $("#lng").val());
                },
                btn2:function(index){layer.close(index);},
                cancel: function(){}
            });
        });
    }
    
</script>
<!-- script 结束 -->