<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--
Created by GaoXiang
Date: 2018-06-18 16:06:34 星期一
Version: 1.0
用户消息添加页面
--%>

<div class="portlet light">

    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="#" class="form-horizontal" id="save-module">
            <div class="form-body">

                <div class="row">

                    <div class="col-md-6 hide">
                        <div class="form-group">
                            <label class="control-label col-md-3">userId</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="ids" value="${ids}" placeholder="userId">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">类型</label>
                            <div class="col-md-9">
                                <select class="form-control" name="type">
                                    <option value="0">app消息</option>
                                    <option selected value="1">特别通知</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">标题</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="title" placeholder="标题" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2">消息内容</label>
                            <div class="col-md-10">
                                <textarea class="form-control" name="content"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>
<!-- script 开始 -->
<script>

    $(function(){
        //初始化页面
        initPage();
    });

    /**
     * 保存用户消息
     */
    function save(){
        var param = tools.formParams("save-module");
        if(tools.valid("save-module")){
            tools.post("/personalMessage/save",param,function(data){
                if(data.success){
                    layer.msg('发送成功', {icon: 1,time:1000},function(){
                        //关闭弹窗
                        layer.close(layer_addPersonalMessage);
                    });
                }else{
                    tools.errorTip(data.code,data.message);
                }
            });
        }
    }

</script>
<!-- script 结束 -->