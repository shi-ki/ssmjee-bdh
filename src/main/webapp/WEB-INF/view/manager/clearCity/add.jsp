<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--
Created by GaoXiang
Date: 2018-10-16 10:01:04 星期二
Version: 1.0
开通城市添加页面
--%>

<%--<link href="/static/css/select2.css" type="text/css">--%>
<div class="portlet light">

    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="#" class="form-horizontal" id="save-module">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-6 hide">
                        <div class="form-group">
                            <label class="control-label col-md-3">ID</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="id" value="${longId}" placeholder="默认生成，ID">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 hide" >
                        <div class="form-group">
                            <label class="control-label col-md-3">cityId</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="cityId" required name="cityId" placeholder="cityId">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">开通城市</label>
                            <div class="col-md-9">
                                <select id="area" name="cityName" class="select2" onchange="chooseCity()">
                                    <option value="" selected="selected">请选择城市</option>
                                    <c:forEach var="item" items="${city}">
                                        <option data-id="${item.id}" value="${item.name}">${item.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">纬度</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="lat" placeholder="纬度">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">经度</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="lng" placeholder="经度">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="button" class="btn green" onclick="save();">保存开通城市</button>
                                <button type="button" class="btn default" onclick="layer.close(layer_addModule);">取消</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6"> </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>
<!-- script 开始 -->
<script type="text/javascript" src="/static/js/select2.full.js"/>
<script>

    $(function(){
        //初始化页面
        initPage();
        $("#area").select2({
            zIndex:99999999,
            tags: true,
        });


    });

    /**
     * 保存开通城市
     */
    function save(){
        var param = tools.formParams("save-module");
        if(tools.valid("save-module")){
            tools.post("/clearCity/save",param,function(data){
                if(data.success){
                    layer.msg('开通城市保存成功', {icon: 1,time:1000},function(){
                        //刷新列表页面
                        toPage(null);
                        //关闭弹窗
                        layer.close(layer_addModule);
                    });
                }else{
                    tools.errorTip(data.code,data.message);
                }
            });
        }
    }

    function chooseCity() {
        var a = $("#area  option:selected").data("id");
       $("#cityId").val(a);
    }


</script>
<!-- script 结束 -->