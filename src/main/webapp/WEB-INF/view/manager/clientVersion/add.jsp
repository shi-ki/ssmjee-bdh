<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--
Created by GaoXiang
Date: 2018-06-23 14:11:11 星期六
Version: 1.0
版本信息添加页面
--%>

<div class="portlet light">

    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="#" class="form-horizontal" id="save-module" enctype="multipart/form-data">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-6 hide">
                        <div class="form-group">
                            <label class="control-label col-md-3">ID</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="id" value="${longId}" placeholder="默认生成，ID">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">版本号</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="versionCode" placeholder="版本号">
                            </div>
                        </div>
                    </div>


                    <div class="col-md-6 hide">
                        <div class="form-group">
                            <label class="control-label col-md-3">url</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="url" id="url" placeholder="info">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">备注</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="intro" placeholder="备注">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">上传文件</label>
                            <div class="col-md-9">
                                <input type="file" id="fileupload" name="file" class="hide"
                                       style="max-width: 150px;display: inline;">
                                <a href="javascript:void(0)" class="btn btn-primary btn-xsmall"
                                   onclick="startUp();">选择文件</a>
                            </div>

                        </div>
                        <div class="progress fileupload-progress active">
                            <div class="progress-bar progress-bar-success" role="progressbar" style="width: 0%;">0%</div>
                        </div>
                        <%--<div>--%>
                            <%--<label>--%>
                                <%--<input type="file" id="fileupload" name="file" class="hide"--%>
                                       <%--style="max-width: 150px;display: inline;">--%>
                                <%--<a href="javascript:void(0)" class="btn btn-primary btn-xsmall"--%>
                                   <%--onclick="startUp();">选择文件</a>--%>
                            <%--</label>--%>
                        <%--</div>--%>
                        <%--<div class="row">--%>
                        <%--<div class=" col-md-5">--%>
                        <%--<button type="button" class="btn green" onclick="changeHead();">--%>
                        <%--<i class="fa  fa-cog fa-spin "></i>提交--%>
                        <%--</button>--%>
                        <%--<button type="button" class="btn default" onclick="history.go(-1);">--%>
                        <%--<i class="fa  fa-refresh fa-spin "></i>返回--%>
                        <%--</button>--%>
                        <%--</div>--%>
                        <%--</div>--%>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">文件名</label>
                            <div class="col-md-9">
                                <input type="text" readonly class="form-control" id="fileName" required name="fileName" placeholder="文件名">
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </form>

        <!-- END FORM-->
    </div>
</div>

<div class="col-md-2 hide" id="cParam">
    <table>
        <tr>
            <td style="text-align: right;">src：</td>
            <td><input type="text" id="src" name="src" class="input-xsmall"></td>
        </tr>

    </table>
</div>
<!-- script 开始 -->
<script>
    var jcrop_api;
    $(function(){
        //初始化页面
        initPage();
    });
    $(document).ready(function () {
        initFileupload();
        // setJcrop();
    });
    /**
     * 设置默认选框
     */
    function setJcrop() {
        $('#editImg').Jcrop({
            aspectRatio: 1 / 1,
            boxWidth: 500
        }, function () {
            // Store the API in the jcrop_api variable
            jcrop_api = this;
            jcrop_api.disable();
        });

        //console.log("图片选择器",jcrop_api);
    }
    /**
     * 保存版本信息
     */
    function save(){
        var param = tools.formParams("save-module");
        var jindu = $(".progress-bar").text();

        if (jindu=="100%"){
            if(tools.valid("save-module")){
                $.ajax({url:"/clientVersion/save",data:param,type:"POST",dataType:"JSON",beforeSend: function(){window.layer_load = layer.load();},
                    success:function(data){
                        layer.close(layer_load);
                        if(data.success){
                            layer.msg('版本信息保存成功', {icon: 1,time:1000},function(){
                                //刷新列表页面
                                toPage(null);
                                //关闭弹窗
                                layer.close(layer_addModule);
                            });
                        } else {
                            tools.errorTip(data.code,data.message);
                        }
                    }
                });
            }
        }else {
            tools.errorTip(1,"请等待上传完毕！");
        }

    }


    function initFileupload() {
        //console.log("开始上传");
        $('#fileupload').fileupload({

            url: '/clientVersion/uploadVersionImage',

            formData: {},//如果需要额外添加参数可以在这里添加
            dataType: 'json',
            // addClass: "img-thumbnail",
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                console.log(data.loaded);
                console.log(data.total);
                $('.progress-bar').css(
                    'width',
                    progress + '%'
                );
                $('.progress-bar').text(progress + '%');
            },
            done: function (e, result) {
                //done方法就是上传完毕的回调函数，其他回调函数可以自行查看api
                //注意result要和jquery的ajax的data参数区分，这个对象包含了整个请求信息
                //返回的数据在result.result中
                var data = result.result;
                if (data.success) {
                    var path = "/download/version/"+data.message;
                    $("#url").val(path);
                    $("#fileName").val(data.message);
                } else {
                    var _case = {
                        1: "请选择文件！",
                        3: "文件大小超过限制！"
                    };
                    tools.errorTip( data.code,data.message);
                }

            }


        });
    }
    function startUp() {
        fileupload.click();
    }

</script>
<!-- script 结束 -->