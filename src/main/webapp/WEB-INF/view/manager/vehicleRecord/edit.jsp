<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="ssm" uri="http://ssm.elangzhi.com/jsp/tag/functions" %>
<%--
Created by GaoXiang
Date: 2018-07-23 17:25:10 星期一
Version: 1.0
过车记录编辑页面
--%>

<div class="portlet light" >
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="#" class="form-horizontal" id="edit-module">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-6 hide">
                        <div class="form-group">
                            <label class="control-label col-md-3">ID</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="id" value="${data.id}" placeholder="默认生成，ID">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">unid</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="unid" value="${data.unid}" placeholder="unid">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">plateNo</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="plateNo" value="${data.plateNo}" placeholder="plateNo">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">plateColor</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="plateColor" value="${data.plateColor}" placeholder="plateColor">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">passTime</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="passTime" value="${data.passTime}" placeholder="passTime">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">parkCode</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="parkCode" value="${data.parkCode}" placeholder="parkCode">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">parkName</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="parkName" value="${data.parkName}" placeholder="parkName">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">gateCode</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="gateCode" value="${data.gateCode}" placeholder="gateCode">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">gateName</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="gateName" value="${data.gateName}" placeholder="gateName">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">laneNo</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="laneNo" value="${data.laneNo}" placeholder="laneNo">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">laneName</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="laneName" value="${data.laneName}" placeholder="laneName">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">carType</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="carType" value="${data.carType}" placeholder="carType">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">direct</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="direct" value="${data.direct}" placeholder="direct">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">berthCode</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="berthCode" value="${data.berthCode}" placeholder="berthCode">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="button" class="btn green" onclick="edit();">修改过车记录</button>
                                <button type="button" class="btn default" onclick="layer.close(layer_editModule);">取消</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6"> </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>
<!-- script 开始 -->
<script>

    $(function(){
        //初始化页面
        initPage();
    });

    /**
     * 保存编辑的过车记录
     */
    function edit(){
        var param = tools.formParams("edit-module");
        if(tools.valid("edit-module")){
            tools.post("/vehicleRecord/update",param,function(data){
                if(data.success){
                    layer.msg('过车记录修改成功', {icon: 1,time:1000},function(){
                        //刷新列表页面
                        toPage(null);
                        //关闭弹窗
                        layer.close(layer_editModule);
                    });
                }else{
                    tools.errorTip(data.code,data.message);
                }
            });
        }
    }
</script>
<!-- script 结束 -->