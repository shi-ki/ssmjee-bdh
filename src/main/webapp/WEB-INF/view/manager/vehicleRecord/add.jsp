<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--
Created by GaoXiang
Date: 2018-07-23 17:25:09 星期一
Version: 1.0
过车记录添加页面
--%>

<div class="portlet light">

    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="#" class="form-horizontal" id="save-module">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-6 hide">
                        <div class="form-group">
                            <label class="control-label col-md-3">ID</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="id" value="${longId}" placeholder="默认生成，ID">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">unid</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="unid" placeholder="unid">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">plateNo</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="plateNo" placeholder="plateNo">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">plateColor</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="plateColor" placeholder="plateColor">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">passTime</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="passTime" placeholder="passTime">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">parkCode</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="parkCode" placeholder="parkCode">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">parkName</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="parkName" placeholder="parkName">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">gateCode</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="gateCode" placeholder="gateCode">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">gateName</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="gateName" placeholder="gateName">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">laneNo</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="laneNo" placeholder="laneNo">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">laneName</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="laneName" placeholder="laneName">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">carType</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="carType" placeholder="carType">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">direct</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="direct" placeholder="direct">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">berthCode</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="berthCode" placeholder="berthCode">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="button" class="btn green" onclick="save();">保存过车记录</button>
                                <button type="button" class="btn default" onclick="layer.close(layer_addModule);">取消</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6"> </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>
<!-- script 开始 -->
<script>

    $(function(){
        //初始化页面
        initPage();
    });

    /**
     * 保存过车记录
     */
    function save(){
        var param = tools.formParams("save-module");
        if(tools.valid("save-module")){
            tools.post("/vehicleRecord/save",param,function(data){
                if(data.success){
                    layer.msg('过车记录保存成功', {icon: 1,time:1000},function(){
                        //刷新列表页面
                        toPage(null);
                        //关闭弹窗
                        layer.close(layer_addModule);
                    });
                }else{
                    tools.errorTip(data.code,data.message);
                }
            });
        }
    }

</script>
<!-- script 结束 -->