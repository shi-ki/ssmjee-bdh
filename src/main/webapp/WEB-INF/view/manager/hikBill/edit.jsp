<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="ssm" uri="http://ssm.elangzhi.com/jsp/tag/functions" %>
<%--
Created by GaoXiang
Date: 2018-09-07 14:39:44 星期五
Version: 1.0
海康账单记录编辑页面
--%>

<div class="portlet light" >
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="#" class="form-horizontal" id="edit-module">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-6 hide">
                        <div class="form-group">
                            <label class="control-label col-md-3">ID</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="id" value="${data.id}" placeholder="默认生成，ID">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">parkCode</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="parkCode" value="${data.parkCode}" placeholder="parkCode">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">parkName</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="parkName" value="${data.parkName}" placeholder="parkName">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">inUnid</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="inUnid" value="${data.inUnid}" placeholder="inUnid">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">billNo</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="billNo" value="${data.billNo}" placeholder="billNo">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">inTime</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="inTime" value="${data.inTime}" placeholder="inTime">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">outTime</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="outTime" value="${data.outTime}" placeholder="outTime">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">inDateTime</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="inDateTime" value="${data.inDateTime}" placeholder="inDateTime">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">outDateTime</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="outDateTime" value="${data.outDateTime}" placeholder="outDateTime">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">parkTime</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="parkTime" value="${data.parkTime}" placeholder="parkTime">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">plateNo</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="plateNo" value="${data.plateNo}" placeholder="plateNo">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">plateColor</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="plateColor" value="${data.plateColor}" placeholder="plateColor">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">carType</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="carType" value="${data.carType}" placeholder="carType">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">needPay</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="needPay" value="${data.needPay}" placeholder="needPay">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">createTime</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="createTime" value="${data.createTime}" placeholder="createTime">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">payStatus</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="payStatus" value="${data.payStatus}" placeholder="payStatus">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">status</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="status" value="${data.status}" placeholder="status">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">type</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="type" value="${data.type}" placeholder="type">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="button" class="btn green" onclick="edit();">修改海康账单记录</button>
                                <button type="button" class="btn default" onclick="layer.close(layer_editModule);">取消</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6"> </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>
<!-- script 开始 -->
<script>

    $(function(){
        //初始化页面
        initPage();
    });

    /**
     * 保存编辑的海康账单记录
     */
    function edit(){
        var param = tools.formParams("edit-module");
        if(tools.valid("edit-module")){
            tools.post("/hikBill/update",param,function(data){
                if(data.success){
                    layer.msg('海康账单记录修改成功', {icon: 1,time:1000},function(){
                        //刷新列表页面
                        toPage(null);
                        //关闭弹窗
                        layer.close(layer_editModule);
                    });
                }else{
                    tools.errorTip(data.code,data.message);
                }
            });
        }
    }
</script>
<!-- script 结束 -->