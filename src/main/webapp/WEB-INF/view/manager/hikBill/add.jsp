<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--
Created by GaoXiang
Date: 2018-09-07 14:39:44 星期五
Version: 1.0
海康账单记录添加页面
--%>

<div class="portlet light">

    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="#" class="form-horizontal" id="save-module">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-6 hide">
                        <div class="form-group">
                            <label class="control-label col-md-3">ID</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="id" value="${longId}" placeholder="默认生成，ID">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">parkCode</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="parkCode" placeholder="parkCode">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">parkName</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="parkName" placeholder="parkName">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">inUnid</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="inUnid" placeholder="inUnid">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">billNo</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="billNo" placeholder="billNo">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">inTime</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="inTime" placeholder="inTime">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">outTime</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="outTime" placeholder="outTime">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">inDateTime</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="inDateTime" placeholder="inDateTime">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">outDateTime</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="outDateTime" placeholder="outDateTime">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">parkTime</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="parkTime" placeholder="parkTime">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">plateNo</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="plateNo" placeholder="plateNo">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">plateColor</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="plateColor" placeholder="plateColor">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">carType</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="carType" placeholder="carType">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">needPay</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="needPay" placeholder="needPay">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">createTime</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="createTime" placeholder="createTime">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">payStatus</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="payStatus" placeholder="payStatus">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">status</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="status" placeholder="status">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">type</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="type" placeholder="type">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="button" class="btn green" onclick="save();">保存海康账单记录</button>
                                <button type="button" class="btn default" onclick="layer.close(layer_addModule);">取消</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6"> </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>
<!-- script 开始 -->
<script>

    $(function(){
        //初始化页面
        initPage();
    });

    /**
     * 保存海康账单记录
     */
    function save(){
        var param = tools.formParams("save-module");
        if(tools.valid("save-module")){
            tools.post("/hikBill/save",param,function(data){
                if(data.success){
                    layer.msg('海康账单记录保存成功', {icon: 1,time:1000},function(){
                        //刷新列表页面
                        toPage(null);
                        //关闭弹窗
                        layer.close(layer_addModule);
                    });
                }else{
                    tools.errorTip(data.code,data.message);
                }
            });
        }
    }

</script>
<!-- script 结束 -->