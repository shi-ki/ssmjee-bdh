<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="ssm" uri="http://ssm.elangzhi.com/jsp/tag/functions" %>
<%--
Created by GaoXiang
Date: 2018-05-07 16:25:10 星期一
Version: 1.0
监控编辑页面
--%>

<div class="portlet light" >
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="#" class="form-horizontal" id="edit-module">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-6 hide">
                        <div class="form-group">
                            <label class="control-label col-md-3">ID</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="id" value="${data.id}" placeholder="默认生成，ID">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">编号</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="number" value="${data.number}" placeholder="编号">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">名称</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="name" value="${data.name}" placeholder="名称">
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">经纬度</label>
                            <div class="col-md-9">
                                <input type="text" id="latlng" class="form-control" value="${data.lat},${data.lng}" onclick="showMap();"  style="cursor: not-allowed;" readonly required placeholder="请点击此文本框">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">地址</label>
                            <div class="col-md-9">
                                <input type="text" id="address" class="form-control" required name="address" value="${data.address}" placeholder="地址">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 hide">
                        <div class="form-group">
                            <label class="control-label col-md-3">lat</label>
                            <div class="col-md-9">
                                <input type="text" id="lat" class="form-control" required name="lat" value="${data.lat}" placeholder="lat">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 hide">
                        <div class="form-group">
                            <label class="control-label col-md-3">lng</label>
                            <div class="col-md-9">
                                <input type="text" id="lng" class="form-control" required name="lng" value="${data.lng}" placeholder="lng">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">类型</label>
                            <div class="col-md-9">
                                <select class="form-control" name="type">
                                	<option ${data.type == 0 ? "selected" : ""} value="0">普通监控探头</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">状态</label>
                            <div class="col-md-9">
                            	<select class="form-control" name="status">
                                	<option ${data.status == 0 ? "selected" : ""} value="0">异常</option>
                                	<option ${data.status == 1 ? "selected" : ""} value="1">正常</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">播放地址</label>
                            <div class="col-md-9">
                            	<input type="text" class="form-control" name="playUrl" value="${data.playUrl}" placeholder="播放地址">
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>
<!-- script 开始 -->
<script>

    $(function(){
        //初始化页面
        initPage();
    });

    /**
     * 保存编辑的监控
     */
    function edit(){
        var param = tools.formParams("edit-module");
        if(tools.valid("edit-module")){
            tools.post("/monitor/update",param,function(data){
                if(data.success){
                    layer.msg('监控修改成功', {icon: 1,time:1000},function(){
                        //刷新列表页面
                        toPage(null);
                        //关闭弹窗
                        layer.close(layer_editModule);
                    });
                }else{
                    tools.errorTip(data.code,data.message);
                }
            });
        }
    }
    
    function showMap(){
        $.post("/parkDepot/map", null, function(html){
            window.layer_map = layer.open({
                id:"layer-map",
                type: 1,
                title:false,
                area:["900px", "600px"],
                content: html,
                anim:1,
                shadeClose:false,
                btn:["确定", "取消"],
                yes:function(){
                    getMarkInfo();
                    $("#latlng").val($("#lat").val() + "," + $("#lng").val());
                },
                btn2:function(index){layer.close(index);},
                cancel: function(){}
            });
        });
    }
    
</script>
<!-- script 结束 -->