<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--
Created by GaoXiang
Date: 2018-09-25 14:36:04 星期二
Version: 1.0
上传轮播图添加页面
--%>

<div class="portlet light">

    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="#" class="form-horizontal" id="save-module">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-6 hide">
                        <div class="form-group">
                            <label class="control-label col-md-3">ID</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="id" value="${longId}" placeholder="默认生成，ID">
                                <input type="hidden" class="form-control" name="name" value="" id="fileName" placeholder="默认生成，ID">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">选择图片</label>
                            <div class="col-md-9">
                                <input type="hidden" id="url" class="form-control" required name="url" placeholder="url">
                                <input type="file" required name="file"  class="hidden"  id="img"/>
                                <img id="imgUrl" src="/static/front/image/uploadImg.png"   onclick="$('#img').click();"  style="width: 120px;height: 120px;"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="button" class="btn green" onclick="save();">保存上传轮播图</button>
                                <button type="button" class="btn default" onclick="layer.close(layer_addModule);">取消</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6"> </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>
<!-- script 开始 -->
<script>
    var firstImg ="";
    $(function(){
        //初始化页面
        initPage();
        $("#img").fileupload({
            url: "/image/uploadImage",
            dataType: 'json',
            formData: {},
            add: function (e, data) {
                data.submit();
            },
            done: function (e, data) {
                console.log(data)
                var result = data.result;
                var imgUrl = result.data.url;
                var name = result.data.name;
                console.log(name+"====="+imgUrl);
                $("#fileName").val(name);
                $("#url").val(imgUrl);
                if (result.success == false) {
                    alert("上传失败");
                } else {
                    if(firstImg !=""){
                        $.ajax({
                            url: "/image/delImage",
                            data: {"url": imgUrl},
                            type: "post",
                            dataType: "json",
                            success: function (data) {
                                $("#imgUrl").attr("src","/sysImage/showImg?src="+imgUrl);
                                firstImg =imgUrl;
                            }
                        });
                    }else{
                        $("#imgUrl").attr("src","/sysImage/showImg?src="+imgUrl);
                        firstImg =imgUrl;
                    }

                }
            },
        }).on('fileuploadadd', function (e, data) {
            var files=data.files;
            if(navigator.userAgent.indexOf("MSIE 8.0")>0 || navigator.userAgent.indexOf("MSIE 9.0")>0){
                var objPreviewSizeFake = $('<img style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=image);width:300px;visibility:hidden;"  />').appendTo('body').getDOMNode();
                objPreviewSizeFake.filters.item('DXImageTransform.Microsoft.AlphaImageLoader').src = src;
                //var objPreviewSizeFake = $(".conImage").get(0);//jquery对象转化为DOM对象
                var fileupload = document.getElementById("head");
                var $fileupload = $(fileupload);
                $fileupload.select();
                $fileupload.blur();
                path = document.selection.createRange().text;

                if (/"\w\W"/.test(path)) {
                    path = path.slice(1,-1);
                }

                objPreviewSizeFake.filters.item('DXImageTransform.Microsoft.AlphaImageLoader').src = path;
                var width_img=objPreviewSizeFake.offsetWidth;
                var height_img = objPreviewSizeFake.offsetHeight;
                // if (width_img/height_img != 1/1)  {
                if (width_img!=1080 || height_img != 600) {
                    layer.msg("上传的图片的宽高比例不符合要求，当前图片宽度" + width_img + "px,高度" + height_img + "px，要求：宽度1080px，高度600px!!!");
                    data.abort();
                }
                document.selection.empty();
            }else{
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    var reader = new FileReader();
                    var ret = [];
                    reader.onload = function(theFile) {
                        var image = new Image();
                        image.onload = function() {
                            if (this.width!= 1080 || this.height != 600) {
                            // if (this.width/this.height != 1/1)  {
                                layer.msg("上传的图片的宽高比例不符合要求，当前图片宽度" + this.width + "px,高度" + this.height + "px，要求：宽度1080px，高度600px");
                                $("#imgUrl").attr("src","/static/front/images/uploadImg.png");
                                firstImg ="";
                                data.abort();
                            }
                        };
                        image.src = theFile.target.result;
                    }
                    reader.readAsDataURL(file);
                }
            }
        });

    });

    /**
     * 保存上传轮播图
     */
    function save(){
        var param = tools.formParams("save-module");
        if(tools.valid("save-module")){
            tools.post("/image/save",param,function(data){
                if(data.success){
                    layer.msg('上传轮播图保存成功', {icon: 1,time:1000},function(){
                        //刷新列表页面
                        toPage(null);
                        //关闭弹窗
                        layer.close(layer_addModule);
                    });
                }else{
                    tools.errorTip(data.code,data.message);
                }
            });
        }
    }




</script>
<!-- script 结束 -->