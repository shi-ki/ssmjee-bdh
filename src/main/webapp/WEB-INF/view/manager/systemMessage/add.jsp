<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--
Created by GaoXiang
Date: 2018-06-19 00:46:26 星期二
Version: 1.0
系统消息添加页面
--%>

<div class="portlet light">

    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="#" class="form-horizontal" id="save-module">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-6 hide">
                        <div class="form-group">
                            <label class="control-label col-md-3">ID</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="id" value="${longId}" placeholder="默认生成，ID">
                            </div>
                        </div>
                    </div>

					<div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2">标题</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="title" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2">发送内容</label>
                            <div class="col-md-10">
                                <textarea class="form-control" rows="10" name="content"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>
<!-- script 开始 -->
<script>

    $(function(){
        //初始化页面
        initPage();
    });

    /**
     * 保存系统消息
     */
    function save(){
        var param = tools.formParams("save-module");
        if(tools.valid("save-module")){
        	$.ajax({url:"/systemMessage/save",data:param,type:"POST",dataType:"JSON",beforeSend: function(){window.layer_load = layer.load();},
        		success:function(data){
        			layer.close(layer_load);
        			if(data.success){
                        layer.msg('系统消息保存成功', {icon: 1,time:1000},function(){
                            //刷新列表页面
                            toPage(null);
                            //关闭弹窗
                            layer.close(layer_addModule);
                        });
                    } else {
                        tools.errorTip(data.code,data.message);
                    }
        		}
        	});
        }
    }

</script>
<!-- script 结束 -->