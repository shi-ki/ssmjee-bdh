var map;

var point = new BMap.Point(119.491737, 39.840167);  // 创建点坐标
var zoom = 13;

var  mapStyle = {
    features: ["road", "building","water","land"],//隐藏地图上的poi
    style : "midnight"  //设置地图风格为高端黑
};

$(document).ready(function(){
	
	$("#map").width(window.innerWidth - 25).height(window.innerHeight - 25);
	
	map = new BMap.Map("map");          // 创建地图实例
    map.centerAndZoom(point, zoom);                 // 初始化地图，设置中心点坐标和地图级别
    map.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
    map.setMapStyle(mapStyle);
    // map.addEventListener("tilesloaded", showRoads);
    initData();
});


function initData() {
    $.ajax({
        url: "/present/getParkDepots",
        type: "GET",
        data: null,
        dataType: "JSON",
        async:false,
        success: function (data) {
            if (data.length > 0) {
                var opts = {
                    width: 250,     // 信息窗口宽度
                    height: 180,     // 信息窗口高度
                    title: "信息窗口", // 信息窗口标题
                    enableMessage: true//设置允许信息窗发送短息
                };
                var data_info = [];
                initDataMarker(data, data_info, opts);
            }

        }
    });
}

function initDataMarker(data, data_info, opts) {
    for (var d in data) {
        var parkMark;
        var dd = [data[d].parkLongitude, data[d].parkLatitude, "名称：" + data[d].parkDepot.name + "地址：" + data[d].parkDepot.address + "</br>" + "车位数：" + data[d].parkDepot.carNum + "</br>" + "已用车位：" + data[d].parkDepot.carUsedNum];
        data_info.push(dd);
        var content = dd;
        if (data[d].parkType == 4) {
            parkMark = new BMap.Marker(new BMap.Point(data[d].parkLongitude, data[d].parkLatitude), {icon: new BMap.Icon("/images/map-img/marker16.png", new BMap.Size(32, 32))});
            parkMark.setLabel(new BMap.Label(data[d].parkName, {offset: new BMap.Size(20, -10)}));
        } else if (data[d].parkType == 5) {
            parkMark = new BMap.Marker(new BMap.Point(data[d].parkLongitude, data[d].parkLatitude), {icon: new BMap.Icon("/images/map-img/marker16_1.png", new BMap.Size(32, 32))});
            parkMark.setLabel(new BMap.Label(data[d].parkName, {offset: new BMap.Size(20, -10)}));
        } else {
            parkMark = new BMap.Marker(new BMap.Point(data[d].parkLongitude, data[d].parkLatitude), {icon: new BMap.Icon("http://api.map.baidu.com/img/markers.png", new BMap.Size(23, 25))});
            parkMark.setLabel(new BMap.Label(data[d].parkName, {offset: new BMap.Size(20, -10)}));
        }
        map.addOverlay(parkMark);
        //console.log("++++++++++++++++" + data[d]);
        extracted(parkMark, data, d, opts);
    }
}


function extracted(parkMark, data, d, opts) {
    parkMark.addEventListener("click", function (e) {
        var p = e.target;
        var point = new BMap.Point(p.getPosition().lng, p.getPosition().lat);
        var infoWindow = new BMap.InfoWindow("<div>" + "名称：" + data[d].parkDepot.name + "</br>" + "地址：" + data[d].parkDepot.address + "</br>" + "车位数：" + data[d].parkDepot.carNum , opts);  // 创建信息窗口对象
        map.openInfoWindow(infoWindow, point); //开启信息窗口
    });
}
