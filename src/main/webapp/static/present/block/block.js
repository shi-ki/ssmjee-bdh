var map_park_depot;
var map_traffic;
var map_monitor;
var map_guide;

var point = new BMap.Point(119.491737, 39.840167);  // 创建点坐标
var zoom = 13;

var  mapStyle = {
    features: ["road", "building","water","land"],//隐藏地图上的poi
    style : "midnight"  //设置地图风格为高端黑
};

$(document).ready(function(){

    $("#container").width(window.innerWidth).height(window.innerHeight);
    $("#title").width(window.innerWidth);
    $("#project-data").width(window.innerWidth);

    //右上轨迹检索日期框加载
    laydate.render({
        elem: '.bs-date' //指定元素
    });

    showCharts();
    showCharts2();
    showPie();
    //showOracle();
    map_park_depot = new BMap.Map("map-park-depot");          // 创建地图实例
    map_park_depot.centerAndZoom(point, 16);                 // 初始化地图，设置中心点坐标和地图级别
    map_park_depot.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
    map_park_depot.setMapStyle(mapStyle);

    console.log("ddd-1");
    $.ajax({
        url:"/present/getParkDepots",
        type:"GET",
        data:null,
        dataType:"JSON",
        success:function(data){
            console.log("dssdsdsdsdsdsdsd");
            if(data.length > 0){
                console.log("dssdsdsdsssssssssssssssssssssssdsdsdsd");
                for(var d in data){
                    var parkMark;
                    console.log("sssssss" + data[d].parkType)
                    if (data[d].parkType ==4){
                        parkMark = new BMap.Marker(new BMap.Point(data[d].parkLongitude, data[d].parkLatitude), {icon: new BMap.Icon("/images/map-img/marker16.png", new BMap.Size(32, 32))});
                        parkMark.setLabel(new BMap.Label(data[d].parkName,{offset:new BMap.Size(20,-10)}));
                    }else if(data[d].parkType ==5){
                        parkMark = new BMap.Marker(new BMap.Point(data[d].parkLongitude, data[d].parkLatitude), {icon: new BMap.Icon("/images/map-img/marker16_1.png", new BMap.Size(32, 32))});
                        parkMark.setLabel(new BMap.Label(data[d].parkName,{offset:new BMap.Size(20,-10)}));
                    }else {
                        parkMark = new BMap.Marker(new BMap.Point(data[d].parkLongitude, data[d].parkLatitude), {icon: new BMap.Icon("http://api.map.baidu.com/img/markers.png", new BMap.Size(23, 25))});
                        parkMark.setLabel(new BMap.Label(data[d].parkName,{offset:new BMap.Size(20,-10)}));
                    }
                    map_park_depot.addOverlay(parkMark);
                }
            }
        },
		error:function(){
        	console.log("errot");
		}
    });
    
    map_traffic = new BMap.Map("map-traffic");          // 创建地图实例
    map_traffic.centerAndZoom(point, zoom);                 // 初始化地图，设置中心点坐标和地图级别
    map_traffic.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
    map_traffic.setMapStyle(mapStyle);
    map_traffic.addTileLayer(new BMap.TrafficLayer());
    
    map_monitor = new BMap.Map("map-monitor");          // 创建地图实例
    map_monitor.centerAndZoom(point, zoom);                 // 初始化地图，设置中心点坐标和地图级别
    map_monitor.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
    map_monitor.setMapStyle(mapStyle);
    $.ajax({
        url:"/present/getMonitors",
        type:"GET",
        data:null,
        dataType:"JSON",
        success:function(data){
            if(data.length > 0){
                for(var d in data){
                    var parkMark = new BMap.Marker(new BMap.Point(data[d].lng, data[d].lat), {icon: new BMap.Icon("http://api.map.baidu.com/img/markers.png", new BMap.Size(23, 25))});
                    parkMark.setLabel(new BMap.Label(data[d].name,{offset:new BMap.Size(20,-10)}));
                    map_monitor.addOverlay(parkMark);
                }
            }
        }
    });
    
    map_guide = new BMap.Map("map-guide");          // 创建地图实例
    map_guide.centerAndZoom(point, zoom);                 // 初始化地图，设置中心点坐标和地图级别
    map_guide.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
    map_guide.setMapStyle(mapStyle);
    map_guide.addEventListener("tilesloaded", closeCanvas);

});

$(window).resize(function() {
    $("#container").width(window.innerWidth).height(window.innerHeight);
    $("#title").width(window.innerWidth);
    $("#project-data").width(window.innerWidth);
    showCharts();
    showCharts2();
    showPie();
});

function closeCanvas(){
    $("#loading").fadeOut(2000);
    map_guide.removeEventListener("tilesloaded", closeCanvas);
}

function showCharts(){
	$("#charts").remove();
	$("body").append("<div id=\"charts\" class=\"chart-div first-chart\"></div>");
	$.ajax({
		url:"/present/parkDepotData",type:"GET",data:null,dataType:"JSON",
		success:function(data){
			var dom = document.getElementById("charts");
        	var myChart = echarts.init(dom);
        	var app = {};
        	option = null;
        	option = {
        	    title : {
        	        text: '停车位分布统计',
        	        subtext: ''
        	    },
        	    tooltip : {
        	        trigger: 'axis'
        	    },
        	    legend: {
        	        data:['总数','空余']
        	    },
        	    toolbox: {
        	        show : true,
        	        feature : {
//        	            dataView : {show: true, readOnly: false},
//        	            magicType : {show: true, type: ['line', 'bar']},
//        	            restore : {show: true},
        	            saveAsImage : {show: true}
        	        }
        	    },
        	    calculable : true,
        	    xAxis : [
        	        {
        	            type : 'category',
        	            data : data.names
        	        }
        	    ],
        	    yAxis : [
        	        {
        	            type : 'value'
        	        }
        	    ],
        	    series : [
        	        {
        	            name:'总数',
        	            type:'bar',
        	            data:data.totalParklot,
        	            markPoint : {
        	                data : [
        	                    {type : 'max', name: '最大值'},
        	                    {type : 'min', name: '最小值'}
        	                ]
        	            },
        	            markLine : {
        	                data : [
        	                    {type : 'average', name: '平均值'}
        	                ]
        	            }
        	        },
        	        {
        	            name:'空余',
        	            type:'bar',
        	            data:data.parklotLeft,
        	            markPoint : {
        	                data : [
        	                	{type : 'max', name: '最大值'},
        	                    {type : 'min', name: '最小值'}
//        	                    {name : '年最高', value : 182.2, xAxis: 7, yAxis: 183},
//        	                    {name : '年最低', value : 2.3, xAxis: 11, yAxis: 3}
        	                ]
        	            },
        	            markLine : {
        	                data : [
        	                    {type : 'average', name : '平均值'}
        	                ]
        	            }
        	        }
        	    ]
        	};
        	if (option && typeof option === "object") {
        	    myChart.setOption(option, true);
        	}
		}
	});
}

function showCharts2(){
    $("#charts2").remove();
    $("body").append("<div id=\"charts2\" class=\"chart-div second-chart\"></div>");
	$.ajax({
		url:"/present/userData",type:"GET",data:{num:7},dataType:"JSON",
		success:function(data){
			var dom = document.getElementById("charts2");
        	var myChart = echarts.init(dom);
        	var app = {};
        	option = null;
        	option = {
        	    title: {
        	        text: '注册用户数量'
        	    },
        	    tooltip: {
        	        trigger: 'axis'
        	    },
        	    legend: {
        	        data:['注册用户']
        	    },
        	    grid: {
        	        left: '3%',
        	        right: '4%',
        	        bottom: '3%',
        	        containLabel: true
        	    },
        	    toolbox: {
        	        feature: {
        	            saveAsImage: {}
        	        }
        	    },
        	    xAxis: {
        	        type: 'category',
        	        boundaryGap: false,
        	        data: data.weeks
        	    },
        	    yAxis: {
        	        type: 'value'
        	    },
        	    series: [
        	        {
        	            name:'注册用户',
        	            type:'line',
        	            stack: '总量',
        	            data:data.numbers
        	        }
        	    ]
        	};
        	if (option && typeof option === "object") {
        	    myChart.setOption(option, true);
        	}
		}
	});
}

function showPie(){
    $("#pie").remove();
    $("body").append("<div id=\"pie\" class=\"chart-div third-chart\"></div>");
	$.ajax({
		url:"/present/leftParkDepotNum",type:"GET",data:null,dataType:"JSON",
		success:function(data){
			var dom = document.getElementById("pie");
        	var myChart = echarts.init(dom);
        	var app = {};
        	option = null;
        	option = {
        	    title : {
        	        text: '停车位空余分布',
        	        //subtext: '纯属虚构',
        	        x:'center'
        	    },
        	    tooltip : {
        	        trigger: 'item',
        	        formatter: "{a} <br/>{b} : {c} ({d}%)"
        	    },
        	    legend: {
        	        orient: 'vertical',
        	        left: 'left',
        	        data: ['空余停车位','正在使用停车位']
        	    },
        	    series : [
        	        {
        	            name: '访问来源',
        	            type: 'pie',
        	            radius : '55%',
        	            center: ['50%', '60%'],
        	            data:[
        	                {value:data.parklotLeft, name:'空余停车位'},
        	                {value:data.parklotUsing, name:'正在使用停车位'}
        	            ],
        	            itemStyle: {
        	                emphasis: {
        	                    shadowBlur: 10,
        	                    shadowOffsetX: 0,
        	                    shadowColor: 'rgba(0, 0, 0, 0.5)'
        	                }
        	            }
        	        }
        	    ]
        	};
        	if (option && typeof option === "object") {
        	    myChart.setOption(option, true);
        	}
		}
	});
}

function showOracle(){
	var dom = document.getElementById("oracle");
	var myChart = echarts.init(dom);
	var app = {};
	option = null;
	app.title = '环形图';

	option = {
	    tooltip: {
	        trigger: 'item',
	        formatter: "{a} <br/>{b}: {c} ({d}%)"
	    },
	    legend: {
	        orient: 'vertical',
	        x: 'left',
	        data:['直接访问','邮件营销','联盟广告','视频广告','搜索引擎']
	    },
	    series: [
	        {
	            name:'访问来源',
	            type:'pie',
	            radius: ['50%', '70%'],
	            avoidLabelOverlap: false,
	            label: {
	                normal: {
	                    show: false,
	                    position: 'center'
	                },
	                emphasis: {
	                    show: true,
	                    textStyle: {
	                        fontSize: '30',
	                        fontWeight: 'bold'
	                    }
	                }
	            },
	            labelLine: {
	                normal: {
	                    show: false
	                }
	            },
	            data:[
	                {value:335, name:'直接访问'},
	                {value:310, name:'邮件营销'},
	                {value:234, name:'联盟广告'},
	                {value:135, name:'视频广告'},
	                {value:1548, name:'搜索引擎'}
	            ]
	        }
	    ]
	};
	if (option && typeof option === "object") {
	    myChart.setOption(option, true);
	}
}

//放大停车场地图
function showDepot(){
	var width = ($("#container").width() / 4 * 3) + "px";
	var height = ($("#container").height() / 4 * 3) + "px";
	//iframe层
	layer.open({
	  type: 2,
	  anim: 5,
	  title: "停车系统",
	  resize: false,
	  closeBtn: 1, //不显示关闭按钮
	  isOutAnim: true,
	  shadeClose: false,
	  shade: 0.6,
	  area: [width, height],
	  content: "/present/depot" //iframe的url
	});
}

//放大交通路况地图
function showTraffic(){
	var width = ($("#container").width() / 4 * 3) + "px";
	var height = ($("#container").height() / 4 * 3) + "px";
	//iframe层
	layer.open({
	  type: 2,
	  anim: 5,
	  title: "实时路况系统",
	  resize: false,
	  closeBtn: 1, //不显示关闭按钮
	  isOutAnim: true,
	  shadeClose: false,
	  shade: 0.6,
	  area: [width, height],
	  content: "/present/traffic" //iframe的url
	});
}

//放大监控地图
function showMonitor(){
	var width = ($("#container").width() / 4 * 3) + "px";
	var height = ($("#container").height() / 4 * 3) + "px";
	//iframe层
	layer.open({
	  type: 2,
	  anim: 5,
	  title: "监控系统",
	  resize: false,
	  closeBtn: 1, //不显示关闭按钮
	  isOutAnim: true,
	  shadeClose: false,
	  shade: 0.6,
	  area: [width, height],
	  content: "/present/monitor" //iframe的url
	});
}

//放大监控地图
function showGuide(){
	var width = ($("#container").width() / 4 * 3) + "px";
	var height = ($("#container").height() / 4 * 3) + "px";
	//iframe层
	layer.open({
	  type: 2,
	  anim: 5,
	  title: "三级诱导系统",
	  resize: false,
	  closeBtn: 1, //不显示关闭按钮
	  isOutAnim: true,
	  shadeClose: false,
	  shade: 0.6,
	  area: [width, height],
	  content: "/present/guide" //iframe的url
	});
}

function deleteBorder(obj){
	$(obj).css("border", "");
}

function findTrailByPlate(obj){
	$(obj).blur();
	var childArray = $(obj).parent().children();
	if(childArray[0].value !== ""){
		if(childArray[1].value !== ""){
			var width = ($("#container").width() / 4 * 3) + "px";
			var height = ($("#container").height() / 4 * 3) + "px";
			//iframe层
			layer.open({
			  type: 2,
			  anim: 5,
			  title: "行车轨迹",
			  resize: false,
			  closeBtn: 1,
			  isOutAnim: true,
			  shadeClose: false,
			  shade: 0.6,
			  area: [width, height],
			  content: "/present/trail?plateNumber=" + childArray[0].value + "&executeDate=" + childArray[1].value //iframe的url
			});
		} else {
			$(childArray[1]).css("border", "red solid 1px");
			layer.msg("请选择查询日期", {icon: 2, time: 1000});
		}
	} else {
		$(childArray[0]).css("border", "red solid 1px");
		layer.msg("请输入车牌号码", {icon: 2, time: 1000});
	}
}