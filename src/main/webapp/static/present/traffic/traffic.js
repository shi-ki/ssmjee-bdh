var map;

var point = new BMap.Point(119.491737, 39.840167);  // 创建点坐标
var zoom = 13;

var  mapStyle = {
    features: ["road", "building","water","land"],//隐藏地图上的poi
    style : "midnight"  //设置地图风格为高端黑
};

$(document).ready(function(){
	
	$("#map").width(window.innerWidth - 25).height(window.innerHeight - 25);
	
	map = new BMap.Map("map");          // 创建地图实例
    map.centerAndZoom(point, zoom);                 // 初始化地图，设置中心点坐标和地图级别
    map.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
    map.setMapStyle(mapStyle);
    map.addTileLayer(new BMap.TrafficLayer());
    map.addEventListener("tilesloaded", showRoads);
    
});

function showRoads(){
    map.removeEventListener("tilesloaded", showRoads);
}