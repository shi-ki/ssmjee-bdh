var map;

var point = new BMap.Point(119.491737, 39.840167);  // 创建点坐标
var zoom = 13;

var  mapStyle = {
    features: ["road", "building","water","land"],//隐藏地图上的poi
    style : "midnight"  //设置地图风格为高端黑
};

$(document).ready(function(){
	
	$("#map").width(window.innerWidth - 25).height(window.innerHeight - 25);
	
	map = new BMap.Map("map");          // 创建地图实例
    map.centerAndZoom(point, zoom);                 // 初始化地图，设置中心点坐标和地图级别
    map.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
    map.setMapStyle(mapStyle);
    map.addEventListener("tilesloaded", showRoads);
    
});

function showRoads(){
    $.ajax({
        url:"/present/findTrailByPlate",type:"GET",data:{plateNumber: $("#plate-number").val(),executeDate:$("#execute-date").val()},dataType:"JSON",
        success:function(data){
            if(data.success){
                data = data.data;
                if(data.length > 0){
                    var driving = new BMap.DrivingRoute(map);
                    for(var i in data){
                        var address;
                        if(i == 0){
                            address = "起点：";
                        } else if(i == (data.length - 1)){
                            address = "终点：";
                        } else {
                            address = "";
                        }
                        map.addOverlay(new BMap.Marker(new BMap.Point(data[i].lng, data[i].lat)));
                        map.addOverlay(new BMap.Label(address,{position:new BMap.Point(data[i].lng, data[i].lat)}));
                        if(i > 0) {
                            driving.search(new BMap.Point(data[i-1].lng, data[i-1].lat), new BMap.Point(data[i].lng, data[i].lat));
                        }
                    }

                    driving.setSearchCompleteCallback(function(){
                        var pts = driving.getResults().getPlan(0).getRoute(0).getPath();//通过驾车实例，获得一系列点的数组
                        var polyline = new BMap.Polyline(pts, {strokeColor:"greenyellow", strokeWeight:6, strokeOpacity:0.5});
                        map.addOverlay(polyline);
                    });
                } else {
                	parent.layer.msg("未查找到行车轨迹", {icon: 2, time: 1000});
                }
            }
        }
    });
    map.removeEventListener("tilesloaded", showRoads);
}