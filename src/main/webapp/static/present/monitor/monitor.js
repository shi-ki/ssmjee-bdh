var map;

var point = new BMap.Point(119.491737, 39.840167);  // 创建点坐标
var zoom = 13;

var  mapStyle = {
    features: ["road", "building","water","land"],//隐藏地图上的poi
    style : "midnight"  //设置地图风格为高端黑
};

$(document).ready(function(){
	
	$("#map").width(window.innerWidth - 25).height(window.innerHeight - 25);
	
	map = new BMap.Map("map");          // 创建地图实例
    map.centerAndZoom(point, zoom);                 // 初始化地图，设置中心点坐标和地图级别
    map.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
    map.setMapStyle(mapStyle);
    map.addEventListener("tilesloaded", showRoads);
    
    $.ajax({
        url:"/present/getMonitors",
        type:"GET",
        data:null,
        dataType:"JSON",
        success:function(data){
            if(data.length > 0){
                for(var d in data){
                    var monitorMark = new BMap.Marker(new BMap.Point(data[d].lng, data[d].lat), {icon: new BMap.Icon("http://api.map.baidu.com/img/markers.png", new BMap.Size(23, 25))});
                    monitorMark.setLabel(new BMap.Label(data[d].name,{offset:new BMap.Size(20,-10)}));
                    monitorClick(data[d].number, monitorMark);
                    map.addOverlay(monitorMark);
                }
            }
        }
    });
    
});

function showRoads(){
    map.removeEventListener("tilesloaded", showRoads);
}

function monitorClick(number, monitorMark) {
	monitorMark.addEventListener("click", function(e){
		layer.open({
			type: 2,
			anim: 5,
			title: "监控视频",
			resize: true,
			closeBtn: 1,
			isOutAnim: true,
			shadeClose: false,
			shade: 0.6,
			area: ["800px", "600px"],
			content: "/app/dict/monitor?number=" + number //iframe的url
});
    });
}

