$(function () {

    //延迟初始化
    $(document).ready(function () {
        setTimeout(function () {
            init();
        }, 50);
        setTimeout(function () {
            $('#PlayViewOCX').css({
                'width': '100%',
                'height': '100%'
            });
        }, 4000);
    });

    //初始化
    function init() {
        var OCXobj = document.getElementById("PlayViewOCX");
        var txtInit = $("#config").val();
        OCXobj.ContainOCX_Init(txtInit);
    }

    //提交按钮
    $('.submit').on('click', function () {
        var PalyType = $('.PalyType').val();
        var SvrIp = $('.SvrIp').val();
        var SvrPort = $('.SvrPort').val();
        var appkey = $('.appkey').val();
        var appSecret = $('.appSecret').val();
        var time = $('.time').val();
        var timeSecret = $('.timeSecret').val();
        var httpsflag = $('.httpsflag').val();
        var CamList = $('.CamList').val();

        var param = 'ReqType:' + PalyType + ';' + 'SvrIp:' + SvrIp + ';' + 'SvrPort:' + SvrPort + ';' + 'Appkey:' + appkey + ';' + 'AppSecret:' + appSecret + ';' + 'time:' + time + ';' + 'timesecret:' + timeSecret + ';' + 'httpsflag:' + httpsflag + ';' + 'CamList:' + CamList + ';';
		console.log(param);
        play_ocx_do(param);

    });
	
    //提交按钮
    $('.exe-submit').on('click', function () {
        var PalyType = $('.PalyType').val();
        var SvrIp = $('.SvrIp').val();
        var SvrPort = $('.SvrPort').val();
        var appkey = $('.appkey').val();
        var appSecret = $('.appSecret').val();
        var time = $('.time').val();
        var timeSecret = $('.timeSecret').val();
        var httpsflag = $('.httpsflag').val();
        var CamList = $('.CamList').val();

        var param = 'hikvideoclient://' + 'ReqType:' + PalyType + ';' + 'VersionTag:' + 'artemis' + ';' + 'SvrIp:' + SvrIp + ';' + 'SvrPort:' + SvrPort + ';' + 'Appkey:' + appkey + ';' + 'AppSecret:' + appSecret + ';' + 'time:' + time + ';' + 'timesecret:' + timeSecret + ';' + 'httpsflag:' + httpsflag + ';' + 'CamList:' + CamList + ';';
		console.log(param);
        play_ocx_do(param);

    });


    ////OCX控件视频处理函数
    function play_ocx_do(param) {
        if ("null" == param || "" == param || null == param || "undefined" == typeof param) {
            return;
        } else {
            var OCXobj = document.getElementById("PlayViewOCX");
            OCXobj.ContainOCX_Do(param);
        }
    }
});