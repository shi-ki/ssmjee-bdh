var map;
var point = new BMap.Point(119.524111, 39.841867);  // 创建点坐标
var zoom = 15;
var datas = [];
var  mapStyle = {
    features: ["road", "building","water","land"],//隐藏地图上的poi
    style : "midnight"  //设置地图风格为高端黑
};
$(document).ready(function(){

    $.ajax({
        url:"/present/getMapPoint",
        type:"GET",
        data:null,
        dataType:"JSON",
        //async:true,
        success:function(data){
            if(data.length > 0){
                for(var d in data){
                    var point = {};
                    point.lng = data[d].lng;
                    point.lat = data[d].lat;
                    point.icon = '/images/map-img/marker.png';
                    point.pauseTime = 3;
                    if (data[d].isMarker==0){
                        point.isMarker = false;
                        point.html = '';
                    }else {
                        point.isMarker = true;
                        point.html = "<img class='mapIImmgg' src=\""+data[d].img+"\"></br>"+data[d].name;
                    }
                    datas.push(point);
                }
                console.log("开始加载地图点位信息");
                console.log(datas);
                initMap();
            }
        }
    });

});

function initMap(){
    //var bmap = document.getElementById('map');
    map = new BMap.Map("map");
    // new BMap.mapst
    map.centerAndZoom(point,zoom);//设置中心点和级别（级别是1-20）数字越大越是放大
    map.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
    map.setMapStyle(mapStyle);
    map.addEventListener("tilesloaded", showRoads);

    initPolyLine(datas);
    addMarkers(datas);
    initLuShu(datas);
}


function showRoads(){
    map.removeEventListener("tilesloaded", showRoads);
}



// function getDatas(){
//
//
//     var datas = [
//         {lng:108.988452,lat:34.392405,html:'啥也没有',pauseTime:3,isMarker:false,icon:'marker.png'},
//         {lng:108.945046,lat:34.381442,html:'<img src="/images/map-img/meng.jpg" /></br>西安北站到了',pauseTime:3,isMarker:true,icon:'marker.png'},
//         {lng:108.915438,lat:34.372742,html:'啥也没有',pauseTime:3,isMarker:false,icon:'marker.png'},
//         {lng:108.844579,lat:34.355817,html:'啥也没有',pauseTime:3,isMarker:false,icon:'marker.png'},
//         {lng:108.767253,lat:34.34628,html:'啥也没有',pauseTime:3,isMarker:false,icon:'marker.png'},
//         {lng:108.740663,lat:34.349856,html:'咸阳站到了',pauseTime:3,isMarker:true,icon:'marker.png'},
//         {lng:108.67268,lat:34.336861,html:'<img src="meng.jpg" /></br>咸阳秦都站到了',pauseTime:3,isMarker:true,icon:'marker.png'},
//         {lng:108.571351,lat:34.305735,html:'啥也没有',pauseTime:3,isMarker:false,icon:'marker.png'},
//         {lng:108.4946,lat:34.296431,html:'兴平站到了',pauseTime:3,isMarker:true,icon:'marker.png'},
//         {lng:108.418136,lat:34.279728,html:'啥也没有',pauseTime:3,isMarker:false,icon:'marker.png'}
//     ];
//     return datas;
// }


// 地图上添加显示的点位
function addMarkers(datas){
    var markers = getMarkers(datas);
    for(dataIndex in datas){
        map.addOverlay(datas[dataIndex]);//添加站点marker
    }
}

//初始化需要显示的点位
function getMarkers(datas){
    var markers = [];
    for(dataIndex in datas){
        if(datas[dataIndex].isMarker){
            var icon = new BMap.Icon(datas[dataIndex].icon, new BMap.Size(32,32),{anchor: new BMap.Size(16, 32)});//声明站点标注
            markers.push(new BMap.Marker(new BMap.Point(datas[dataIndex].lng, datas[dataIndex].lat),{icon:icon}));
        }
    }
    return markers;
}


function initPolyLine(datas){
    var points = getPoint(datas);
    var sy = new BMap.Symbol(BMap_Symbol_SHAPE_BACKWARD_OPEN_ARROW, {
        scale: 0.6,//图标缩放大小
        strokeColor:'#fff',//设置矢量图标的线填充颜色
        strokeWeight: '2',//设置线宽
    });
    var icons = new BMap.IconSequence(sy, '10', '30');
    var polyline = new BMap.Polyline(points, {
        enableEditing: false,//是否启用线编辑，默认为false
        enableClicking: true,//是否响应点击事件，默认为true
        icons:[icons],
        strokeColor:"#5298ff", //折线颜色
        strokeWeight:6, //折线的宽度，以像素为单位
        strokeOpacity:0.8 //折线的透明度，取值范围0 - 1
    });
    map.addOverlay(polyline);//添加轨迹到地图
}

//初始化线路点位
function getPoint(datas){
    var points = [];
    for(dataIndex in datas){
        points.push(new BMap.Point(datas[dataIndex].lng, datas[dataIndex].lat));
    }
    return points;
}

function initLuShu(datas){
    var lushu = new BMapLib.LuShu(map, getPoint(datas), {
        //landmarkPois:此参数是路书移动的时候碰到这个点会触发pauseTime停留中设置的时间，单位为秒，经纬度误差超过十米不会停止
        landmarkPois:getLandmarkPois(datas),
        defaultContent: '带您观光 ...',
        speed: 100,//速度，单位米每秒
        /* 1、需要把图片和代码放在同一个文件夹下面
        * 2、size()是设置图片大小，图片过大可以截取
        * 3、anchor是设置偏移，默认是图片最中间，设置偏移目的是让图片底部中间与坐标重合
        */
        icon: new BMap.Icon('/images/map-img/car.png', new BMap.Size(48, 48), {anchor: new BMap.Size(24, 34)}),//声明高铁标注
        autoView: false,
        enableRotation: false
    });
    lushu.start();
}

function getLandmarkPois(datas){
    var points = [];
    for(dataIndex in datas){
        if(datas[dataIndex].isMarker){
            points.push(datas[dataIndex]);
        }
    }
    return points;
}

